#templatenet.py - template dictionary structure for STN-GPe network
#
# ver: 0.0
# rev: 2017-01-17

net_params = {
    'Cell_STN': {
        'N' : 300,
        'gid_rank': 0,
        'shape': 'grid',
        'N_subpop': 4,
        'Assign': 'Random',
        'Int_Con_p': 0.,
        'Ext_Con_p': 0,
        'autosyn': False,
        'V_ini': ('normal', -65.0, 3.0), 
        },
    'Cell_GPeP':{
        'N': 600,
        'gid_rank': 1,
        'shape': 'grid',
        'N_subpop': 1,
        'Assign': 'Random',
        'Int_Con_p': 0.01,
        'Ext_Con_p': 0.,
        'autosyn': False,
        'V_ini': ('normal', -65.0, 3.0), 
        },
    'Cell_GPeA':{
        'N': 300,
        'gid_rank': 2,
        'shape': 'grid',
        'N_subpop': 1,
        'Assign': 'Random',
        'Int_Con_p': 0.01,
        'Ext_Con_p': 0.,
        'autosyn': False,
        'V_ini': ('normal', -65.0, 3.0), 
        },
    'Con_STN_GPeP': {'p': 0.02, 'reci': False, 'seed': None},
    'Con_STN_GPeA': {'p': 0.02, 'reci': False, 'seed': None},
    'Con_GPeP_STN': {'p': 0.02, 'reci': False, 'seed': None},
    'Con_GPeA_STN': {'p': 0.0, 'reci': False, 'seed': None},
    'Con_GPeP_GPeA': {'p': 0.01, 'reci': False, 'seed': None},
    'Con_GPeA_GPeP': {'p': 0.01, 'reci': False, 'seed': None},
    'Seed': [6723162, 78, 7397222, 4331071, 250973, 9425580, 1134070, 9540660, 6294439, 3708213], 
 #    'Seed': -1,
    'architecture': 'LocalInh',
    'con_mat': False,
    }
