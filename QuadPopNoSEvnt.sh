#!/bin/bash

#SBATCH -N 1
#SBATCH -n 48 
#SBATCH -t 0:30:00
#SBATCH --job-name=Full_Network_Swing
#SBATCH -p skx-normal 
#SBATCH --mail-type=ALL
#SBATCH --mail-user=prannath_moolchand@brown.edu

export I_MPI_EXTRA_FILESYSTEM_LIST=garbage
module load phdf5
ibrun python3 runsim.py param/QuadPopNoSEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopNoSReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
