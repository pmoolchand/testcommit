#! python3 

from mpi4py import MPI 
import h5py, sys, time, datetime

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Serif}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)

import matplotlib.figure as mpl_figure
#import matplotlib.gridspec as gs
import matplotlib.ticker as ticker
#import matplotlib.text as text
#import matplotlib.markers as markers
#import matplotlib.lines as lines
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import numpy as np 
import scipy.stats as sts
import itertools as it
from functools import reduce
from AnalysisClass import Analysis
#from AnalysisClass import MultiAnalysis, VarEffects

#import statsmodels.api as sm                                                                                                                                                                          
#import statsmodels.genmod.generalized_linear_model as GLM

sys_name = 'ANATAC'

fed_spc = np.dtype('U64, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
#fed_spc = np.dtype('S24, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
fed_alg = np.dtype([('shft', np.bool), ('t0', np.float, 2), ('tend', np.float, 2), ('Dur', np.float, 2), ('Int', np.int, 2), ('TPart', np.float, 3), ('TgtSub', np.int, 2), ('AlgSub', np.int, 4)])
fed_unq = np.dtype([('fd', fed_spc), ('algn', fed_alg), ('fils', np.ndarray)])

cond_arr = np.dtype([('cond', 'U32'), ('idx', np.ndarray)]) 
task_arr = np.dtype([('Cond_idx', np.uint32), ('Reg_idx', np.uint32), ('Result', 'U32'), ('avg', np.bool), ('Cell_ass', 'U16'), ('zscor', np.bool)])

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
#tikloc = ticker.MaxNLocator(nbins=4)


class GLM_Analysis():
    def __init__(self, fil, param_dict, intval=100, tmin=1250, tmax=2500):

        # Set parallel machinery
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.N_hosts = MPI.COMM_WORLD.Get_size()
        self.intval = intval
        self.fil = fil
        self.trec = 0.025
        self.tmin =500 
        self.tmax = 3500
     #   self.tmin =500 
     #   self.tmax = 3500
        self.tmin_ana = tmin 
        self.tmax_ana = tmax 
    #    self.tmin_ana = 1250 
    #    self.tmax_ana = 2500 
        self.align = 1500
        print(tmin, tmax, self.tmin_ana, self.tmax_ana, 'bnd')
        self.timana = np.arange(self.tmin_ana, self.tmax_ana, self.trec)
        print(self.timana.shape, 'lop')
        self.timanaidx = np.array((self.timana-self.tmin)/self.trec, dtype=int) 
        self.N_tims = self.timanaidx.shape[0]

        print('GLM', self.N_tims)

        for k,v in param_dict.items():
            setattr(self, k, v)

        self.load_data()
        self.filter_feeds()
        self.get_filtered_param()
        self.get_cond_sim()

        self.get_multichores()

      #  for chore, res in self.my_chores:
      #    #  chor_seg = self.uniq_feeds[self.uniq_sims[chore[1],1]]['fd']['f8']
      #      chor_seg = [self.uniq_combo[self.uniq_sims[chore[1],0]], self.uniq_feeds[self.uniq_sims[chore[1],1]]]
      #      self.run_chore(chore, res, chor_seg)
 #       for chore in self.cond_arr: 
 #           self.run_chore(chore, chor_seg)

    def run_chore(self, chore, res, chor_seg):
        res_dict =  {
                     'STN_pp': "self.run_pp(chore, 'STN_spk', chor_seg)",
                     'NetSynplot': "self.run_timser(chore, 'NetSyn', chor_seg)",
                     'LFOplot': "self.run_timser(chore, 'STN_LFO_Amp', chor_seg, sqr=True)",
                    }
        eval(res_dict[res])

    def run_pp(self, chore, cell, chor_seg):     
        cell_spk = self.get_spk(chore, cell, self.tmin_ana, self.tmax_ana)
        cell_pp, tbin = self.get_pp(cell_spk, tbin=self.tbin)
        # for pers in ['fd', 'sb']:
        for pers in ['fd']:
            cellfdsp_pp = self.get_sum_spk(cell_pp, chore, pers=pers)
        return cellfdsp_pp
#            self.plot_pp(cellfdsp_pp, tbin, tit='STNpp'+pers+chore[0], chor_seg=chor_seg, pers=pers, ylim=self.ylimSTNpp)

#    def run_timser(self, chore, avgprop, chor_seg, sqr=False):
    def run_timser(self, ax, chore, avgprop, chor_seg, sqr=False, otherstim=False, cumsum=False, confunits=None, skip_ax=False):
        bigavgshap = self.filobj[avgprop].shape
        Netprop = np.empty(shape=(chore[1].shape[0],)+bigavgshap[1:-1]+(self.N_tims,), dtype=np.float32)
        for idx, sim in np.ndenumerate(chore[1]):
            Netprop[idx] = np.array(self.filobj[avgprop][sim,:,:,:,self.timanaidx])
#        self.LFOAmp = np.array(self.filobj['STN_LFO_Amp'][:,:,:,:,self.timanaidx])[chore[1]] 
#        for pers in ['fd']:
     #       self.plot_timser(Netprop, chore, tit=avgprop+pers+chore[0], chor_seg=chor_seg, pers=pers, sqr=sqr)
      #  for pers in ['fd']:
            #self.plot_timseravg(Netprop, chore, tit=avgprop+pers+chore[0], chor_seg=chor_seg, pers=pers, sqr=sqr)
        if skip_ax:
            return None, Netprop
        else:
            ax, arr = self.get_timseravg_axs(ax, Netprop, chore, chor_seg, pers='fd', sqr=sqr, otherstim=otherstim, cumsum=cumsum, confunits=confunits)
 #       print(arr.shape)
            return ax, arr


    def get_spk(self, chore, cellname, tmin, tmax):
        all_spk = np.array(self.filobj[cellname][chore[1],:,:]) 
        trim_spk = np.empty(shape=all_spk.shape, dtype=np.ndarray) 
        timposts = np.array([tmin, tmax])
        for idx, spk in np.ndenumerate(all_spk):
            trim_spk[idx] = self.trim_spike_range(spk, timposts)[0]    
        return trim_spk 
        
    def get_ISI(self, spk_data):
        spk_ISI = np.empty(shape=spk_data.shape, dtype=np.ndarray)
        for idx, spk in np.ndenumerate(spk_data):
            spk_ISI[idx] = np.diff(spk)
        return spk_ISI

    def plot_timser(self, fdsp_timser, chore, tit, chor_seg, pers='fd', sqr=False):
        fig = mpl_figure.Figure(figsize=(40, 40))
        canvas = FigureCanvas(fig)

        tim = self.timana - self.align 
      #  chor_lab = ['{:02d}%'.format(int(i*100)) for i in chor_seg]
        chor_lab = ['{:02d}%'.format(int(i*100)) for i in chor_seg[1]['fd']['f8']]

        shp = fdsp_timser.shape
        N_trials = shp[1] 
        N_rows = shp[2] if pers=='fd' else shp[3]
        N_rows_plot = N_rows+1 if pers=='fd' else N_rows

        gl = fig.add_gridspec(N_rows_plot, N_trials+1)
#        ax_arr = self.get_axs_arr(fig, gl, form=False)
        ax_arr = np.empty(shape=gl.get_geometry(), dtype='O')
        ax_arr = self.axis_create_align(ax_arr, gl, fig, xlab='', ylab='', form=True, style=True)


        if pers=='fd':
            rel_mat = fdsp_timser[:,:,:,4,:] 
            rel_mat = rel_mat**2 if sqr else rel_mat
            for i, j in it.product(range(N_rows), range(N_trials)): 
                ax_arr[i+1,j+1].plot(tim, rel_mat[:,j,i].T, lw=0.4)
            for i in range(N_rows):
                ax_arr[i+1,0].plot(tim, np.mean(rel_mat[:,:,i], axis=1).T, lw=0.4)
            for j in range(N_trials):
                ax_arr[0,j+1].plot(tim, np.mean(rel_mat[:,j,:], axis=1).T, lw=0.4)
            ax_arr[0,0].plot(tim, np.mean(rel_mat[:,:,:], axis=(0,1)).T, lw=0.4)
        elif pers=='sb':
            tmp_mat = np.zeros(shape=fdsp_timser.shape, dtype=np.float32)
            for idx, Nc in np.ndenumerate(self.fdsb_N[chore[1]]):
                tmp_mat[idx] = Nc * fdsp_timser[idx]
            tmp_mat = np.sum(tmp_mat, axis=2)
            tmp_mat[:,:,-1,:] = np.sum(tmp_mat[:,:,:-1,:], axis=2)
            N_sub = np.sum(self.fdsb_N[chore[1]], axis=2) 

            for idx, Ntot in np.ndenumerate(N_sub):
                tmp_mat[idx] = tmp_mat[idx]/Ntot
            
            for i, j in it.product(range(N_rows-1), range(N_trials)): 
                ax_arr[i+1,j+1].plot(tim, tmp_mat[:,j,i].T, lw=0.4)
            for i in range(N_rows-1):
                ax_arr[i+1,0].plot(tim, np.mean(tmp_mat[:,:,i], axis=1).T, lw=0.4)
            for j in range(N_trials):
                ax_arr[0,j+1].plot(tim, np.mean(tmp_mat[:,j,:-1], axis=1).T, lw=0.4)
            ax_arr[0,0].plot(tim, np.mean(tmp_mat[:,:,4], axis=1).T, lw=0.4)

    

        for _, ax in np.ndenumerate(ax_arr):
#            ax.set_ylim(top=0.2)
            ax.legend(chor_lab, title='Seg', fontsize='x-small')

        fig.tight_layout()
        figtim = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        figsvtit = '{!s}_{!s}'.format(tit, figtim)
        svformat = self.saveformat 
        fig.savefig('{!s}_{!s}.{!s}'.format(self.prefix, figsvtit, svformat))

    def get_timseravg_axs(self, ax, fdsp_timser, chore, chor_seg, pers='fd', sqr=False, otherstim=False, cumsum=False, confunits=None):
        tim = self.timana - self.align 
      #  chor_lab = ['{:02d}%, {:.2e}'.format(int(i*100),j) for i,j in zip(chor_seg[1]['fd']['f8'], chor_seg[0]['Ctx_STN_N'])]
        chor_lab = ['{:02d}%'.format(int(i*100)) for i in chor_seg[1]['fd']['f8']]
        
        NMunq = np.unique(chor_seg[0]['Ctx_STN_N'], return_inverse=True)
        segunq = np.unique(chor_seg[1]['fd']['f8'], return_inverse=True)
        fdunq = np.unique(chor_seg[1]['fd']['f0'], return_inverse=True)
        segcol = self.colors 
        nmstyl = self.lnstyl

        # Pull data combined across all subpopulations, hence index 4, 0-3 indiviudal subpop 
        rel_mat = fdsp_timser[:,:,:,4,:] 
        rel_mat = rel_mat**2 if sqr else rel_mat

        if confunits is None:
        # index 2 to pull for conflict detector pops only
            rel_mat_cumm = np.sum(rel_mat[:,:,2,:], axis=-1)*0.025 if cumsum else None 

            ax.plot(tim, np.mean(rel_mat[:,:,2], axis=1).T)

            for lindx, ln in enumerate(ax.lines):
             #   ln.set_color(segcol[segunq[1][lindx%segunq[0].shape[0]]]) 
                ln.set_color(segcol[segunq[1][lindx]]) 

            if otherstim:

                br = segcol[5] 
                # Pull individual subopop 0 and 1

                # marker version
              #  ax.plot(tim, np.mean(rel_mat[:,:,0], axis=1).T, marker='x', markevery=4000, markersize=6, color='k')
              #  ax.plot(tim, np.mean(rel_mat[:,:,1], axis=1).T, marker='o', markevery=4000, markersize=6, color=br)
              #  ax.plot(tim, np.mean(rel_mat[:,:,0], axis=1).T+np.mean(rel_mat[:,:,1], axis=1).T, marker='D', markevery=4000, markersize=6, color='darkgoldenrod')

                ax.plot(tim, np.mean(rel_mat[:,:,0], axis=1).T, color='k')
                ax.plot(tim, np.mean(rel_mat[:,:,1], axis=1).T, color=br)
                ax.plot(tim, np.mean(rel_mat[:,:,0], axis=1).T+np.mean(rel_mat[:,:,1], axis=1).T, color='darkgoldenrod')

              #  for lindx, ln in enumerate(ax.lines):
            # #       ln.set_color(segcol[segunq[1][lindx%segunq[0].shape[0]]]) 
              #      ln.set_color(segcol[0])



            #    for lindx, ln in enumerate(ax.lines):
            #        ln.set_ls(nmstyl[NMunq[1][lindx%NMunq[0].shape[0]]]) 
            #        ln.set_ls(nmstyl[fdunq[1][lindx%fdunq[0].shape[0]]]) 
            #   #     ln.set_ls(nmstyl[fdunq[1][lindx]]) 

                ax.plot([],[], color=segcol[0], label='Both') 
        # with marker
              #  ax.plot([],[], color='k', marker = 'x', label='$1^{st}$') 
              #  ax.plot([],[], color=br, marker = 'o', label='$2^{nd}$') 
              #  ax.plot([],[], color='darkgoldenrod', marker = 'D', label='$1^{st}+2^{nd}$') 

                ax.plot([],[], color='k', label='$1^{st}$') 
                ax.plot([],[], color=br, label='$2^{nd}$') 
                ax.plot([],[], color='darkgoldenrod', label='$1^{st}+2^{nd}$') 

               # ax.legend(frameon=False, fontsize='x-large')
                ax.legend(frameon=False,) 

        else:

            rel_mat_cumm = np.sum(rel_mat[:,:,confunits,:], axis=-1)*0.025 if cumsum else None 
            ax.plot(tim, np.mean(rel_mat[:,:,confunits], axis=(1,2)).T)
            N_axlines = len(ax.lines)
            start_idx = (N_axlines-1)//3 * 3
            for lindx, ln in enumerate(ax.lines[start_idx:start_idx+3]):
             #   ln.set_color(segcol[segunq[1][lindx%segunq[0].shape[0]]]) 
                ln.set_color(segcol[segunq[1][lindx]]) 

      #      for sgidx,sg in enumerate(segunq[0]):
              #  ax.plot([],[], color=segcol[sgidx], label='Seg: {:03d}%'.format(int(sg*100)))
      #          ax.plot([],[], color=segcol[sgidx], label='{:02d}%'.format(int(sg*100)))
#            for nmidx, nm in enumerate(NMunq[0]):
#                ax.plot([],[], color='k', ls=nmstyl[nmidx], label='NMDA: {:.2e}'.format(nm)) 
#
#            for fdidx, fd in enumerate(fdunq[0]):
#                ax.plot([],[], color='k', ls=nmstyl[fdidx], label=fd) 
            
    #        if sqr:
    #            ax.set_ylabel('Average LFO power [$nA^2$]')
    #        else:
    #            ax.set_ylabel('Average Synaptic current [$nA$]')



#            ax.set_ylim(top=3.5e1)
#            ax.set_xlabel('time [ms]')
      #      ax.legend(chor_lab, title='Seg')
    #        ax.legend(title='Seg:color, CtxStim:ls')
      #      ax.legend(title='Seg:color')
     #       ax.legend(title='Seg', frameon=False)
    #        fig.suptitle('$\Delta$ = $\infty$ ms')
        return ax, rel_mat_cumm 


    def plot_timseravg(self, fdsp_timser, chore, tit, chor_seg, pers='fd', sqr=False):
        fig = mpl_figure.Figure(figsize=(5, 4))
        canvas = FigureCanvas(fig)
     #   fig.suptitle('Same Intensities')
        ax = fig.add_subplot(1,1,1)
        ax = self.style_axis(ax)

        ax=self.get_timseravg_axs(ax, fdsp_timser, chore, chor_seg, pers='fd', sqr=sqr)

        fig.suptitle('Contrast 1-1 vs 2-0')
        fig.tight_layout()
        figtim = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        figsvtit = '{!s}_{!s}Contra11'.format(tit, figtim)
        fig.savefig('{!s}_{!s}.{!s}'.format(self.prefix, figsvtit, 'pdf'), transparent=True)
        fig.savefig('{!s}_{!s}.{!s}'.format(self.prefix, figsvtit, 'pgf'), transparent=True)

        
    def get_STNpp_ax(self, ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0):
        tim = np.arange(self.tmin_ana, self.tmax_ana, tbin) - self.align
        if pers=='fd':
            ax.plot(tim, np.cumsum(np.mean(fdsp_pp[:,:,2], axis=1), axis=1).T)
#            ax.plot(tim, np.mean(fdsp_pp[:,:,2], axis=1).T)
#            ax.plot(tim, np.mean(fdsp_pp[:,:,2], axis=1).T)
        elif pers=='sb':
            ax.plot(tim, np.mean(fdsp_pp[:,:,sbpop], axis=1).T)

     #   chor_lab = ['{:02d}%'.format(int(i*100)) for i in chor_seg]

      #  ax.legend(chor_lab, title='Seg', fontsize='small')
    
        if ylim is not None:
            ax.set_ylim(top=ylim)

        return ax


    def plot_pp(self, fdsp_pp, tbin, tit, chor_seg, pers='fd', ylim=None):
        fig = mpl_figure.Figure(figsize=(40, 40))
        canvas = FigureCanvas(fig)

        shp = fdsp_pp.shape
        N_trials = shp[1] 
        tim = np.arange(self.tmin_ana, self.tmax_ana, tbin) - self.align
        N_rows = shp[2]
        N_rows_plot = N_rows+1 if pers=='fd' else N_rows

        gl = fig.add_gridspec(N_rows_plot, N_trials+1)
#        ax_arr = self.get_axs_arr(fig, gl, form=False)
        ax_arr = np.empty(shape=gl.get_geometry(), dtype='O')
        ax_arr = self.axis_create_align(ax_arr, gl, fig, xlab='', ylab='', form=False, style=True)

        if pers=='fd':
            for i, j in it.product(range(N_rows), range(N_trials)): 
                ax_arr[i+1,j+1].plot(tim, fdsp_pp[:,j,i].T, lw=0.4)
            for i in range(N_rows):
                ax_arr[i+1,0].plot(tim, np.mean(fdsp_pp[:,:,i], axis=1).T, lw=0.4)
            for j in range(N_trials):
                ax_arr[0,j+1].plot(tim, np.mean(fdsp_pp[:,j,:], axis=1).T, lw=0.4)
            ax_arr[0,0].plot(tim, np.mean(fdsp_pp[:,:,:], axis=(0,1)).T, lw=0.4)
        elif pers=='sb':
            for i, j in it.product(range(N_rows-1), range(N_trials)): 
                ax_arr[i+1,j+1].plot(tim, fdsp_pp[:,j,i].T, lw=0.4)
            for i in range(N_rows-1):
                ax_arr[i+1,0].plot(tim, np.mean(fdsp_pp[:,:,i], axis=1).T, lw=0.4)
            for j in range(N_trials):
                ax_arr[0,j+1].plot(tim, np.mean(fdsp_pp[:,j,:-1], axis=1).T, lw=0.4)
            ax_arr[0,0].plot(tim, np.mean(fdsp_pp[:,:,4], axis=1).T, lw=0.4)

    
        chor_lab = ['{:02d}%'.format(int(i*100)) for i in chor_seg]

        for _, ax in np.ndenumerate(ax_arr):
            ax.legend(chor_lab, title='Seg', fontsize='xx-small')
    
        if ylim is not None:
            for _, ax in np.ndenumerate(ax_arr):
                ax.set_ylim(top=ylim)

        fig.tight_layout()
        figtim = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        figsvtit = '{!s}_{!s}'.format(tit, figtim)
        svformat = self.saveformat 
        fig.savefig('{!s}_{!s}.{!s}'.format(self.prefix, figsvtit, svformat))

    def get_sum_spk(self, spk_pp, chore, pers='fd'):
        rel_fdsb = self.fdsb[chore[1]]
        fsp_shp = rel_fdsb.shape
        N_subpop =  fsp_shp[-1] 
        rdim = 2 if pers=='fd' else 3
        fdsppp_shp = fsp_shp[:2] + (fsp_shp[rdim], spk_pp.shape[-1])

        fdsp_pp = np.zeros(shape=fdsppp_shp, dtype=np.float32)

        if pers=='fd':
            for idx, gid_idx in np.ndenumerate(rel_fdsb[:,:,:,4]):
                fdsp_pp[idx] = np.mean(spk_pp[idx[:2]][gid_idx], axis=0)
        elif pers=='sb':
            for idx, _ in np.ndenumerate(rel_fdsb[:,:,0,0]):
                for j in range(N_subpop):
                    tmp = []
                    for i in range(fsp_shp[2]):
                        tmp.append(self.fdsb[idx+(i,j)])
                    gid_idx = reduce(np.union1d, tmp) 
                    fdsp_pp[idx+(j,)] = np.mean(spk_pp[idx[:2]][gid_idx], axis=0)
        return fdsp_pp
        

    def get_pp(self, spk_data, tbin=1):
        prec = tbin/self.trec
        prec = max(1,prec)
        print('shap', spk_data.shape, prec)
      #  gid_pp_tmp = np.zeros(shape=spk_data.shape+(int((self.tmax-self.tmin)/self.trec)+1,), dtype=int)
        gid_pp_tmp = np.zeros(shape=spk_data.shape+(self.N_tims,), dtype=int)
        print(gid_pp_tmp.shape, 'grand')
        
        for idx, spk in np.ndenumerate(spk_data):
          #  spk_arr = np.array((self.trim_spike_range(spk, np.array([self.tmin_ana, self.tmax_ana]))[0]-self.tmin)/self.trec, dtype=int) 
            #spk_arr = np.array((self.trim_spike_range(spk, np.array([self.tmin_ana, self.tmax_ana]))[0]-self.tmin)/self.trec, dtype=int) 
            spk_tmp = np.array((spk-self.tmin_ana)/self.trec, dtype=int)
            gid_pp_tmp[idx][spk_tmp] = int(1)

        if prec > 1:
            tposts = np.rint(np.arange(0, gid_pp_tmp.shape[-1], prec)).astype(int) 
            gid_pp_arr = np.zeros(shape=(spk_data.shape+ (tposts.size,)), dtype=int)
            for i,_ in enumerate(tposts[:-1]):
                gid_pp_arr[:,:,:,i] = np.sum(gid_pp_tmp[:,:,:,tposts[i]:tposts[i+1]], axis=-1)
            print(gid_pp_arr.shape)
            gid_pp_arr[:,:,:,-1] = np.sum(gid_pp_tmp[:,:,:, tposts[-1]:], axis=-1)
        else: 
            gid_pp_arr = gid_pp_tmp
        return gid_pp_arr, tbin


    def get_axs_arr(self, fig, gl, form=True):
        axs_arr = np.empty(shape=gl.get_geometry(), dtype='O')
        for i, _ in np.ndenumerate(axs_arr):
            axs_arr[i] = fig.add_subplot(gl[i])
            self.style_axis(axs_arr[i], form=form)
        return axs_arr

    def trim_spike_range(self, tvec, timeposts):
        splidx = np.searchsorted(tvec, timeposts)
        spk_post_arr = np.empty(shape=(1,), dtype='O')
        spk_post_arr[:] = np.split(tvec, splidx)[1:-1] 
        return spk_post_arr

    def get_resdata(self, res_data, sims, spop, avg=False):
        dat_dict = {
                    'Sil': ('Silence', True),
                    'Bur': ('Burst', True),
                    'Fir': ('Firing', True),
                    'AUC_LFO': ('PowerLFO', False),
                    }
        att = dat_dict[res_data]
        data = getattr(self, att[0])[sims] 
        compute = att[1]
        N_tri = 1 if avg else self.N_trials

        sbpop_dat = self.fdsb[sims]
        walkidx = [idx for idx, _ in np.ndenumerate(sbpop_dat[:,:,0,0])]


        if compute:
            if spop == 'Fdcom':
                N_cell_idx = self.fdcom
                cell_idx_mat = np.empty(sbpop_dat.shape[:2]+(N_cell_idx,), dtype=np.ndarray)
                for idx in walkidx: 
                    sbidx = sbpop_dat[idx]    
                    for m in range(N_cell_idx):
                        cell_idx_mat[idx][m] = reduce(np.union1d, sbidx[m,:])

            elif spop == 'SubPop':
                N_cell_idx = self.N_subpop
                cell_idx_mat = np.empty(sbpop_dat.shape[:2]+(N_cell_idx,), dtype=np.ndarray)
                for idx in walkidx: 
                    sbidx = sbpop_dat[idx]    
                    for m in range(N_cell_idx):
                        cell_idx_mat[idx][m] = reduce(np.union1d, sbidx[:,m])

            result_mat = np.empty(shape=data.shape[:3]+(N_cell_idx,), dtype=np.float32)
            for cmidx, cim in np.ndenumerate(cell_idx_mat):
                result_mat[cmidx[:2]][:, cmidx[2]] = np.mean(data[cmidx[:-1]][:, cim], axis=-1)
        elif res_data == 'AUC_LFO':
            sbpop_dat_N = self.fdsb_N[sims]
            for idx in walkidx:
                data[idx] = np.sqrt(data[idx]) * sbpop_dat_N[idx]
            if spop == 'Fdcom':
                N_cell_idx = self.fdcom
                result_mat = np.sum(data, axis=-1)
            elif spop == 'SubPop':
                N_cell_idx = self.N_subpop
                result_mat = np.sum(data, axis=-2)
            result_mat = (result_mat/self.N_STN)**2
        result = np.mean(result_mat, axis=1) if avg else result_mat
        result = result.reshape(sims.shape[0]*N_tri,2,N_cell_idx)  
        return result 

    def get_multichores(self):
     #   avg_dat_dict =  {
     #                   'Ind': self.fdcom * self.N_subpop,
     #                   'SubPop': self.N_subpop,
     #                   'Fdcom': self.fdcom,
     #                   'AllPop': 1,
     #                   }
        N_cond = self.N_cond      
        N_res = len(self.GLM_Ana)

        chores = np.array([(i,j) for i,j in it.product(self.cond_arr, self.GLM_Ana)])
        
  #      rgrs_lab = np.empty(shape=N_rgrs, dtype=np.ndarray)

  #      rgrs_mat = np.empty(shape=(self.N_cond, N_rgrs), dtype=np.ndarray)
  #      for jidx, j in enumerate(self.cond_arr):
  #          for rgidx, rg in enumerate(self.GLM_Ana):
  #              rgrs_mat[jidx, rgidx], rgrs_lab[rgidx]  = self.get_regressors(rg[0], j, rg[2])

  #      N_res = np.empty((N_rgrs,2), dtype=np.int)
  #      for idx, i in enumerate(self.GLM_Ana):
  #          N_res[idx,0] = len(i[1])
  #          #num = 0
  #          #for k in i[3]:
  #          #    num += avg_dat_dict[k] 
  #          #N_res[idx,1] = num
  #          N_res[idx,1] = len(i[3]) 
        N_chores = chores.shape[0]

      #  chores = np.empty(shape=N_chores, dtype=task_arr)
      #  c_idx = 0
      #  for cnrgidx, cdrg in np.ndenumerate(rgrs_mat):
      #      for ij in it.product(range(N_res[cnrgidx[1], 0]), range(N_res[cnrgidx[1], 1])):
      #       #  chores[c_idx] = cnrgidx[0], cnrgidx[1], ij[0], self.GLM_Ana[cnrgidx[1]][2], self.GLM_Ana[cnrgidx[1]][3][ij[1]], self.GLM_Ana[cnrgidx[1]][2] 
      #         chores[c_idx] = cnrgidx[0], cnrgidx[1], self.GLM_Ana[cnrgidx[1]][1][ij[0]], self.GLM_Ana[cnrgidx[1]][2], self.GLM_Ana[cnrgidx[1]][3][ij[1]], self.GLM_Ana[cnrgidx[1]][4] 
      #         c_idx += 1 

        self.my_chores_idx = np.arange(self.rank, N_chores, self.N_hosts) 
        self.N_mychores =  self.my_chores_idx.shape[0]
        self.my_chores = chores[self.my_chores_idx]


    def load_data(self):
        self.filobj = h5py.File(self.fil, 'r+', driver='mpio', comm=MPI.COMM_WORLD)

        self.datshape = self.filobj['NetSyn'].shape
        self.N_trials = self.datshape[1]
        self.fdcom = self.datshape[2]
        self.N_subpop = self.datshape[3]
        self.N_timpts = self.datshape[-1]

        self.uniq_combo = np.array(self.filobj['uniq_combo']) 
        self.N_params = self.uniq_combo.shape[0]
        tmp = np.array(self.filobj['uniq_feeds'])
        self.feed_arr = np.array(tmp, dtype=fed_unq)
#        self.feed_arr['fd']['f0'] = np.array(self.feed_arr['fd']['f0'], dtype='U')
        self.N_feeds = self.feed_arr.shape[0]
        self.uniq_sims = np.array(self.filobj['uniq_sims'])
        self.uniq_feeds = np.array(self.filobj['uniq_feeds'])

        self.fdsb = np.array(self.filobj['STN_fdsp'])
        self.fdsb_N = np.array(self.filobj['STN_fdsp_N'])
#        self.Silence = np.array(self.filobj['STN_Sil'])
#        self.Burst = np.array(self.filobj['STN_Bur'])
#        self.Firing = np.array(self.filobj['STN_Fir'])
#        self.PowerLFO = np.array(self.filobj['STN_AUC_LFO'])
#        self.N_STN = self.Silence.shape[-1]

#        self.filobj.close()

    def get_cond_sim(self):
        self.N_cond = len(self.filt_dict['cond'])
        self.N_condarr = max(1, self.N_cond)
        self.cond_arr = np.empty(shape=self.N_condarr, dtype=cond_arr)
        all_sims = np.arange(self.uniq_sims.shape[0])
        if self.N_cond:
            cond_idx = 0
          #  for k,v in sorted(self.filt_dict['cond']).items():
            for k in sorted(self.filt_dict['cond']):
                v = self.filt_dict['cond'][k]
                prm, fd = v
                if prm == 'All':
                    uniq_prm = all_sims 
                else:
                    prm_idx = np.where(self.param_idx['cond']==prm)[0] 
                    prm_arr = self.param_idx['idx'][prm_idx][0]
                    uniq_prm = []
                    for i in prm_arr:
                        uniq_prm.append(np.where(self.uniq_sims[:,0]==i)[0])
                    uniq_prm = np.concatenate(uniq_prm)
                if fd == 'All':
                    uniq_fd = all_sims 
                else:
                    fd_idx = np.where(self.feed_idx['cond']==fd)[0] 
                    fd_arr = self.feed_idx['idx'][fd_idx][0]
                    uniq_fd = []
                    for i in fd_arr:
                        uniq_fd.append(np.where(self.uniq_sims[:,1]==i)[0])
          #          print(fd_arr)
                    uniq_fd = np.concatenate(uniq_fd)
                self.cond_arr[cond_idx] = k , np.intersect1d(uniq_prm, uniq_fd, assume_unique=True)
                cond_idx += 1
        else:
            self.cond_arr[0] = 'All', all_sims 

    def filter_feeds(self):
        track_idx = {}
        def_idx = np.arange(self.N_feeds)
        for k in self.filt_dict['feed'].keys():
            track_idx[k] = def_idx 
            for cons, v in self.filt_dict['feed'][k]:
                arr = self.get_field(cons)
                keep_idx = []
                if type(v[0]) is not tuple:
                    for val in v:
                        tmp_idx = np.where(arr[track_idx[k]]==val)[0]
                        keep_idx.append(track_idx[k][tmp_idx])
                        track_idx[k] = track_idx[k][np.delete(np.arange(track_idx[k].shape[0]), tmp_idx)] 
                    track_idx[k] = reduce(np.union1d, keep_idx)
                else:
                    for val in v:
                        tmp_idx = np.where((arr[track_idx[k]]>=val[0]) & (arr[track_idx[k]]<=val[1]))[0]
                        track_idx[k] = track_idx[k][np.intersect1d(np.arange(track_idx[k].shape[0]), tmp_idx, assume_unique=True)] 
    
        track_idx['All'] = def_idx
        self.feed_idx = np.empty(shape=len(track_idx), dtype=cond_arr) 
        idx = 0
        for k,v in track_idx.items():
            self.feed_idx[idx] = k, v 
            idx += 1
#        print(self.feed_idx)

    def get_filtered_param(self):
        track_idx = {}
        def_idx = np.arange(self.N_params)
        for k in self.filt_dict['param'].keys():
          #  track_idx[k] = def_idx 
            track = {}
            for j,v in self.filt_dict['param'][k].items():
                track[j] = def_idx
                arr = self.uniq_combo[j]
                keep_idx = []
                for val in v:
                    tmp_idx = np.where(arr[track[j]]==val)[0]
                    keep_idx.append(track[j][tmp_idx])
                    track[j] = track[j][np.delete(np.arange(track[j].shape[0]), tmp_idx)] 
                track[j] = reduce(np.union1d, keep_idx) 
            track_idx[k] = reduce(np.intersect1d, [*track.values()]) if len(self.filt_dict['param']) else def_idx  
        track_idx['All'] = def_idx
    
        self.param_idx = np.empty(shape=len(track_idx), dtype=cond_arr) 
        idx = 0
        for k,v in track_idx.items():
            self.param_idx[idx] = k, v 
            idx += 1
      #  print(self.param_idx)
   #     filt_idx = reduce(np.intersect1d, [*track_idx.values()]) if len(self.filt_dict) else def_idx 
   #     self.filt_param_arr = self.uniq_param_combo[filt_idx]

    def get_field(self, fld):
        fld_dict = {
                    'fds': self.feed_arr['fd']['f0'],
                    'Seg': self.feed_arr['fd']['f8'],
                    'Int1': self.feed_arr['algn']['Int'][:,0],
                    'Int2': self.feed_arr['algn']['Int'][:,1],
                    'Dur1': self.feed_arr['algn']['Dur'][:,0],
                    'Dur2': self.feed_arr['algn']['Dur'][:,1],
                    'Ovlp': self.feed_arr['algn']['TPart'][:,0],
                    'Dlay': self.feed_arr['algn']['TPart'][:,1],
                    'PstO': self.feed_arr['algn']['TPart'][:,2],
                    }
        return fld_dict[fld]


    def axis_create_align(self, axr, gl, fig, xlab='', ylab='', form=True, style=True):
        gl_gem = gl.get_geometry()
        ax_shp = axr.shape
        if gl_gem == ax_shp:
            axr[0,0] = fig.add_subplot(gl[0,0])
            for i in range(1,gl_gem[1]):
                axr[0,i] = fig.add_subplot(gl[0,i], sharey=axr[0,0])
            for j in range(1,gl_gem[0]):
                axr[j,0] = fig.add_subplot(gl[j,0], sharey=axr[0,0])
                axr[j,0].set_ylabel(ylab)
            for i,j in it.product(range(1,gl_gem[0]), range(1, gl_gem[1])):
                axr[i,j] = fig.add_subplot(gl[i,j], sharex=axr[0,j], sharey=axr[i,0])
            for j in range(gl_gem[1]):
                axr[-1,j].set_xlabel(xlab)
            axr[0,0].set_ylabel(ylab)
            if style:
                for i, ax in np.ndenumerate(axr):
                    self.style_axis(ax,form=form)

        else:
            print('Axis array and Gridspec geometry mismatch')
        return axr
    
    def axis_create_inset(self, axr, xlab='', ylab=''):
        ax_shp = axr.shape
        for i, j in it.product(range(ax_shp[0]), range(ax_shp[1])):
            axr[i,j,1] = inset_axes(axr[i, j, 0], width="70%", height="60%", loc=1)
            axr[i,j,1].patch.set_alpha(0)
        axr[0,0,1].set_xlabel(xlab)
        axr[0,0,1].set_ylabel(ylab)
        return axr

    def style_axis(self, ax, top=False, right=False, bottom=True, left=True, offset=5, form=True):
        spn = ['top', 'bottom', 'right', 'left']
        for sp in spn:
            ax.spines[sp].set_position(('outward', offset))
            ax.spines[sp].set_visible(eval(sp))
      #  ax.spines['left'].set_position(('outward', offset))
        if form:
            ax.yaxis.set_major_formatter(formatter)
       # ax.spines['top'].set_visible(top)
       # ax.spines['right'].set_visible(right)
    
        return ax


    def old_style_axis(self, ax, top=False, right=False, offset=5):
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-1,1))
        ax.spines['top'].set_visible(top)
        ax.spines['right'].set_visible(right)

        ax.spines['bottom'].set_position(('data', 0))
        ax.spines['left'].set_position(('outward', offset))

        ax.xaxis.set_ticklabels([])
      #  ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)

    def rem_fill_nsg(self, axis, pat_no):
        pats = axis.patches 
        br = pats[pat_no]
        br.set_fill(False)
       # axis.text(br.get_x() + br.get_width() / 2, br.get_y() + br.get_height(), '$*$',
       #     ha='center', va='bottom')

def exec_GLM_ana(fils):
    param_dict = {}
    popof = {
                'param': {
                        # 'Low':  {'Ctx_STN_A': [1.833e-06], 'GPe_STN': [1e-5]},
                        # 'Mid':  {'Ctx_STN_A': [3.666e-05], 'GPe_STN': [1e-5]},
                            'NMDAGLV':  {'Ctx_STN_A': [3.666e-06], 'GPe_STN': [8e-6],}, 
     #                       'NMDAGLV':  {'Ctx_STN_A': [3.666e-06], }, 
#                            'NMDAGLV':  {'Ctx_STN_A': [1.833e-05], }, 
#                            'NMDAGMV':  {'Ctx_STN_A': [1.833e-05], 'GPe_STN': [8e-6],}, 
                       #    'NMDAGMV':  {'Ctx_STN_A': [3.666e-05], 'GPe_STN': [1e-5],}, 
                       #    'NMDAGLV':  {'Ctx_STN_A': [7.33e-06], 'GPe_STN': [1e-5],}, 
                  #       'Low':  {'Ctx_STN_A': [7.33e-06, 3.666e-05], 'GPe_STN': [1e-5]},
   #                      'High':  {'GPe_STN': [1.5e-5],}, 
            #            'BaseNM': {'GPe_STN': [1e-5],}, 
                    },

                'feed': {
#                         'Low': [('fds', ['RhythF22NE50_150']),]
#                         'Low': [('fds', ['RhythF22NE50_Sing25']),]
#                         'Low': [('fds', ['F22NE50_20']),],
#                        'Low': [('fds', ['F22NH2']),  ],
#                         'High': [('fds', ['F22NH4']), ('Int1', [(125,250)]) ],
#                         'NoSeg': [ ('Seg', [(0.1,0.5)]) ],
                        'HiHi': [('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],
                      #  'HiHi': [('Seg', [(0.1,0.95)]), ('Dlay', [(66,68)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],
                     #   'HiLo': [('Seg', [(0.1,0.95)]), ('Dlay', [(66,68)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(25,45)]), ('Int2', [(6,12)]) ],
                     #   'LoLo': [('Seg', [(0.1,0.9)]), ('Dlay', [(102,104)]), ('Dur1', [(25,45)]), ('Int1', [(6,12)]), ('Dur2', [(25,45)]), ('Int2', [(6,12)]) ],
                      #  'LoHi': [('Seg', [(0.1,0.95)]), ('Dlay', [(66,68)]), ('Dur1', [(25,45)]), ('Int1', [(6,12)]), ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],
                     #   'tes': [('Seg', [(0.1,0.95)]), ('Dlay', [(1,15)]), ('Dur1', [(25,45)]), ('Int1', [(6,12)]), ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],



                    },
                'cond': {
                      #  'NoSegHiHiGNLDel005' : ('NMDAGLV','Low'),
                        'RhythmicEvent' : ('NMDAGLV','HiHi'),
                  #      'NoSegHiHiGNLDel005' : ('NMDAGLV','All'),

            #            'NoSegHiHiGNL' : ('BaseNM','Low'),
                      #  'NoSegHiHiGNM' : ('NMDAGMV','All'),
           #             'Test' : ('NMDAGLV','All'),
            #            'Kest' : ('NMDAGMV','All'),
                     #   'NoSegHiHiGNLDel067' : ('NMDAGLV','HiHi'),
                     #   'NoSegHiHiGNMDel067' : ('NMDAGMV','HiHi'),
                     #   'NoSegHiLoGNLDel067' : ('NMDAGLV','HiLo'),
                     #   'NoSegHiLoGNMDel067' : ('NMDAGMV','HiLo'),
                     #   'NoSegLoLoGNLDel067' : ('NMDAGLV','LoLo'),
                     #   'NoSegLoLoGNMDel067' : ('NMDAGMV','LoLo'),
                     #   'NoSegLoHiGNLDel067' : ('NMDAGLV','LoHi'),
                     #   'NoSegLoHiGNMDel067' : ('NMDAGMV','LoHi'),
                    #     'Low': [('fds', ['F22NH2']), ('Int1', [(300,600)]) ],
#                        'Low': [('fds', ['F22NH2']),  ],
#                         'High': [('fds', ['F22NH4']), ('Int1', [(125,250)]) ],
#                        'lopo': ('NMDAGLV', 'Low') 
                        },
            }
    GLMAna = [
               # 'STN_pp', 
               # 'NetSynplot', 
                'LFOplot'
         #   ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ('F', 'Dur0'), ('F', 'Dur1'), ('F', 'Int0'), ('F', 'Int1'), ('I', 'Ctx_STN_N$Dur0$Int0'), ('I', 'Ctx_STN_N$Dur1$Int1')],
                  #  ['Sil', 'Bur', 'Fir', 'AUC_LFO'], True, ['SubPop', 'Fdcom'], False), 
          #          ['Sil'], True, ['SubPop'], False), 
         #   ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ],
         #   ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('I', 'Ctx_STN_N$Del') ],
         #           ['Sil',  'AUC_LFO'], True, ['SubPop', 'Fdcom' ], False), 
            ] 
         
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (5, 5)),# '--'
                            (0, (1, 5)), #':'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]

    param_dict['GLM_Ana'] = GLMAna
    param_dict['saveformat'] = 'pdf'
    param_dict['tbin'] = 5
    param_dict['ylimSTNpp'] = None 
    param_dict['prefix'] = 'jyango'

    param_dict['filt_dict'] = popof
    GLM_cls = GLM_Analysis(fils, param_dict, intval=100)
#    GLM_cls = GLM_Analysis_Event(fils)

if __name__ == '__main__':
    fils = sys.argv[1:]

    if fils[0].endswith('.hdf5'):
        exec_GLM_ana(fils[0]) 
    else:
        print('Invalid HDF5 file... skipping')
