#! python3

import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.gridspec as gs
import matplotlib.style as mpl_style
import matplotlib.image as mpimg
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import plot_utils as plut

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])

def get_spike_profile():
    filtri1='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPANMDA-000.hdf5' 
    exp_path1='/P000/F1H4/F000/SimpleNet4/N000'

    filtri2='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPANMDA-000.hdf5'
    exp_path2='/P001/F1H4/F000/SimpleNet4/N000'

    timrng=[1250, 2250]

    ancls1 = Plots(filtri1, exp_path1, tmin=timrng[0], tmax=timrng[1]) 
    ancls2 = Plots(filtri2, exp_path2, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
                'fig_siz': (6.5, 10),
              #  'fig_siz': (10,8 ),
                'glshap': (4,2),
            #    'wratio': (1,0.3,1)
                }


    fig, gl = ancls1.create_fig(figprops, constrained_layout=True)

#    gl.update(wspace=0, hspace=0.0)
    gl.tight_layout(fig, pad=0)
    
    
    def plot_tri():
        
        def set_axes():
            axr_arr = np.empty(shape=gl.get_geometry(), dtype='O')

            for idx, _ in np.ndenumerate(axr_arr):
                axr_arr[idx] = fig.add_subplot(gl[idx])
                ancls1.style_axis(axr_arr[idx], form=False)

            axr_arr[0,0].get_shared_x_axes().join(*axr_arr[:,0])
            axr_arr[0,0].get_shared_x_axes().join(*axr_arr[:,1])

            for ax in axr_arr[2,:]:
                ancls1.style_axis(ax, form=False)

            for ax in axr_arr[:,1]:
                ax.axes.get_yaxis().set_visible(False)
                ax.spines['left'].set_visible(False)

            return axr_arr

        def plot_data(axr_arr):
            axr_arr[:4,0] = plot_triphasic(ancls1, exp_path1, axr_arr[:,0], timrng)
            axr_arr[:4,1] = plot_triphasic(ancls2, exp_path2, axr_arr[:,1], timrng, rlab=True)


            return axr_arr


        axr_arr = set_axes()
        axr_arr = plot_data(axr_arr)
        

        return axr_arr
    

#    gl_brake = gs.GridSpecFromSubplotSpec(6, 1, subplot_spec=gl[:, 0], wspace=0.0, hspace=0.5, height_ratios=(1,1,1,1,1,1))
#    gl_triph = gs.GridSpecFromSubplotSpec(6, 1, subplot_spec=gl[:, 2], wspace=0.0, hspace=0.5, height_ratios=(1,1,1,1,1,1))

    if False:
        axr_arr = np.empty(shape=(6,2), dtype='O')
        for i in range(6):
            axr_arr[i,0] = fig.add_subplot(gl_brake[i,0])
            axr_arr[i,1] = fig.add_subplot(gl_triph[i,0])
    
        for i in range(4):
            ancls1.style_axis(axr_arr[i,0], form=False)
            ancls2.style_axis(axr_arr[i,1], form=False)
        for i in range(4,6):
            axr_arr[i,0].axis('off')
            axr_arr[i,1].axis('off')
    
        ancls1.style_axis(axr_arr[2,0], form=True)
        ancls2.style_axis(axr_arr[2,1], form=True)
    
    
        axr_arr[:4,0] = plot_triphasic(ancls1, exp_path1, axr_arr[:4,0], timrng)
        axr_arr[:4,1] = plot_triphasic(ancls2, exp_path2, axr_arr[:4,1], timrng, rlab=True)


#    axr_arr[0,0].set_ylabel('STN unit', color='r')
#    axr_arr[0,0].set_title('STN raster and PSTH') 
#
#    axr_arr[1,0].set_ylabel('$V_m$ [mV]')
#    axr_arr[1,0].set_title('Typical $V_m$ responses')
#
#    axr_arr[2,0].legend(['NetSyn','GABA','NMDA','AMPA'])
#    axr_arr[2,0].set_title('PSCs')
#    axr_arr[2,0].set_ylabel('$I_{syn}$ [nA]')
#
#    axr_arr[3,0].set_ylabel('Frequency [Hz]')
#    axr_arr[3,0].set_xlabel('time [ms]')
#    axr_arr[3,0].set_title('Spectrogram')
#
#    cellMC  = mpimg.imread('CellNMC.png')
#    cellCG  = mpimg.imread('CellNCG.png')
#    nettri  = mpimg.imread('nettri.png')
#    netbra  = mpimg.imread('netbrake.png')
#    axr_arr[4,0].imshow(cellCG)
#    axr_arr[5,0].imshow(netbra)
#    axr_arr[4,1].imshow(cellMC)
#    axr_arr[5,1].imshow(nettri)


    axr_arr = plot_tri()

    filnam='PopoNewTriphasic'
#    fig.align_ylabels()
#    fig.align_xlabels()


    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, bbox_inches = 'tight', pad_inches = 0)

def plot_triphasic(ancls, exp_path, axs_arr, timrng, rlab=False):

    ctx_lw = 2
    axr_arr = axs_arr.reshape(2,2, order='F')
    Ctx_feeds, N_Ctx_feeds = ancls.anacl.get_ctx_feed(exp_path)

# triraster

    STNpop_spks = ancls.get_spk_arr(exp_path, ancls.STNgids) 
  #  STNpop_spks = spk_arr[self.STNgids]
    
    STN_pp, tbin = ancls.anacl.get_gid_pp(exp_path, ancls.STNgids, tbin=5) 
    STN_sum = np.sum(STN_pp, axis=0)
    
    timalgn=1500
    tposts = np.arange(ancls.anacl.tmin-timalgn, ancls.anacl.tmax+tbin-timalgn, tbin)
    


    for gidx, tvec in enumerate(STNpop_spks):
        axr_arr[0,0].eventplot(np.concatenate(tvec)-timalgn, lineoffsets=gidx, linelengths=0.95, linewidths=2, alpha=0.5, color='r')
    
    axr_arr[0,0].plot([0,80],[80,80], lw=ctx_lw, color='k')
    spk_cnt_ax = axr_arr[0,0].twinx()
    spk_cnt_ax = ancls.style_axis(spk_cnt_ax, top=False, right=True, bottom=False, left=False, form=False)
    spk_cnt_ax.bar(tposts, STN_sum, width=tbin, align='center', alpha=0.75, color='k')
 #    spk_cnt_ax.yaxis.tick_left()
    if rlab:
        spk_cnt_ax.yaxis.set_label_text('Spike Count', color='k')



    
# dep block 
    idx_lst = ancls.STNgids[np.array([0, 10, 25, 50])] 
    
    STN_idx = ancls.gids[idx_lst] 
    STN_arr = np.array(ancls.filobj[exp_path]['raw/STNSta_V'][STN_idx,:])
    
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500
    STN_arr=STN_arr[:,tidx[0]:tidx[1]]
    
    axr_arr[1,0].plot(tim, STN_arr.T)
    axr_arr[1,0].plot([0,80],[33,33], lw=ctx_lw, color='k')
        
# syn currents
    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    axr_arr[0,1].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 
 #   axr_arr[0,1].plot([0,80],[0.2e-2,0.2e-2], lw=ctx_lw, color='k')

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[1,1].imshow(STNspec, cmap='plasma', origin='lower', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[1,1].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

#    for _,ax in np.ndenumerate(axr_arr):
#        ancls.anacl.upd_axis_ticks(ax.axes.xaxis, Ctx_feeds, N_Ctx_feeds)

    return axr_arr.reshape(4, order='F')

def spiking_rates():
    filRH='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_Spiking_SingPop_curr_spikes-000.hdf5'
    filLE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighLow-000.hdf5'
    filHE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'

    timrng=[1500, 2500]
    anRH=Plots(filRH, None, tmin=timrng[0], tmax=timrng[1]) 
    anLE=Plots(filLE, None, tmin=timrng[0], tmax=timrng[1]) 
    anHE=Plots(filHE, None, tmin=timrng[0], tmax=timrng[1]) 

    N_NMDA_pos = [1,4,7] 
    N_NMDA = 3 
    NM_lab = ['Low', 'Med', 'High']

    RH = [(2,'2'), (4,'4'), (8,'8'), (12.5,'13'), (20,'20'), (32,'32')]
    RH_plot=np.array([2,4,8,12.5,20,32])

    EV = np.array([10, 31, 50, 80, 125, 250, 500])

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
           #     'fig_siz': (8, 4),
                'fig_siz': (10,6 ),
                'glshap': (1,3),
                }

    fig, gl = anRH.create_fig(figprops)

    axr_arr = np.empty(shape=(1,3), dtype='O')
    for i, _ in np.ndenumerate(axr_arr):
        axr_arr[i] = fig.add_subplot(gl[i])
        anRH.style_axis(axr_arr[i], form=False)

    for _, ax in np.ndenumerate(axr_arr): 
        axr_arr[0,0].get_shared_y_axes().join(axr_arr[0,0], ax) 

    RH_rat = np.zeros(shape=(len(RH), N_NMDA)) 
    EL_rat = np.zeros(shape=(len(EV), N_NMDA)) 
    EH_rat = np.zeros(shape=(len(EV), N_NMDA)) 

    for idx, i in enumerate(N_NMDA_pos):
        for jidx, j in enumerate(RH):
            expt = 'P{:03d}/F1H{!s}/F000/SimpleNet4/N000'.format(i, j[1])
            STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anRH.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            RH_rat[jidx,idx] = get_spk_rate(pp, 1000/j[0], 'R')

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anLE.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            EL_rat[jidx,idx] = get_spk_rate(pp, 150, 'E')

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anHE.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            EH_rat[jidx,idx] = get_spk_rate(pp, 150, 'E')

        axr_arr[0,0].plot(RH_plot, RH_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,1].plot(EV, EL_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,2].plot(EV, EH_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,2].legend(NM_lab)
        axr_arr[0,1].set_xscale('log')
        axr_arr[0,2].set_xscale('log')
        axr_arr[0,1].xaxis.set_major_formatter(ticker.ScalarFormatter())
        axr_arr[0,2].xaxis.set_major_formatter(ticker.ScalarFormatter())



    axr_arr[0,0].set_ylabel('Average Burst Rates [spikes/s]')
    axr_arr[0,0].set_xlabel('Driving frequency [Hz]')
    axr_arr[0,1].set_xlabel('Event Duration [ms]')
    axr_arr[0,2].set_xlabel('Event Duration [ms]')
    axr_arr[0,0].set_title('Rhythmic')
    axr_arr[0,1].set_title('Low Intensity')
    axr_arr[0,2].set_title('High Intensity')


    filnam='CtxSpkratepre'
    fig.tight_layout()
    axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(0., 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )
    
    axr_arr[0,1].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(1/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    axr_arr[0,2].annotate('PosC', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(2/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)


def run_mulfil(ancls, pres, ax, otherstim=False, idx=None, confunits=None):
    for chore, res in ancls.my_chores:
        chor_seg = [ancls.uniq_combo[ancls.uniq_sims[chore[1],0]], ancls.uniq_feeds[ancls.uniq_sims[chore[1],1]]]
    if pres=='LFOplot':
        print(chor_seg)
        ax, arr = ancls.run_timser(ax, chore, 'STN_LFO_Amp', chor_seg, sqr=True, otherstim=otherstim, cumsum=True, confunits=confunits)
    elif pres=='NetSyn':
        ax, arr = ancls.run_timser(ax, chore, 'NetSyn', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits)
    elif res=='STN_pp':
        cell_spk = ancls.get_spk(chore, 'STN_spk', ancls.tmin_ana, ancls.tmax_ana)
        cell_pp, tbin = ancls.get_pp(cell_spk, tbin=ancls.tbin)
        fdsp_pp = ancls.run_pp(chore, 'STN_spk', chor_seg)
        ax = ancls.get_STNpp_ax(ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0) 
        if idx is not None:
            t0_idx = int((1500-ancls.tmin_ana)/tbin)
            t_idx = int(idx/tbin) 
            avgspkr = 1000*np.sum(fdsp_pp[:,:,2, t0_idx:t_idx], axis=-1)/idx 
        arr = avgspkr 
    return ax, arr


def get_spk_rate(pp, T, typ, ofset=10, tbin=5):
    tbin_of = int(ofset//5)
    # pp = avg
    nnz_idx = np.where(pp[tbin_of:]>0)[0][0]
    T_idx = int(T//tbin)
    if typ == 'R':
        rat = np.sum(pp[tbin_of+nnz_idx:T_idx])*1000/(tbin*(T_idx-nnz_idx)) if nnz_idx < T_idx else 0
    elif typ == 'E':
        rat = np.sum(pp[tbin_of+nnz_idx:tbin_of+nnz_idx+T_idx])*1000/T if nnz_idx < T_idx else 0
    return rat 

def plot_spec_cur(ancls, exp_path, gids, axr_arr, coord, timrng):

    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    i,j = coord
    axr_arr[i,j+3].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[i,j].imshow(STNspec, cmap='plasma', origin='bottom', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[i,j].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    return axr_arr

def get_resfreq(ancls, exp_path, gids, timrng):
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 
    ind = np.unravel_index(np.argmax(STNspec, axis=None), STNspec.shape)
    maxfreq = freq[ind[0]]
    return maxfreq
    


def get_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] = spcpow
    return spcmat

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))



if __name__ == '__main__':
#    plot_v()
#    TFpanel()
#    plot_triphasic()
#    spiking_rates()
#    reso_freq()
#    conflict()
#    segctx()

    get_spike_profile()


#    get_spike_profile()    
