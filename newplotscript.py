#! python3

import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.gridspec as gs
import matplotlib.style as mpl_style
import matplotlib.image as mpimg
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])

def plot_v():
    #filSing='/scratch/04119/pmoolcha/STNGPe/2019-06-15/TAC20190615_OneCell-000.hdf5'
    #filF0 ='/scratch/04119/pmoolcha/STNGPe/2019-05-28/TAC20190528_Spiking_SingPop_curr_spikes-000.hdf5'
    #Singexpt_path='P000/F0intrin/F000/OneCell/N000'
    #F0expt_path='P000/F0intrin/F000/SimpleNet4/N001'

    filF0='/Volumes/Work/sandbox/Code/SGP35N75HD/TAC20200707_Spiking_SingPop_F0intrin-000.hdf5'
    filSing ='/Volumes/Work/LabData/STNGPe/2020-07-08/MBP20200708_OneCell-000.hdf5'
    Singexpt_path='P000/F0intrin/F000/OneCell/N000'
    F0expt_path='P000/F0intrin/F000/SimpleNet4/N000'


    ancls = Plots(filF0, F0expt_path) 
    anclsing = Plots(filSing, Singexpt_path) 

    figprops = {                                                                                                                                                                                    
                'fig_siz': (5, 7),
         #       'fig_siz': (10, 8),
                'glshap': (7,1),
                'hratio': (1,1,1,0.1,1,1,1),
                }
    fig, gl = ancls.create_fig(figprops)
    gl.update(wspace=0.025, hspace=0.1)
    axr_arr = ancls.get_axs_arr(fig, gl, form=False)

    for i in [1,2,4,5,6]: 
        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[i,0])

    axr_arr[3,0].set_axis_off()

    for i in range(6): 
        axr_arr[i,0].spines['bottom'].set_visible(False)
        axr_arr[i,0].xaxis.set_visible(False)

    axr_arr[4:,:] = ancls.get_indV(F0expt_path, axr_arr[4:,:], timrng=[500,2500])
    axr_arr[:3,:] = anclsing.get_indV(Singexpt_path, axr_arr[:3,:], idx={'STN':[0],'GPP':[0],'GPA':[0]}, timrng=[500,2500])

    axr_arr[0,0].set_title('Single Cells Autonomous Activity [mV]', pad=0.2)
    axr_arr[4,0].set_title('Single Cells in STN-GPe network', pad =0)
    axr_arr[-1,0].set_xlabel('time [ms]')

    axr_arr[0,0].set_ylabel('$V_{STN}$', color='r')
    axr_arr[4,0].set_ylabel('$V_{STN}$', color='r')
    axr_arr[1,0].set_ylabel('$V_{GPeP}$', color='b')
    axr_arr[5,0].set_ylabel('$V_{GPeP}$', color='b')
    axr_arr[2,0].set_ylabel('$V_{GPeA}$', color='g')
    axr_arr[6,0].set_ylabel('$V_{GPeA}$', color='g')

#    axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0., 1), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top'
#        )
#    gl.tight_layout(fig)

#    axr_arr[4,0].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0., 0.5), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top',
#        )
    fig.tight_layout()

    filnam='Vplots'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def TFpanel():
    
#    filSing='/scratch/04119/pmoolcha/STNGPe/2019-06-15/TAC20190615_OneCell-000.hdf5'
#    filF0 ='/scratch/04119/pmoolcha/STNGPe/2019-05-28/TAC20190528_Spiking_SingPop_curr_spikes-000.hdf5'
#    filampa='/scratch/04119/pmoolcha/STNGPe/2019-06-15/TAC20190615_AMPAonly-000.hdf5'
#    filctx ='/scratch/04119/pmoolcha/STNGPe/2019-06-15/TAC20190615_AMPANMDA-000.hdf5'

#    Singexpt_path='P000/F0intrin/F000/OneCell/N000'
#    F0expt_path='P000/F0intrin/F000/SimpleNet4/N001'
#    ampa_path='P001/F1H4/F000/SimpleNet4/N000'
#    ctx_path='P001/F1H4/F000/SimpleNet4/N000'

    filSing='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_OneCell-000.hdf5' 
    Singexpt_path='P000/F0intrin/F000/OneCell/N000'
    anclsing = Plots(filSing, Singexpt_path) 


    filF0 ='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Intrin4-000.hdf5' 
 #   filampa='/Volumes/LaCie/SimulationData/TAC20200919_Spiking_SingPop_Reg_AMPA-001.hdf5' 
    filampa='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPA-001.hdf5' 
    filnmda ='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_NMDA-000.hdf5' 
    filctx ='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPANMDA-000.hdf5' 


    F0expt_path='P000/F0intrin/F000/SimpleNet4/N000'
    ampa_path='P002/F1H4/F000/SimpleNet4/N000'
    nmda_path='P002/F1H4/F000/SimpleNet4/N000'
    ctx_path='P002/F1H4/F000/SimpleNet4/N000'

    anclsintri = Plots(filF0, F0expt_path) 
    anclsampa= Plots(filampa, ampa_path) 
    anclsnmda= Plots(filnmda, nmda_path) 
    anclsctx = Plots(filctx, ctx_path) 

    _, _, _, ISTN, tim = anclsing.get_curr(Singexpt_path, net=False, cap=True)

    tidx = slice(tim.shape[0])
    _, _, _, ISG, _ = anclsintri.get_curr(F0expt_path, net=True, cap=True )
    _, _, _, Iamp, _ = anclsampa.get_curr(ampa_path, net=True, cap=True, )
    _, _, _, Inmd, _ = anclsnmda.get_curr(nmda_path, net=True, cap=True, )
    _, _, _, Ictx, _ = anclsctx.get_curr(ctx_path, net=True, cap=True, )

    ISG=ISG[tidx]
    Iamp=Iamp[tidx]
    Inmd=Inmd[tidx]
    Ictx=Ictx[tidx]


    freq = np.geomspace(2,64,66)

    STNspec = get_spectral_matrix(ISTN, freq, tim) 
    SGspec = get_spectral_matrix(ISG, freq, tim) 
    ampspec = get_spectral_matrix(Iamp, freq, tim) 
    nmdspec = get_spectral_matrix(Inmd, freq, tim) 
    ctxspec = get_spectral_matrix(Ictx, freq, tim) 

    vmin = min(SGspec.min(), ampspec.min(), nmdspec.min(), ctxspec.min())
    vmax = max(SGspec.max(), ampspec.max(), nmdspec.max(), ctxspec.max())

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
                'fig_siz': (8, 10),
                'glshap': (5,3),
                'wratio': (6,3, 0.25)
                }
    fig, gl = anclsintri.create_fig(figprops)
    fig.suptitle('Spectrograms of Net Capacitative Currents')
#    axr_arr = ancls.get_axs_arr(fig, gl, form=False)

    cbarsingax = fig.add_subplot(gl[0,-1]) 
    cbarnetax = fig.add_subplot(gl[1:,-1]) 
    axr_arr = np.empty(shape=(5,2), dtype='O')
    for i, _ in np.ndenumerate(axr_arr): 
        axr_arr[i] = fig.add_subplot(gl[i])
        anclsintri.style_axis(axr_arr[i], form=True)
    #for _, ax in np.ndenumerate(axr_arr[:,2]): 

    for _, ax in np.ndenumerate(axr_arr[:,1]): 
        axr_arr[0,1].get_shared_x_axes().join(axr_arr[-1,1], ax) 
        axr_arr[0,1].get_shared_y_axes().join(axr_arr[-1,1], ax) 
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
        ax.set_aspect('equal', adjustable='box')

    for _, ax in np.ndenumerate(axr_arr[1:,0]): 
        axr_arr[1,0].get_shared_x_axes().join(axr_arr[1,0], ax) 
        axr_arr[1,0].get_shared_y_axes().join(axr_arr[1,0], ax) 

    for _, ax in np.ndenumerate(axr_arr[:,1]): 
        axr_arr[0,1].get_shared_x_axes().join(axr_arr[0,1], ax) 


   # axr_arr[0,0].imshow(STNspec, norm=colors.LogNorm(vmin=vmin, vmax=vmax), cmap='plasma', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
  #  axr_arr[0,2].imshow(STNspec, cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
  #  axr_arr[1,2].imshow(SGspec,  cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
  #  axr_arr[2,2].imshow(ampspec, cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
  #  axr_arr[3,2].imshow(nmdspec, cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
  #  img=axr_arr[4,2].imshow(ctxspec, cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
    
    sig = (ISTN, STNspec)
    timslice = slice(35000,55000)
    axr_arr[0,0].plot(tim[timslice], sig[0][timslice])
    imgsing=axr_arr[0,1].imshow(sig[1], cmap='plasma', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])

    for i, sig in enumerate([(ISG,SGspec), (Iamp,ampspec), (Inmd,nmdspec)]):
        axr_arr[4,0].plot(tim[timslice], sig[0][timslice])
        axr_arr[i+1,1].imshow(sig[1], cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
      #  axr_arr[i+1,1].imshow(sig[1], cmap='plasma',  aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])

    sig = (Ictx, ctxspec)
    axr_arr[4,0].plot(tim[timslice], sig[0][timslice])
    img=axr_arr[4,1].imshow(sig[1], cmap='plasma', vmin=vmin, vmax=vmax, aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])
 #   img=axr_arr[4,1].imshow(sig[1], cmap='plasma', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6])

    for i, tit in enumerate(['Single STN cell', 'No cortical input', 'AMPA only', 'NMDA only', 'AMPA + NMDA']):
        axr_arr[i,0].set_title(tit, y=0.9)

    axr_arr[2,0].set_ylabel('Mean Capacitative Current [nA]')
    axr_arr[2,1].set_ylabel('Frequency [Hz]')

  #  axr_arr[0,0].set_ylabel('Frequency')
  #  axr_arr[1,0].set_ylabel('Frequency')
  #  axr_arr[1,0].set_xlabel('time [ms]')
  #  axr_arr[1,1].set_xlabel('time [ms]')

  #  axr_arr[0,0].set_title('Single STN')
  #  axr_arr[0,1].set_title('STN-GPe network')
  #  axr_arr[1,0].set_title('AMPA only')
  #  axr_arr[1,1].set_title('AMPA + NMDA')

    for _, ax in np.ndenumerate(axr_arr[:,1]): 
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
 #   fig.suptitle('Spectrograms of Net Capacitative Currents')

  #  axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
  #      xytext=(0., 1), textcoords='figure fraction',
  #      horizontalalignment='left', verticalalignment='top'
  #      )
#    fig.colorbar(img, use_gridspec=True)

#    cbarax, kw = mpl.colorbar.make_axes_gridspec([axr_arr[0,1], axr_arr[1,1]], orientation='vertical', fraction=0.05, pad=0.1)
    cbarsing = mpl.colorbar.Colorbar(cbarsingax, imgsing, format=formatter)
    cbarnet = mpl.colorbar.Colorbar(cbarnetax, img, format=formatter)


    filnam='TFpanelpreNewTuningnmda'
    fig.tight_layout()
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)


def get_spike_profile():
    filtri1='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPANMDA-000.hdf5' 
    exp_path1='/P000/F1H4/F000/SimpleNet4/N000'

    filtri2='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_Spiking_SingPop_Reg_AMPANMDA-000.hdf5'
    exp_path2='/P001/F1H4/F000/SimpleNet4/N000'

    timrng=[1250, 2250]

    ancls1 = Plots(filtri1, exp_path1, tmin=timrng[0], tmax=timrng[1]) 
    ancls2 = Plots(filtri2, exp_path2, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
                'fig_siz': (6.5, 10),
              #  'fig_siz': (10,8 ),
                'glshap': (1,3),
                'wratio': (1,0.3,1)
                }


    fig, gl = ancls1.create_fig(figprops)

    gl.update(wspace=0, hspace=0.0)
    gl.tight_layout(fig, pad=0)
    
    
    gl_brake = gs.GridSpecFromSubplotSpec(6, 1, subplot_spec=gl[:, 0], wspace=0.0, hspace=0.5, height_ratios=(1,1,1,1,1,1))
    gl_triph = gs.GridSpecFromSubplotSpec(6, 1, subplot_spec=gl[:, 2], wspace=0.0, hspace=0.5, height_ratios=(1,1,1,1,1,1))

    axr_arr = np.empty(shape=(6,2), dtype='O')
    for i in range(6):
        axr_arr[i,0] = fig.add_subplot(gl_brake[i,0])
        axr_arr[i,1] = fig.add_subplot(gl_triph[i,0])

    for i in range(4):
        ancls1.style_axis(axr_arr[i,0], form=False)
        ancls2.style_axis(axr_arr[i,1], form=False)
    for i in range(4,6):
        axr_arr[i,0].axis('off')
        axr_arr[i,1].axis('off')

    ancls1.style_axis(axr_arr[2,0], form=True)
    ancls2.style_axis(axr_arr[2,1], form=True)

    for i in range(4):
        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[i,0]) 
        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[i,1]) 

    axr_arr[:4,0] = plot_triphasic(ancls1, exp_path1, axr_arr[:4,0], timrng)
    axr_arr[:4,1] = plot_triphasic(ancls2, exp_path2, axr_arr[:4,1], timrng, rlab=True)


    axr_arr[0,0].set_ylabel('STN unit', color='r')
    axr_arr[0,0].set_title('STN raster and PSTH') 

    axr_arr[1,0].set_ylabel('$V_m$ [mV]')
    axr_arr[1,0].set_title('Typical $V_m$ responses')

    axr_arr[2,0].legend(['NetSyn','GABA','NMDA','AMPA'])
    axr_arr[2,0].set_title('PSCs')
    axr_arr[2,0].set_ylabel('$I_{syn}$ [nA]')

    axr_arr[3,0].set_ylabel('Frequency [Hz]')
    axr_arr[3,0].set_xlabel('time [ms]')
    axr_arr[3,0].set_title('Spectrogram')

    cellMC  = mpimg.imread('CellNMC.png')
    cellCG  = mpimg.imread('CellNCG.png')
    nettri  = mpimg.imread('nettri.png')
    netbra  = mpimg.imread('netbrake.png')
    axr_arr[4,0].imshow(cellCG)
    axr_arr[5,0].imshow(netbra)
    axr_arr[4,1].imshow(cellMC)
    axr_arr[5,1].imshow(nettri)



    filnam='NewTriphasic'
    fig.align_ylabels()
    fig.align_xlabels()


    fig.tight_layout()
#    axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0., 1), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top'
#        )
#
#    axr_arr[0,1].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0.5, 1), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top'
#        )
#
#    axr_arr[1,0].annotate('PosC', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0., 0.5), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top'
#        )
#
#    axr_arr[1,1].annotate('PosD', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0.5, 0.5), textcoords='figure fraction',
#        horizontalalignment='left', verticalalignment='top'
#        )
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def plot_triphasic(ancls, exp_path, axs_arr, timrng, rlab=False):

    ctx_lw = 2
    axr_arr = axs_arr.reshape(2,2, order='F')
    Ctx_feeds, N_Ctx_feeds = ancls.anacl.get_ctx_feed(exp_path)

# triraster

    STNpop_spks = ancls.get_spk_arr(exp_path, ancls.STNgids) 
  #  STNpop_spks = spk_arr[self.STNgids]
    
    STN_pp, tbin = ancls.anacl.get_gid_pp(exp_path, ancls.STNgids, tbin=5) 
    STN_sum = np.sum(STN_pp, axis=0)
    
    timalgn=1500
    tposts = np.arange(ancls.anacl.tmin-timalgn, ancls.anacl.tmax+tbin-timalgn, tbin)
    


    for gidx, tvec in enumerate(STNpop_spks):
        axr_arr[0,0].eventplot(np.concatenate(tvec)-timalgn, lineoffsets=gidx, linelengths=0.95, linewidths=2, alpha=0.5, color='r')
    
    axr_arr[0,0].plot([0,80],[80,80], lw=ctx_lw, color='k')
    spk_cnt_ax = axr_arr[0,0].twinx()
    spk_cnt_ax = ancls.style_axis(spk_cnt_ax, top=False, right=True, bottom=False, left=False, form=False)
    spk_cnt_ax.bar(tposts, STN_sum, width=tbin, align='center', alpha=0.75, color='k')
 #    spk_cnt_ax.yaxis.tick_left()
    if rlab:
        spk_cnt_ax.yaxis.set_label_text('Spike Count', color='k')



    
# dep block 
    idx_lst = ancls.STNgids[np.array([0, 10, 25, 50])] 
    
    STN_idx = ancls.gids[idx_lst] 
    STN_arr = np.array(ancls.filobj[exp_path]['raw/STNSta_V'][STN_idx,:])
    
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500
    STN_arr=STN_arr[:,tidx[0]:tidx[1]]
    
    axr_arr[1,0].plot(tim, STN_arr.T)
    axr_arr[1,0].plot([0,80],[33,33], lw=ctx_lw, color='k')
        
# syn currents
    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][ancls.STNgids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    axr_arr[0,1].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 
 #   axr_arr[0,1].plot([0,80],[0.2e-2,0.2e-2], lw=ctx_lw, color='k')

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[1,1].imshow(STNspec, cmap='plasma', origin='bottom', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[1,1].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

#    for _,ax in np.ndenumerate(axr_arr):
#        ancls.anacl.upd_axis_ticks(ax.axes.xaxis, Ctx_feeds, N_Ctx_feeds)

    return axr_arr.reshape(4, order='F')

def spiking_rates():
    filRH='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_Spiking_SingPop_curr_spikes-000.hdf5'
    filLE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighLow-000.hdf5'
    filHE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'

    timrng=[1500, 2500]
    anRH=Plots(filRH, None, tmin=timrng[0], tmax=timrng[1]) 
    anLE=Plots(filLE, None, tmin=timrng[0], tmax=timrng[1]) 
    anHE=Plots(filHE, None, tmin=timrng[0], tmax=timrng[1]) 

    N_NMDA_pos = [1,4,7] 
    N_NMDA = 3 
    NM_lab = ['Low', 'Med', 'High']

    RH = [(2,'2'), (4,'4'), (8,'8'), (12.5,'13'), (20,'20'), (32,'32')]
    RH_plot=np.array([2,4,8,12.5,20,32])

    EV = np.array([10, 31, 50, 80, 125, 250, 500])

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
           #     'fig_siz': (8, 4),
                'fig_siz': (10,6 ),
                'glshap': (1,3),
                }

    fig, gl = anRH.create_fig(figprops)

    axr_arr = np.empty(shape=(1,3), dtype='O')
    for i, _ in np.ndenumerate(axr_arr):
        axr_arr[i] = fig.add_subplot(gl[i])
        anRH.style_axis(axr_arr[i], form=False)

    for _, ax in np.ndenumerate(axr_arr): 
        axr_arr[0,0].get_shared_y_axes().join(axr_arr[0,0], ax) 

    RH_rat = np.zeros(shape=(len(RH), N_NMDA)) 
    EL_rat = np.zeros(shape=(len(EV), N_NMDA)) 
    EH_rat = np.zeros(shape=(len(EV), N_NMDA)) 

    for idx, i in enumerate(N_NMDA_pos):
        for jidx, j in enumerate(RH):
            expt = 'P{:03d}/F1H{!s}/F000/SimpleNet4/N000'.format(i, j[1])
            STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anRH.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            RH_rat[jidx,idx] = get_spk_rate(pp, 1000/j[0], 'R')

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anLE.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            EL_rat[jidx,idx] = get_spk_rate(pp, 150, 'E')

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            STN_pp, tbin = anHE.anacl.get_gid_pp(expt, ctxSTNgids, tbin=5) 
            pp = np.mean(STN_pp, axis=0)
            EH_rat[jidx,idx] = get_spk_rate(pp, 150, 'E')

        axr_arr[0,0].plot(RH_plot, RH_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,1].plot(EV, EL_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,2].plot(EV, EH_rat[:,idx], label=NM_lab[idx], marker='D')
        axr_arr[0,2].legend(NM_lab)
        axr_arr[0,1].set_xscale('log')
        axr_arr[0,2].set_xscale('log')
        axr_arr[0,1].xaxis.set_major_formatter(ticker.ScalarFormatter())
        axr_arr[0,2].xaxis.set_major_formatter(ticker.ScalarFormatter())



    axr_arr[0,0].set_ylabel('Average Burst Rates [spikes/s]')
    axr_arr[0,0].set_xlabel('Driving frequency [Hz]')
    axr_arr[0,1].set_xlabel('Event Duration [ms]')
    axr_arr[0,2].set_xlabel('Event Duration [ms]')
    axr_arr[0,0].set_title('Rhythmic')
    axr_arr[0,1].set_title('Low Intensity')
    axr_arr[0,2].set_title('High Intensity')


    filnam='CtxSpkratepre'
    fig.tight_layout()
    axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(0., 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )
    
    axr_arr[0,1].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(1/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    axr_arr[0,2].annotate('PosC', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(2/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def reso_freq():

    filRH='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_Spiking_SingPop_curr_spikes-000.hdf5'
    filLE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighLow-000.hdf5'
    filHE='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'

    timrng=[1250, 2500]
    anRH=Plots(filRH, None, tmin=timrng[0], tmax=timrng[1]) 
    anLE=Plots(filLE, None, tmin=timrng[0], tmax=timrng[1]) 
    anHE=Plots(filHE, None, tmin=timrng[0], tmax=timrng[1]) 

    N_NMDA_pos = [1,4,7] 
    N_NMDA = 3 
    NM_lab = ['Low', 'Med', 'High']

    RH = [(2,'2'), (4,'4'), (8,'8'), (12.5,'13'), (20,'20'), (32,'32')]
#    RH = [(2,'2'), (4,'4')]
    RH_plot=np.array([2,4,8,12.5,20,32])
#    RH_plot=np.array([2,4])

    EV = np.array([10, 31, 50, 80, 125, 250, 500])
#    EV = np.array([10, 125])

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
           #     'fig_siz': (8, 4),
                'fig_siz': (8,6),
             #   'fig_siz': (12,9),
                'glshap': (2,3),
                }

    fig, gl = anRH.create_fig(figprops)

    axr_arr = np.empty(shape=(2,5), dtype='O')
    for i, _ in np.ndenumerate(axr_arr[:,:3]):
        axr_arr[i] = fig.add_subplot(gl[i])
        anRH.style_axis(axr_arr[i], form=False)

    for _, ax in np.ndenumerate(axr_arr[:,:2]): 
        axr_arr[0,0].get_shared_y_axes().join(axr_arr[0,0], ax) 
        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], ax) 

    for i in range(2):
        for j in range(2):
            axr_arr[i,j+3] = inset_axes(axr_arr[i,j], width='100%', height='35%')
#            axr_arr[i,j+3] = axr_arr[i,j].inset_axes([0.6, 0.6, 0.35, 0.35]) 
            anRH.style_axis(axr_arr[i,j+3], bottom=False, form=True)
           # axr_arr[i,j+3].axes.get_xaxis().set_visible(False)
            axr_arr[i,j+3].set_axis_off()

    for _, ax in np.ndenumerate(axr_arr[:,3:]): 
        axr_arr[0,3].get_shared_y_axes().join(axr_arr[0,3], ax) 
        axr_arr[0,3].get_shared_x_axes().join(axr_arr[0,3], ax) 

    axr_arr[0,2].get_shared_y_axes().join(axr_arr[0,2], axr_arr[1,2]) 

    RH_rat = np.zeros(shape=(len(RH), N_NMDA)) 
    EL_rat = np.zeros(shape=(len(EV), N_NMDA)) 
    EH_rat = np.zeros(shape=(len(EV), N_NMDA)) 

    RHspec=[4,20]

    for jidx, j in enumerate(RHspec):
        expt = 'P004/F1H{!s}/F000/SimpleNet4/N000'.format(j)
        STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
        ctxSTNgids = np.where(STNgids==1)[0]
        axr_arr = plot_spec_cur(anRH, expt, ctxSTNgids, axr_arr, (0,jidx), timrng)

    expt = 'P004/F1E10/F000/SimpleNet4/N000'
    STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
    ctxSTNgids = np.where(STNgids==1)[0]
    axr_arr = plot_spec_cur(anLE, expt, ctxSTNgids, axr_arr, (1,0), timrng)

    expt = 'P007/F1E80/F000/SimpleNet4/N000'
    STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
    ctxSTNgids = np.where(STNgids==1)[0]
    axr_arr = plot_spec_cur(anHE, expt, ctxSTNgids, axr_arr, (1,1), timrng)

    cmapmpl = mpl.cm.get_cmap('tab10')
    colors = cmapmpl(np.arange(10))

    for idx, i in enumerate(N_NMDA_pos):
        for jidx, j in enumerate(RH):
            expt = 'P{:03d}/F1H{!s}/F000/SimpleNet4/N000'.format(i, j[1])
            STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            RH_rat[jidx,idx] = get_resfreq(anRH, expt, ctxSTNgids, timrng) 

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            EL_rat[jidx,idx] = get_resfreq(anLE, expt, ctxSTNgids, timrng) 

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            EH_rat[jidx,idx] = get_resfreq(anHE, expt, ctxSTNgids, timrng) 


        axr_arr[0,2].plot(RH_plot, RH_rat[:,idx], label=NM_lab[idx], color=colors[idx], marker='D')
        axr_arr[0,2].legend(NM_lab, title='NMDA', frameon=False)
        axr_arr[1,2].plot(EV, EL_rat[:,idx], marker='D', color=colors[idx])
        axr_arr[1,2].plot(EV, EH_rat[:,idx], marker='o', color=colors[idx])

    axr_arr[1,2].set_xscale('log')
    axr_arr[1,2].xaxis.set_major_formatter(ticker.ScalarFormatter())
    axr_arr[1,2].plot([],[], marker='D', label='Low', color='k')
    axr_arr[1,2].plot([],[], marker='o', label='High', color='k')
    axr_arr[1,2].legend(title='Stim Int)', frameon=False)
    axr_arr[1,2].axhspan(2, 8, xmin=0, xmax=1, color='k', alpha=0.2, lw=None) 
    axr_arr[0,2].axhspan(2, 8, xmin=0, xmax=1, color='k', alpha=0.2, lw=None) 
    axr_arr[0,2].set_ylabel('Resonant Freq. [Hz]')
    axr_arr[1,2].set_ylabel('Resonant Freq. [Hz]')
    axr_arr[0,2].set_title('Rhythmic Stim')
    axr_arr[1,2].set_title('Event Stim')
        
    axr_arr[0,0].set_ylabel('Frequency [Hz]')
    axr_arr[0,0].set_title('Rhythmic 4 Hz\nMid NMDA')
    axr_arr[1,0].set_ylabel('Frequency [Hz]')
    axr_arr[1,0].set_xlabel('time [ms]')
    axr_arr[0,1].set_title('Rhythmic 20 Hz\nMid NMDA')
    axr_arr[1,1].set_xlabel('time [ms]')

    axr_arr[1,0].set_title('Event 10 ms, Low Stim\nMid NMDA')
    axr_arr[1,1].set_title('Event 80 ms, High Stim\nHigh NMDA')

    axr_arr[0,2].set_xlabel('Driving frequency [Hz]')
    axr_arr[1,2].set_xlabel('Event Duration [ms]')


    fig.tight_layout()
    axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(0., 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )
    
    axr_arr[0,1].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(1/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    axr_arr[0,2].annotate('PosC', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(2/3, 1), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    axr_arr[1,0].annotate('PosD', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(0., 1/2), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )
    
    axr_arr[1,1].annotate('PosE', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(1/3, 1/2), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    axr_arr[1,2].annotate('PosF', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(2/3, 1/2), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    filnam='resopow'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def conflict():
   # filSing='/scratch/04119/pmoolcha/STNGPe/SingRhythmStim.hdf5'
    #filConf='/scratch/04119/pmoolcha/STNGPe/AllRhythmStim.hdf5' 

#    filSing='/scratch/04119/pmoolcha/STNGPe/SingMultiRhyth.hdf5'
#    filConf='/scratch/04119/pmoolcha/STNGPe/MultiRhyth.hdf5'

   # filSing='/scratch/04119/pmoolcha/STNGPe/DozMultiRhyth.hdf5'
  #  filConf='/scratch/04119/pmoolcha/STNGPe/DozMultiRhyth.hdf5'
#    filSing='/scratch/04119/pmoolcha/STNGPe/BigDozConf.hdf5'
  #  filConf='/scratch/04119/pmoolcha/STNGPe/BigDozConf.hdf5'
    filConf='/scratch/04119/pmoolcha/STNGPe/BigDozSing.hdf5'
    filSing='/scratch/04119/pmoolcha/STNGPe/BigDozSing.hdf5'

    fildum='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'

    timrng=[1250, 8000]
    andum=Plots(fildum, None, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
                'fig_siz': (12, 12),
             #   'fig_siz': (16,12),
             #   'fig_siz': (12,9),
             #   'glshap': (3,4),
                'glshap': (4,4),
                }

    fig, gl = andum.create_fig(figprops)
#    axr_arr = np.empty(shape=(3,4), dtype='O')
    axr_arr = np.empty(shape=(4,4), dtype='O')
    for i, _ in np.ndenumerate(axr_arr):
        axr_arr[i] = fig.add_subplot(gl[i])
        andum.style_axis(axr_arr[i], form=False)

    for i in range(3):
        for _, ax in np.ndenumerate(axr_arr[i,:]): 
            axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], ax) 
            axr_arr[i,0].get_shared_y_axes().join(axr_arr[i,0], ax) 
#    for _, ax in np.ndenumerate(axr_arr): 
#        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], ax) 

#    for i in range(1,3):
#        for _, ax in np.ndenumerate(axr_arr[i,:]): 
#            axr_arr[i,0].get_shared_y_axes().join(axr_arr[i,0], ax) 
#
#    axr_arr[0,0].get_shared_y_axes().join(axr_arr[0,0], axr_arr[0,1]) 
#    axr_arr[0,2].get_shared_y_axes().join(axr_arr[0,2], axr_arr[0,3]) 

    param_dict={}
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (5, 5)),# '--'
                            (0, (1, 5)), #':'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]
    param_dict['tbin'] = 5
    param_dict['ylimSTNpp'] = None 
    param_dict['GLM_Ana'] = ['LFOplot', 'NetSynplot', 'STN_pp'] 


    mainparam = {
            'param': {
                      #  'NMDAGLV':  {'Ctx_STN_A': [1.833e-05], }, 
                        'NMDAGLV':  {'Ctx_STN_A': [3.666e-06], }, 
                },
            'feed': {
                    'Conf': [('fds', ['RhythF22NE50_150']),] 
                },
            'cond': {
                    'RhythmicEvent' : ('NMDAGLV','Conf'),
                    },
        }             

  #  filtfds= ['RhythF22NE50_Sing25', 'RhythF22NE50_Sing150', 'RhythF22NE50_25', 'RhythF22NE50_150']

  #  filtfds= ['SingMultRhythF22NE50_25', 'SingMultRhythF22NE50_150', 'MultRhythF22NE50_25', 'MultRhythF22NE50_150']
 #   filtfds= ['DozRhythF22NE50_25', 'ReReDozRhythF22NE50_75', 'DozRhythF22NE50_150', 'ReReDozRhythF22NE50_350']
  #  filtfds= ['DozRhythF22NE50_150', 'ReReDozRhythF22NE50_200', 'ReReDozRhythF22NE50_275', 'ReReDozRhythF22NE50_450']
 #   filtfds= ['ReReDozRhythF22NE50_75', 'ReReDozRhythF22NE50_125', 'DozRhythF22NE50_150', 'ReReDozRhythF22NE50_200']
#    filtfds= ['DozSingF22NE50_25', 'DozSingF22NE50_75', 'DozSingF22NE50_150', 'DozSingF22NE50_350']
    filtfds= ['DozSingF22NE50_150', 'DozSingF22NE50_200', 'DozSingF22NE50_275', 'DozSingF22NE50_450']
#    filtfds= ['DozSingF22NE50_75', 'DozSingF22NE50_125', 'DozSingF22NE50_150', 'DozSingF22NE50_200']

    cumsum_arr = np.empty(shape=(3,4,3,4))

  #  filtset= [parmsetSing25, parmsetSing150, parmsetConf25, parmsetConf150]
    pres = ['LFOplot', 'NetSyn', 'STN_spk']
    plotpos = [0, 2, 3]
    fils = [filSing, filConf]
 #   filidx= [0,0,1,1]
    filidx= [1,1,0,1]

#    IEI = ['25', '75', '150', '350']
#    IEIdur = np.array([2380, 2930, 3755, 5955]) - 1500

#    IEI = ['75', '125', '150', '200']
#    IEIdur = np.array([2930, 3480, 3755, 4305]) - 1500

    IEI = ['150', '200', '275', '450']
    IEIdur = np.array([3755, 4305, 5130, 7055]) - 1500

    for j in range(4):
     #   param_dict['filt_dict'] = filtset[j] 
        mainparam['feed']={'Conf': [('fds', [filtfds[j]]),]} 
        param_dict['filt_dict'] = mainparam
    #    anobj=GLM_Analysis(fils[filidx[j]], param_dict)
        anobj=GLM_Analysis(fils[filidx[j]], param_dict, tmin=timrng[0], tmax=timrng[1])
        for i in range(2):
            axr_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], axr_arr[i,j], idx=None)
        for i in [2]:
            axr_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], axr_arr[i,j], idx=IEIdur[j])

        anobj.filobj.close() 
       
    # cumsum: [LFO, IEI, seg, tri]
#    print(cumsum_arr[0,0,0,:])


    axr_arr[0,0].set_ylabel('Avg. LFO pow. [$nA^2$]')
    axr_arr[1,0].set_ylabel('Avg. $I_{syn}$ [$nA$]')
    axr_arr[2,0].set_ylabel('Cumm. Avg. Spike Count')


    cumsum_tmp = cumsum_arr[0,:,:,:] 
   
    for j in range(4):
        cumsum_tmp[j,:,:] = cumsum_tmp[j,:,:]/IEIdur[j]


    #for i, iei in enumerate(IEI):
   # axr_arr[3,0].bar(cumsum_tmp[:,0,:].T, orientation='vertical', yerr=cumsum_err[:,0])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, positions=[0,4,8,11], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][0]), labels=[25, 75,150,350]) 
#    axr_arr[3,0].boxplot(cumsum_tmp[:,1,:].T, positions=[1,5,9,12], showmeans=False, patch_artist=True, boxprops=dict(facecolor=param_dict['colors'][1], color=param_dict['colors'][1]), labels=[25,75,150,350])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,2,:].T, positions=[2,6,10,13], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][2]), labels=[25,75,150,350])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, positions=[1,4,7,10], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][0], facecolor=None), labels=[25, 75,150,350]) 
#    axr_arr[3,0].boxplot(cumsum_tmp[:,2,:].T, positions=[2,5,8,11], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][2], facecolor=None), labels=[25,75,150,350])

    t_IEI = np.zeros(shape=(4,3,2)) 

    for j,i in it.combinations(range(4), 2):
        t_IEI[i,j,:] = sts.ttest_ind(cumsum_tmp[i,0,:], cumsum_tmp[j,0,:], equal_var=False) 
    print(t_IEI[:,:,-1])

    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, showmeans=True, labels=[25,75,150,350]) 

    for k in [1,2]:
        for j,i in it.combinations(range(4), 2):
            t_IEI[i,j,:] = sts.ttest_ind(cumsum_tmp[i,k,:], cumsum_tmp[j,k,:], equal_var=False) 
        print(t_IEI[:,:,-1])



    
    t_seg = np.zeros(shape=(3,2,2)) 

    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(cumsum_tmp[2,i,:], cumsum_tmp[2,j,:], equal_var=False) 
    print(t_seg[:,:,-1])





    axr_arr[3,1].boxplot(cumsum_tmp[2,:,:].T, showmeans=True, labels=['25%', '55%', '85%'])
#    axr_arr[3,1].set_ylim(bottom=10)
    labpos = axr_arr[3,1].get_xticks()
#    axr_arr[3,1].plot([labpos[0], labpos[2]], [11,11], color='k')
#    axr_arr[3,1].annotate('*', (0.5*(labpos[0]+labpos[2]), 11.5), xycoords='data', color='k')

#    axr_arr[0,0].set_title('$\Delta$= 25 ms')
#    axr_arr[0,1].set_title('$\Delta$= 75 ms')
#    axr_arr[0,2].set_title('$\Delta$= 150 ms')
#    axr_arr[0,3].set_title('$\Delta$= 350 ms')

#    axr_arr[0,0].set_title('$\Delta$= 75 ms')
#    axr_arr[0,1].set_title('$\Delta$= 125 ms')
#    axr_arr[0,2].set_title('$\Delta$= 150 ms')
#    axr_arr[0,3].set_title('$\Delta$= 200 ms')

    axr_arr[0,0].set_title('$\Delta$= 150 ms')
    axr_arr[0,1].set_title('$\Delta$= 200 ms')
    axr_arr[0,2].set_title('$\Delta$= 275 ms')
    axr_arr[0,3].set_title('$\Delta$= 450 ms')

    for k in [0,1,3]:
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(cumsum_tmp[k,i,:], cumsum_tmp[k,j,:], equal_var=False) 
        print(t_seg[:,:,-1])


    print('spikes')

    spksum_tmp = cumsum_arr[2,:,:,:] 
    t_IEI = np.zeros(shape=(4,3,2)) 

    for j,i in it.combinations(range(4), 2):
        t_IEI[i,j,:] = sts.ttest_ind(spksum_tmp[i,0,:], spksum_tmp[j,0,:], equal_var=False) 
    print(t_IEI[:,:,-1])

    axr_arr[3,2].boxplot(spksum_tmp[:,0,:].T, showmeans=True, labels=[25,75,150,350]) 

    for k in [1,2]:
        for j,i in it.combinations(range(4), 2):
            t_IEI[i,j,:] = sts.ttest_ind(spksum_tmp[i,k,:], spksum_tmp[j,k,:], equal_var=False) 
        print(t_IEI[:,:,-1])


    t_seg = np.zeros(shape=(3,2,2)) 

    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(spksum_tmp[2,i,:], spksum_tmp[2,j,:], equal_var=False) 
    print(t_seg[:,:,-1])

    axr_arr[3,3].boxplot(spksum_tmp[2,:,:].T, showmeans=True, labels=['25%', '55%', '85%'])


    for k in [0,1,3]:
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(spksum_tmp[k,i,:], spksum_tmp[k,j,:], equal_var=False) 
        print(t_seg[:,:,-1])



    for i in range(4):
        axr_arr[-2,i].set_xlabel('time [ms]')




    axr_arr[-1,0].set_xlabel('Inter Event Interval, $\Delta$ [ms]')
    axr_arr[-1,0].set_ylabel('Avg. Theta Pow. during Stim, [$nA^2$]')
    axr_arr[-1,0].annotate('All Sig.\np<0.05', (0.95, 0.2), xycoords='axes fraction', ha='right')
    axr_arr[-1,0].set_title('Seg = 25%')
    axr_arr[-1,2].set_xlabel('Inter Event Interval, $\Delta$ [ms]')
    axr_arr[-1,2].set_title('Seg = 25%')
    axr_arr[-1,1].set_xlabel('Segregation Level')
    axr_arr[-1,2].set_ylabel('Avg. Spk. rate during Stim, [spk/s]')
    axr_arr[-1,1].set_title('$\Delta$ = 150 ms')
    axr_arr[-1,2].annotate('All Sig.\np<0.05', (0.95, 0.2), xycoords='axes fraction', ha='right')
    axr_arr[-1,3].set_xlabel('Segregation Level')
    axr_arr[-1,3].set_title('$\Delta$ = 150 ms')

#    fig.text(0.25, 0.95, 'No Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
#    fig.text(0.75, 0.95, 'Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
  #  br = param_dict['colors'][5]
  #  ctx_lw=3
  #  ypos1= np.array([4.4e1, 4.4e1])
  #  ypos2 = ypos1-0.2e1
  #  ypos1mod= np.array([1.1e1, 1.1e1])

  #  axr_arr[0,0].plot([0,50], ypos1mod, [75,125], ypos1mod, [150,200], ypos1mod, color='k', lw=ctx_lw)
  #  axr_arr[0,2].plot([0,50], ypos1, [75,125], ypos1, [150,200], ypos1, color='k', lw=ctx_lw)

  #  axr_arr[0,1].plot([0,50], ypos1mod, [200,250], ypos1mod, [400,450], ypos1mod, color='k', lw=ctx_lw)
  #  axr_arr[0,3].plot([0,50], ypos1, [200,250], ypos1, [400,450], ypos1, color='k', lw=ctx_lw)

  #  axr_arr[0,2].plot([5,55], ypos2, [80,130], ypos2, [155,205], ypos2, color=br, lw=ctx_lw)
  #  axr_arr[0,3].plot([5,55], ypos2, [205,255], ypos2, [405,455], ypos2, color=br, lw=ctx_lw)

  #  stim25 = [0, 75, 150]
  #  stim150 = [0, 200, 400]
  #  for i in [0,2]:
  #      for j in stim25:
  #          axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
  #  for i in [1,3]:
  #      for j in stim150:
  #          axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
  #          axr_arr[0,i].axvline(j+5, color=br, ls='--', lw=0.1, alpha=0.5)

    verpos = [1, 3/4, 1/2, 1/4]
    horpos = [0, 1/4, 1/2, 3/4]

    seglev=[25, 55, 85]
    for i in range(3):
        axr_arr[0,1].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
        axr_arr[2,2].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
   # axr_arr[-1,2].legend(title='Seg', fontsize='x-large', frameon=False)
    axr_arr[0,1].legend(title='Seg', frameon=False)
    axr_arr[2,2].legend(title='Seg', frameon=False)


#    for j,i in it.product(range(1,4), range(2)):
#        axr_arr[i,j].set_axis_off()

    #for j in range(1,4):
    #    axr_arr[1,j].set_axis_off()
    #axr_arr[0,-1].set_axis_off()
    #axr_arr[0,1].set_axis_off()

    #for i in range(1,4):
    #    axr_arr[-1,i].get_yaxis().set_visible(False)
    #    axr_arr[-1,i].spines['left'].set_visible(False)
    #for i in range(2):
    #    axr_arr[i,0].get_xaxis().set_visible(False)
    #    axr_arr[i,0].spines['bottom'].set_visible(False)

    #axr_arr[0,2].get_xaxis().set_visible(False)
    #axr_arr[0,2].spines['bottom'].set_visible(False)

    

    fig.align_ylabels(axr_arr[:, 0])
    fig.align_ylabels(axr_arr[:, 2])
    fig.tight_layout()
    alp_rep = np.reshape(alpbt[:axr_arr.size], axr_arr.shape)
    for idx, ax in np.ndenumerate(axr_arr):
    #    ax.annotate('P{:d}{:d}'.format(idx[0], idx[1]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
    #    xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
    #    horizontalalignment='left', verticalalignment='top'
    #    )

        ax.annotate('{!s}'.format(alp_rep[idx]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

 #   filnam='quiLowDozConfAll'
    filnam='quiHighDozSingAll'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.svg'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def segctx(): 
    filSing='/scratch/04119/pmoolcha/STNGPe/SingRhythmStim.hdf5'
    filDel5='/scratch/04119/pmoolcha/STNGPe/ConflictDat.hdf5'
 #   filDeloth='/scratch/04119/pmoolcha/STNGPe/HiHiDel.hdf5'
    filDeloth='/scratch/04119/pmoolcha/STNGPe/RedoHiHiDel.hdf5'
    filxt='/scratch/04119/pmoolcha/STNGPe/Delayextra.hdf5'

#    fildum='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'
    newfildum='/scratch/04119/pmoolcha/STNGPe/2020-11-28/TAC20201128_Spiking_SingPop_Evt_AMPANMDAHiInt-000.hdf5'
    newconf='/scratch/04119/pmoolcha/STNGPe/ConflictPlots/ConflictDat.hdf5'
    newsing='/scratch/04119/pmoolcha/STNGPe/ConflictPlots/SingConflictDat.hdf5'

    timrng=[1350, 1850]
    andum=Plots(newfildum, None, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {                                                                                                                                                                                    
                'fig_siz': (9,9),
              #  'fig_siz': (12,9),
                'glshap': (3,3)
                }
    ctx_lw=6

    fig, gl = andum.create_fig(figprops)
    axr_arr = np.empty(shape=(3,3), dtype='O')
    for i, _ in np.ndenumerate(axr_arr):
        axr_arr[i] = fig.add_subplot(gl[i])
        andum.style_axis(axr_arr[i], form=False)

   # for idx, ax in np.ndenumerate(axr_arr):
#AXIS JOIN
#    for idx in it.product(range(2), range(3)): 
#        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[idx]) 
#      #  if idx != (0,0):
#      #      axr_arr[0,1].get_shared_y_axes().join(axr_arr[0,1], ax) 
#        axr_arr[0,1].get_shared_y_axes().join(axr_arr[0,1], axr_arr[idx]) 
#
#    axr_arr[2,0].get_shared_y_axes().join(axr_arr[2,0], axr_arr[2,1])

    param_dict={}
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (5, 5)),# '--'
                            (0, (1, 5)), #':'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]
    param_dict['tbin'] = 5
    param_dict['ylimSTNpp'] = None 
    param_dict['GLM_Ana'] = ['LFOplot', 'NetSynplot', 'STN_pp'] 


    mainparam = {
            'param': {
                        'NMDAGLV':  {'Ctx_STN_A': [1.833e-05], }, 
                },
            'feed': {
                    'Conf': [('fds', ['RhythF22NE50_150']),] 
                },
            'cond': {
                    'RhythmicEvent' : ('NMDAGLV','Conf'),
                    },
        }             

    filtparcom = list([
                        {'Ctx_STN_A': [3.666e-06], },
                        {'Ctx_STN_A': [3.666e-06], },
                        {'Ctx_STN_A': [3.666e-05], },
                      #  {'Ctx_STN_A': [1.833e-05], },
                      #  {'Ctx_STN_A': [3.666e-06], },
                      #  {'Ctx_STN_A': [3.666e-06], 'GPe_STN': [8.0e-6]},
#                        {'Ctx_STN_A': [3.666e-06], 'GPe_STN': [8.0e-6]},
#                        {'Ctx_STN_A': [3.666e-06], 'GPe_STN': [8.0e-6]},
                      #  {'Ctx_STN_A': [1.833e-05], 'GPe_STN': [8.0e-6]},
                      #  {'Ctx_STN_A': [1.833e-05], 'GPe_STN': [8.0e-6]},
                        ])


    filtfds= list([ 
                     #   [('fds', ['F1E50']), ('Seg', [(0.2,0.9)]), ('Int1', [(2,4)])],
                     #   [('fds', ['F22NE50']), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]), ('Int1', [(2,4)]), ('Int2', [(2,4)]),],
                     #   [('fds', ['F22NE50']), ('Seg', [(0.2,0.9)]), ('Dlay', [(9, 11)]), ('Int1', [(2,4)]),  ('Int2', [(2,4)]),],
                        [('fds', ['F1E50']), ('Seg', [(0.2,0.9)]), ('Int1', [(2,4)])],
                        [('fds', ['F22NE50']), ('Seg', [(0.2,0.9)]), ('Dlay', [(30,40)]), ('Int1', [(2,4)]), ('Int2', [(2,4)]),],
                        [('fds', ['F22NE50']), ('Seg', [(0.2,0.9)]), ('Dlay', [(4, 6)]), ('Int1', [(2,4)]),  ('Int2', [(2,4)]),],

                   #     [('fds', ['F22NE50']), ('Int1', [(2,4)])],
             #           [('fds', ['F22NE50']), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]),],
             #           [('fds', ['F22NE50']), ('Dlay', [(9, 11)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]),],
                    #    [('fds', ['F22NE50']), ('Dlay', [(7.1, 7.3)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]),],
                    #    [('fds', ['F22NE50xt']), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]),],
                    #    [('fds', ['F22NE50']), ('Dlay', [(9,11)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]),],
                        ],) 

    print(filtfds)

  #  filtset= [parmsetSing25, parmsetSing150, parmsetConf25, parmsetConf150]
#    pres = ['LFOplot', 'NetSyn', 'STN_spk']

# OLD
 #   fils = [filSing, filDel5, filDeloth, filxt] 
 #   filidx= [0,1,2,3,2]
 #   idx_lst = [(0,0), (0,1), (1,0), (1,1), (1,2)]
 #   filparcomidx = [1, 2, 1,1,1] 

# NEW
    fils = [newsing, newconf, newconf] 
    filidx= [0,1,2,3,2]
    idx_lst = [(0,0), (0,1), (0,2)]
    filparcomidx = [1, 2, 1,1,1] 

 #   filparcomidx = [1, 0,0,0]
 #   filidx= [1,2,3,2]
 #   idx_lst = [(0,1), (1,0), (1,1), (1,2)]

    cumsum_arr = np.zeros(shape=(5,3,4))

    for j, idx in enumerate(idx_lst):
     #   param_dict['filt_dict'] = filtset[j] 
        mainparam['feed']={'Conf': filtfds[j]} 
     #   mainparam['param']={'NMDAGLV': filtparcom[filparcomidx[j]]} 
        mainparam['param']={'NMDAGLV': filtparcom[0]} 
        param_dict['filt_dict'] = mainparam
        anobj=GLM_Analysis(fils[filidx[j]], param_dict, tmin=timrng[0], tmax=timrng[1])
        axr_arr[idx], cumsum_arr[j] = run_mulfil(anobj, 'LFOplot', axr_arr[idx], otherstim=False)
        anobj.filobj.close() 
       
    mainparam['feed']={'Conf': [('fds', ['F22NE50']), ('Seg', [(0.2,0.3)]), ('Dlay', [(4,6)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)])],} 
    param_dict['filt_dict'] = mainparam
    mainparam['param']={'NMDAGLV': filtparcom[0]}

#DELAY 
 #   anobj=GLM_Analysis(filDel5, param_dict, tmin=timrng[0], tmax=timrng[1])
 #   axr_arr[0,2], _ = run_mulfil(anobj, 'LFOplot', axr_arr[0,2], otherstim=True)
 #   anobj.filobj.close() 


    sprmn_coeff = np.full(shape=(5,4,2), fill_value=np.nan) 
    for i in range(5):
        for j in range(4):
            sprmn_coeff[i,j,:] = sts.spearmanr(cumsum_arr[i,:,j], [25,55,85])

    print(sprmn_coeff)


#    print(cumsum_arr[0,:,:])
#    print(cumsum_arr[1,:,:])
    cumsum_arr[0,:,:] = cumsum_arr[0,:,:]/50
    cumsum_arr[1,:,:] = cumsum_arr[1,:,:]/55
    cumsum_arr[2,:,:] = cumsum_arr[2,:,:]/57.25
    cumsum_arr[4,:,:] = cumsum_arr[4,:,:]/60

    delarr = np.array([1,2,4])

    axr_arr[2,2].boxplot(cumsum_arr[delarr,0,:].T, showmeans=True, labels=[5, 7.25, 10])
#    axr_arr[2,2].boxplot(sprmn_coeff[delarr,:,0].T, labels=[5, 7.25, 10])

    labpos = axr_arr[2,2].get_xticks()
    axr_arr[2,2].plot([labpos[0], labpos[1]], [110,110], color='k')
    axr_arr[2,2].annotate('*', (0.5*(labpos[0]+labpos[1]), 111), xycoords='data', color='k')
    axr_arr[2,2].plot([labpos[0], labpos[2]], [120,120], color='k')
    axr_arr[2,2].annotate('*', (0.5*(labpos[0]+labpos[2]), 121), xycoords='data', color='k')

    t_seg = np.zeros(shape=(3,2,2)) 
    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(cumsum_arr[0,i,:], cumsum_arr[0,j,:], equal_var=False) 
    print(t_seg[:,:,-1])
    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(cumsum_arr[1,i,:], cumsum_arr[1,j,:], equal_var=False) 
    print(t_seg[:,:,-1])

   # axr_arr[2,0].boxplot(cumsum_arr[0,:,:].T, showmeans=True, positions=[1,2,3,5,6,7], labels=[25,55,85, 25,55,85]) 
    axr_arr[2,0].boxplot(cumsum_arr[0,:,:].T, showmeans=True, labels=['25%','55%','85%']) 
    axr_arr[2,1].boxplot(cumsum_arr[1,:,:].T, showmeans=True, labels=['25%','55%','85%']) 

    labpos = axr_arr[2,1].get_xticks()
    axr_arr[2,1].plot([labpos[0], labpos[1]], [60,60], color='k')
    axr_arr[2,1].annotate('*', (0.5*(labpos[0]+labpos[1]), 61), xycoords='data', color='k')
    axr_arr[2,1].plot([labpos[0], labpos[2]], [40,40], color='k')
    axr_arr[2,1].annotate('*', (0.5*(labpos[0]+labpos[2]), 41), xycoords='data', color='k')

   # axr_arr[0,0].set_ylabel('Avg. LFO pow. [$nA^2$]')
   # axr_arr[1,0].set_ylabel('Avg. LFO pow. [$nA^2$]')
    axr_arr[0,0].set_ylabel('Avg. Theta pow. [$nA^2$]')
    axr_arr[1,0].set_ylabel('Avg. Theta pow. [$nA^2$]')
 #   axr_arr[1,0].set_label('Avg. $I_{syn}$ [$nA$]')
 #   axr_arr[2,0].set_label('Cumm. Avg. Spike Count')

    axr_arr[2,0].set_ylabel('Avg. Theta Pow. during Stim, [$nA^2$]')
    axr_arr[2,0].set_title('No Conflict')
    axr_arr[2,1].set_title('Conflict\n $\delta$ = 5 ms')
    axr_arr[2,0].set_xlabel('Segregation Level')
    axr_arr[2,1].set_xlabel('Segregation Level')
    
    axr_arr[2,2].set_xlabel('Delay [ms]')
    axr_arr[2,2].set_title('Conflict\nSeg = 25%')
 #   axr_arr[0,0].set_title('$\Delta$= 25 ms')
 #   axr_arr[0,1].set_title('$\Delta$= 150 ms')
 #   axr_arr[0,2].set_title('$\Delta$= 25 ms')
 #   axr_arr[0,3].set_title('$\Delta$= 150 ms')



    t_IEI = np.zeros(shape=(5,5,2)) 

    print(cumsum_arr.shape)
    for j,i in it.combinations(range(5), 2):
        t_IEI[i,j,:] = sts.ttest_ind(cumsum_arr[i,0,:], cumsum_arr[j,0,:], equal_var=False) 
    print(t_IEI[1,:,-1])
    print(t_IEI[2,:,-1])
    print(t_IEI[4,:,-1])



    for i in range(3):
        axr_arr[-2,i].set_xlabel('time [ms]')

 #   fig.text(0.25, 0.95, 'No Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
    br = param_dict['colors'][5]
    ypos1= np.array([3.7e1, 3.7e1])
    ypos2 = ypos1-0.15e1

#    axr_arr[0,0].plot([0,50], [6.2,6.2], color='k', lw=ctx_lw)
 #   axr_arr[0,0].plot([0,50], [15,15], color='k', lw=ctx_lw)

 #   axr_arr[0,1].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #   axr_arr[0,2].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #   axr_arr[1,0].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #   axr_arr[1,2].plot([0,50], ypos1, color='k', lw=ctx_lw)

 #   axr_arr[1,1].plot([0,52.5], ypos1, color='k', lw=ctx_lw)

 #   axr_arr[0,1].plot([5,55], ypos2, color=br, lw=ctx_lw)
 #   axr_arr[0,2].plot([5,55], ypos2, color=br, lw=ctx_lw)
 #   axr_arr[1,0].plot([7.5,57.5], ypos2, color=br, lw=ctx_lw)
 #   axr_arr[1,1].plot([7.5,57.5], ypos2, color=br, lw=ctx_lw)
 #   axr_arr[1,2].plot([10,60], ypos2, color=br, lw=ctx_lw)


 #   axr_arr[0,0].set_title('No Conflict')
 #   axr_arr[0,1].set_title('Conflict\n$\delta$ = 5 ms, $\cap$ = 45 ms')
 #   axr_arr[0,2].set_title('Conflict\n$\delta$ = 5 ms')
 #   axr_arr[1,0].set_title('Conflict\n$\delta$ = 7.25 ms, $\cap$ = 42.75 ms')
 #   axr_arr[1,1].set_title('Conflict\n$\delta$ = 7.5 ms, $\cap$ = 45 ms')
 #   axr_arr[1,2].set_title('Conflict\n$\delta$ = 10 ms, $\cap$ = 40 ms')


 #   axr_arr[0,3].plot([0,50], ypos1, [200,250], ypos1, [400,450], ypos1, color='k', lw=ctx_lw)

 #   axr_arr[0,2].plot([5,55], ypos2, [80,130], ypos2, [155,205], ypos2, color=br, lw=ctx_lw)
 #   axr_arr[0,3].plot([5,55], ypos2, [205,255], ypos2, [405,455], ypos2, color=br, lw=ctx_lw)

 #   stim25 = [0, 75, 150]
 #   stim150 = [0, 200, 400]
 #   for i in [0,2]:
 #       for j in stim25:
 #           axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
 #   for i in [1,3]:
 #       for j in stim150:
 #           axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
 #           axr_arr[0,i].axvline(j+5, color=br, ls='--', lw=0.1, alpha=0.5)

    verpos = [1, 2/3, 1/3-1/32]
    horpos = [0, 1/3, 2/3] 

    seglev=[25, 55, 85]
    for i in range(3):
        axr_arr[1,0].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
  #  axr_arr[1,0].legend(title='Seg', fontsize='large', frameon=False)
  #  axr_arr[1,0].legend(title='Seg', frameon=False, fontsize='x-large')
    axr_arr[1,0].legend(title='Seg', frameon=False, )

 #   for j,i in it.product(range(1,4), range(2)):
    axr_arr[0,-1].set_axis_off()

    for i in range(1,3):
        axr_arr[-2,i].get_yaxis().set_visible(False)
        axr_arr[-2,i].spines['left'].set_visible(False)
    for j in range(2):
        axr_arr[0,j].get_xaxis().set_visible(False)
        axr_arr[0,j].spines['bottom'].set_visible(False)



    alp_rep = np.reshape(alpbt[:axr_arr.size], axr_arr.shape)
    fig.tight_layout()
    for idx, ax in np.ndenumerate(axr_arr):
    #    ax.annotate('P{:d}{:d}'.format(idx[0], idx[1]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
    #    xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
    #    horizontalalignment='left', verticalalignment='top'
    #    )

        ax.annotate('{!s}'.format(alp_rep[idx]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
        horizontalalignment='left', verticalalignment='top'
        )

    filnam='testNEwSetnewctxsegpremod'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def run_mulfil(ancls, pres, ax, otherstim=False, idx=None, confunits=None):
    for chore, res in ancls.my_chores:
        chor_seg = [ancls.uniq_combo[ancls.uniq_sims[chore[1],0]], ancls.uniq_feeds[ancls.uniq_sims[chore[1],1]]]
    if pres=='LFOplot':
        print(chor_seg)
        ax, arr = ancls.run_timser(ax, chore, 'STN_LFO_Amp', chor_seg, sqr=True, otherstim=otherstim, cumsum=True, confunits=confunits)
    elif pres=='NetSyn':
        ax, arr = ancls.run_timser(ax, chore, 'NetSyn', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits)
    elif res=='STN_pp':
        cell_spk = ancls.get_spk(chore, 'STN_spk', ancls.tmin_ana, ancls.tmax_ana)
        cell_pp, tbin = ancls.get_pp(cell_spk, tbin=ancls.tbin)
        fdsp_pp = ancls.run_pp(chore, 'STN_spk', chor_seg)
        ax = ancls.get_STNpp_ax(ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0) 
        if idx is not None:
            t0_idx = int((1500-ancls.tmin_ana)/tbin)
            t_idx = int(idx/tbin) 
            avgspkr = 1000*np.sum(fdsp_pp[:,:,2, t0_idx:t_idx], axis=-1)/idx 
        arr = avgspkr 
    return ax, arr


def get_spk_rate(pp, T, typ, ofset=10, tbin=5):
    tbin_of = int(ofset//5)
    # pp = avg
    nnz_idx = np.where(pp[tbin_of:]>0)[0][0]
    T_idx = int(T//tbin)
    if typ == 'R':
        rat = np.sum(pp[tbin_of+nnz_idx:T_idx])*1000/(tbin*(T_idx-nnz_idx)) if nnz_idx < T_idx else 0
    elif typ == 'E':
        rat = np.sum(pp[tbin_of+nnz_idx:tbin_of+nnz_idx+T_idx])*1000/T if nnz_idx < T_idx else 0
    return rat 

def plot_spec_cur(ancls, exp_path, gids, axr_arr, coord, timrng):

    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    i,j = coord
    axr_arr[i,j+3].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[i,j].imshow(STNspec, cmap='plasma', origin='bottom', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[i,j].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    return axr_arr

def get_resfreq(ancls, exp_path, gids, timrng):
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 
    ind = np.unravel_index(np.argmax(STNspec, axis=None), STNspec.shape)
    maxfreq = freq[ind[0]]
    return maxfreq
    


def get_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] = spcpow
    return spcmat

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))



if __name__ == '__main__':
#    plot_v()
#    TFpanel()
#    plot_triphasic()
#    spiking_rates()
#    reso_freq()
#    conflict()
    segctx()



#    get_spike_profile()    
