#! python3

import h5py
import matplotlib.gridspec as gs

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.gridspec as gs
import matplotlib.style as mpl_style
import matplotlib.image as mpimg
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])

def segctx():

    newfildum='/scratch/04119/pmoolcha/STNGPe/2020-11-28/TAC20201128_Spiking_SingPop_Evt_AMPANMDAHiInt-000.hdf5'
    newconf='/scratch/04119/pmoolcha/STNGPe/ConflictPlots/ConflictDat.hdf5'
    newsing='/scratch/04119/pmoolcha/STNGPe/ConflictPlots/SingConflictDat.hdf5'

    timrng=[1350, 1850]
    andum=Plots(newfildum, None, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {                                                                                                                                                                                    
                'fig_siz': (12,12),
              #  'fig_siz': (12,9),
                'glshap': (3,2),
                'hratio': [1.5, 4, 3],
                'wratio': [3,2],
                }
    ctx_lw=6

    fig, gl = andum.create_fig(figprops, constrained_layout=True)
    gl.tight_layout(fig)

    stimdurs = np.array([10, 20, 31, 50, 80])
    N_stimdurs = stimdurs.shape[0] 
    stimdelays = np.array([5, 10, 37])
    N_stimdelays = stimdelays.shape[0]

    seglab = [25, 55, 85]


    param_dict={}
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (5, 5)),# '--'
                            (0, (1, 5)), #':'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]
    param_dict['tbin'] = 5
    param_dict['ylimSTNpp'] = None 
    param_dict['GLM_Ana'] = ['LFOplot', 'NetSynplot', 'STN_pp'] 


    mainparam = {
            'param': {
                        'NMDAGLV':  {'Ctx_STN_A': [1.833e-05], }, 
                },
            'feed': {
                    'Conf': [('fds', ['RhythF22NE50_150']),] 
                },
            'cond': {
                    'RhythmicEvent' : ('NMDAGLV','Conf'),
                    },
        }             

    filtparcom = list([
                        {'Ctx_STN_A': [3.666e-06], },
                        {'Ctx_STN_A': [3.666e-06], },
                        {'Ctx_STN_A': [3.666e-05], },
                        ])

    int1, int2 = [(2,4)], [(2,4)] 

    def plot_conf():

        def set_axes():
            Confspec = gs.GridSpecFromSubplotSpec(1,5, subplot_spec=gl[0, :])
            global Confaxr_arr
            Confaxr_arr = np.empty(shape=Confspec.get_geometry(), dtype='O')

            for idx, _  in np.ndenumerate(Confaxr_arr):
                Confaxr_arr[idx] = fig.add_subplot(Confspec[idx])
                andum.style_axis(Confaxr_arr[idx], form=True)
    
            Confaxr_arr[0, 0].get_shared_y_axes().join(*Confaxr_arr[0, :2])
            Confaxr_arr[0, 3].get_shared_y_axes().join(*Confaxr_arr[0, 3:])
            Confaxr_arr[0,1].axes.get_yaxis().set_visible(False)
            Confaxr_arr[0,1].spines['left'].set_visible(False)
            Confaxr_arr[0,4].axes.get_yaxis().set_visible(False)
            Confaxr_arr[0,4].spines['left'].set_visible(False)

            for i in range(3):
                Confaxr_arr[0, 0].plot([],[], label='{:d}%'.format(seglab[i]), color=param_dict['colors'][i])
            Confaxr_arr[0, 0].legend(title='Seg', frameon=False, )

            Confaxr_arr[0, 0].set_title('No Conflict')
            Confaxr_arr[0, 1].set_title('Conflict')
            tit = Confaxr_arr[0, 2].set_title('Supralinear Theta\nConflict Detectors')
            Confaxr_arr[0, 3].set_title('AMPA')
            Confaxr_arr[0, 4].set_title('NMDA')

            Confaxr_arr[0, 0].set_ylabel('Avg. theta pow. [$\mu V^2$]')
     #       Confaxr_arr[0, 2].set_ylabel('Avg. theta pow.\nSTN categories [$\mu V^2$]')
            Confaxr_arr[0, 3].set_ylabel('Avg. $I_{syn}$ [$nA$]')

            Confaxr_arr[0, 2].set_xlabel('Time [$ms$]')

            global tit_prop
            tit_prop = tit.get_font_properties()
       #     get_avg_pos(Confaxr_arr[0, :2], 'tit', 'oioehrjbnkmn knoin bbjbi \n', tit_font)

            return Confaxr_arr
    
        def plot_data(Confaxr_arr):
            filtfds_conf = list([ 
                                [('fds', ['F1E10']), ('Seg', [(0.2,0.9)]), ('Int1', int1), ('Int2', int2),],
                                [('fds', ['F22NE10']), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]), ('Int1', int1), ('Int2', int2),],
                                [('fds', ['F22NE10']), ('Seg', [(0.2,0.3)]), ('Dlay', [(4,6)]), ('Int1', int1), ('Int2', int2),],
                                ],
                         ) 

            mainparam['feed']={'Conf': filtfds_conf[0]} 
            mainparam['param']={'NMDAGLV': filtparcom[0]} 
            param_dict['filt_dict'] = mainparam
            anobj=GLM_Analysis(newsing, param_dict, tmin=timrng[0], tmax=timrng[1])
            Confaxr_arr[0, 0], rel_mat = run_mulfil(anobj, 'LFOplot', Confaxr_arr[0, 0], otherstim=False, confunits=np.arange(3))
            anobj.filobj.close() 
    
            mainparam['feed']={'Conf': filtfds_conf[1]} 
            anobj=GLM_Analysis(newconf, param_dict, tmin=timrng[0], tmax=timrng[1])
            Confaxr_arr[0, 1], rel_mat  = run_mulfil(anobj, 'LFOplot', Confaxr_arr[0, 1], otherstim=False, confunits=np.arange(3))
            anobj.filobj.close() 
    
            mainparam['feed']={'Conf': filtfds_conf[2]} 
            anobj=GLM_Analysis(newconf, param_dict, tmin=timrng[0], tmax=timrng[1])
            Confaxr_arr[0, 2], _ = run_mulfil(anobj, 'LFOplot', Confaxr_arr[0, 2], otherstim=True)
            anobj.filobj.close() 
    
            curtimrng = [1450, 1475, 1575, 1750]
            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[1], tmax=curtimrng[2])
            Confaxr_arr[0, 3], _ = run_mulfil(anobj, 'AMPA', Confaxr_arr[0, 3], otherstim=True)
            anobj.filobj.close() 
    
            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[0], tmax=curtimrng[3])
            Confaxr_arr[0, 4], _ = run_mulfil(anobj, 'NMDA', Confaxr_arr[0, 4], otherstim=True)
            anobj.filobj.close() 

            Confaxr_arr[0, 3].get_legend().remove()
            Confaxr_arr[0, 4].get_legend().remove()

        set_axes()
        plot_data(Confaxr_arr)
#
    #    label_axes(fig, Confaxr_arr[0, :2], 'tit', 'All Cortically Stimulated Units\n', tit_prop, pad=None)
    #    label_axes(fig, Confaxr_arr[0, 3:], 'tit', 'Conflict Detectors\n', tit_prop, pad=None)

    #    label_subfig(fig, Confaxr_arr[0, 0], 'A\n', tit_prop)
    #    label_subfig(fig, Confaxr_arr[0, 2], 'B\n', tit_prop)
    #    label_subfig(fig, Confaxr_arr[0, 3], 'C\n', tit_prop)

    def plot_HH():

        def set_axes():
            HiHispec = gs.GridSpecFromSubplotSpec(N_stimdurs, N_stimdelays, subplot_spec=gl[1, 0]) 
            HiStspec = gs.GridSpecFromSubplotSpec(N_stimdurs, 1, subplot_spec=gl[1, 1]) 
    
            HiStaxr_arr = np.empty(shape=HiStspec.get_geometry(), dtype='O')
            for idx, _ in np.ndenumerate(HiStaxr_arr):
                HiStaxr_arr[idx] = fig.add_subplot(HiStspec[idx])
                andum.style_axis(HiStaxr_arr[idx], form=True)
    
            HiHiaxr_arr = np.empty(shape=HiHispec.get_geometry(), dtype='O')
            for idx, _ in np.ndenumerate(HiHiaxr_arr):
                HiHiaxr_arr[idx] = fig.add_subplot(HiHispec[idx])
                andum.style_axis(HiHiaxr_arr[idx], form=False)
                axy_ref = HiHiaxr_arr[idx[0], 0]
                axy_ref.get_shared_y_axes().join(axy_ref, HiHiaxr_arr[idx])
                axx_ref = HiHiaxr_arr[0, 0]
                axx_ref.get_shared_x_axes().join(axx_ref, HiHiaxr_arr[idx])
    
            for _, ax in np.ndenumerate(HiHiaxr_arr[:-1,:]):
               ax.axes.get_xaxis().set_visible(False)
               ax.spines['bottom'].set_visible(False)
            for _, ax in np.ndenumerate(HiHiaxr_arr[:,1:]):
               ax.axes.get_yaxis().set_visible(False)
               ax.spines['left'].set_visible(False)
    
            for _, ax in np.ndenumerate(HiStaxr_arr[:-1,:]):
               ax.axes.get_xaxis().set_visible(False)
               ax.spines['bottom'].set_visible(False)


            HiHiaxr_arr[2,0].set_ylabel('Average Theta Power [$\mu V^2$]')
            HiHiaxr_arr[-1,1].set_xlabel('Time [$ms$]')
            for delidx, delay in enumerate(stimdelays):
                HiHiaxr_arr[0, delidx].set_title('$\delta$ = {:2d} $ms$'.format(delay))

            return HiHiaxr_arr, HiStaxr_arr


        def plot_data(HiHiaxr_arr, HiStaxr_arr):
            cumsum_arr = np.zeros(shape=(N_stimdurs,N_stimdelays,3,4))

            deloffset = np.arange(N_stimdelays) * (len(seglab)+1)
            

            for stimidx, stim in enumerate(stimdurs):
                feedt = 'F22NE{!s}'.format(stim)

                filtfds_hihi= list([ 
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]),    ('Int1', int1), ('Int2', int2),],
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(9,11)]),   ('Int1', int1), ('Int2', int2),],
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(30, 40)]), ('Int1', int1), ('Int2', int2),],
                                    ],) 

                for j in range(N_stimdelays):
                    mainparam['feed']={'Conf': filtfds_hihi[j]} 
                    mainparam['param']={'NMDAGLV': filtparcom[0]} 
                    param_dict['filt_dict'] = mainparam

                    anobj=GLM_Analysis(newconf, param_dict, tmin=timrng[0], tmax=timrng[1])
                    HiHiaxr_arr[stimidx, j], cumsum_arr[stimidx,j] = run_mulfil(anobj, 'LFOplot', HiHiaxr_arr[stimidx, j], otherstim=False)
                    segcol = anobj.colors
                    anobj.filobj.close() 
              
                for seg in range(3):
                    prop_dict = {'color': segcol[seg]}
                    HiStaxr_arr[stimidx, 0].boxplot(cumsum_arr[stimidx,:,seg].T, vert=True, meanline=True, positions=deloffset+seg, 
                                                        boxprops=prop_dict, meanprops={'color':'k'}, medianprops=prop_dict, whiskerprops=prop_dict)


                HiStaxr_arr[stimidx, 0].set_xlim([-1,11])

            tick_pos = np.delete(np.arange(N_stimdelays * (len(seglab)+1)-1), deloffset-1)
            tick_lab = [seg for i in range(N_stimdelays) for seg in seglab]
         #   HiStaxr_arr[stimidx, 0].set_xticklabels([seglab[seg]]*3)
            HiStaxr_arr[-1, 0].set_xticks(tick_pos)
            HiStaxr_arr[-1, 0].set_xticklabels(tick_lab)
            HiStaxr_arr[-1, 0].set_xlabel('Segregation Level (\%)')

        HiHiaxr_arr, HiStaxr_arr = set_axes()
        plot_data(HiHiaxr_arr, HiStaxr_arr)

    def plot_detc():

        def set_axes():
            Detcspec = gs.GridSpecFromSubplotSpec(2,5, subplot_spec=gl[2, :], width_ratios=[2,2,1.5,1.5,1.5], wspace=0.05)
    
            Detcaxr_arr = np.empty(shape=Detcspec.get_geometry(), dtype='O')
            for idx, _ in np.ndenumerate(Detcaxr_arr):
                Detcaxr_arr[idx] = fig.add_subplot(Detcspec[idx])
                andum.style_axis(Detcaxr_arr[idx], form=True)
    
            for _, ax in np.ndenumerate(Detcaxr_arr[0,:2]): 
                ax.axes.get_xaxis().set_visible(False)
                ax.spines['bottom'].set_visible(False)

            for _, ax in np.ndenumerate(Detcaxr_arr[:,:2]):
                Detcaxr_arr[0, 0].get_shared_y_axes().join(Detcaxr_arr[0, 0], ax)
                andum.style_axis(ax, form=True)

            col_slc = np.array([1,3,4])
            for _, ax in np.ndenumerate(Detcaxr_arr[:,col_slc]):
                ax.axes.get_yaxis().set_visible(False)
                ax.spines['left'].set_visible(False)

            for _, ax in np.ndenumerate(Detcaxr_arr[:, 2:]):
                Detcaxr_arr[0, 2].get_shared_y_axes().join(Detcaxr_arr[0, 2], ax)

            for delidx, delay in enumerate(stimdelays):
                Detcaxr_arr[0, 2+delidx].set_title('$\delta$ = {:2d} $ms$'.format(delay))


            Detcaxr_arr[0,0].set_ylabel('Avg. $I_{syn}$ [$nA$]')
            Detcaxr_arr[0,2].set_ylabel('$q_{NMDA}:q_{AMPA}$ ratio')

            Detcaxr_arr[1,0].set_xlabel('Time [$ms$]')
            Detcaxr_arr[0,3].set_xlabel('Cortical Stim. Overlap [$ms$]')
            Detcaxr_arr[1,3].set_xlabel('Total stim. duration [$ms$]')

        #    mpl.artist.setp(Detcaxr_arr[:,0], ylabel='Avg. $I_{syn}$ [$nA$]')
        #    mpl.artist.setp(Detcaxr_arr[:,2], ylabel='$q_{NMDA}:q_{AMPA}$ ratio')
        #    mpl.artist.setp(Detcaxr_arr[-1,:2], xlabel='Time [$ms$]')
        #    mpl.artist.setp(Detcaxr_arr[2:,0], xlabel='Cortical Stim. Overlap [$ms$]')
        #    mpl.artist.setp(Detcaxr_arr[2:,1], xlabel='Total stim. duration [$ms$]')

            return Detcaxr_arr
    
    
        def plot_data(Detcaxr_arr):
            filtfds_detc = list([ 
                                [('fds', ['F22NE10']), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]), ('Int1', int1), ('Int2', int2),],
                                [('fds', ['F22NE10']), ('Seg', [(0.2,0.9)]), ('Dlay', [(9,11)]), ('Int1', int1), ('Int2', int2),],
                                ],
                         ) 
    
            mainparam['param']={'NMDAGLV': filtparcom[0]} 
            mainparam['feed']={'Conf': filtfds_detc[0]} 
            param_dict['filt_dict'] = mainparam



            curtimrng = [1450, 1475, 1600, 1750]
            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[1], tmax=curtimrng[2])
            Detcaxr_arr[0, 0], _ = run_mulfil(anobj, 'NMDA', Detcaxr_arr[0, 0])
            anobj.filobj.close() 
    
            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[1], tmax=curtimrng[2])
            Detcaxr_arr[0, 1], _ = run_mulfil(anobj, 'AMPA', Detcaxr_arr[0, 1])
            anobj.filobj.close() 

            mainparam['feed']={'Conf': filtfds_detc[1]} 
            param_dict['filt_dict'] = mainparam

            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[1], tmax=curtimrng[2])
            Detcaxr_arr[1, 0], _ = run_mulfil(anobj, 'NMDA', Detcaxr_arr[1, 0])
            anobj.filobj.close() 
    
            anobj=GLM_Analysis(newconf, param_dict, tmin=curtimrng[1], tmax=curtimrng[2])
            Detcaxr_arr[1, 1], _ = run_mulfil(anobj, 'AMPA', Detcaxr_arr[1, 1])
            anobj.filobj.close() 


            for axidx, ax in np.ndenumerate(Detcaxr_arr[:, :2]):
                ax.axvline(x=0, ls='--', color='k')
                ax.axvline(x=5, ls='--', color='k')
                ax.axvline(x=10, ls='--', color='k')

     #   plot_curs()

        def plot_curr_AUC(Detcaxr_arr):
            currAUC_arr = np.zeros(shape=(N_stimdurs,N_stimdelays,2,3))

            overlap_tot = np.empty(shape=(N_stimdurs,N_stimdelays, 2))

            for idx, dur in enumerate(stimdurs):
                for jdx, delay  in enumerate(stimdelays):
                    overlap_tot[idx,jdx,0] = max(0, dur-delay)   
                    overlap_tot[idx,jdx,1] = 2 * dur - overlap_tot[idx,jdx,0] 

            for stimidx, stim in enumerate(stimdurs):
                feedt = 'F22NE{!s}'.format(stim)

                filtfds_hihi= list([ 
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(4,6)]),    ('Int1', int1), ('Int2', int2),],
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(9,11)]),   ('Int1', int1), ('Int2', int2),],
                                    [('fds', [feedt]), ('Seg', [(0.2,0.9)]), ('Dlay', [(30, 40)]), ('Int1', int1), ('Int2', int2),],
                                    ],) 

                for j in range(N_stimdelays):
                    mainparam['feed']={'Conf': filtfds_hihi[j]} 
                    mainparam['param']={'NMDAGLV': filtparcom[0]} 
                    param_dict['filt_dict'] = mainparam

                    anobj=GLM_Analysis(newconf, param_dict, tmin=timrng[0], tmax=timrng[1])
                    _, resNMDA  = run_mulfil(anobj, 'NMDA', None, otherstim=False, skip_ax=True)
                    currAUC_arr[stimidx, j, 0] = np.mean(np.sum(resNMDA[:,:,2,4,:], axis=-1), axis=-1)*-0.025
                    _, resAMPA  = run_mulfil(anobj, 'AMPA', None, otherstim=False, skip_ax=True)
                    currAUC_arr[stimidx, j, 1] = np.mean(np.sum(resAMPA[:,:,2,4,:], axis=-1), axis=-1)*-0.025
                    segcol = anobj.colors
                    anobj.filobj.close() 

            for j in range(N_stimdelays):
#                lnstyl = param_dict['lnstyl'][j]
                for seg in range(3):
                    col = segcol[seg]
                    Detcaxr_arr[0, 2+j].plot(overlap_tot[:,j,0], currAUC_arr[:, j, 0, seg]/currAUC_arr[:, j, 1, seg], color=col, marker='x')
                    Detcaxr_arr[1, 2+j].plot(overlap_tot[:,j,1], currAUC_arr[:, j, 0, seg]/currAUC_arr[:, j, 1, seg], color=col, marker='x')

       # plot_curr_AUC()
        Detcaxr_arr = set_axes()        
        plot_data(Detcaxr_arr)
        plot_curr_AUC(Detcaxr_arr)

    plot_detc()
    plot_conf()
    plot_HH()
#    gl.tight_layout(fig)


    if False:
        label_axes(fig, Confaxr_arr[0, :2], 'tit', 'All Cortically Stimulated Units\n', tit_prop, pad=None)
        label_axes(fig, Confaxr_arr[0, 3:], 'tit', 'Conflict Detectors\n', tit_prop, pad=None)
        
        label_subfig(fig, Confaxr_arr[0, 0], 'A\n', tit_prop)
        label_subfig(fig, Confaxr_arr[0, 2], 'B\n', tit_prop)
        label_subfig(fig, Confaxr_arr[0, 3], 'C\n', tit_prop)

    fillab='MJF'
# trying no conf

#    mainparam['feed']={'Conf': [('fds', ['F22NE50']), ('Seg', [(0.2,0.3)]), ('Dlay', [(4,6)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)])],} 
#    param_dict['filt_dict'] = mainparam
#    mainparam['param']={'NMDAGLV': filtparcom[0]}

#DELAY 
 #   anobj=GLM_Analysis(filDel5, param_dict, tmin=timrng[0], tmax=timrng[1])
 #   axr_arr[0,2], _ = run_mulfil(anobj, 'LFOplot', axr_arr[0,2], otherstim=True)
 #   anobj.filobj.close() 


    if False:
        sprmn_coeff = np.full(shape=(5,4,2), fill_value=np.nan) 
        for i in range(5):
            for j in range(4):
                sprmn_coeff[i,j,:] = sts.spearmanr(cumsum_arr[i,:,j], [25,55,85])



#        print(cumsum_arr[0,:,:])
#        print(cumsum_arr[1,:,:])
        cumsum_arr[0,:,:] = cumsum_arr[0,:,:]/50
        cumsum_arr[1,:,:] = cumsum_arr[1,:,:]/55
        cumsum_arr[2,:,:] = cumsum_arr[2,:,:]/57.25
        cumsum_arr[4,:,:] = cumsum_arr[4,:,:]/60

        delarr = np.array([1,2,4])

        axr_arr[2,2].boxplot(cumsum_arr[delarr,0,:].T, showmeans=True, labels=[5, 7.25, 10])
#        axr_arr[2,2].boxplot(sprmn_coeff[delarr,:,0].T, labels=[5, 7.25, 10])

        labpos = axr_arr[2,2].get_xticks()
        axr_arr[2,2].plot([labpos[0], labpos[1]], [110,110], color='k')
        axr_arr[2,2].annotate('*', (0.5*(labpos[0]+labpos[1]), 111), xycoords='data', color='k')
        axr_arr[2,2].plot([labpos[0], labpos[2]], [120,120], color='k')
        axr_arr[2,2].annotate('*', (0.5*(labpos[0]+labpos[2]), 121), xycoords='data', color='k')

        t_seg = np.zeros(shape=(3,2,2)) 
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(cumsum_arr[0,i,:], cumsum_arr[0,j,:], equal_var=False) 
        print(t_seg[:,:,-1])
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(cumsum_arr[1,i,:], cumsum_arr[1,j,:], equal_var=False) 
        print(t_seg[:,:,-1])

   #     axr_arr[2,0].boxplot(cumsum_arr[0,:,:].T, showmeans=True, positions=[1,2,3,5,6,7], labels=[25,55,85, 25,55,85]) 
        axr_arr[2,0].boxplot(cumsum_arr[0,:,:].T, showmeans=True, labels=['25%','55%','85%']) 
        axr_arr[2,1].boxplot(cumsum_arr[1,:,:].T, showmeans=True, labels=['25%','55%','85%']) 

        labpos = axr_arr[2,1].get_xticks()
        axr_arr[2,1].plot([labpos[0], labpos[1]], [60,60], color='k')
        axr_arr[2,1].annotate('*', (0.5*(labpos[0]+labpos[1]), 61), xycoords='data', color='k')
        axr_arr[2,1].plot([labpos[0], labpos[2]], [40,40], color='k')
        axr_arr[2,1].annotate('*', (0.5*(labpos[0]+labpos[2]), 41), xycoords='data', color='k')

   #     axr_arr[0,0].set_ylabel('Avg. LFO pow. [$nA^2$]')
   #     axr_arr[1,0].set_ylabel('Avg. LFO pow. [$nA^2$]')
        axr_arr[0,0].set_ylabel('Avg. Theta pow. [$nA^2$]')
        axr_arr[1,0].set_ylabel('Avg. Theta pow. [$nA^2$]')
 #       axr_arr[1,0].set_label('Avg. $I_{syn}$ [$nA$]')
 #       axr_arr[2,0].set_label('Cumm. Avg. Spike Count')

        axr_arr[2,0].set_ylabel('Avg. Theta Pow. during Stim, [$nA^2$]')
        axr_arr[2,0].set_title('No Conflict')
        axr_arr[2,1].set_title('Conflict\n $\delta$ = 5 ms')
        axr_arr[2,0].set_xlabel('Segregation Level')
        axr_arr[2,1].set_xlabel('Segregation Level')
        
        axr_arr[2,2].set_xlabel('Delay [ms]')
        axr_arr[2,2].set_title('Conflict\nSeg = 25%')
 #       axr_arr[0,0].set_title('$\Delta$= 25 ms')
 #       axr_arr[0,1].set_title('$\Delta$= 150 ms')
 #       axr_arr[0,2].set_title('$\Delta$= 25 ms')
 #       axr_arr[0,3].set_title('$\Delta$= 150 ms')



        t_IEI = np.zeros(shape=(5,5,2)) 

        print(cumsum_arr.shape)
        for j,i in it.combinations(range(5), 2):
            t_IEI[i,j,:] = sts.ttest_ind(cumsum_arr[i,0,:], cumsum_arr[j,0,:], equal_var=False) 
        print(t_IEI[1,:,-1])
        print(t_IEI[2,:,-1])
        print(t_IEI[4,:,-1])



        for i in range(3):
            axr_arr[-2,i].set_xlabel('time [ms]')

 #       fig.text(0.25, 0.95, 'No Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
        br = param_dict['colors'][5]
        ypos1= np.array([3.7e1, 3.7e1])
        ypos2 = ypos1-0.15e1

#        axr_arr[0,0].plot([0,50], [6.2,6.2], color='k', lw=ctx_lw)
 #       axr_arr[0,0].plot([0,50], [15,15], color='k', lw=ctx_lw)

 #       axr_arr[0,1].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #       axr_arr[0,2].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #       axr_arr[1,0].plot([0,50], ypos1, color='k', lw=ctx_lw)
 #       axr_arr[1,2].plot([0,50], ypos1, color='k', lw=ctx_lw)

 #       axr_arr[1,1].plot([0,52.5], ypos1, color='k', lw=ctx_lw)

 #       axr_arr[0,1].plot([5,55], ypos2, color=br, lw=ctx_lw)
 #       axr_arr[0,2].plot([5,55], ypos2, color=br, lw=ctx_lw)
 #       axr_arr[1,0].plot([7.5,57.5], ypos2, color=br, lw=ctx_lw)
 #       axr_arr[1,1].plot([7.5,57.5], ypos2, color=br, lw=ctx_lw)
 #       axr_arr[1,2].plot([10,60], ypos2, color=br, lw=ctx_lw)


 #       axr_arr[0,0].set_title('No Conflict')
 #       axr_arr[0,1].set_title('Conflict\n$\delta$ = 5 ms, $\cap$ = 45 ms')
 #       axr_arr[0,2].set_title('Conflict\n$\delta$ = 5 ms')
 #       axr_arr[1,0].set_title('Conflict\n$\delta$ = 7.25 ms, $\cap$ = 42.75 ms')
 #       axr_arr[1,1].set_title('Conflict\n$\delta$ = 7.5 ms, $\cap$ = 45 ms')
 #       axr_arr[1,2].set_title('Conflict\n$\delta$ = 10 ms, $\cap$ = 40 ms')


 #       axr_arr[0,3].plot([0,50], ypos1, [200,250], ypos1, [400,450], ypos1, color='k', lw=ctx_lw)

 #       axr_arr[0,2].plot([5,55], ypos2, [80,130], ypos2, [155,205], ypos2, color=br, lw=ctx_lw)
 #       axr_arr[0,3].plot([5,55], ypos2, [205,255], ypos2, [405,455], ypos2, color=br, lw=ctx_lw)

 #       stim25 = [0, 75, 150]
 #       stim150 = [0, 200, 400]
 #       for i in [0,2]:
 #           for j in stim25:
 #               axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
 #       for i in [1,3]:
 #           for j in stim150:
 #               axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
 #               axr_arr[0,i].axvline(j+5, color=br, ls='--', lw=0.1, alpha=0.5)

        verpos = [1, 2/3, 1/3-1/32]
        horpos = [0, 1/3, 2/3] 

        seglev=[25, 55, 85]
        for i in range(3):
            axr_arr[1,0].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
  #      axr_arr[1,0].legend(title='Seg', fontsize='large', frameon=False)
  #      axr_arr[1,0].legend(title='Seg', frameon=False, fontsize='x-large')
        axr_arr[1,0].legend(title='Seg', frameon=False, )


#REM    OVE AXIS 
 #       for j,i in it.product(range(1,4), range(2)):
#        axr_arr[0,-1].set_axis_off()
#        for i in range(1,3):
#            axr_arr[-2,i].get_yaxis().set_visible(False)
#            axr_arr[-2,i].spines['left'].set_visible(False)
        for j in range(2):
            axr_arr[0,j].get_xaxis().set_visible(False)
            axr_arr[0,j].spines['bottom'].set_visible(False)



        alp_rep = np.reshape(alpbt[:axr_arr.size], axr_arr.shape)
        fig.tight_layout()
        for idx, ax in np.ndenumerate(axr_arr):
        #    ax.annotate('P{:d}{:d}'.format(idx[0], idx[1]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
        #    xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
        #    horizontalalignment='left', verticalalignment='top'
        #    )

            ax.annotate('{!s}'.format(alp_rep[idx]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
            xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
            horizontalalignment='left', verticalalignment='top'
            )


#    fig.tight_layout()
    filnam='testNEwSetnewctxsegpremod'+fillab
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

def run_mulfil(ancls, pres, ax, otherstim=False, idx=None, confunits=None, skip_ax=False):
    for chore, res in ancls.my_chores:
        chor_seg = [ancls.uniq_combo[ancls.uniq_sims[chore[1],0]], ancls.uniq_feeds[ancls.uniq_sims[chore[1],1]]]
    if pres=='LFOplot':
        print(chor_seg)
        ax, arr = ancls.run_timser(ax, chore, 'STN_LFO_Amp', chor_seg, sqr=True, otherstim=otherstim, cumsum=True, confunits=confunits, skip_ax=skip_ax)
    elif pres=='NetSyn':
        ax, arr = ancls.run_timser(ax, chore, 'NetSyn', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits, skip_ax=skip_ax)
    elif pres=='NMDA':
        ax, arr = ancls.run_timser(ax, chore, 'NMDA', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits, skip_ax=skip_ax)
    elif pres=='AMPA':
        ax, arr = ancls.run_timser(ax, chore, 'AMPA', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits, skip_ax=skip_ax)
    elif res=='STN_pp':
        cell_spk = ancls.get_spk(chore, 'STN_spk', ancls.tmin_ana, ancls.tmax_ana)
        cell_pp, tbin = ancls.get_pp(cell_spk, tbin=ancls.tbin)
        fdsp_pp = ancls.run_pp(chore, 'STN_spk', chor_seg)
        ax = ancls.get_STNpp_ax(ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0) 
        if idx is not None:
            t0_idx = int((1500-ancls.tmin_ana)/tbin)
            t_idx = int(idx/tbin) 
            avgspkr = 1000*np.sum(fdsp_pp[:,:,2, t0_idx:t_idx], axis=-1)/idx 
        arr = avgspkr 
    return ax, arr


def get_spk_rate(pp, T, typ, ofset=10, tbin=5):
    tbin_of = int(ofset//5)
    # pp = avg
    nnz_idx = np.where(pp[tbin_of:]>0)[0][0]
    T_idx = int(T//tbin)
    if typ == 'R':
        rat = np.sum(pp[tbin_of+nnz_idx:T_idx])*1000/(tbin*(T_idx-nnz_idx)) if nnz_idx < T_idx else 0
    elif typ == 'E':
        rat = np.sum(pp[tbin_of+nnz_idx:tbin_of+nnz_idx+T_idx])*1000/T if nnz_idx < T_idx else 0
    return rat 

def plot_spec_cur(ancls, exp_path, gids, axr_arr, coord, timrng):

    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    i,j = coord
    axr_arr[i,j+3].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[i,j].imshow(STNspec, cmap='plasma', origin='bottom', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[i,j].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    return axr_arr

def get_resfreq(ancls, exp_path, gids, timrng):
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 
    ind = np.unravel_index(np.argmax(STNspec, axis=None), STNspec.shape)
    maxfreq = freq[ind[0]]
    return maxfreq
    


def get_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] = spcpow
    return spcmat

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))


def label_axes(fig, axes, prop, text, font_prop, pad=None):

    fig_w, fig_h = fig.get_size_inches()


    N_axes = len(axes)
    ax_coords = np.empty(shape=(N_axes, 2, 2))
    for i, ax in enumerate(axes):
        ax_coords[i] = ax.get_position().get_points()

    xmin = min(ax_coords[:,0,0])
    xmax = max(ax_coords[:,1,0])
    xcen = (xmin + xmax)/2

    ymin = min(ax_coords[:,0,1])
    ymax = max(ax_coords[:,1,1])
    ycen = (ymin + ymax)/2


    if prop=='tit':
        pad = mpl.rcParams['axes.titlepad']-2.5 if pad is None else pad
        fig_pad_frac = pad/72 / fig_h 
        ypos = ymax + fig_pad_frac

        fig.text(x=xcen, y=ypos, s='{!s}'.format(text),
        horizontalalignment='center', verticalalignment='bottom',
        fontproperties=font_prop, transform=fig.transFigure)

    elif prop=='x':

        pad = 20.625 if pad is None else pad  

        fig_pad_frac = pad/72 / fig_h 
        ypos = ymin - fig_pad_frac

        fig.text(x=xcen+0.25, y=ypos, s='{!s}'.format(text), 
        horizontalalignment='center', verticalalignment='top',
        fontproperties=font_prop, transform=fig.transFigure)

    elif prop=='y':
        pad = 31.75 if pad is None else pad
        
        fig_pad_frac = pad/72 / fig_w
        xpos = xmin - fig_pad_frac
        
        fig.text(x=xpos, y=ycen, s='{!s}'.format(text),
        horizontalalignment='center', verticalalignment='center', rotation=90,
        fontproperties=font_prop, transform=fig.transFigure)
            

def label_subfig(fig, ax, text, font_prop, xpad=None, ypad=None, xcoord=None, ycoord=None):
    fig_w, fig_h = fig.get_size_inches()

    axcord = ax.get_position().get_points()
    x0 = axcord[0,0]
    y1 = axcord[1,1]

    ypad = mpl.rcParams['axes.titlepad']-2.5 if ypad is None else ypad
    fig_ypad_frac = ypad/72 / fig_h 

    xpad = 31.75 if xpad is None else xpad  
    fig_xpad_frac = xpad/72 / fig_w 

    new_prop = font_prop.copy()
    new_prop.set_weight('bold')

    xpos = x0-fig_xpad_frac if xcoord is None else xcoord
    ypos = y1+fig_ypad_frac if ycoord is None else ycoord

    fig.text(x=xpos, y=ypos, s='{!s}'.format(text), 
    horizontalalignment='right', verticalalignment='bottom', 
    fontproperties=new_prop, transform=fig.transFigure)

if __name__ == '__main__':
#    plot_v()
#    TFpanel()
#    plot_triphasic()
#    spiking_rates()
#    reso_freq()
#    conflict()

    condlist = [
                ([(2,4)], [(2,4)], 'HiHi'),
#                ([(2,4)], [(8,10)], 'HiLo'),
#                ([(8,10)], [(8,10)], 'LoLo'),
#                ([(8,10)], [(2,4)], 'LoHi'),
                ]

  #  for E in [10, 20, 31, 40, 50, 80, 125, 160, 200, 250, 500, 3150, 5030, 5031]:
#    for E in [50, 80, 125, 160, 200, 250, 500, 3150, 5031]:
  #      for cond in condlist:
  #          segctx('E{:d}{!s}'.format(E, cond[2]), 'F22NE{!s}'.format(E), cond[0], cond[1])

#    segctx('E{:d}{!s}'.format(E, cond[2]), 'F22NE{!s}'.format(E), cond[0], cond[1])
    segctx()


#    segctx()





#    get_spike_profile()    
