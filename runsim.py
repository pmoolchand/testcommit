#!/usr/bin/env python3
#
# runsim.py - main program to run simulations, optimized for HDF5 storage
#
# ver: 2.3 
# rev: 2019-02-06 - Explicit Feed attrs 

import sys, datetime, os, shutil, time
import importlib as implb
import itertools as it
from mpi4py import MPI
from neuron import h as nrn
nrn.load_file("stdrun.hoc")
from fn.params_default import get_params_default
import numpy as np
from class_net import Network
import gc
import h5py

def get_param_dict(paramfile):

    f = open(paramfile, 'r')
    fl = f.readlines()
    cart_prod_keys = []
    RecList = []
    RecDict = {'STNSta': [], 'STNSyn': [], 'GPPSta': [], 'GPPSyn': [], 'GPASta': [], 'GPASyn': [], 'STNClm': [], 'GPeClm': []}
    rec_dict = {}
    testdict = {}
    upfeeddict = {'sig': None, 'glcon': None}
    newdict = {line.split(':')[0].strip():line.split(':')[1][1:-1].strip() for line in fl if not (line.startswith('#') or line.startswith('\n'))}

    if 'qFeedx_up' in newdict.keys():
        upfeeddict['sig'] = newdict['qFeedx_up']
        newdict.pop('qFeedx_up')

    factor_keys = []

    for key, val in newdict.items():
        if (val.startswith('[') and val.endswith(']')):
            tmp = []
            for item in val[1:-1].split(','):
                tmp.append(validate_string(item))
            tmp = list(set(tmp))
            if len(tmp) > 1:
                cart_prod_keys.append(key)
                newdict[key] = np.sort(tmp)
            else:
                newdict[key] = tmp[0]
        elif val.startswith('*'):
            splt = val.split('*')
            factor_keys.append((key, validate_string(splt[1]), validate_string(splt[2])))
        else:
            newdict[key] = validate_string(val)

        if key.startswith('Rec') and newdict[key] is True:
            rec_dict.update({key[3:]:None})
            RecList.append(key[3:])

    factor_keys.sort()
    for fac in factor_keys:
        if fac[2] not in newdict.keys():
            print('Invalid taget key for multiplier')
            factor_keys.remove(fac)
        else:
            if not isinstance(fac[1], (int, float)):
                print('Invalid value as factor')
                factor_keys.remove(fac)

    for rec in RecList:
        RecDict[rec[0:6]].append(rec[6:])

    cart_prod_keys.sort()

    # Convert to list
    for key in ['ext_feed_file', 'net_file']:
        if key not in cart_prod_keys:
            newdict[key] = newdict[key].split()
        else:
            cart_prod_keys.remove(key)

    if '_V' in RecDict['STNSta']:
        newdict['RecSTNSta_V'] = True
        RecDict['STNSta'].remove('_V')

    if '_V' in RecDict['GPPSta']:
        newdict['RecGPPSta_V'] = True
        RecDict['GPPSta'].remove('_V')

    if '_V' in RecDict['GPASta']:
        newdict['RecGPASta_V'] = True
        RecDict['GPASta'].remove('_V')

    return newdict, cart_prod_keys, rec_dict, RecDict, factor_keys, upfeeddict

def validate_string(s):
    if s == 'True':
        return True
    elif s == 'False':
        return False
    else:
        try:
            return int(s)
        except ValueError:
            try:
                return float(s)
            except ValueError:
                return s.strip()

def get_dir_path(param_dict):
    # get simulation path

    datenow = datetime.datetime.now()
    datepath = os.path.join(param_dict['save_dir'], datenow.strftime("%Y-%m-%d"))
    sys_name = param_dict['sys_name']
    fil_pre = '{!s}{!s}_{!s}'.format(sys_name, datenow.strftime("%Y%m%d"), param_dict['sim_prefix'])

    # check for existing in datpath
    if os.path.isdir(datepath):
        dirlist = os.listdir(datepath)
        tmplist = [item for item in dirlist if item.startswith(fil_pre)]
        if tmplist:
            tmplist.sort()
            n = int((tmplist[-1].split('-')[1]).split('.')[0])
        else:
            n = -1
    else:
        n = -1

    dirpath = os.path.join(datepath, '{!s}-{:03d}'.format(fil_pre,n+1))

    return datepath, dirpath

def run_simulation(default_dict, network, ext_feed, RecDict, sim_file, sim_path):

    t0 = time.time()

    pc = nrn.ParallelContext()
    pc.barrier()
    pc.gid_clear()
    rank = int(pc.id())
    host_list = range(int(pc.nhost()))

    p = default_dict
    nrn.tstop = p['tstop']
    nrn.dt = p['dt']
    net = Network(p, network, ext_feed, RecDict)

    if rank == 0:
        # set t vec to record
        t_vec = nrn.Vector()
        t_vec.record(nrn._ref_t)

    # sets the default max solver step in ms (purposefully large)
    pc.set_maxstep(10)

    nrn.finitialize()
    nrn.fcurrent()
    nrn.frecord_init()

    # actual simulation
    pc.psolve(nrn.tstop)

    spktimes = np.array(net.spiketimes.to_python())
    spkgids = np.array(net.spikegids.to_python(), dtype=int)

    N = {}
    Ntot= {}
    N['STN'] = len(net.own_gid_dict['STNCell'])
    N['GPP'] = len(net.own_gid_dict['GPePCell'])
    N['GPA'] = len(net.own_gid_dict['GPeACell'])
    N['cells'] = N['STN'] + N['GPP'] + N['GPA']
    N['Ctx'] = len(net.own_gid_dict['CtxInput'])
    N['MSN'] = len(net.own_gid_dict['MSNInput'])
    Ntot['STN'] = network['STN']['N']
    Ntot['GPP'] = network['GPeP']['N']
    Ntot['GPA'] = network['GPeA']['N']
    Ntot['cells'] = Ntot['STN'] + Ntot['GPP'] + Ntot['GPA']
    Ntot['Ctx'] = len(net.gid_dict['CtxInput'])
    Ntot['MSN'] = len(net.gid_dict['MSNInput'])

    unique_gids = np.unique(spkgids)
    spkdict = {gid: [] for gid in unique_gids}

    cell_gid_spikes = unique_gids[unique_gids<Ntot['cells']]

    for gid, spktim in zip(spkgids, spktimes):
        spkdict[gid].append(spktim)

    for gid, spks in spkdict.items():
        trec_idx  = np.searchsorted(spks, p['trec_ini'])
        spkdict[gid] = spks[trec_idx:] 

    # share the spike data
    mpicom = MPI.COMM_WORLD

    if rank != 0:
        mpicom.send(spkdict, dest=0, tag=101)
        mpicom.send(cell_gid_spikes, dest=0, tag=102)

    if rank == 0:
        for src in host_list:
            if src != 0:
                spkdict.update(mpicom.recv(source=src, tag=101))
                cell_gid_spikes = np.append(cell_gid_spikes, mpicom.recv(source=src, tag=102))

    # create indices of time points to be written
    t_write = np.arange(p['trec_ini']/p['dt'], (p['tstop']/p['dt'])+p['dt'], p['trec']/p['dt'], dtype = int)

    N_col = t_write.size

    RecList = []
    RecValDict = {}
    StoreDict = {}

    # Create lists
    for key, val in RecDict.items():
        for var in val:
            RecList.append(key+var)
            RecValDict[key+var] = np.full((N[key[0:3]], N_col), np.NaN)

    if p['RecSTNSta_V']:
        RecList.append('STNSta_V')
        RecValDict['STNSta_V'] = np.full((N['STN'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['STNCell']):
            RecValDict['STNSta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    if p['RecGPPSta_V']:
        RecList.append('GPPSta_V')
        RecValDict['GPPSta_V'] = np.full((N['GPP'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['GPePCell']):
            RecValDict['GPPSta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    if p['RecGPASta_V']:
        RecList.append('GPASta_V')
        RecValDict['GPASta_V'] = np.full((N['GPA'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['GPeACell']):
            RecValDict['GPASta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    RecList.sort()
    RecListSTN = [var for var in RecList if var.startswith('STN')]
    RecListGPP = [var for var in RecList if var.startswith('GPP')]
    RecListGPA = [var for var in RecList if var.startswith('GPA')]

    for idx, gid_cell in enumerate(net.own_gid_obj['STNCell']):
        for state in RecDict['STNSta']:
            RecValDict['STNSta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['STNSyn']:
            RecValDict['STNSyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

   # for idx, gid_cell in enumerate(net.own_gid_obj['GPeCell']):
    for idx, gid_cell in enumerate(net.own_gid_obj['GPePCell']):
        for state in RecDict['GPPSta']:
            RecValDict['GPPSta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['GPPSyn']:
            RecValDict['GPPSyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

    for idx, gid_cell in enumerate(net.own_gid_obj['GPeACell']):
        for state in RecDict['GPASta']:
            RecValDict['GPASta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['GPASyn']:
            RecValDict['GPASyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

    writefile = h5py.File(sim_file, 'a', driver='mpio', comm=MPI.COMM_WORLD, libver='latest')
    #writefile.atomic = True
    writefile.create_group(sim_path)

    for dat in RecList:
        StoreDict[dat] = writefile.create_dataset(os.path.join(sim_path, 'raw', dat), (Ntot[dat[0:3]], N_col), dtype='f4')

    if net.own_gid_dict['STNCell']:
        STNcoord = np.array(net.own_gid_dict['STNCell'])
        for dat in RecListSTN:
            StoreDict[dat][STNcoord,:] = RecValDict[dat]

    if net.own_gid_dict['GPePCell']:
        GPePcoord = np.array(net.own_gid_dict['GPePCell']) - Ntot['STN']
        for dat in RecListGPP:
            StoreDict[dat][GPePcoord,:] = RecValDict[dat]

    if net.own_gid_dict['GPeACell']:
        GPeAcoord = np.array(net.own_gid_dict['GPeACell']) - Ntot['STN'] - Ntot['GPP']
        for dat in RecListGPA:
            StoreDict[dat][GPeAcoord,:] = RecValDict[dat]

    writefile.close()

    if rank == 0:

        writefile = h5py.File(sim_file, 'a', libver='latest')

        spkdt = h5py.special_dtype(vlen=np.dtype('f4'))
        giddt = h5py.special_dtype(vlen=np.dtype('u2'))
        spk = writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'raw', 'spikes')), (Ntot['cells'],), dtype=spkdt, fillvalue=None)

        for gid in cell_gid_spikes:
            spk[gid] = np.array(spkdict[gid])

        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'cells')), data = np.string_(net.network['cells']))
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'timesteps')), data = np.array(t_vec.to_python())[t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'con_mat')), data = net.network['connectivity_matrix'], dtype='u1')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_STN')), data = net.STN_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_GPeP')), data = net.GPeP_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_GPeA')), data = net.GPeA_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_STN')), data = net.STN_clamp_idx, dtype='u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_GPeP')), data = net.GPeP_clamp_idx, dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_GPeA')), data = net.GPeA_clamp_idx, dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_STN')), data = net.gid_dict['STNCell'], dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_GPeP')), data = net.gid_dict['GPePCell'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_GPeA')), data = net.gid_dict['GPeACell'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_Ctx')), data = net.own_gid_dict['CtxInput'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'feeds_Ctx')), data = net.Ctx_Feed_Combos, dtype = spkdt)
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'Ctx_asg_STN')), data = net.STN_Ctx_Combo_idx, dtype = 'u2')

        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'STN_subpop')), data = net.network['STN']['subpop_list'], dtype=giddt)

        tfin = time.time()
        writefile[sim_path]['props'].attrs['UTC_start'] = '{!s}'.format(time.asctime(time.gmtime(t0)))
        writefile[sim_path]['props'].attrs['UTC_end'] = '{!s}'.format(time.asctime(time.gmtime(tfin)))
        writefile[sim_path]['props'].attrs['LOC_start'] = '{!s}'.format(time.asctime(time.localtime(t0)))
        writefile[sim_path]['props'].attrs['LOC_end'] = '{!s}'.format(time.asctime(time.localtime(tfin)))
        writefile[sim_path]['props'].attrs['duration_s'] = '{:.3f}'.format(tfin-t0)

        writefile.close()

    pc.barrier()
    pc.gid_clear()
    del net
    del spkdict
    gc.collect()

    if rank == 0:
        print('Completion time: {:.3f} s'.format(time.time()-t0))

def exec_sim(paramfile, sys_det):

    # Set the Parallel Machinery
    mpicom = MPI.COMM_WORLD

    pc = nrn.ParallelContext()
    rank = int(pc.id())
    host_list = range(int(pc.nhost()))

    # Get parameter files
    param_dict, cart_prod_keys, rec_dict, RecDict, factor_keys, upfeeddict  = get_param_dict(paramfile)

    datepath, dirpath = get_dir_path(param_dict)

    if rank == 0:
        os.makedirs(datepath, exist_ok=True)

    # Get params default
    default_dict = get_params_default()
    if rank == 0:
        def_dict_list = [(key, val) for key, val in default_dict.items()]
        param_def_list = [(key, val) for key, val in  param_dict.items()]

    # Cross-check param file with params_default
    for key in param_dict.keys():
        if not key in default_dict.keys():
           print('{!s} not in params_default'.format(key))
           sys.exit(1)

    param_dict['upfeed'] = upfeeddict

    # Get params_default and set up Cartesian product of parameters
    cart_prod_vals = []
    for key in cart_prod_keys:
        cart_prod_vals.append(param_dict[key])

    # create param combo
    param_combo = []
    for p_idx, comb in enumerate(it.product(*cart_prod_vals)):
        comb_dict = {key:val for key, val in zip(cart_prod_keys, comb)}
        for fac in factor_keys:
            comb_dict[fac[0]] = fac[1] * comb_dict[fac[2]]
        param_combo.append((comb_dict, p_idx))

    # hierarchy stuff
    N_extfeed_trials = max(1, param_dict['N_extfeed_trials'])
    N_network_trials = max(1, param_dict['N_network_trials'])

    if rank == 0:
        extfeed_trial_seeds = np.random.randint(1e7, size=(N_extfeed_trials))
        network_trial_seeds = np.random.randint(1e7, size=(N_network_trials))
    else:
        extfeed_trial_seeds = None
        network_trial_seeds = None

    extfeed_trial_seeds = mpicom.bcast(extfeed_trial_seeds, root = 0)
    network_trial_seeds = mpicom.bcast(network_trial_seeds, root = 0)

    extfeed_list = []
    network_list = []

    extfeed_seed_list = []
    network_seed_list = []

    for idx, files in enumerate(param_dict['ext_feed_file']):
        if not os.path.isfile(os.path.join(os.getcwd(), 'extfeeds', files+'.py')):
            print('\nExternal feed file {!s} does not exist! Exiting gracefully...\n'.format(files))
            sys.exit(1)
        else:
            extfeeditem = implb.import_module('extfeeds.'+files).ext_feed
         #   extfeed_list.append((files, (extfeeditem, idx), flat_extfeed(extfeeditem)))
            extfeed_list.append((files, (extfeeditem, idx), None))
            file_seeds = [seed for seed in extfeeditem['Seed'][:N_extfeed_trials] if seed > 0 ]
            extfeed_seed_list.append(np.concatenate((file_seeds, extfeed_trial_seeds[len(file_seeds):])))

    for idx, files in enumerate(param_dict['net_file']):
        if not os.path.isfile(os.path.join(os.getcwd(), 'networks', files+'.py')):
            print('\nNetwork file {!s} does not exist! Exiting gracefully...\n'.format(files))
            sys.exit(1)
        else:
            networkitem = implb.import_module('networks.'+files).net_params
            network_list.append((files, (networkitem, idx), flat_network(networkitem)))
            file_seeds = [seed for seed in networkitem['Seed'][:N_network_trials] if seed > 0 ]
            network_seed_list.append(np.concatenate((file_seeds, network_trial_seeds[len(file_seeds):])))

    len_ext = len(extfeed_list)
    len_net = len(network_list)
    len_par = len(param_combo)

    # Create and close HDF5 file, sync
    sim_file = dirpath+'.hdf5'

    if rank == 0:
        # Write filename to text file to access via shell
        with open('{!s}_{!s}.txt'.format(sys_det[0], sys_det[3]), 'a') as f:
            f.write(sim_file)

    sim_fil = h5py.File(sim_file, 'w-', driver='mpio', comm=MPI.COMM_WORLD, libver='latest')

    sim_fil.attrs['N_Params'] = len_par 
    sim_fil.attrs['n_hosts'] = int(pc.nhost())
    sim_fil.attrs['UTC_start'] = '{!s}'.format(time.asctime(time.gmtime()))
    sim_fil.attrs['LOC_start'] = '{!s}'.format(time.asctime(time.localtime()))
    sim_fil.attrs['JOB_ID'] = sys_det[0]
    sim_fil.attrs['NODE_ID'] = sys_det[1]
    sim_fil.attrs['QOS'] = sys_det[2]
    sim_fil.attrs['CLUSTER'] = sys_det[3]

    sim_fil.close()

    len_sim = len_par * len_net * len_ext * N_extfeed_trials * N_network_trials
    sim_idx = 0

    if param_dict['fixed_hier'] == 'extfeed':
        for pidx, param in enumerate(param_combo):
            param_dict.update(param[0])
            default_dict.update(param_dict)
            for fidx, feed in enumerate(extfeed_list):
                for ftri in range(N_extfeed_trials):
                    feed[1][0].update({'Seed': int(extfeed_seed_list[fidx][ftri])})
                    for nidx, netw in enumerate(network_list):
                        for ntri in range(N_network_trials):
                            netw[1][0].update({'Seed': int(network_seed_list[nidx][ntri])})
                            sim_path = os.path.join('P{:03d}'.format(pidx), feed[0], 'F{:03d}'.format(ftri), netw[0], 'N{:03d}'.format(ntri))
                            sim_idx += 1
                            if rank == 0:
                                print('Sim: {:2d}/{:d}, ParameterSet: {:d}/{:d}, FeedSet: {:d}/{:d}, FeedTrial: {:d}/{:d}, NetworkSet: {:d}/{:d}, NetworkTrial: {:d}/{:d}'.\
                                format(sim_idx, len_sim, pidx+1, len_par, fidx+1, len_ext, ftri+1, N_extfeed_trials, nidx+1, len_net, ntri+1, N_network_trials ), end=' ')
                            pc.barrier()
                            run_simulation(default_dict, netw[1][0], feed[1][0], RecDict, sim_file, sim_path)
                            pc.barrier()

    else:
        for pidx, param in enumerate(param_combo):
            param_dict.update(param[0])
            default_dict.update(param_dict)
            for fidx, feed in enumerate(extfeed_list):
                for ftri in range(N_extfeed_trials):
                    feed[1][0].update({'Seed': int(extfeed_seed_list[fidx][ftri])})
                    for nidx, netw in enumerate(network_list):
                        for ntri in range(N_network_trials):
                            netw[1][0].update({'Seed': int(network_seed_list[nidx][ntri])})
                            sim_path = os.path.join('P{:03d}'.format(pidx), netw[0], 'N{:03d}'.format(ntri), feed[0], 'F{:03d}'.format(ftri))
                            sim_idx += 1
                            if rank == 0:
                                print('Sim: {:2d}/{:d}, ParameterSet: {:d}/{:d}, FeedSet: {:d}/{:d}, FeedTrial: {:d}/{:d}, NetworkSet: {:d}/{:d}, NetworkTrial: {:d}/{:d}'.\
                                format(sim_idx, len_sim, pidx+1, len_par, fidx+1, len_ext, ftri+1, N_extfeed_trials, nidx+1, len_net, ntri+1, N_network_trials ), end=' ')
                            pc.barrier()
                            run_simulation(default_dict, netw[1][0], feed[1][0], RecDict, sim_file, sim_path)
                            pc.barrier()


    if rank == 0:

        writefile = h5py.File(sim_file, 'a', libver='latest')
        write_attr(writefile, def_dict_list)
        write_attr(writefile, param_def_list)

        writefile.attrs['net_file'] = [np.string_(i) for i in param_dict['net_file']] 
        writefile.attrs['ext_feed_file'] = [np.string_(i) for i in param_dict['ext_feed_file']] 

        if param_dict['fixed_hier'] == 'extfeed':
            for param, feed, fidx, network, nidx in it.product(param_combo, extfeed_list, range(N_extfeed_trials), network_list, range(N_network_trials)):
                write_attr(writefile['P{:03d}'.format(param[1])], flat_param(param[0]))
              #  write_attr(writefile['P{:03d}'.format(param[1])][feed[0]], feed[2])
                write_attr(writefile['P{:03d}'.format(param[1])][feed[0]], flat_extfeed(feed[1][0]))
                writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)].attrs['Seed'] = int(extfeed_seed_list[feed[1][1]][fidx])
                write_attr(writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)][network[0]], network[2])
                writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)][network[0]]['N{:03d}'.format(nidx)].attrs['Seed'] = int(network_seed_list[network[1][1]][nidx])
        else:
            for param, network, nidx, feed, fidx in it.product(param_combo, network_list, range(N_network_trials), extfeed_list, range(N_extfeed_trials)):
                write_attr(writefile['P{:03d}'.format(param[1])], flat_param(param[0]))
                write_attr(writefile['P{:03d}'.format(param[1])][network[0]], network[2])
                writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)].attrs['Seed'] = int(network_seed_list[network[1][1]][nidx])
              #  write_attr(writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]], feed[2])
                write_attr(writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]], flat_extfeed(feed[1][0]))
                writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]]['F{:03d}'.format(fidx)].attrs['Seed'] = int(extfeed_seed_list[feed[1][1]][fidx])

        writefile.close()

    if pc.nhost > 1:
        pc.runworker()
        pc.done()
    nrn.quit()

def flat_extfeed(extfeed_dict):
    flat_list = []
    for key, val in extfeed_dict.items():
        if type(val) is dict:
            for kkey, vval in val.items():
                if type(vval) is dict:
                    for kkkey, vvval in vval.items():
                        flat_list.append(('{!s}_{!s}_{!s}'.format(key, kkey, kkkey), vvval))
                else:
                    flat_list.append(('{!s}_{!s}'.format(key, kkey), vval))
        else:
            flat_list.append((key, val))
    return flat_list

def flat_network(network_dict):
    flat_list = []
    for key, val in network_dict.items():
        if type(val) is dict:
            for kkey, vval in val.items():
                flat_list.append(('{!s}_{!s}'.format(key, kkey), vval))
        else:
            flat_list.append((key, val))
    return flat_list

def flat_param(param_dic):
    param_dic_list = []
    for key, val in param_dic.items():
        param_dic_list.append((key, val))
    return param_dic_list

def write_attr(obj, attlist):
    for key, val in attlist:
        try:
            obj.attrs[key] = val
        except:
            obj.attrs[key] = np.string_(val)

if __name__ == "__main__":
    # reads the specified param file

    if sys.argv[1].endswith('.param'):
        paramfile = sys.argv[1]

        if len(sys.argv) == 6:
            sys_det = (sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])  # JOB_ID, NODE_NAME, QOS, CLUSTER
        else:
            sys_det = ('NA', 'NA', 'NA', 'NA')

        exec_sim(paramfile, sys_det)

    else:
        print('Usage: {!s} param_input incorrect'.format(sys.argv[0]))
        sys.exit(1)
