#! python3 

from mpi4py import MPI 
import h5py, sys, time, datetime

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Serif}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl

# Choose Backend, Create Canvas
mpl.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

#mpl.use('pgf')
#from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)

import matplotlib.figure as mpl_figure
#import matplotlib.gridspec as gs
import matplotlib.ticker as ticker
#import matplotlib.text as text
#import matplotlib.markers as markers
#import matplotlib.lines as lines
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import numpy as np 
import scipy.stats as sts
import itertools as it
from functools import reduce
#from AnalysisClass import MultiAnalysis, VarEffects

#import statsmodels.api as sm                                                                                                                                                                          
import statsmodels.genmod.generalized_linear_model as GLM

sys_name = 'ANATAC'

fed_spc = np.dtype('U16, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
fed_alg = np.dtype([('shft', np.bool), ('t0', np.float, 2), ('tend', np.float, 2), ('Dur', np.float, 2), ('Int', np.int, 2), ('TPart', np.float, 3), ('TgtSub', np.int, 2), ('AlgSub', np.int, 4)])
fed_unq = np.dtype([('fd', fed_spc), ('algn', fed_alg), ('fils', np.ndarray)])

cond_arr = np.dtype([('cond', 'U16'), ('idx', np.ndarray)]) 
task_arr = np.dtype([('Cond_idx', np.uint32), ('Reg_idx', np.uint32), ('Result', 'U16'), ('avg', np.bool), ('Cell_ass', 'U16'), ('zscor', np.bool)])



class GLM_Analysis():
    def __init__(self, fil, param_dict, intval=100):

        # Set parallel machinery
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.N_hosts = MPI.COMM_WORLD.Get_size()
        self.intval = intval
        self.fil = fil

        for k,v in param_dict.items():
            setattr(self, k, v)

        self.load_data()
        self.filter_feeds()
        self.get_filtered_param()
        self.get_cond_sim()
        self.get_multichores()

        for chore in self.my_chores:
            self.run_chore(chore)

    def run_chore(self, chore):
        regr = self.rgrs_mat[chore['Cond_idx'], chore['Reg_idx']]
        cond = self.cond_arr[chore['Cond_idx']]
        res = self.get_resdata(chore['Result'], cond['idx'], chore['Cell_ass'], chore['avg']) 
        GLMres = self.get_GLM_res(regr, res, False)
  #      self.plot_GLM_res(self.GLM_Ana[chore['Reg_idx']][0], GLMres)
        self.plot_GLM_res(chore, GLMres)

  #  def plot_GLM_res(self, reglist, GLM_res, fdsp='SubPop'):
    def plot_GLM_res(self, chore, GLM_res):

        reglist = self.GLM_Ana[chore['Reg_idx']][0]
        fdsp = chore['Cell_ass']


        ylab_dict = {
                'Fdcom': ['$1^{st} Stim$', '$2^{nd} Stim$', '$Both Stims$', '$No Stim$'],
                'SubPop': ['$1^{st} SubPop$', '$2^{nd} SubPop$', '$3^{rd} SubPop$', '$4^{th} SubPop$', '$All Pop$'],
                }

        reglab = self.rgrs_lab[chore['Reg_idx']]
        ylab = ylab_dict[fdsp]

        N_regrs = len(reglist) 
        N_rows = GLM_res.shape[1]

        titlab = [chore['Result'], fdsp, self.cond_arr[chore['Cond_idx']]['cond'], 'Avg' if chore['avg'] else 'NoAvg', 'Reg{:02d}'.format(N_regrs)]
        figtit = ' '.join(titlab)

        rel_rows = np.array([i for i in range(N_rows) if GLM_res[0,i] is not None])
        fighght = N_rows+1
        fig = mpl_figure.Figure(figsize=(1.5*N_regrs, fighght))
        canvas = FigureCanvas(fig)
     #   h_ratios = np.concatenate(([0.5], [1.5 for i in range(N_rows)]))
     #   tit = fig.add_gridspec(1, N_regrs, bottom=1.5*N_rows/fighght)
     #   gl = fig.add_gridspec(N_rows, N_regrs, top=1.5*N_rows/fighght)
        gl = fig.add_gridspec(N_rows, N_regrs)
        ax_arr = np.empty(shape=(N_rows, N_regrs), dtype='O')
        ax_arr = self.axis_create_align(ax_arr,gl, fig)
        for i in rel_rows: 
            res0 = np.array([GLM_res[0,i].params, GLM_res[0,i].bse])
            res1 = np.array([GLM_res[1,i].params, GLM_res[1,i].bse])
            for j in range(1, N_regrs+1):
                ax_arr[i,j-1].bar([0,1],[res0[0,j], res1[0,j]], yerr=[res0[1,j], res1[1,j]])
        for i in range(N_regrs):
            ax_arr[0,i].set_title(reglab[i])

        for idx, rG in np.ndenumerate(GLM_res):
            if rG is not None:
                sigv = rG.pvalues[1:]
                for j in range(N_regrs): 
                    if sigv[j] >= 0.05:
                        self.rem_fill_nsg(ax_arr[idx[1],j], idx[0])

        for i in range(N_rows):
            ax_arr[i,0].set_ylabel(ylab[i])

      #  tit.tight_layout(fig)
      #  gl.tight_layout(fig)
        fig.suptitle(figtit)
        fig.tight_layout()
        figtim = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        figsvtit = '{!s}_{!s}'.format('_'.join(titlab), figtim)

        svformat = self.saveformat 
        fig.savefig('{!s}_{!s}.{!s}'.format(self.prefix, figsvtit, svformat))


    def get_GLM_res(self, regressors, var, zscor=False):
        if zscor:
            regressors = sts.zscore(regressors, axis=0)
        regressors = np.hstack((np.ones(shape=(regressors.shape[0],1)), regressors)) 
        var_shap = var.shape[1:]
        GLM_res = np.empty(shape=var_shap, dtype='O')
        for i, j in it.product(range(var_shap[0]), range(var_shap[1])):
            res = var[:,i,j]
            if not np.isnan(res).all():
                GLM_obj =  GLM.GLM(res, regressors)
                GLM_res[i,j] = GLM_obj.fit()
        return GLM_res 


    def get_resdata(self, res_data, sims, spop, avg=False):
        dat_dict = {
                    'Sil': ('Silence', True),
                    'Bur': ('Burst', True),
                    'Fir': ('Firing', True),
                    'AUC_LFO': ('PowerLFO', False),
                    }
        att = dat_dict[res_data]
        data = getattr(self, att[0])[sims] 
        compute = att[1]
        N_tri = 1 if avg else self.N_trials

        sbpop_dat = self.fdsb[sims]
        walkidx = [idx for idx, _ in np.ndenumerate(sbpop_dat[:,:,0,0])]


        if compute:
            if spop == 'Fdcom':
                N_cell_idx = self.fdcom
                cell_idx_mat = np.empty(sbpop_dat.shape[:2]+(N_cell_idx,), dtype=np.ndarray)
                for idx in walkidx: 
                    sbidx = sbpop_dat[idx]    
                    for m in range(N_cell_idx):
                        cell_idx_mat[idx][m] = reduce(np.union1d, sbidx[m,:])

            elif spop == 'SubPop':
                N_cell_idx = self.N_subpop
                cell_idx_mat = np.empty(sbpop_dat.shape[:2]+(N_cell_idx,), dtype=np.ndarray)
                for idx in walkidx: 
                    sbidx = sbpop_dat[idx]    
                    for m in range(N_cell_idx):
                        cell_idx_mat[idx][m] = reduce(np.union1d, sbidx[:,m])

            result_mat = np.empty(shape=data.shape[:3]+(N_cell_idx,), dtype=np.float32)
            for cmidx, cim in np.ndenumerate(cell_idx_mat):
                result_mat[cmidx[:2]][:, cmidx[2]] = np.mean(data[cmidx[:-1]][:, cim], axis=-1)
        elif res_data == 'AUC_LFO':
            sbpop_dat_N = self.fdsb_N[sims]
            for idx in walkidx:
                data[idx] = np.sqrt(data[idx]) * sbpop_dat_N[idx]
            if spop == 'Fdcom':
                N_cell_idx = self.fdcom
                result_mat = np.sum(data, axis=-1)
            elif spop == 'SubPop':
                N_cell_idx = self.N_subpop
                result_mat = np.sum(data, axis=-2)
            result_mat = (result_mat/self.N_STN)**2
        result = np.mean(result_mat, axis=1) if avg else result_mat
        result = result.reshape(sims.shape[0]*N_tri,2,N_cell_idx)  
        return result 

    def get_multichores(self):
        avg_dat_dict =  {
                        'Ind': self.fdcom * self.N_subpop,
                        'SubPop': self.N_subpop,
                        'Fdcom': self.fdcom,
                        'AllPop': 1,
                        }
        N_cond = self.N_cond      
        N_rgrs = len(self.GLM_Ana)

        rgrs_lab = np.empty(shape=N_rgrs, dtype=np.ndarray)

        rgrs_mat = np.empty(shape=(self.N_cond, N_rgrs), dtype=np.ndarray)
        for jidx, j in enumerate(self.cond_arr):
            for rgidx, rg in enumerate(self.GLM_Ana):
                rgrs_mat[jidx, rgidx], rgrs_lab[rgidx]  = self.get_regressors(rg[0], j, rg[2])

        N_res = np.empty((N_rgrs,2), dtype=np.int)
        for idx, i in enumerate(self.GLM_Ana):
            N_res[idx,0] = len(i[1])
            #num = 0
            #for k in i[3]:
            #    num += avg_dat_dict[k] 
            #N_res[idx,1] = num
            N_res[idx,1] = len(i[3]) 
        N_chores = np.sum(np.product(N_res, axis=1)) * N_cond

        chores = np.empty(shape=N_chores, dtype=task_arr)
        c_idx = 0
        for cnrgidx, cdrg in np.ndenumerate(rgrs_mat):
            for ij in it.product(range(N_res[cnrgidx[1], 0]), range(N_res[cnrgidx[1], 1])):
             #  chores[c_idx] = cnrgidx[0], cnrgidx[1], ij[0], self.GLM_Ana[cnrgidx[1]][2], self.GLM_Ana[cnrgidx[1]][3][ij[1]], self.GLM_Ana[cnrgidx[1]][2] 
               chores[c_idx] = cnrgidx[0], cnrgidx[1], self.GLM_Ana[cnrgidx[1]][1][ij[0]], self.GLM_Ana[cnrgidx[1]][2], self.GLM_Ana[cnrgidx[1]][3][ij[1]], self.GLM_Ana[cnrgidx[1]][4] 
               c_idx += 1 

        self.rgrs_lab = rgrs_lab
        self.rgrs_mat = rgrs_mat 
        self.my_chores_idx = np.arange(self.rank, N_chores, self.N_hosts) 
        self.N_mychores =  self.my_chores_idx.shape[0]
        self.my_chores = chores[self.my_chores_idx]
        

    def load_data(self):
        self.filobj = h5py.File(self.fil, 'r+', driver='mpio', comm=MPI.COMM_WORLD)

        self.datshape = self.filobj['NetSyn'].shape
        self.N_trials = self.datshape[1]
        self.fdcom = self.datshape[2]
        self.N_subpop = self.datshape[3]
        self.N_timpts = self.datshape[-1]

        self.uniq_combo = np.array(self.filobj['uniq_combo']) 
        self.N_params = self.uniq_combo.shape[0]
        tmp = np.array(self.filobj['uniq_feeds'])
        self.feed_arr = np.array(tmp, dtype=fed_unq)
#        self.feed_arr['fd']['f0'] = np.array(self.feed_arr['fd']['f0'], dtype='U')
        self.N_feeds = self.feed_arr.shape[0]
        self.uniq_sims = np.array(self.filobj['uniq_sims'])

        self.fdsb = np.array(self.filobj['STN_fdsp'])
        self.fdsb_N = np.array(self.filobj['STN_fdsp_N'])
        self.Silence = np.array(self.filobj['STN_Sil'])
        self.Burst = np.array(self.filobj['STN_Bur'])
        self.Firing = np.array(self.filobj['STN_Fir'])
        self.PowerLFO = np.array(self.filobj['STN_AUC_LFO'])
        self.N_STN = self.Silence.shape[-1]

        self.filobj.close()

    def get_regressors(self, reglist, i, avg=False): 
        # Load regressors
        reg_dict = {
                    'Seg': "self.feed_arr['fd']['f8']",
                    'Del': "self.feed_arr['algn']['TPart'][:,0]",
                    'Ovlp': "np.clip(self.feed_arr['algn']['TPart'][:,1], a_min=0, a_max=None)",
                    'Dur0': "self.feed_arr['algn']['Dur'][:,0]",
                    'Dur1': "self.feed_arr['algn']['Dur'][:,1]",
                    'Int0': "self.feed_arr['algn']['Int'][:,0]",
                    'Int1': "self.feed_arr['algn']['Int'][:,1]",
                    }

        lab_dict = {
                    'Ctx_STN_N': '$NMDA_{Ctx}$',    
                    'GPe_STN': '$GABA_{GPeP}$',    
                    'Del': '$Delay$',    
                    'Ovlp': '$Overlap$',    
                    'Seg': '$Seg$',
                    'Dur0': '$Dur_0$',
                    'Dur1': '$Dur_1$',
                    'Int0': '$Int_0$',
                    'Int1': '$Int_1$',
                    }    

        reglist_acc = dict([('F', []),('P', []), ('I', [])])

        for reg in reglist:
            reglist_acc[reg[0]].append(reg[1])

        N_P = len(reglist_acc['P'])
        N_F = len(reglist_acc['F'])
        N_I = len(reglist_acc['I'])
        UPF = np.concatenate((reglist_acc['P'], reglist_acc['F'])) 

        I_ind = []
        I_ind_lab = []
        for intrc in reglist_acc['I']:
            tmp = set(intrc.split('$'))
            tmp_add = []
            tmp_lab = []
            for vr in tmp:
                vridx = np.where(UPF==vr)[0]
                if vridx.size:
                    tmp_add.append(vridx[0])
                    tmp_lab.append(lab_dict[vr][1:-1])                    
                else:
                    print('Incompatible Interaction Variables')
                    exit()
            I_ind.append(tuple(tmp_add))
            I_ind_lab.append('${!s}$'.format('x'.join(tmp_lab)))

        UPF_lab = [lab_dict[lb] for lb in UPF]
        reg_lab = np.concatenate((UPF_lab, I_ind_lab))
        

        N_PF = N_P + N_F
        N_regressors = N_PF + N_I 
        N_tri = 1 if avg else self.N_trials
        #reg_mat = np.empty(shape=self.N_cond, dtype=cond_arr) 
        #regressors = np.empty(shape=self.N_cond, dtype=cond_arr) 
        #
        #for jdx, i in enumerate(self.cond_arr): 
        #    mat_siz = i['idx'].shape[0] 
        #    reg_mat[jdx] = i['cond'], np.empty(shape=(mat_siz, N_tri, N_regressors)) 

        #    for pidx, param in enumerate(reglist_acc['P']):
        #        reg_mat[jdx]['idx'][:,0,pidx] = self.uniq_combo[param][self.uniq_sims[i['idx'],0]]

        #    for fidx, fd in enumerate(reglist_acc['F']):
        #        reg_mat[jdx]['idx'][:,0,fidx+N_P] =  eval(reg_dict[fd])[self.uniq_sims[i['idx'],1]]

        #    for m in range(1, N_tri):
        #        reg_mat[jdx]['idx'][:,m,:] = reg_mat[jdx]['idx'][:,0,:]

        #    regressors[jdx] = i['cond'], reg_mat[jdx]['idx'].reshape(mat_siz*N_tri, N_regressors) 
        
        mat_siz = i['idx'].shape[0] 
        reg_mat = np.empty(shape=(mat_siz, N_tri, N_regressors)) 

        for pidx, param in enumerate(reglist_acc['P']):
            reg_mat[:,0,pidx] = self.uniq_combo[param][self.uniq_sims[i['idx'],0]]

        for fidx, fd in enumerate(reglist_acc['F']):
            reg_mat[:,0,fidx+N_P] =  eval(reg_dict[fd])[self.uniq_sims[i['idx'],1]]
            if fd in ['Int0', 'Int1']:
                reg_mat[:,0,fidx+N_P] = reg_mat[:,0,fidx+N_P]**-1

        for intidx, intrc in enumerate(I_ind):
            tmpmat = np.ones(shape=mat_siz)
            for mv in range(len(intrc)):
                tmpmat *= reg_mat[:,0,intrc[mv]]
            reg_mat[:,0,intidx+N_PF] = tmpmat

        for m in range(1, N_tri):
            reg_mat[:,m,:] = reg_mat[:,0,:]

        regressors = reg_mat.reshape(mat_siz*N_tri, N_regressors) 
        return regressors, reg_lab
    
    def get_cond_sim(self):
        self.N_cond = len(self.filt_dict['cond'])
        self.N_condarr = max(1, self.N_cond)
        self.cond_arr = np.empty(shape=self.N_condarr, dtype=cond_arr)
        all_sims = np.arange(self.uniq_sims.shape[0])
        if self.N_cond:
            cond_idx = 0
          #  for k,v in sorted(self.filt_dict['cond']).items():
            for k in sorted(self.filt_dict['cond']):
                v = self.filt_dict['cond'][k]
                prm, fd = v
                if prm == 'All':
                    uniq_prm = all_sims 
                else:
                    prm_idx = np.where(self.param_idx['cond']==prm)[0] 
                    prm_arr = self.param_idx['idx'][prm_idx][0]
                    uniq_prm = []
                    for i in prm_arr:
                        uniq_prm.append(np.where(self.uniq_sims[:,0]==i)[0])
                    uniq_prm = np.concatenate(uniq_prm)
                if fd == 'All':
                    uniq_fd = all_sims 
                else:
                    fd_idx = np.where(self.feed_idx['cond']==fd)[0] 
                    fd_arr = self.feed_idx['idx'][fd_idx][0]
                    uniq_fd = []
                    for i in fd_arr:
                        uniq_fd.append(np.where(self.uniq_sims[:,1]==i)[0])
                    uniq_fd = np.concatenate(uniq_fd)
                self.cond_arr[cond_idx] = k , np.intersect1d(uniq_prm, uniq_fd, assume_unique=True)
                cond_idx += 1
        else:
            self.cond_arr[0] = 'All', all_sims 

    def filter_feeds(self):
        track_idx = {}
        def_idx = np.arange(self.N_feeds)
        for k in self.filt_dict['feed'].keys():
            track_idx[k] = def_idx 
            for cons, v in self.filt_dict['feed'][k]:
                arr = self.get_field(cons)
                keep_idx = []
                if type(v[0]) is not tuple:
                    for val in v:
                        tmp_idx = np.where(arr[track_idx[k]]==val)[0]
                        keep_idx.append(track_idx[k][tmp_idx])
                        track_idx[k] = track_idx[k][np.delete(np.arange(track_idx[k].shape[0]), tmp_idx)] 
                    track_idx[k] = reduce(np.union1d, keep_idx)
                else:
                    for val in v:
                        tmp_idx = np.where((arr[track_idx[k]]>=val[0]) & (arr[track_idx[k]]<=val[1]))[0]
                        track_idx[k] = track_idx[k][np.intersect1d(np.arange(track_idx[k].shape[0]), tmp_idx, assume_unique=True)] 
    
        track_idx['All'] = def_idx
        self.feed_idx = np.empty(shape=len(track_idx), dtype=cond_arr) 
        idx = 0
        for k,v in track_idx.items():
            self.feed_idx[idx] = k, v 
            idx += 1
#        print(self.feed_idx)

    def get_filtered_param(self):
        track_idx = {}
        def_idx = np.arange(self.N_params)
        for k in self.filt_dict['param'].keys():
          #  track_idx[k] = def_idx 
            track = {}
            for j,v in self.filt_dict['param'][k].items():
                track[j] = def_idx
                arr = self.uniq_combo[j]
                keep_idx = []
                for val in v:
                    tmp_idx = np.where(arr[track[j]]==val)[0]
                    keep_idx.append(track[j][tmp_idx])
                    track[j] = track[j][np.delete(np.arange(track[j].shape[0]), tmp_idx)] 
                track[j] = reduce(np.union1d, keep_idx) 
            track_idx[k] = reduce(np.intersect1d, [*track.values()]) if len(self.filt_dict['param']) else def_idx  
        track_idx['All'] = def_idx
    
        self.param_idx = np.empty(shape=len(track_idx), dtype=cond_arr) 
        idx = 0
        for k,v in track_idx.items():
            self.param_idx[idx] = k, v 
            idx += 1
      #  print(self.param_idx)
      #  filt_idx = reduce(np.intersect1d, [*track_idx.values()]) if len(self.filt_dict) else def_idx 
      #  self.filt_param_arr = self.uniq_param_combo[filt_idx]

    def get_field(self, fld):
        fld_dict = {
                    'fds': self.feed_arr['fd']['f0'],
                    'Seg': self.feed_arr['fd']['f8'],
                    'Int1': self.feed_arr['algn']['Int'][:,0],
                    'Int2': self.feed_arr['algn']['Int'][:,1],
                    'Dur1': self.feed_arr['algn']['Dur'][:,0],
                    'Dur2': self.feed_arr['algn']['Dur'][:,1],
                    'Ovlp': self.feed_arr['algn']['TPart'][:,0],
                    'Dlay': self.feed_arr['algn']['TPart'][:,1],
                    'PstO': self.feed_arr['algn']['TPart'][:,2],
                    }
        return fld_dict[fld]


        
        self.N_regressors = self.uniq






        self.reg_list = [(0, 'NMDA'), (1, 'GABA'), (2, 'SEG'), (3, 'PHASE'), (4, 'TMPRD'), (5, 'INT1stCTX'), (6, 'INT2ndCTX'), (7, 'NMDAxGABA'), (8, 'NMDAxSEG'), (9, 'NMDAxPHASE'),
                            (10, 'NMDAxINT1stCTX'), (11, 'NMDAxINT2ndCTX')] 

        self.pick_reg = np.arange(6)

        self.N_regres = self.pick_reg.shape[0] 
        # (NMDA 0, GABA 1, NMDAxGABA 2, Seg 3, phase 4, freq 5, stim1int 6, stim2int 7, NMDAxSeg 8, NMDAxphase 9, NMDAxstim1 10, NMDAxstim2 11)  
        # (NMDA 0, GABA 1, NMDAxGABA 2, Seg 3, phase 4, freq 5, stim1int 6, stim2int 7, NMDAxSeg 8, NMDAxphase 9, NMDAxstim1 10, NMDAxstim2 11)  
        self.regressors = np.empty(shape=(self.N_sims, self.N_regres))

        
        self.reg_tit = [self.reg_list[i][1] for i in self.pick_reg]

     #   self.N_res = 6
        self.N_res = 5
        # (ISI0, Bur, Fir, Theta28, Theta48, Beta1230, Beta1532)
        self.observ = np.empty(shape=(self.N_sims, 5, 2, self.N_res))

        self.my_chores = np.arange(self.rank, self.N_sims, self.N_hosts)


        for cidx in self.my_chores: 
            self.regressors[cidx] = self.get_sim_att(cidx) 
            self.get_ISI0_Bur(cidx)
#            self.get_theta_pow(cidx)

        self.zscor_reg = self.regressors
#        self.zscor_reg = sts.zscore(self.regressors, axis=1) 

        self.reg_wconst = np.hstack((np.ones(shape=(self.N_sims,1)), self.zscor_reg))
        self.reg_wconst_lst = MPI.COMM_WORLD.gather(self.reg_wconst, root=0)        

        self.observ_lst = MPI.COMM_WORLD.gather(self.observ, root=0)        

        if self.rank == 0:
            self.align_rank(self.reg_wconst, self.reg_wconst_lst)
            self.align_rank(self.observ, self.observ_lst)

        self.reg_wconst = MPI.COMM_WORLD.bcast(self.reg_wconst, root=0)
        self.observ = MPI.COMM_WORLD.bcast(self.observ, root=0)

        MPI.COMM_WORLD.barrier()

        self.GLMs = []

        for ijk, _ in np.ndenumerate(self.observ[0,:,:,:]):
            self.GLMs.append(ijk)

        self.GLMs = np.array(self.GLMs, dtype=tuple)

        self.my_GLM = self.GLMs[self.rank:len(self.GLMs):self.N_hosts] 

        self.fam_links = [
                       #  (GLM.families.links.inverse_squared, 'inv_sqr'),
                         (GLM.families.links.inverse_power, 'inv_pow'),
                         (GLM.families.links.identity, 'id'),
                         (GLM.families.links.log, 'log'),
                         ]

      #  self.link_lab = ['inv_sqr', 'inv_pow', 'id', 'log']
#        self.link_lab = ['log', 'id', 'log']

     #   self.family = np.array([GLM.families.InverseGaussian(),
     #                           GLM.families.InverseGaussian(), 
     #                           GLM.families.InverseGaussian(), 
     #                           GLM.families.InverseGaussian(), 
     #                           GLM.families.InverseGaussian(), 
     #                           GLM.families.InverseGaussian()])
        self.family = np.array([GLM.families.Gaussian(link[0]) for link in self.fam_links])
        self.link_lab = [link[1] for link in self.fam_links]
        self.fam_lab = ['Gau']


        self.reslts = [0]
#        self.GLM_res = np.empty(shape=self.observ[0,:,:,:].shape+(2,), dtype='O')
        self.GLM_res = np.empty(shape=self.observ[0,:,:,:].shape+(2,len(self.fam_links)), dtype='O')

        for ijk in self.my_GLM:
            i,j,k = ijk
            if k in self.reslts: 
              #  self.GLM_res[i,j,k,0] = GLM.GLM(self.observ[:,i,j,k], self.reg_wconst, family=GLM.families.Gaussian())
                for lnkidx, _ in enumerate(self.fam_links):
             #   self.GLM_res[i,j,k,0] = GLM.GLM(self.observ[:,i,j,k], self.reg_wconst, family=self.family[k])
             #   self.GLM_res[i,j,k,1] = self.GLM_res[i,j,k,0].fit() 
#                print(self.rank, 'rank', i,j,k, self.GLM_res[i,j,k,1].params)
                    self.GLM_res[i,j,k,0,lnkidx] = GLM.GLM(self.observ[:,i,j,k], self.reg_wconst, family=self.family[lnkidx])
                    self.GLM_res[i,j,k,1,lnkidx] = self.GLM_res[i,j,k,0,lnkidx].fit() 

        self.GLM_res_lst = MPI.COMM_WORLD.gather(self.GLM_res, root=0) 
        if self.rank == 0:
            self.align_results(self.GLM_res, self.GLM_res_lst)

        self.GLM_res = MPI.COMM_WORLD.bcast(self.GLM_res, root=0)

      #  self.results_var = ['Silence [ms]', 'Burst [spk/s]', 'Firing [spk/s]', '$\theta$ [4-8] Hz power', '$\theta$ [2-8] Hz power', '$\beta$ [12-30] Hz power', '$\beta$ [16-32] Hz'] 
        self.results_var = ['Silence [ms]', 'Burst [spk/s]', 'Firing rate over T [Hz]', '$\theta$ [4-8] Hz power', '$\theta$ [2-8] Hz power']
        self.res_tit = ['Sil', 'Bur', 'CFi', 'T48', 'T28', 'B12', 'B16']

        # self.reg_list = [(0, 'NMDA'), (1, 'GABA'), (2, 'SEG'), (3. 'PHASE'), (4, 'TMPRD'), (5, 'INT1stCTX'), (6, 'INT2ndCTX'), (7, 'NMDAxGABA'), (8, 'NMDAxSEG'), (9, 'NMDAxPHASE'),
        #                    (10, 'NMDAxINT1stCTX'), (11, 'NMDAxINT2ndCTX')] 

        self.NMDA_con = np.array([3.666e-6, 9.165e-6, 1.833e-5, 3.666e-5, 4.583e-5]) * 1.4206 
        self.GPe_con = np.array([4e-6, 8e-6, 1e-5])
        self.segre = np.array([0.25, 0.55, 0.85, 1.00])
        self.phase = np.array([13, 37, 67, 103, 193, 269])
        self.stim_per = np.array([80, 125, 250, 500])
        self.stim1_I = np.array([1, 3])
        self.stim2_I = np.array([1, 3])
        self.NMDAseg = np.unique([i for i in it.product(self.NMDA_con, self.segre)])
        self.NMDAGABA = np.unique([i for i in it.product(self.NMDA_con, self.GPe_con)])
        self.NMDAphase = np.unique([i for i in it.product(self.NMDA_con, self.phase)])
        self.NMDA1ststim = np.unique([i for i in it.product(self.NMDA_con,  self.stim1_I)])
        self.NMDA2ndstim = np.unique([i for i in it.product(self.NMDA_con,  self.stim2_I)])

        self.all_reg = np.array([self.NMDA_con, self.GPe_con, self.segre, self.phase, self.stim_per, self.stim1_I, self.stim2_I, 
                                 self.NMDAGABA, self.NMDAseg, self.NMDAphase, self.NMDA1ststim, self.NMDA2ndstim], dtype=np.ndarray) 

        self.pick_dat = self.all_reg[self.pick_reg]


        self.ax_tit = ['SB00', 'SB01', 'SBALL', 'SB02', 'SB03']

        t1 = time.time()
        print(t1-t0)

        res_lnk_tup = [(i,j) for i,j in it.product(range(len(self.reslts)), range(len(self.fam_links)))]

      #  self.my_plots = range(self.rank, len(self.results_var), self.N_hosts)
        self.my_plots = range(self.rank, len(res_lnk_tup), self.N_hosts)

        for idx in self.my_plots: 
            ridx, lnkidx = res_lnk_tup[idx]
            self.plot_res(ridx, lnkidx)
            #    self.plot_res_dist(idx)

        t2 = time.time()
        print(t2-t0)

    #    if self.rank == 0:
    #        ISI0_S1_F1_glm = sm.GLM(self.observ[:,2,0,0], self.reg_wconst, family=sm.families.Gamma())
    #        ISI0_S1_F1_glm_res = ISI0_S1_F1_glm.fit()
    #        print('ISI0_S1_F1_glm', ISI0_S1_F1_glm_res.params)
    #        print(self.rank, time.time() - t1)

       # if self.rank == 1:
       #     ISI0_S1_F1_glm = sm.GLM(self.observ[:,2,0,0], self.reg_wconst, family=sm.families.Gamma())
       #     ISI0_S1_F1_glm_res = ISI0_S1_F1_glm.fit()
       #     print('ISI0_S1_F1_glm', ISI0_S1_F1_glm_res.params)
       #     print(self.rank, time.time() - t1)

    def plot_res_dist(self, idx):
        fig = mpl.figure.Figure(figsize=(20,10))
        canvas = FigureCanvas(fig)
        gl = gs.GridSpec(5, 2)
        ax_arr = np.empty(shape=(5,2), dtype='O')
        ax_arr = self.axis_create_align(ax_arr,gl, fig)
        ax_order = np.array([2,1,0,3,4])
    

 #       self.observ = np.empty(shape=(self.N_sims, 5, 2, self.N_res))

        for (sbidx,), ax_o in np.ndenumerate(ax_order):
                ax_arr[ax_o,0].hist(self.observ[:,sbidx,1,idx], bins=50, density=True, label='1ST STIM')
                ax_arr[ax_o,1].hist(self.observ[:,sbidx,0,idx], bins=50, density=True, label='2ND STIM')
                ax_arr[ax_o,0].set_xlabel('distribution of means {!s}_ 1ST STIM'.format(self.res_tit[idx]))
                ax_arr[ax_o,1].set_xlabel('distribution of means {!s}_ 2ND STIM'.format(self.res_tit[idx]))
                ax_arr[ax_o,0].set_title(self.ax_tit[ax_o])
                ax_arr[ax_o,1].set_title(self.ax_tit[ax_o])
                
        fig.suptitle(self.results_var[idx])
        fig.tight_layout()
        fig.savefig('DistResReg_{!s}.pdf'.format(self.res_tit[idx]))
            
    def plot_res(self, idx, lnkidx):
        fig = mpl.figure.Figure(figsize=(30,15))
        canvas = FigureCanvas(fig)
        gl = gs.GridSpec(5, self.pick_reg.shape[0])
        ax_arr = np.empty(shape=(5, self.pick_reg.shape[0]), dtype='O')
        ax_arr = self.axis_create_align(ax_arr,gl, fig)
        results =  self.GLM_res[:,:,idx,1, lnkidx] 
        ax_order = np.array([2,1,0,3,4])

        for (sbidx,), ax_o in np.ndenumerate(ax_order):
            params_f1 = results[sbidx,0].params
            params_f2 = results[sbidx,1].params
            for (abidx,), abc in np.ndenumerate(self.pick_dat): 
                ax_arr[ax_o, abidx].plot(abc, params_f2[abidx+1]*abc, marker='*', label='1ST STIM')
                ax_arr[ax_o, abidx].plot(abc, params_f1[abidx+1]*abc, marker='o', label='2ND STIM')
                ax_arr[ax_o, abidx].legend(loc='best')
                ax_arr[ax_o, abidx].set_xlabel(self.reg_tit[abidx])
                ax_arr[ax_o, abidx].set_title(self.ax_tit[ax_o])
                
        fig.suptitle(self.results_var[idx])
        fig.tight_layout()
        fig.savefig('SyncReg_noZ{!s}_{!s}_{!s}_{:03d}.pdf'.format(self.res_tit[idx], self.link_lab[lnkidx], self.fam_lab[0], self.pick_reg.shape[0]))

    def axis_create_align(self, axr, gl, fig, xlab='', ylab='', style=True):
        gl_gem = gl.get_geometry()
        ax_shp = axr.shape
        if gl_gem == ax_shp:
            axr[0,0] = fig.add_subplot(gl[0,0])
            for i in range(1,gl_gem[1]):
                axr[0,i] = fig.add_subplot(gl[0,i]) #, sharey=axr[0,0])
            for j in range(1,gl_gem[0]):
                axr[j,0] = fig.add_subplot(gl[j,0]) #, sharey=axr[0,0])
                axr[j,0].set_ylabel(ylab)
            for i,j in it.product(range(1,gl_gem[0]), range(1, gl_gem[1])):
                axr[i,j] = fig.add_subplot(gl[i,j]) #, sharex=axr[0,j], sharey=axr[i,0])
            for j in range(gl_gem[1]):
                axr[-1,j].set_xlabel(xlab)
            axr[0,0].set_ylabel(ylab)
            if style:
                for i, ax in np.ndenumerate(axr):
                    self.style_axis(ax)

        else:
            print('Axis array and Gridspec geometry mismatch')
        return axr
    
    def axis_create_inset(self, axr, xlab='', ylab=''):
        ax_shp = axr.shape
        for i, j in it.product(range(ax_shp[0]), range(ax_shp[1])):
            axr[i,j,1] = inset_axes(axr[i, j, 0], width="70%", height="60%", loc=1)
            axr[i,j,1].patch.set_alpha(0)
        axr[0,0,1].set_xlabel(xlab)
        axr[0,0,1].set_ylabel(ylab)
        return axr

    def style_axis(self, ax, top=False, right=False, offset=5):
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-1,1))
        ax.spines['top'].set_visible(top)
        ax.spines['right'].set_visible(right)

        ax.spines['bottom'].set_position(('data', 0))
        ax.spines['left'].set_position(('outward', offset))

        ax.xaxis.set_ticklabels([])
      #  ax.xaxis.set_major_formatter(formatter)
        ax.yaxis.set_major_formatter(formatter)

    def rem_fill_nsg(self, axis, pat_no):
        pats = axis.patches 
        br = pats[pat_no]
        br.set_fill(False)
       # axis.text(br.get_x() + br.get_width() / 2, br.get_y() + br.get_height(), '$*$',
       #     ha='center', va='bottom')

def exec_GLM_ana(fils):
    param_dict = {}
    popof = {
                'param': {
#[3.666e-6, 1.833e-5, 3.666e-5]
                       #  'Low':  {'Ctx_STN_A': [7.33e-06, 3.666e-05], 'GPe_STN': [1e-5]},
                       #  'High':  {'GPe_STN': [1.5e-5],}, 
                      #   'Low':  {'Ctx_STN_A': [3.666e-06], },
                      #   'High':  {'Ctx_STN_A': [1.833e-05],}, 
                       #  'NMDALMV':  {'Ctx_STN_A': [3.666e-06, 1.833e-05],}, 
                       #  'NMDAMHV':  {'Ctx_STN_A': [1.833e-05, 3.666e-05],}, 
                     #    'NMDAGLMV':  {'Ctx_STN_A': [3.666e-06, 1.833e-05], 'GPe_STN': [8e-6],}, 
                         'NMDAGLV':  {'Ctx_STN_A': [3.666e-06], 'GPe_STN': [8e-6],}, 
                         'NMDAGMV':  {'Ctx_STN_A': [1.833e-05], 'GPe_STN': [8e-6],}, 
                     #    'NMDAGMHV':  {'Ctx_STN_A': [1.833e-05, 3.666e-05], 'GPe_STN': [8e-6],}, 
                     #    'NMDAGAll':  {'GPe_STN': [8e-6],}, 
                    },

                'feed': {
                   #      'High': [('Dlay', [(1,50)]) ],
#                         'Ovlp': [('Ovlp', [(1,50)]) ],
                       #  'LongOvl': [('Ovlp', [(20,300)]) ],
    #                     'Low': [('fds', ['F22NH2']), ('Int1', [(300,600)]) ],
#                        'Low': [('fds', ['F22NH2']),  ],
#                         'High': [('fds', ['F22NH4']), ('Int1', [(125,250)]) ],
#                         'NoSeg': [ ('Seg', [(0.1,0.95)]) ],
                       #  'Seg': [ ('Seg', [(0.95, 1.1)]) ],

                         'HiHi': [('Seg', [(0.1,0.95)]), ('Dlay', [(1,50)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],
                         'HiLo': [('Seg', [(0.1,0.95)]), ('Dlay', [(1,50)]), ('Dur1', [(45,55)]), ('Int1', [(2,4)]),  ('Dur2', [(25,45)]), ('Int2', [(6,12)]) ],
                         'LoLo': [('Seg', [(0.1,0.95)]), ('Dlay', [(1,50)]), ('Dur1', [(25,45)]), ('Int1', [(6,12)]), ('Dur2', [(25,45)]), ('Int2', [(6,12)]) ],
                         'LoHi': [('Seg', [(0.1,0.95)]), ('Dlay', [(1,50)]), ('Dur1', [(25,45)]), ('Int1', [(6,12)]), ('Dur2', [(45,55)]), ('Int2', [(2,4)]) ],

                    },
                'cond': {
                        'NoSegShortHiHiNLMCond' : ('NMDAGLV','HiHi'),
                        'NoSegShortHiHiNMHCond' : ('NMDAGMV','HiHi'),
                        'NoSegShortHiLoNLMCond' : ('NMDAGLV','HiLo'),
                        'NoSegShortHiLoNMHCond' : ('NMDAGMV','HiLo'),
                        'NoSegShortLoLoNLMCond' : ('NMDAGLV','LoLo'),
                        'NoSegShortLoLoNMHCond' : ('NMDAGMV','LoLo'),
                        'NoSegShortLoHiNLMCond' : ('NMDAGLV','LoHi'),
                        'NoSegShortLoHiNMHCond' : ('NMDAGMV','LoHi'),
                      #  'OvlpNLMCond' : ('NMDAGLMV','MLong'),
                      #  'OvlpNMHCond' : ('NMDAGMHV','MLong'),
                      #  'OvlpAllCond' : ('NMDAGAll','MLong'),
                     #   'HighParamNMDA' : ('High','All'),
                     #   'LowParamNMDA' : ('Low','All'),
                     #   'lopo': ('High', 'Low') 
                      #  'SegNM': ('All', 'Seg'), 
                      #  'SegAll': ('NMDALMV', 'Seg'), 
                     #   'HighDelNM': ('All', 'High'), 
                     #   'LowDelNM': ('All', 'Low'), 
                    #    'NoSegHiHi' : ('All', 'HiHi'),
                    #    'NoSegHiLo' : ('All', 'HiLo'),
                    #    'NoSegLoHi' : ('All', 'LoHi'),
                    #    'NoSegLoLo' : ('All', 'LoLo'),
        
                   #     'NMHiHi' : ('NMDALMV', 'HiHi'),
                   #     'NMHiLo' : ('NMDALMV', 'HiLo'),
                   #     'NMLoHi' : ('NMDALMV', 'LoHi'),
                   #     'NMLoLo' : ('NMDALMV', 'LoLo'),
                        },
            }
    GLMAna = [
#            ([ ('P', 'GPe_STN'), ('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ('F', 'Dur0'), ('F', 'Dur1'), ('F', 'Int0'), ('F', 'Int1'),], 
#                    ['Sil', 'Bur', 'Fir', 'AUC_LFO'], True, ['SubPop', 'Fdcom'], False), 
#            ([('P', 'Ctx_STN_N'), ('F', 'Del'), ('F', 'Ovlp'),  ('F', 'Seg'), ('F', 'Dur0'), ('F', 'Dur1'), ('F', 'Int0'), ('F', 'Int1')], 
            ([('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ],
        #    ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ('F', 'Dur0'), ('F', 'Dur1'), ('F', 'Int0'), ('F', 'Int1'), ('I', 'Ctx_STN_N$Dur0$Int0'), ('I', 'Ctx_STN_N$Dur1$Int1')], 
#                    ['AUC_LFO'], False, ['Fdcom',  ], False), 
                #    ['Sil', 'Bur', 'Fir', ], False, ['SubPop',  ], False), 
                    ['Sil', 'Bur', 'Fir', 'AUC_LFO'], False, ['SubPop', 'Fdcom'], False), 
         #   ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('F', 'Ovlp'), ('F', 'Seg'), ],
          #  ([('P', 'Ctx_STN_N'), ('P', 'GPe_STN'), ('F', 'Del'), ('I', 'Ctx_STN_N$Del') ],
          #          ['Sil',  'AUC_LFO'], True, ['SubPop', 'Fdcom' ], False), 
            ] 
         
    param_dict['GLM_Ana'] = GLMAna
    param_dict['saveformat'] = 'pdf'
    param_dict['prefix'] = 'galo'

    param_dict['filt_dict'] = popof
    GLM_cls = GLM_Analysis(fils, param_dict, intval=100)
#    GLM_cls = GLM_Analysis_Event(fils)

if __name__ == '__main__':
    fils = sys.argv[1:]

    if fils[0].endswith('.hdf5'):
        exec_GLM_ana(fils[0]) 
    else:
        print('Invalid HDF5 file... skipping')
