#! python3
# ver: 1.4.5
# rev: 2018-12-15 - Freq, ISI bimodal

# Import machinery
from mpi4py import MPI
import h5py, os, sys

import itertools as it
from functools import reduce

# Matplotlib
import matplotlib as mpl
#
import matplotlib.ticker as ticker
import matplotlib.text as text

#import matplotlib.markers as markers
##import matplotlib.lines as lines
#from mpl_toolkits.axes_grid1.inset_locator import inset_axes

# Import computation modules 
import numpy as np
import scipy.signal as sps

####################################################################
#feed_dt = np.dtype([('Feed', np.unicode_, 16), ('F0mod', np.unicode_, 16), ('F1mod', np.unicode_, 16), ('F0nums', np.float, 6), ('F0spk', np.int, 2), ('F1nums', np.float, 6), ('F1spk', np.int, 2)])
fed_spc = np.dtype('U64, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
feed_dt = np.dtype([('spcs', fed_spc), ('TgtSub', np.int, 2)])
fed_alg = np.dtype([('shft', np.bool), ('t0', np.float, 2), ('tend', np.float, 2), ('Dur', np.float, 2), ('Int', np.int, 2), ('TPart', np.float, 3), ('TgtSub', np.int, 2), ('AlgSub', np.int, 4)])
fed_dat = np.dtype([('fil', np.int, 5), ('fd', fed_spc), ('algn', fed_alg)])

fed_unq = np.dtype([('fd', fed_spc), ('algn', fed_alg), ('fils', np.ndarray)])
net_unq = np.dtype([('nt', 'U16'), ('fils', np.ndarray)])
pfnc_dt = np.dtype([('fils', np.ndarray), ('fd_seeds', np.ndarray), ('nt_seeds', np.ndarray)])
#sys_name = 'ANATAC' 
sim_pat = np.dtype('i4, U64, (3)i4, i4, i4')

class Analysis():
    def __init__(self, datafile, tmin=0., tmax=3000., tbreaks=[]): 
      #  pass
      #  self.datafile = datafile
      #  self.file = h5py.File(self.datafile, 'r', driver='mpio', comm=MPI.COMM_WORLD)

        self.old_init(datafile, tmin=0., tmax=3000., tbreaks=[])
    #    self.new_init(datafile, tmin=0., tmax=3000., tbreaks=[])


    def new_init(self,datafile, tmin=0., tmax=3000., tbreaks=[]):
        self.file = datafile 
        self.pre_file = self.file.filename.split('/')[-1][:-5]

        self.timesteps = np.array(self.file['S000/props/timesteps'])

        self.tstop = self.timesteps[-1]
        self.trec_ini = self.timesteps[0]

        self.trec = self.timesteps[1]-self.trec_ini 

        self.tmin = max(self.trec_ini, tmin)
        self.tmax = min(tmax, self.tstop)

        self.fs = 1000/self.trec
        self.nyq = self.fs/2
        self.tbreaks = np.sort(tbreaks)
        self.tbreaks = self.tbreaks[np.where((self.tbreaks>=self.tmin) & (self.tbreaks<=self.tmax))] 
        self.timeposts =  reduce(np.union1d, (np.array([self.tmin, self.tmax]), self.tbreaks))
        self.timpostidx = np.array((self.timeposts - self.trec_ini)/self.trec, dtype=int)
        self.timeints = np.diff(self.timeposts)

        self.N_Params = len(self.file['param_combo']) 
        self.N_network_trials = self.file['network_seed'] 
        self.N_extfeed_trials = self.file['extfeed_seed'] 

        self.net_file = np.array(self.file['network_seed']['net'], dtype=str)
        self.ext_feed_file = np.array(self.file['extfeed_seed']['extfeed'], dtype=str)

        self.fixed_hier = 'extfeed' 

        self.N_timpts = int((self.tmax-self.tmin)/self.trec)+1
        self.alltimpts = np.arange(self.trec_ini, self.tstop+self.trec, self.trec) 


    def old_init(self, datafile, tmin=0., tmax=3000., tbreaks=[]):

        self.file = datafile 
        self.pre_file = self.file.filename.split('/')[-1][:-5]
        self.tstop = self.file.attrs['tstop']
        self.trec = self.file.attrs['trec']
        self.trec_ini = self.file.attrs['trec_ini']
        self.tmin = max(self.trec_ini, tmin)
        self.tmax = min(tmax, self.tstop)
        self.fs = 1000/self.trec
        self.nyq = self.fs/2
        self.tbreaks = np.sort(tbreaks)
        self.tbreaks = self.tbreaks[np.where((self.tbreaks>=self.tmin) & (self.tbreaks<=self.tmax))] 
        self.timeposts =  reduce(np.union1d, (np.array([self.tmin, self.tmax]), self.tbreaks))
        self.timpostidx = np.array((self.timeposts - self.trec_ini)/self.trec, dtype=int)
        self.timeints = np.diff(self.timeposts)
        self.N_Params = self.file.attrs['N_Params']
        self.N_network_trials = self.file.attrs['N_network_trials']
        self.N_extfeed_trials = self.file.attrs['N_extfeed_trials']
        self.net_file = np.array(self.file.attrs['net_file'], dtype=str)
        self.ext_feed_file = np.array(self.file.attrs['ext_feed_file'], dtype=str)
        self.fixed_hier = self.file.attrs['fixed_hier']
        self.N_timpts = int((self.tmax-self.tmin)/self.trec)+1
        self.alltimpts = np.arange(self.trec_ini, self.tstop+self.trec, self.trec) 

    # Get data hierarchy
    def get_hier(self):
        self.params = ['P{:03d}'.format(par) for par in range(self.N_Params)]
        self.get_param_comb()
        feeds = ['F{:03d}'.format(par) for par in range(self.N_extfeed_trials)]
        nets = ['N{:03d}'.format(par) for par in range(self.N_network_trials)]
   
        if self.fixed_hier == 'extfeed':      
            comb = it.product(self.params, self.ext_feed_file, feeds, self.net_file, nets)
        else:
            comb = it.product(self.params, self.net_file, nets, self.ext_feed_file, feeds)

     #   self.comb_paths = [(str.join('/', com), self.param_txt[com[0]]) for com in comb] 
        self.comb_paths = [(str.join('/', com), self.param_dict[com[0]]) for  com in comb] 
        self.comb_paths.sort()

    def get_param_comb(self):
     #   self.param_dict = np.empty(shape=self.N_Params, dtype=dict)
        self.param_dict = {} 
        if self.N_Params > 1:
            #self.param_txt = {par:self.get_att(par) for par in self.params}
            for pidx, par in enumerate(self.params): 
                self.param_dict['P{:03d}'.format(pidx)] = dict(self.file[par].attrs)
        else:
            self.param_dict['P000'] = {}

#    def get_att(self, var):
#        att_txt = ''
#        att_dict = {}
#        for att,val in self.file[var].attrs.items():
#            att_dict[att] = val
#            att_txt += '{!s}: {:.2e}\n'.format(att, val)    
#        return att_txt, att_dict
        
    # Interperse spike_times between timeposts
    # [timeposts_intervals: spk_times]
    def trim_spike_range(self, tvec, timeposts):
        splidx = np.searchsorted(tvec, timeposts)
        spk_post_arr = np.empty(shape=self.timeints.size, dtype='O')
        spk_post_arr[:] = np.split(tvec, splidx)[1:-1] 
        return spk_post_arr 

    # Get subpop spikes array, row = gid, col = interpersed spike times between timeposts 
    # [gid, timpst_int: spk_times]
    def get_gids_trim_spike(self, gid_nplist, spk_data):
        subpop_spk_arr = np.empty(shape=(gid_nplist.size, self.timeints.size), dtype='O')
        for idx, gid in enumerate(gid_nplist):
            subpop_spk_arr[idx,:] = self.trim_spike_range(spk_data[gid], self.timeposts)
        return subpop_spk_arr

    # Get array of spikes array 
    # [gid, timpst_int: spk_times]
    def get_gid_spk(self, expt_path, gids):
        spk_data = self.file[expt_path]['raw']['spikes']
        gid_spk_arr = np.empty(shape=(gids.size, self.timeints.size), dtype='O')
        gid_spk_arr = self.get_gids_trim_spike(gids, spk_data)
        return gid_spk_arr

    # Converts spike trains to point processes
    # [gid, tim_bins: spk (subpop based gid_ref_idx)]
#    def get_gid_pp(self, expt_path, spk_subp, subpop_idx_ranges, N_gids, tbin=0.025):
    def get_gid_pp(self, expt_path, gids, tbin=0.025):
        prec = tbin/self.trec
        spk_data = self.file[expt_path]['raw']['spikes']
        prec = max(1,prec)
        N_gids = gids.size
        gid_pp_tmp = np.zeros(shape=(N_gids, int((self.tmax-self.tmin)/self.trec)+1), dtype=int)
        
       # for sidx, subpop in enumerate(spk_subp):
       #     for gidc, gidx in enumerate(subpop_idx_ranges[sidx]):
       #         spk_arr = np.array((self.trim_spike_range(self.spk_data[subpop[0][gidc]], np.array([self.tmin, self.tmax]))[0]-self.tmin)/self.trec, dtype=int) 
       #         gid_pp_tmp[gidx, spk_arr] = int(1)
#        for sidx in range(spk_subp.shape[0]):
#            for gidc, gidx in zip(spk_subp[sidx,0], spk_subp[sidx,1]):
#                spk_arr = np.array((self.trim_spike_range(spk_data[gidc], np.array([self.tmin, self.tmax]))[0]-self.tmin)/self.trec, dtype=int) 
#                gid_pp_tmp[gidx, spk_arr] = int(1)
#
        for  gidx, gid in enumerate(gids):
            spk_arr = np.array((self.trim_spike_range(spk_data[gid], np.array([self.tmin, self.tmax]))[0]-self.tmin)/self.trec, dtype=int) 
            gid_pp_tmp[gidx, spk_arr] = int(1)

        if prec > 1:
            tposts = np.rint(np.arange(0, gid_pp_tmp.shape[1], prec)).astype(int) 
            gid_pp_arr = np.zeros(shape=(N_gids, tposts.size), dtype=int)
            for i,_ in enumerate(tposts[:-1]):
                gid_pp_arr[:,i] = np.sum(gid_pp_tmp[:, tposts[i]:tposts[i+1]], axis=1)
            gid_pp_arr[:,-1] = np.sum(gid_pp_tmp[:, tposts[-1]:], axis=1)
        else: 
            gid_pp_arr = gid_pp_tmp
        return gid_pp_arr, tbin

    # Get ISIs per gid between timeposts
    # [gid, timpst_int: ISI_times (subpop based gid_ref_idx)]
    def get_gid_ISI(self, gid_spk_arr):
        gsar_shape = gid_spk_arr.shape
        gid_ISI_arr = np.empty(shape=gsar_shape, dtype='O')
        for gidx, tidx in it.product(np.arange(gsar_shape[0]), np.arange(gsar_shape[1])): 
            gid_ISI_arr[gidx, tidx] = np.diff(gid_spk_arr[gidx, tidx])
        return gid_ISI_arr        

    def get_gid_freq(self, gid_spk_arr):
        gsar_shape = gid_spk_arr.shape
        gid_freq_arr = np.zeros(shape=gsar_shape)
        for gidx, tidx in it.product(np.arange(gsar_shape[0]), np.arange(gsar_shape[1])): 
            gid_freq_arr[gidx, tidx] = gid_spk_arr[gidx, tidx].size/self.timeints[tidx]*1000
        return gid_freq_arr


    # Get mean of cap curr for each subpop
    # [subpop, timesteps], time
    def get_subpop_cap_cur(self, spk_subpop, expt_path, Tsam=int(1)):
        tim = np.array(self.file[expt_path]['props']['timesteps'])
        t_idx = np.arange((self.tmin-self.trec_ini)/self.trec, (self.tmax-self.trec_ini)/self.trec+1, Tsam, dtype=int)
        tim = tim[t_idx]
        subpop_curr_arr = np.zeros(shape=(spk_subpop.shape[0], tim.size))

    #    intrin = self.file[expt_path]['raw']['STNSta_i_int'][:, t_idx]
        ampa = self.file[expt_path]['raw']['STNSyn_AMPA_Ctx'][:, t_idx]
        nmda = self.file[expt_path]['raw']['STNSyn_NMDA_Ctx'][:, t_idx]
        gaba = self.file[expt_path]['raw']['STNSyn_GABA_GPe'][:, t_idx]
     #   capa = intrin + ampa + nmda + gaba
        capa = ampa + nmda + gaba

        for sidx, subpop in enumerate(spk_subpop):
            subpop_curr_arr[sidx,:]  = np.mean(capa[subpop[0], :], axis=0) 
        return subpop_curr_arr, tim

    def get_subpop_allcurr(self, spk_subpop, expt_path, Tsam=int(1)):
        tim = np.array(self.file[expt_path]['props']['timesteps'])
        t_idx = np.arange((self.tmin-self.trec_ini)/self.trec, (self.tmax-self.trec_ini)/self.trec+1, Tsam, dtype=int)
        tim = tim[t_idx]
        subpop_allcurr_arr = np.zeros(shape=(spk_subpop.shape[0], 6, tim.size))

   #     intrin = self.file[expt_path]['raw']['STNSta_i_int'][:, t_idx]
        ampa = self.file[expt_path]['raw']['STNSyn_AMPA_Ctx'][:, t_idx]
        nmda = self.file[expt_path]['raw']['STNSyn_NMDA_Ctx'][:, t_idx]
        gaba = self.file[expt_path]['raw']['STNSyn_GABA_GPe'][:, t_idx]

        for sidx, subpop in enumerate(spk_subpop):
    #        subpop_allcurr_arr[sidx,0,:]  = np.mean(intrin[subpop[0], :], axis=0) 
            subpop_allcurr_arr[sidx,1,:]  = np.mean(ampa[subpop[0], :], axis=0) 
            subpop_allcurr_arr[sidx,2,:]  = np.mean(nmda[subpop[0], :], axis=0) 
            subpop_allcurr_arr[sidx,3,:]  = np.mean(gaba[subpop[0], :], axis=0) 
            subpop_allcurr_arr[sidx,4,:]  = np.sum(subpop_allcurr_arr[sidx,1:4, :], axis=0)
            subpop_allcurr_arr[sidx,5,:]  = np.sum(subpop_allcurr_arr[sidx,1:4, :], axis=0)
#            subpop_allcurr_arr[sidx,5,:]  = np.sum(subpop_allcurr_arr[sidx,[0,4], :], axis=0)

        return subpop_allcurr_arr, tim

    def get_filtered_sig(self, sig, filt):
        b, a = filt 
      #  filt_sig = sps.lfilter(b, a, sig)
        filt_sig = sps.filtfilt(b, a, sig)
        return filt_sig
        

    def get_subpop_spec(self, subpop_cap_cur, freq, tim):
        N_subpop = subpop_cap_cur.shape[0]
        subpop_spec_mat = np.empty(shape=(N_subpop, freq.size, tim.size), dtype='float') 
        for sidx in range(N_subpop):
        #    subpop_spec_mat[sidx,:,:] = get_spectral_matrix(subpop_cap_cur[sidx], freq, tim)
            subpop_spec_mat[sidx,:,:] = my_spectral_matrix(subpop_cap_cur[sidx], freq, tim)
        #    subpop_spec_mat[sidx,:,:] = new_spectral_matrix(subpop_cap_cur[sidx], self.transmorlet, tim)

        return subpop_spec_mat


    def get_bands_avg(self, sbp_spec_mat, band_posts_idx, tim_size):
        N_subpop = sbp_spec_mat.shape[0] 
    #    band_posts_idx = np.array([0, 31, 62, 93, 124, freq.size]) 
    #    add_bands_idx = [(0,93), (80,121)] 
    #    band_widths = np.diff(band_posts)
        N_bands = band_posts_idx.shape[0]
        subpop_spec_band_av = np.empty(shape=(N_subpop, N_bands, tim_size), dtype='float') 
#        filt_list = [self.banddel, self.bandthe, self.bandalp, self.bandbet, self.bandgam]
        for sidx in range(N_subpop):
            for i in range(N_bands):
              #  subpop_spec_band_av[sidx,i,:] = self.get_filtered_sig(subpop_cap_cur[sidx], filt_list[i])**2
#                subpop_spec_band_av[sidx,i,:] = self.get_filtered_sig(subpop_cap_cur[sidx], filt_list[i])
              #  subpop_spec_band_av[sidx,i,:] = np.mean(subpop_spec_mat[sidx, band_posts_idx[i]:band_posts_idx[i+1] ,:], axis=0)/band_widths[i]
                subpop_spec_band_av[sidx,i,:] = np.mean(sbp_spec_mat[sidx, band_posts_idx[i,0]:band_posts_idx[i,1] ,:], axis=0)
        #    for jidx, j in enumerate(add_bands_idx):
        #        subpop_spec_band_av[sidx,N_bands+jidx,:] = np.mean(subpop_spec_mat[sidx, j[0]:j[1] ,:], axis=0)
        return subpop_spec_band_av
        
    def get_ctx_feed(self, expt_path):
        Ctx_feeds = np.array(self.file[expt_path]['props']['feeds_Ctx'], dtype='O')
        N_Ctx_feeds = int(np.log2(Ctx_feeds.size)) 
        return Ctx_feeds, N_Ctx_feeds

    # Plot spike raster on subpop basis
    # [subpop: axis] 
    def plot_subpop_spikes(self, spk_subp, gid_spk_arr, subpop_axis, spk_col):
        # spk_subp = [(gidlist, offset, color)]
        col = ['k', 'b', 'r', 'g', 'c', 'm', 'y']
        subp_count = 0
      #  for sidx, subpop in enumerate(spk_subp):
      #      for gidx, gid in enumerate(idx_ranges[sidx]):
      #          tvec = gid_spk_arr[gid] 
      #          subpop_axis[sidx].eventplot(np.concatenate(tvec), lineoffsets=subp_count+subpop[1]+gidx, linelengths=0.5, linewidths=0.5 ,color=subpop[2])
        for sidx in range(spk_subp.shape[0]):
            for gidx, gid in enumerate(spk_subp[sidx,0]):
                tvec = gid_spk_arr[gid] 
                subpop_axis[sidx].eventplot(np.concatenate(tvec), lineoffsets=subp_count+gidx, linelengths=0.5, linewidths=0.5, color=col[spk_col[gid]])
          #  subp_count += subpop[0].size
        return subpop_axis

    # Plot ISI histograms on subpop basis
    # [subpop, timpst_int: axis]
#    def plot_subpop_ISIs(self, spk_subp, gid_ISI_arr, axis_arr, bins=50, orien='horizontal', co=(False, 50), density=False, histtype='bar', summ=True, box=True):
    def plot_subpop_ISIs(self, spk_subp, gid_ISI_arr, axis_arr, bins=50, orien='horizontal', co=(False, 50)):
        for sidx in range(spk_subp.shape[0]):
            for tidx in np.arange(gid_ISI_arr.shape[1]):
                concat_subpop_ISI = np.concatenate((gid_ISI_arr[spk_subp[sidx,0], tidx]))
                axis_arr[sidx, tidx, 0].hist(concat_subpop_ISI, bins=bins, orientation=orien, color='k', alpha=0.3)

#                brk = co[0]
#                if brk is True:
#                    concat_above = concat_subpop_ISI[concat_subpop_ISI>=co[1]]
#                    concat_below = concat_subpop_ISI[concat_subpop_ISI<=co[1]]
#                    if concat_above.size == concat_subpop_ISI.size or concat_below.size == concat_subpop_ISI.size:
#                        brk = False
                        
#                if brk is False:
              #  if box:
                axis_arr[sidx, tidx, 1].boxplot(concat_subpop_ISI, showmeans=True, vert=True)
              #  if summ:
                mean = np.mean(concat_subpop_ISI) 
                median = np.median(concat_subpop_ISI) 
                std = np.std(concat_subpop_ISI) 
                axis_arr[sidx, tidx, 1].boxplot(concat_subpop_ISI, showmeans=True, vert=True)
                axis_arr[sidx, tidx, 1].text(0.1, 0.5, '{:>8}: {:.2f}\n{:>8}: {:.2f}\n{:>8}: {:.2e}\n{:>8}: {:8f}\n{:>8}: {:.2f}'\
                .format('mean', mean, 'median', median, 'std', std, 'cv', std/mean, 'mean_f', 1000/mean,\
                fontsize='x-small'), verticalalignment='center', family='monospace', transform=axis_arr[sidx, tidx, 1].transAxes)
#                else:
#                    axis_arr[sidx, tidx, 1].boxplot(concat_above, showmeans=True, vert=True)
#                    axis_arr[sidx, tidx, 1].boxplot(concat_below, showmeans=True, vert=True)
#                    mean_ab = np.mean(concat_above) 
#                    mean_bl = np.mean(concat_below) 
#                    med_ab = np.median(concat_above) 
#                    med_bl = np.median(concat_below) 
#                    std_ab = np.std(concat_above) 
#                    std_bl = np.std(concat_below) 
#                    axis_arr[sidx, tidx, 1].text(0.1, 0.8, '{:>6}: {:.2f}\n{:>6}: {:.2f}\n{:>6}: {:.2e}\n{:>6}: {:8f}\n{:>6}: {:.2f}'\
#                    .format('mean', mean_ab, 'median', med_ab, 'std', std_ab, 'cv', std_ab/mean_ab, 'mean_f', 1000/mean_ab,\
#                    fontsize='x-small'), verticalalignment='center', family='monospace', transform=axis_arr[sidx, tidx, 1].transAxes)
#                    axis_arr[sidx, tidx, 1].text(0.1, 0.2, '{:>6}: {:.2f}\n{:>6}: {:.2f}\n{:>6}: {:.2e}\n{:>6}: {:8f}\n{:>6}: {:.2f}'\
#                    .format('mean', mean_bl, 'median', med_bl, 'std', std_bl, 'cv', std_bl/mean_bl, 'mean_f', 1000/mean_bl,\
#                    fontsize='x-small'), verticalalignment='center', family='monospace', transform=axis_arr[sidx, tidx, 1].transAxes)

        axis_arr[0,0,0].set_ylabel('ISI [ms]')

        return axis_arr

    def plot_subpop_freq(self, spk_subp, gid_freq_arr, axis_arr, bins=50, orien='horizontal'):
        for sidx in range(spk_subp.shape[0]):
            for tidx in np.arange(gid_freq_arr.shape[1]):
                concat_subpop_freq = gid_freq_arr[spk_subp[sidx,0], tidx]
                axis_arr[sidx, tidx, 0].hist(concat_subpop_freq, bins=bins, orientation=orien, color='k', alpha=0.3)
                axis_arr[sidx, tidx, 1].boxplot(concat_subpop_freq, showmeans=True, vert=True)
                mean = np.mean(concat_subpop_freq) 
                median = np.median(concat_subpop_freq) 
                std = np.std(concat_subpop_freq) 
                axis_arr[sidx, tidx, 1].text(0.1, 0.5, '{:>8}: {:.2f}\n{:>8}: {:.2f}\n{:>8}: {:.2e}\n{:>8}: {:8f}\n{:>8}: {:.2f}'\
                .format('mean', mean, 'median', median, 'std', std, 'cv', std/mean, 'mean_ISI', 1000/mean,\
                fontsize='x-small'), verticalalignment='center', family='monospace', transform=axis_arr[sidx, tidx, 1].transAxes)

        axis_arr[0,0,0].set_ylabel('Firing rate [spk/s]')

        return axis_arr

    # Plot spike counts
    def plot_subpop_spkcounts(self, spk_subp, gid_spk_cnt, tbin, axis_arr):
        tposts = np.arange(self.tmin, self.tmax+tbin, tbin)
        for sidx in range(spk_subp.shape[0]):
            spk_cnt_ax = axis_arr[sidx].twinx()
            spk_cnt_ax.bar(tposts, np.sum(gid_spk_cnt[spk_subp[sidx,0],:], axis=0), width=tbin, align='edge', alpha=0.1, color='k')
            spk_cnt_ax.yaxis.tick_right()
            spk_cnt_ax.yaxis.set_label_text('Spike Count', color='k')
        return axis_arr

    def plot_subpop_curr(self, axis_arr, tim, subpop_allcurr):
        N_shape = subpop_allcurr.shape 
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-2,2))
        col = ['k', 'b', 'r', 'g', 'c', 'm', 'y']
  #      lab = [r'$I_{int}$', r'$I_{cap}$', r'$I_{AMPA}$', r'$I_{NMDA}$', r'$I_{GABA}$', r'$I_{syn}$']
        lab = [r'$I_{AMPA}$', r'$I_{NMDA}$', r'$I_{GABA}$', r'$I_{syn}$']
        for sidx in range(N_shape[0]):
            idx = 0
   #         for i in [0,5]:
   #             axis_arr[sidx].plot(tim, subpop_allcurr[sidx,i,:].T, lw=0.3, color=col[idx], label=lab[idx])
   #             idx += 1
            axis_arr[sidx].yaxis.set_major_formatter(formatter)
           # axis_arr[sidx].legend(loc=2)
   #         ax = axis_arr[sidx].twinx()
            ax = axis_arr[sidx]
            for i in range(1,5):
                ax.plot(tim, subpop_allcurr[sidx,i,:].T, lw=0.3, color=col[idx], label=lab[idx])
                idx += 1
            ax.yaxis.set_major_formatter(formatter)
            ax.legend(loc=1, framealpha=0)

        return axis_arr


    def plot_powspec_curr(self, axis_arr, tim, curr):
        N_shape = axis_arr.shape 
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-2,2))
        timpoidx = self.timpostidx
        for sidx in range(N_shape[0]):
            for tidx in range(self.timeints.size):
                beg_idx = timpoidx[tidx]
                end_idx = timpoidx[tidx+1]
                sig = curr[sidx, beg_idx:end_idx]
             #   fft_sig = np.fft.rfft(sig)
             #   freq = np.fft.rfftfreq(sig.size, d=self.trec/1000)
             #   axis_arr[sidx, tidx, 0].plot(freq, abs(fft_sig)**2, lw=0.4)
             #   axis_arr[sidx, tidx, 1].plot(freq, np.angle(fft_sig), 'g', lw=0.4)
                axis_arr[sidx, tidx, 0].magnitude_spectrum(sig, Fs=self.fs, pad_to=2*sig.size , lw=0.6)
                axis_arr[sidx, tidx, 0].yaxis.set_major_formatter(formatter)
#                axis_arr[sidx, tidx, 1].angle_spectrum(sig, Fs=self.fs, pad_to=2*sig.size, color='g', lw=0.6)
#                axis_arr[sidx, tidx, 1].yaxis.set_label_position('right')
                axis_arr[sidx, tidx, 0].set_xlim([0,128])
                axis_arr[sidx, tidx, 0].axvspan(2,8, color='k', alpha=0.05)
                axis_arr[sidx, tidx, 0].axvspan(12,30, color='k', alpha=0.05)
        return axis_arr
        
    def plot_spec_welch(self, axis_arr, tim, curr):
        N_shape = axis_arr.shape 
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-2,2))
        timpoidx = self.timpostidx
        for sidx in range(N_shape[0]):
            for tidx in range(self.timeints.size):
                beg_idx = timpoidx[tidx]
                end_idx = timpoidx[tidx+1]
                sig = curr[sidx, beg_idx:end_idx]
              #  f, Pxx = sps.welch(sig, fs=self.fs, window='hanning', nperseg=256, noverlap=None, nfft=2*sig.size, detrend='constant', return_onesided=True, scaling='spectrum') 
                f, Pxx = sps.welch(sig, fs=self.fs, window='hanning', nperseg=sig.size, noverlap=None, nfft=2*sig.size, detrend='constant', return_onesided=True, scaling='spectrum') 
           #     f, Pxx = sps.welch(sig, fs=self.fs, window='hanning', nperseg=sig.size, noverlap=None, nfft=2*sig.size, detrend='constant', return_onesided=True, scaling='density') 

             #   fft_sig = np.fft.rfft(sig)
             #   freq = np.fft.rfftfreq(sig.size, d=self.trec/1000)
             #   axis_arr[sidx, tidx, 0].plot(freq, abs(fft_sig)**2, lw=0.4)
             #   axis_arr[sidx, tidx, 1].plot(freq, np.angle(fft_sig), 'g', lw=0.4)
                axis_arr[sidx, tidx, 0].plot(f, Pxx, lw=0.25)
                axis_arr[sidx, tidx, 1].plot(tim[beg_idx:end_idx], sig, lw=0.1)
                axis_arr[sidx, tidx, 0].yaxis.set_major_formatter(formatter)
#                axis_arr[sidx, tidx, 1].angle_spectrum(sig, Fs=self.fs, pad_to=2*sig.size, color='g', lw=0.6)
#                axis_arr[sidx, tidx, 1].yaxis.set_label_position('right')
                axis_arr[sidx, tidx, 0].set_xlim([0,128])
                axis_arr[sidx, tidx, 0].axvspan(2,8, color='k', alpha=0.05)
                axis_arr[sidx, tidx, 0].axvspan(12,30, color='k', alpha=0.05)
        return axis_arr

    def plot_subpop_specs(self, subpop_spec_mat, axis_arr, freq, tim, subpop_curr, spec_band_av, orientation='horizontal'):
        N_subplot = subpop_spec_mat.shape[0]
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-2,2))

        for sidx in range(N_subplot):
            self.plot_specs(subpop_spec_mat[sidx,:,:], axis_arr[sidx], freq, tim, subpop_curr[sidx,:], spec_band_av[sidx,:,:], formatter, orientation='horizontal')
   #         self.plot_specs(subpop_spec_mat[1,:,:], axis_arr[0], freq, tim, subpop_curr[1,:], spec_band_av[1,:,:], formatter, orientation=orientation)

        #    img = axis_arr[sidx,0].imshow(subpop_spec_mat[sidx,:,:], cmap='YlOrRd', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        #    axis_arr[sidx,0].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
        #    axis_arr[sidx,0].yaxis.set_label_position('right')
        #    curax = axis_arr[sidx,0].twinx()
        #    curax.plot(tim, subpop_curr[sidx,:], 'k', lw=0.2)
        #    curax.yaxis.set_major_formatter(formatter)
        #    curax.yaxis.set_label_position('left')
        #    cbar.ax.yaxis.set_offset_position('left')
        #    cbar.update_ticks()

        return axis_arr

    def plot_specs(self, spec_mat, axis, freq, tim, subpop_curr, spec_band_av, formatter, orientation='horizontal'):
      #  img = axis.imshow(spec_mat, cmap='YlOrRd', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        img = axis.imshow(spec_mat, cmap='plasma', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        axis.yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
        axis.set_ylabel('Frequency [Hz]')
#        axis.yaxis.set_label_position('right')
        axis.yaxis.grid(b=True, lw=0.1, ls =':', alpha=0.3)
    #    cbarax, kw = mpl.colorbar.make_axes(axis, location='bottom', fraction=0.05, pad=0)

        cbarax, kw = mpl.colorbar.make_axes_gridspec(axis, orientation=orientation, fraction=0.05, pad=0.1) 
    #    cbarax, kw = mpl.colorbar.make_axes_gridspec(axis, orientation=orientation, fraction=0.05, pad=0.05) 

        cbar = mpl.colorbar.Colorbar(cbarax, img, format=formatter, **kw)
     #   cbar.ax.yaxis.set_offset_position('left')
     #   cbar.update_ticks()
        curax = axis.twinx()
      #  mrk = [r'$\delta$', r'$\theta$', r'$\alpha$', r'$\beta$', r'$\gamma$', r'$\Theta$', r'$B$', '.', ',']
        lwsz = [0.3, 0.3, 0.3, 0.3, 0.3, 0.8, 0.8]
#        curax.plot(tim, subpop_curr, 'k', lw=0.2)
        # bands = [(2,8,'theta', 0.5, '_'), (12, 30, 'beta', 0.5, '_')]
        bands = self.band_posts
        colr = ['r', 'w']
        leg_lst = []
        for i, sig in enumerate(spec_band_av):
            leg = r'$\{!s}$'.format(bands[i][2])
           # curax.plot(tim, sig, 'k', lw=0.3, marker=mrk[i], markevery=1000)
          #  curax.plot(tim, spec_band_av.T, lw=0.5)
          #  curax.plot(tim, sig, lw=bands[i][3], color=colr[i] , marker=leg, markersize=5, markeredgewidth=0.1, markevery=10000)
            curax.plot(tim, sig, lw=0.7, color=colr[i] , marker=leg, markersize=8, markeredgewidth=0.1, markevery=10000)
            leg_lst.append('{!s}: [{:d}-{:d}] Hz, AUC: {:.3e}'.format(leg, bands[i][0], bands[i][1], np.sum(sig)))
        
       # curax.legend(['[2,4]', '[4,8]', '[8,16]', '[16,32]', '[>32]', r'$\Theta$ [2-8]', r'$B$ [12-30]'])
        curax.legend(leg_lst, loc=2, framealpha=0)
        curax = sing_style_axis(curax, left=False, right=True)
        curax.yaxis.set_major_formatter(formatter)
        curax.yaxis.set_label_position('right')
        curax.set_ylabel('Spectral power')
        return axis

    def get_STN_subpop(self, expt_path):
        STN_subpop = self.file[expt_path]['props']['STN_subpop']
        STNsubpop_nonempty = []
        for STN_sidx, STN_s in enumerate(STN_subpop):
            if len(STN_s):
                STNsubpop_nonempty.append(STN_sidx)
        N_STN_subpop = len(STNsubpop_nonempty) 
        N_gids = len(self.file[expt_path]['props']['gids_STN']) 
        N_STN_subpop = min(N_STN_subpop, N_gids)

        subpop = np.empty(shape=(len(STN_subpop), 2), dtype='O') 
        idx_ini = 0
   #     for sidx, STNpop in enumerate(STN_subpop):
        for sidx in STNsubpop_nonempty: 
            STNpop = STN_subpop[sidx]
            subpop[sidx,0] = np.array(STNpop)
            subpop[sidx,1] = idx_ini + np.arange(subpop[sidx,0].size)    
            idx_ini = subpop[sidx, 1][-1] + 1
    #        N_gids += subpop[sidx,0].size
        return subpop, N_gids

    def upd_axis_ticks(self, axis, feeds, N_feeds):
#        curr_ticks_locs = list(axis.get_xticks())
#      #  curr_ticks_labels = [ w.get_text() for w in axis.get_xticklabels()]
#        curr_ticks_labels = ['{:d}'.format(int(loc)) for loc in curr_ticks_locs] 
#   
#      #  print(curr_ticks_locs)
#        curr_ticks_locs+=[1037, 2017]
#        curr_ticks_labels+=[r'$\blacktriangle$', r'$\blacktriangle$']
#        print(curr_ticks_locs)
#        print(curr_ticks_labels)
#      #  print(curr_ticks_locs)
#      #  print(curr_ticks_lines[-1].get_xdata())
#      #  print(curr_ticks_lines[-1].get_ydata())
#      #  curr_ticks_lines = np.append(curr_ticks_lines, [lines.Line2D((0,),(1,), marker="^")])
#      #  print(curr_ticks_lines)
#
#        axis.set_xticks(curr_ticks_locs)
#        axis.set_xticklabels(curr_ticks_labels)
     #   labels = []

        fed_col = ['b', 'r', 'g', 'c', 'm', 'y']
        axis.axes.set_xlim((self.tmin, self.tmax))
        curr_locs = axis.get_majorticklocs().copy()
        labels = [text.Text(pos, 0, '{:d}'.format(int(pos))) for pos in curr_locs] 

        for fed in range(N_feeds):
            feed = feeds[fed+1]
            curr_locs = np.append(curr_locs, feed)
            axis.set_ticks(curr_locs)
            idx = -2*(np.arange(feed.size)+1)
            tks = axis.get_ticklines()
            for i in idx:
                tks[i].set_marker('^')
                tks[i].set_markersize(8)
                tks[i].set_color(fed_col[fed])

        axis.set_ticklabels(labels)

class MultiAnalysis():
    def __init__(self, fils, tbreaks=[500, 1000, 1500, 2500, 2750], tmin=500, tmax=4500, ad_fd=0, ad_nt=0, subpop_p=[0.85, 0.05], filt_dict={}):
        self.fils = fils
        self.add_feeds = ad_fd
        self.add_nets = ad_nt
        self.tbreaks = tbreaks 
        self.tmin = tmin
        self.tmax = tmax
        self.subpop_p = subpop_p
        self.consistency_check()
        self.get_params()
        self.filt_dict = dict.fromkeys(['param', 'feed'], {}) 
        self.filt_dict.update(filt_dict)

        self.run_defaults()

    def run_defaults(self):
      #  print(self.fil_ary[:,2:])


#        self.unique_accs()
        self.get_params_vals()
        self.track_diff_keys()
#        self.track_pars_diffs()
        self.track_combo_keys()
        self.track_all_diff_keys()
        self.track_pars_diff_keys()
        self.track_uniq_param_comb()

        self.track_uniq_extfd_pars()
        self.track_uniq_net_pars()

#    #    self.get_param_path()
        self.get_param_feed_net_mat()
        self.get_sim_paths()

 #       print(self.pars_dif_keys)
 #       print(self.pars_dif_vals)
 #       print(self.fil_ary[:,11])
 #       print(self.par_vals_fac[:,1])
#        print(self.uniq_fdnt_acc)
#        for unq in srt_uq_lst
#        print(np.sum(self.fil_ary[:,2]))
#        print(self.uniq_feed_pars)
#        print(self.uniq_net_pars)

         

    def consistency_check(self):
        for fil in self.fils:
            if not fil.endswith('hdf5'):
                print('Invalid HDF5 file: {!s}'.format(fil))
                sys.exit() 
         
        # Define parameters that need consistency checks.
        self.Numerics = ['dt', 'trec', 'trec_ini', 'tstop']
        self.Ignore_spc = set(['CLUSTER', 'JOB_ID', 'LOC_start', 'NODE_ID', 'QOS', 'UTC_start', 'comment', 'n_hosts', 'sim_prefix', 'save_dir', 'sys_name'])
        self.Net_Feed = set(['ext_feed_file', 'net_file', 'fixed_hier', 'qFeedx_up', 'N_Params', 'N_extfeed_trials', 'N_network_trials'])
        self.Ignore_attrs = self.Ignore_spc.union(self.Net_Feed)
    
        self.N_fils = len(self.fils)
        self.fil_ary = np.empty(shape=(self.N_fils, 9), dtype='O')

        for filidx, fil in enumerate(self.fils):
            # Store file object - in parallel
            self.fil_ary[filidx,0] = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

            # Get file attributes           
            self.fil_ary[filidx,1] = set(self.fil_ary[filidx,0].attrs.keys()) - self.Ignore_attrs

        # Check for attribute mismatchs, exit if mismatch
        for i in range(1, self.N_fils):
            dif_atrs = self.fil_ary[0,1].symmetric_difference(self.fil_ary[i,1])
            if len(dif_atrs) > 0:
                print('Attributes mismatch: {!s} vs {!s}'.format(self.fils[0], self.fils[i]))
                sys.exit() 
            else:
                for nums in self.Numerics:    
                    if self.fil_ary[0,0].attrs[nums] != self.fil_ary[i,0].attrs[nums]:
                        print('Numerics mismatch: {!s} - {!s} vs {!s}'.format(nums, self.fils[0], self.fils[i]))
                        sys.exit() 
                    else:
                        setattr(self, '{!s}'.format(nums), self.fil_ary[0,0].attrs[nums])

    def load_Ana_class(self):
        self.ana_class = np.empty(shape=self.N_fils, dtype='O')
        for filidx in range(self.N_fils):
            # move later
           # self.ana_class = Analysis(self.fil_ary[filidx,0], tbreaks=self.tbreaks) 
            self.ana_class[filidx] = Analysis(self.fil_ary[filidx,0], tmin=self.tmin, tmax=self.tmax, tbreaks=self.tbreaks) 
        self.alltimpts = self.ana_class[0].alltimpts

    def close_files(self):
        for fil in self.fil_ary[:,0]:
            fil.close()     

    def get_params(self):
        self.N_feeds = 0
        for filidx in range(self.N_fils):
            self.fil_ary[filidx,2] = np.sort(np.char.decode(self.fil_ary[filidx,0].attrs['ext_feed_file']))
            self.fil_ary[filidx,3] = np.sort(np.char.decode(self.fil_ary[filidx,0].attrs['net_file']))

            # Set paths based on storage hierarchy
            if self.fil_ary[filidx,0].attrs['fixed_hier'] == 'extfeed':
                fd_jrn = 'P000'
                nt_jrn = 'P000/{!s}/F000'.format(self.fil_ary[filidx,2][0])
            else:
                fd_jrn = 'P000/{!s}/N000'.format(self.fil_ary[filidx,3][0])
                nt_jrn = 'P000'
                
            self.fil_ary[filidx,4] = np.array([fd_jrn, nt_jrn]) 

            # Get feed attributes
            self.fil_ary[filidx,5] = np.empty(shape=(self.fil_ary[filidx,2].size, 2), dtype='O')
         #   self.fil_ary[filidx,5] = np.empty(shape=(self.fil_ary[filidx,2].size, 2), dtype=feed_dt)
            for fdidx, fd in enumerate(self.fil_ary[filidx,2]):
                self.N_feeds += 1
                feed = self.fil_ary[filidx,0][self.fil_ary[filidx,4][0]][fd]
                self.fil_ary[filidx,5][fdidx,0] = self.get_feed_att(feed, fd)
                fd_seed = [feed['F{:03d}'.format(i)].attrs['Seed'] for i in range(self.fil_ary[filidx,0].attrs['N_extfeed_trials'])]
                self.fil_ary[filidx,5][fdidx,1] = np.array(fd_seed, dtype=int) 

            # Get net attributes
            self.fil_ary[filidx,6] = np.empty(shape=(self.fil_ary[filidx,3].size, 2), dtype='O')
            for ntidx, nt in enumerate(self.fil_ary[filidx,3]):
                net = self.fil_ary[filidx,0][self.fil_ary[filidx,4][1]][nt]
                self.fil_ary[filidx,6][ntidx,0] = self.get_net_att(net)
                nt_seed = [net['N{:03d}'.format(i)].attrs['Seed'] for i in range(self.fil_ary[filidx,0].attrs['N_network_trials'])]
                self.fil_ary[filidx,6][ntidx,1] = np.array(nt_seed, dtype=int) 

            # Get the parameter combos
            N_params = self.fil_ary[filidx,0].attrs['N_Params']
            self.fil_ary[filidx,7] = np.empty(shape=N_params, dtype=dict)
            for i in range(N_params):
                self.fil_ary[filidx,7][i] = {k:float('{:.3e}'.format(v)) for k,v in self.fil_ary[filidx,0]['P{:03d}'.format(i)].attrs.items()}

            # Store combo keys - if any
            self.fil_ary[filidx,8] = np.array(list(self.fil_ary[filidx,7][0].keys())) if len(self.fil_ary[filidx,7][0]) else np.array([])


    def get_params_vals(self):
        # Reads vals and tracks factors 
        self.par_vals_fac = np.empty(shape=(self.N_fils, 2), dtype='O')
        for i in range(self.N_fils):
                self.par_vals_fac[i,:] = self.valid_val(i)

    def track_diff_keys(self): 
        pars_dif_keys_lst = []
        pars_dif_track_pairs = []
        self.kys_track = np.empty(shape=len(self.fil_ary[0,1]),dtype='O') 
    
        # Key-centric perspective 
        for kidx, k in enumerate(self.fil_ary[0,1]):
            self.kys_track[kidx] = k    
            if not k.startswith('Rec'):
                for i,j in it.combinations(range(self.N_fils), 2):
                    if np.setxor1d(self.par_vals_fac[i,0][kidx], self.par_vals_fac[j,0][kidx]).size:
                        pars_dif_keys_lst.append((k, kidx)) 
                        pars_dif_track_pairs.append([i,j]) 
    
        pars_dif_keys = self.get_uniq_pars(pars_dif_keys_lst, pars_dif_track_pairs)
        for kidx, key in enumerate(pars_dif_keys):
            pars_dif_keys[kidx, 1] = np.unique(key[1])

        # np arr of keys with value differences across files
        self.pars_dif_keys = pars_dif_keys

    def track_combo_keys(self):
        combo_keys = [] 
        for filidx in range(self.N_fils):
            fil_combo = self.fil_ary[filidx,8]
            if fil_combo.size: 
                for k in fil_combo: 
                    combo_keys.append(k) 

        uniq_comb_parm = np.unique(combo_keys)
    
        self.combo_keys = np.empty(shape=uniq_comb_parm.shape[0], dtype='O')
        for kidx, key in enumerate(uniq_comb_parm): 
            key_pos = np.where(self.kys_track==key)[0][0]
            self.combo_keys[kidx] = (key, key_pos) 

    def track_all_diff_keys(self):
        all_diff_keys  = np.union1d(self.pars_dif_keys[:,0], self.combo_keys)
        self.all_diff_keys = np.empty(shape=(all_diff_keys.shape[0], 2), dtype='O')
        for kidx, k in enumerate(all_diff_keys):
            self.all_diff_keys[kidx, :] = k

    def track_pars_diff_keys(self):
        # np arr of diff values
        self.pars_dif_vals = np.empty(shape=(self.N_fils, self.all_diff_keys.shape[0]), dtype=np.ndarray)

        for i in range(self.N_fils):
            for kidx, k in enumerate(self.all_diff_keys):
                val = self.par_vals_fac[i,0][k[1]] # k[1] is position of specific key
                vals = np.array([float('{:.3e}'.format(v)) for v in val]) if val.shape else np.array([float('{:.3e}'.format(val))]) 
                self.pars_dif_vals[i, kidx] = vals 

    def track_uniq_param_comb(self):
        kys = self.all_diff_keys 
        N_kys = kys.shape[0]
    #    non_fac_keys = np.empty(shape=self.N_fils, dtype='O') 
        single_params = []

        fil_key_pos = np.empty(shape=(self.N_fils), dtype=np.ndarray)
        for filidx in range(self.N_fils):
            tmp_lst = []
            for key in self.fil_ary[filidx, 8]:
                key_pos = np.where(kys[:,0]==key)[0][0]
                tmp_lst.append(key_pos)
            if len(tmp_lst) == 0: single_params.append(filidx)
            fil_key_pos[filidx] = np.array(tmp_lst)


        param_comb_lst = []
        trk_param_comb_lst = []

        if N_kys:
        # if there exist any param value differences within/across files
            for i in range(self.N_fils):
                cart_prod_vals = []
                for combidx in range(N_kys):
                    cart_prod_vals.append(self.pars_dif_vals[i, combidx])
                for p in it.product(*cart_prod_vals):
                    param_comb_lst.append(p)    
                    trk_param_comb_lst.append(i)

            uniq_param_comb = self.get_uniq_pars(param_comb_lst, trk_param_comb_lst)
            self.uniq_param_comb = np.empty(shape=(uniq_param_comb.shape[0], 3), dtype=np.ndarray)
            self.uniq_param_comb[:, :2] = uniq_param_comb
            for pc in range(self.uniq_param_comb.shape[0]):
                self.uniq_param_comb[pc,2] = np.full(self.uniq_param_comb[pc,1].shape[0], np.nan)

            for pidx, prm in enumerate(self.uniq_param_comb[:,0]):
                dcmp = np.array(prm) 
                for filpos, fil in enumerate(self.uniq_param_comb[pidx,1]):
                    if not fil in single_params:
                        kcoord = fil_key_pos[fil]
                        if kcoord.shape[0]:
                            rel_val = tuple(dcmp[kcoord])
                            for param_idx, prm_dct in enumerate(self.fil_ary[fil, 7]):
                                fil_val = tuple([prm_dct[k] for k in kys[kcoord,0]])
                                if rel_val == fil_val:
                                    self.uniq_param_comb[pidx, 2][filpos] = param_idx   
                    else:
                        self.uniq_param_comb[pidx, 2][filpos] = 0
                    
            prune_par_list = []
            for pidx, prm in enumerate(self.uniq_param_comb[:,0]):
                if np.isnan(self.uniq_param_comb[pidx,2]).all():
                    prune_par_list.append(pidx)

            self.uniq_param_comb = np.delete(self.uniq_param_comb, prune_par_list, axis=0)

            for pidx, prm in enumerate(self.uniq_param_comb[:,0]):
                nan_pos = np.where(np.isnan(self.uniq_param_comb[pidx,2])==True)[0]
                if nan_pos.size:
                    self.uniq_param_comb[pidx,1] = np.delete(self.uniq_param_comb[pidx,1], nan_pos)
                    self.uniq_param_comb[pidx,2] = np.delete(self.uniq_param_comb[pidx,2], nan_pos).astype(int)
                else:
                    self.uniq_param_comb[pidx,2] =  self.uniq_param_comb[pidx,2].astype(int)

        else:
            self.uniq_param_comb = np.empty(shape=(1, 3), dtype=np.ndarray)
            self.uniq_param_comb[0,0] = ()
            self.uniq_param_comb[0,1] = np.arange(self.N_fils) 
            self.uniq_param_comb[0,2] = np.zeros(self.N_fils, dtype=int) 
        
        param_cls = Params(self)
        self.uniq_param_combo = param_cls.filt_param_arr


    def track_uniq_extfd_pars(self):
        fd_cls = Feeds(self)
        self.uniq_feed_pars = fd_cls.uniq_feeds

#        extfd_lst = []
#        trck_fil = []
#        
#        for filidx in range(self.N_fils):
#            for fd_att in self.fil_ary[filidx,5][:,0]:
#                extfd_lst.append(tuple(fd_att[0]))
#                trck_fil.append(filidx)
#
#        _, uniq_ind, inv_ind = np.unique(extfd_lst, return_index=True, return_inverse=True, axis=0)
#        trck_lst = [[] for i in uniq_ind]
#        for ind, fil in zip(inv_ind, trck_fil):
#            trck_lst[ind].append(fil)
#
#        uniq_feeds = np.empty(shape=(uniq_ind.shape[0]), dtype=feed_dt)
#        ufd_filtrk = np.empty(shape=(uniq_ind.shape[0]), dtype=np.ndarray)
#        for fdidx in np.arange(uniq_ind.shape[0]):
#            uniq_feeds[fdidx] = extfd_lst[uniq_ind[fdidx]]
#            ufd_filtrk[fdidx] = np.array(trck_lst[fdidx])
#
#        self.uniq_feed_pars = ([uniq_feeds, ufd_filtrk])
    #    self.uniq_feed_pars = self.get_uniq_pars(extfd_lst, trck_fil)

    def get_feed_att(self, feed, fd_name):
        tmp_lst = []
        Ctx_kys = [k for k in feed.attrs.keys() if k.startswith('Ctx_STN_Feed')]
        Ctx_kys.sort()
        if 'Ctx_STN_Feed0_sig' in Ctx_kys:
            k = len(Ctx_kys)//6
            for i in range(k):
                fd = 'Ctx_STN_Feed{:d}'.format(i)
                tmp_lst.append(fd) 
                val = feed.attrs['{!s}_sig'.format(fd)]  
              #  val_lst = [self.valid_att(att.decode(), new_fd_at=True) for att in val[0]]
                val_lst = [self.valid_att(att, new_fd_at=True) for att in val[0]]
                for v in val_lst:
                    tmp_lst.append(v)
                tmp_lst.append(feed.attrs['{!s}_target_subpop'.format(fd)])
                tmp_lst.append(feed.attrs['{!s}_prop_ass_subpop'.format(fd)])
                tmp_lst.append(feed.attrs['{!s}_prop_oth_subpop'.format(fd)])
        else:
            for k in Ctx_kys:
                tmp_lst.append(k)
                vals = self.dec_feed_att(feed.attrs[k].decode())
                for val in vals:
                    tmp_lst.append(val)
                tmp_lst.append(self.subpop_p[0])
                tmp_lst.append(self.subpop_p[1])

#                   1       2       3           4     5     6   7   8   9   10      11              12      13      14      15      16   17   18   19  20  21  
#['Ctx_STN_Feed0', 1000.0, 2000.0, 'regular', 500.0, inf, 3.0, 5.0, 0, 1.0, 0.1, 'Ctx_STN_Feed1', 1037.0, 2037.0, 'regular', 500.0, inf, 3.0, 5.0, 1, 1.0, 0.1]
#feed_dt = np.dtype('U16, U16, f4, f4, f4, f4, i4, i4, ,i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, i4, f4, f4')
#        idx_map = [3,1,2,4,5,6,7,8,9,10, 14,12,13,15,16,17,18,19,20,21]

#fed_spc = np.dtype('U16, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
#feed_dt = np.dtype([('spcs', fed_spc), ('TgtSub', np.int, 2)])
        idx_map = [3,1,2,4,5,6,7,9,10, 14,12,13,15,16,17,18,20,21]

        fd_obj = np.empty(1, dtype=feed_dt)
        fd_obj[0]['spcs'][0] = fd_name
        for idx, i in enumerate(idx_map):
            fd_obj[0]['spcs'][idx+1] = tmp_lst[i]

        fd_obj[0]['TgtSub'] = tmp_lst[8:20:11] 

#        return tuple(tmp_lst)
        return fd_obj 

    def get_net_att(self, net):
        # To be updated as and when required!
        return None

    def track_uniq_net_pars(self):
        
        net_lst = []
        trck_fil = []        

        for filidx in range(self.N_fils):
            for nt, nt_att in zip(self.fil_ary[filidx,3], self.fil_ary[filidx,6][:,0]): 
                net_lst.append((nt, nt_att))
                trck_fil.append(filidx)

        self.uniq_net_pars = self.get_uniq_pars(net_lst, trck_fil)

        cls_net = Nets(self)
        
        self.uniq_net_pars = cls_net.net_arr

    def get_param_path(self):
        self.par_fil = np.empty(shape=self.N_fils, dtype='O')
        pars_kys = self.pars_dif_keys[:,0]

        if pars_kys.size:
            for filidx in range(self.N_fils):
                fil_Npar = self.fil_ary[filidx,2]
                if fil_Npar > 1:
                    par_obj = self.fil_ary[filidx,0]['P000'].attrs
                    kys = list(par_obj.keys()) 
                    kys_idx = np.array([np.where(pars_kys == k)[0][0] for k in kys]) 
                    kys_out_idx = np.delete(range(pars_kys.size), kys_idx)
                    tmp_par = np.full((fil_Npar, pars_kys.size), np.nan)
                    for n in range(fil_Npar):
                        for i in kys_idx:
                            tmp_par[n,i] = float('{:.3e}'.format(self.fil_ary[filidx,0]['P{:03d}'.format(n)].attrs[pars_kys[i]]))
                        for j in kys_out_idx:
                            tmp_par[n,j] = float('{:.3e}'.format(self.fil_ary[filidx,0].attrs[pars_kys[j]])) 
                else:
                    tmp_par = np.full((1,pars_kys.size), np.nan)
                    for kidx, k in enumerate(pars_kys):
                        tmp_par[0,kidx] = float('{:.3e}'.format(self.fil_ary[filidx,0].attrs[k]))
                        
                self.par_fil[filidx] = tmp_par            

            for param in self.uniq_param_comb:
                for filidx in param[1]:
                    for pridx, prm in enumerate(self.par_fil[filidx]):
                       # if np.isclose(tuple(prm), param[0], atol=1e-19).all():
                        if tuple(prm) == param[0]:
                            param[2].append('P{:03d}'.format(pridx))

        else:
            self.uniq_param_comb[:,2] = [['P000'] for itm in  self.uniq_param_comb[:,0]]

    def get_param_feed_net_mat(self):
        N_comb = self.uniq_param_combo.shape[0]
        N_exfd = self.uniq_feed_pars.shape[0]
        N_net = self.uniq_net_pars.shape[0]

#        self.param_fdnt_mat = np.empty(shape=(N_comb, N_exfd, N_net, 2), dtype=np.ndarray)
        self.param_fdnt_mat = np.empty(shape=(N_comb, N_exfd, N_net), dtype=pfnc_dt)
        self.param_feed_trk = []

        for c, f, n in it.product(range(N_comb), range(N_exfd), range(N_net)):
          #  prm_fd = reduce(np.intersect1d, (self.uniq_param_comb[c,1], self.uniq_feed_pars[f,1], self.uniq_net_pars[n,1])) 
            # comb and combo NOT same
            prm_fd = reduce(np.intersect1d, [self.uniq_param_combo[c]['fils'][:,0], self.uniq_feed_pars[f]['fils'], self.uniq_net_pars[n]['fils']]) 
            self.param_fdnt_mat[c,f,n]['fils'] = prm_fd 
            if prm_fd.size:
                self.param_feed_trk.append((c,f,n))
                # Store seeds
                tmp = np.empty(shape=(prm_fd.size,2), dtype=np.ndarray)
                for filidx, fil in enumerate(prm_fd):
                    fd_pos = np.where(self.fil_ary[fil,2]==self.uniq_feed_pars[f]['fd']['f0'])[0][0]
                    tmp[filidx,0] =  self.fil_ary[fil,5][fd_pos,1]
                    nt_pos = np.where(self.fil_ary[fil,3]==self.uniq_net_pars[n]['nt'])[0][0]
                    tmp[filidx,1] =  self.fil_ary[fil,6][nt_pos,1] 
                self.param_fdnt_mat[c,f,n]['fd_seeds'] = tmp[:,0] 
                self.param_fdnt_mat[c,f,n]['nt_seeds'] = tmp[:,1] 

        self.param_feed_trk = np.array(self.param_feed_trk)

    def get_sim_paths(self, purge='same_seed'):
        add_feeds = self.add_feeds
        add_nets = self.add_nets
        N_comb = self.uniq_param_comb.shape[0]
        N_exfd = self.uniq_feed_pars.shape[0]
        N_net = self.uniq_net_pars.shape[0]
      #  self.sim_path_pre = np.empty((N_comb, N_exfd, N_net, 2), dtype=np.ndarray)
        self.sim_path_pre = np.empty((N_comb, N_exfd, N_net), dtype=pfnc_dt)

        # Deal with the networks
        for c,f,n in self.param_feed_trk:
            relv_fils = self.param_fdnt_mat[c,f,n]['fils'] 
            Nn_relv_fils = np.array([self.fil_ary[fil,6][0,1].shape[0] for fil in relv_fils]) 

        Nn_min = min(min(Nn_relv_fils), add_nets+1) 

        if add_feeds == 0:
            for c,f,n in self.param_feed_trk:
                self.sim_path_pre[c,f,n]['fils'] = np.array([self.param_fdnt_mat[c,f,n]['fils'][0]])   # store the file pointers 
                self.sim_path_pre[c,f,n]['fd_seeds'] = np.array([[0]])  # store the feed seeds pos  
#                self.sim_path_pre[c,f,n,1][0,1] = np.array([0])  # store the net seeds pos  
                self.sim_path_pre[c,f,n]['nt_seeds'] = np.array([np.arange(Nn_min)])


        elif add_feeds > 0 and purge == 'same_seed':
            for c,f,n in self.param_feed_trk:
                relv_fils = self.param_fdnt_mat[c,f,n]['fils'] 
                Nf_relv_fils = np.array([self.fil_ary[fil,5].shape[0] for fil in relv_fils])
                Nf_max = max(Nf_relv_fils)

                # Ignore Network variations FOR NOW!!
              #  Nn_max = max(Nn_relv_fils)
              #  Nn_min = min(Nn_max, add_nets+1)

                if Nf_max >= add_feeds+1:
                    idx = np.where(Nf_relv_fils == Nf_max)[0][0]    
                    #HACK net

                    self.sim_path_pre[c,f,n]['fils'] = np.array([relv_fils[idx]]) #Store file number of max
#                    self.sim_path_pre[c,f,n,1] = np.empty(shape=(1,2), dtype=np.ndarray) 
                    self.sim_path_pre[c,f,n]['fd_seeds'] = np.array([np.arange(add_feeds+1)])  # store the feed seeds pos  
                  #  self.sim_path_pre[c,f,n,1][0,1] = np.arange(Nn_min)  # store the net seeds pos  
                    self.sim_path_pre[c,f,n]['nt_seeds'] = np.array([np.arange(Nn_min)])
                    

                else:
                    seed_list = []
                    trk_list = []
                    for fil in relv_fils:
                        fd_pos = np.where(self.fil_ary[fil,2]==self.uniq_feed_pars[f]['fd'][0])[0][0]
                        for sdidx, seed in enumerate(self.fil_ary[fil,5][fd_pos,1]):
                            seed_list.append(seed)
                            trk_list.append((fil, sdidx))

                    uq_seeds = self.get_uniq_pars(seed_list, trk_list) 
                    Nf_min = min(len(uq_seeds), add_feeds+1)
                    trm_uq_seeds = uq_seeds[:Nf_min]
                    refil_ar = []
                    refil_sd = []

                    for _, pos in trm_uq_seeds:
                        refil_ar.append(pos[0][0])
                        refil_sd.append(pos[0][1])

                    fil_pos = self.get_uniq_pars(refil_ar, refil_sd)

                    self.sim_path_pre[c,f,n]['fils'] = np.array(fil_pos[:,0])
                  #  self.sim_path_pre[c,f,n,1] = np.empty(shape=(fil_pos.shape[0],2), dtype=np.ndarray)
                    self.sim_path_pre[c,f,n]['fd_seeds'] = np.empty(shape=(fil_pos.shape[0]), dtype=np.ndarray)
                    self.sim_path_pre[c,f,n]['nt_seeds'] = np.empty(shape=(fil_pos.shape[0]), dtype=np.ndarray)

                    #HACK
                 #   Nn_min = 1 

                    for fpidx, fildx in enumerate(self.sim_path_pre[c,f,n]['fils']):
                       # self.sim_path_pre[c,f,n,1][fpidx,0] = np.array(fil_pos[fpidx,1])
                        self.sim_path_pre[c,f,n]['fd_seeds'][fpidx] = np.array(fil_pos[fpidx,1])
                       # self.sim_path_pre[c,f,n,1][fpidx,1] = np.arange(min(add_nets+1, self.fil_ary[fildx, 9].size))
                      #  self.sim_path_pre[c,f,n,1][fpidx,1] = np.array([0])
                    #    self.sim_path_pre[c,f,n,1][fpidx,1] = np.arange(Nn_min)
                        self.sim_path_pre[c,f,n]['nt_seeds'][fpidx] = np.array(np.arange(Nn_min))

        self.sim_path_mat = np.empty((N_comb, N_exfd, N_net), dtype=np.ndarray)

        sim_paths = []
        for trkidx, (c,f,n) in enumerate(self.param_feed_trk):
            self.sim_path_mat[c,f,n] = []
            for rfilidx, r_fil in  enumerate(self.sim_path_pre[c,f,n]['fils']):
                pos_rfil_uc = np.where(self.uniq_param_comb[c][1] == r_fil)[0][0]
               # fil_sd = self.sim_path_pre[c,f,n,1][rfilidx,:]
                fil_sd_fd = self.sim_path_pre[c,f,n]['fd_seeds'][rfilidx]
                fil_sd_nt = self.sim_path_pre[c,f,n]['nt_seeds'][rfilidx]
                fil_sd_fd.sort()
                fil_sd_nt.sort()

                tri_idx = 0
                for itm, jtm in it.product(fil_sd_fd, fil_sd_nt):        
                   # self.sim_path_mat[c,f,n].append((r_fil, self.uniq_param_comb[c][2][0], self.uniq_feed_pars[f][0][0], 'F{:03d}'.format(itm), 'SimpleNet4', 'N{:03d}'.format(jtm)))        
                    sim = 'P{:03d}'.format(self.uniq_param_combo[c]['fils'][pos_rfil_uc,1]), self.uniq_feed_pars[f]['fd'][0], 'F{:03d}'.format(itm), self.uniq_net_pars[n]['nt'], 'N{:03d}'.format(jtm)
                    self.sim_path_mat[c,f,n].append((r_fil, sim)) 
                    sim_paths.append((r_fil, os.path.join(*sim), (c,f,n), tri_idx, trkidx))
                    tri_idx += 1

   #     self.sim_paths = np.empty(shape=(len(sim_paths),5), dtype=np.ndarray)
        self.sim_paths = np.empty(shape=(len(sim_paths)), dtype=sim_pat)
        for sidx, sim in enumerate(sim_paths):
            self.sim_paths[sidx]= sim

    def seeds_acc(self, fils, att_idx, add_att):
        relv_fils = fils 
        N_relv_fils  = self.fil_ary[relv_fils,att_idx]
        Nf_max = max(N_relv_fils)
        if Nf_max >= add_att+1:
            idx = np.where(N_relv_fils == Nf_max)[0]    
            self.sim_path_pre[c,f,0] = np.array(relv_fils[idx])
            self.sim_path_pre[c,f,1] = self.param_fdnt_mat[c,f,1][idx][:add_feeds+1] 
        else:
            seed_list = []
            trk_list = []
            for fil in relv_fils:
                for sdidx, seed in enumerate(self.fil_ary[fil,8]):
                    seed_list.append(seed)
                    trk_list.append((fil, sdidx))
        
            uq_seeds = self.get_uniq_pars(seed_list, trk_list) 
            Nf_min = min(len(uq_seeds), add_feeds+1)
            trm_uq_seeds = uq_seeds[:Nf_min]
            refil_ar = []
            refil_sd = []
        
            for _, pos in trm_uq_seeds:
                refil_ar.append(pos[0][0])
                refil_sd.append(pos[0][1])

    def get_uniq_pars(self, var_lst, trk_lst):
        uniq_lst = sorted(set(var_lst))
        uniq_lst_pars = np.empty(shape=(len(uniq_lst),2), dtype='O')
        uniq_lst_pars[:,0] = uniq_lst
        uniq_lst_pars[:,1] = [[] for itm in uniq_lst] 

        for fdp, fl in zip (var_lst, trk_lst):
            for sridx, uq in enumerate(uniq_lst_pars[:,0]):
                if fdp == uq:
                    uniq_lst_pars[sridx,1].append(fl)

        uniq_lst_pars[:,1] = [np.array(el) for el in uniq_lst_pars[:,1]]

        return uniq_lst_pars


    def dec_feed_att(self, stg):
        txt =  stg[10:].split(')')[0].split(',')
        return [self.valid_att(txt[i]) for i in range(7)]

    def valid_att(self, att, new_fd_at=False):
        att = att.strip()
        if att == 'nan': att = np.inf 
        try:
            return float(att)
        except ValueError:
            if new_fd_at == False:
                return np.inf if att == 'None' else att[1:-1]
            else:
                return np.inf if att == 'None' else att
            
    def valid_val(self, fidx):
        par_vals = np.empty(shape=len(self.fil_ary[0,1]), dtype='O')
 
        pars_fac = [] 
        fil_ar = self.fil_ary[fidx, 0]
        for kidx, k in enumerate(self.fil_ary[fidx,1]):
            val = self.fil_ary[fidx,0].attrs[k]
            if isinstance(val, str): 
                if val.startswith('*'):
                    splt = val.split('*') 
                    fac = float(splt[1])
                    par = splt[2]
                    par_val = fil_ar.attrs[par]
                    gh = fac*par_val
                    tmp = [float('{:.3e}'.format(tv) )for tv in gh]
                    par_vals[kidx] = np.array(tmp) 
                    pars_fac.append((par, k, par_val, par_vals[kidx], fac)) 
                else:
                    par_vals[kidx] = val 
            else:
                par_vals[kidx] = val 

        return par_vals, pars_fac

class VarEffects():
    def __init__(self, multanaclas, obj, rel_var, add_att=[]):
        self.multclas = multanaclas
        self.obj = obj
        self.rel_var = rel_var
        self.add_att = add_att
    
        if obj == 'param':
            self.get_rel_param()
        elif obj == 'feed':
            self.get_rel_feed()
        elif obj == 'net':
            self.get_rel_net()
        else:
            print('Invalid object')

    def get_rel_param(self):
        par = self.rel_var 
        ignr_par = []
        rel_vals = []
        for k,v in self.add_att.items():
            if v ==  None:
                ignr_par.append(k)
            else:
                rel_vals.append((k, np.array(v)))
    
        uniq_combo_lst = self.multclas.uniq_param_comb[:,0]
        comb_par = self.multclas.all_diff_keys[:,0]

        # Load values for different 
        uniq_combo_mat = np.empty(shape=(len(uniq_combo_lst), comb_par.shape[0]))
        for (i, j), obj in np.ndenumerate(uniq_combo_mat): 
            uniq_combo_mat[i,j] = uniq_combo_lst[i][j]

        # Get key positions
        par_pos_ar = np.where(comb_par==par)[0]
        ign_pos = [] 

        if len(rel_vals) == 0:
            rel_vals.append((par, uniq_combo_mat[:,par_pos_ar[0]]))

        for pr in ignr_par:
            igprpos = np.where(comb_par==pr)[0]
            if igprpos.size:
                ign_pos.append(igprpos[0])    
    
        if par_pos_ar.size:
            # Position of rel key
            par_pos = par_pos_ar[0]

            if len(comb_par) == 1 and comb_par[0] == par:
            # Check if single comb_par matches rel par => ignore ign_par
                all_c = [i for i in range(len(uniq_combo_lst))]
                com_par = np.array([])
            elif len(comb_par) == len(ign_pos) +1:
            # If rel par and ign_par span comb_par => ignore ign_par
                all_c = [i for i in range(len(uniq_combo_lst))]
                com_par = np.array([])
            else: 
                ign_pos.append(par_pos)
                # Get uniq vals of param that can be varied
                uniq_vals = np.unique(uniq_combo_mat[:,par_pos], return_inverse=True)
                N_uniq_v = uniq_vals[0].size
                uniq_val_mat = np.empty(shape=(N_uniq_v, 2), dtype='O')
                for i in range(N_uniq_v):
                    uniq_val_mat[i,0] = uniq_vals[0][i] 
                    uniq_val_mat[i,1] = [] 
                for j, p in np.ndenumerate(uniq_vals[1]):
                    uniq_val_mat[p,1].append(j[0])
        
                # Check for control of variables for non-ign params
                com_par = np.delete(np.arange(comb_par.shape[0]), ign_pos)
                com_mat = uniq_combo_mat[:,com_par]
    
                all_c = []        
                val_par_mat = np.empty(shape=(N_uniq_v, N_uniq_v), dtype=np.ndarray)        
                for i,j in it.combinations(range(N_uniq_v), 2):
                    i_rows = uniq_val_mat[i,1]
                    j_rows = uniq_val_mat[j,1]
                    row_com  = []
                    for k,l in it.product(i_rows, j_rows):
                        if np.array_equal(com_mat[k,:], com_mat[l,:]):
                      #  if np.allclose(com_mat[k,:], com_mat[l,:]):
                            row_com.append((k,l))
                            all_c.append(k)
                            all_c.append(l)
                    val_par_mat[i,j] = np.array(row_com)


            if len(all_c): 
                all_c = np.unique(all_c)
                # comb_stat: tracks positions of rel c for each rel par
                comb_stat = np.empty(shape=(comb_par.size), dtype=np.ndarray) 
                k_track = []
                for k in rel_vals:
                    # Get key position
                    k_pos = np.where(comb_par==k[0])[0][0]
                    k_track.append(k_pos)
                    if set(k[1]) == set(uniq_combo_mat[all_c,k_pos]):
                        # keep all c's if all rel vals = vals in uniq combo
                        comb_stat[k_pos] = np.arange(all_c.size)    
                    else:
                        # => rel vals are only a proper subset of those in uniq vals
                        kval_lst = []
                        for kval in k[1]:
                            # => for each val of the rel param, track position
                            kval_lst.append(np.where(uniq_combo_mat[all_c,k_pos]==kval)[0])
                        comb_stat[k_pos] = np.concatenate(kval_lst)

                cmn_comb_idx = reduce(np.intersect1d,comb_stat[k_track])

                if len(k_track):
                    all_c = all_c[cmn_comb_idx] 

                lbl_c = np.empty(shape=all_c.shape[0], dtype='O')
                for cidx, c in enumerate(all_c):
                    cmb =  uniq_combo_mat[c,:]
                    lb = '{!s}: {:.3e} '.format(par[:-7], cmb[par_pos])
                    for i in com_par:
                        lb += '{!s}: {:.3e} '.format(comb_par[i][:-7], cmb[i])         
                    lbl_c[cidx] = lb.rstrip()
        
             #   return all_c, lbl_c
            else:
                print('Cannot vary param: no control of variables found')
                all_c = np.array([])
                lbl_c = np.array([])
              #  print(uniq_combo_mat[:,com_par])
    
        else:
            print('Parameter absent in Param Combo')
            all_c = np.array([])
            lbl_c = np.array([])
           # return np.array([]), np.array([])
        self.rel_param = all_c, lbl_c
    
    def get_rel_feed(self):
        par_d = self.rel_var
        par = par_d[0]
        uniq_feeds = self.multclas.uniq_feed_pars
        par_pos = []
        fd_lbl = []
        fd_spcs = par_d[1] 

      #  fd_spcs = {'EInt':((0,501),(0,501)), 'EDur':((500,1000),(500,1500)), 'EOvl':None, 'EPrO':None, 'EPsO':None, 'Seg':((0.7,1.0),(0.7,1))}
     #   fd_spcs = {'EInt':((0,501),(0,501)), 'EDur':((150,2000),(150,1500)), 'EOvl': (0, 2000), 'Seg':((0.7,1.0),(0.7,1))}
        fd_spcs_kys = set(fd_spcs.keys())
        # {'EInt':None, 'EDur':None, 'EOvl':None, 'EPrO':None, 'EPsO':None, 'Seg':None} 

        par_idx = np.where(uniq_feeds[0]['f0']==par)[0]


        if 'Seg' in fd_spcs_kys: 
            par_idx = self.get_idx('Seg', par_idx, fd_spcs, uniq_feeds=uniq_feeds) 

        if 'EInt' in fd_spcs_kys:
            par_idx = self.get_idx('EInt', par_idx, fd_spcs, uniq_feeds=uniq_feeds) 


        if fd_spcs_kys.intersection(['EDur', 'EOvl', 'EPrO', 'EPsO']):
            F0leadsF1 = np.where(uniq_feeds[0]['f2']<=uniq_feeds[0]['f12'])[0] 
            F1leadsF0 = np.delete(np.arange(uniq_feeds[0].shape[0]), F0leadsF1)
        # ('F22NH2', 'regular', 1000., 2000., 500., inf, 3, 5, 0, 1., 0.1, 'regular', 1037., 2037., 500., inf, 3, 5, 1, 1., 0.1) 

            tim_pairs = [(1, 0, 'Dur0'), (3, 2, 'Dur1'), (1, 2, 'Ovl'), (2, 0, 'PrO'), (3, 1, 'PsO')] 
            tim_props = np.full(shape=(uniq_feeds[0].shape[0], len(tim_pairs)+6), fill_value=np.nan)    
            # f0t0 f0te f1t0 f1te f0leadsf1 leadtarget . . .

            F0_seq = ('f2', 'f3', 'f12', 'f13')
            F1_seq = ('f12', 'f13', 'f2', 'f3')

            for idx, i in enumerate(F0_seq):
                tim_props[F0leadsF1, idx] = uniq_feeds[0][i][F0leadsF1] 
                tim_props[F0leadsF1, 4] = 0 
                tim_props[F0leadsF1, 5] = uniq_feeds[0]['f8'][F0leadsF1] 
            for idx, i in enumerate(F1_seq):
                tim_props[F1leadsF0, idx] = uniq_feeds[0][i][F1leadsF0] 
                tim_props[F1leadsF0, 4] = 1 
                tim_props[F1leadsF0, 5] = uniq_feeds[0]['f18'][F1leadsF0] 

            for idx, tp in enumerate(tim_pairs):
                tim_props[:,6+idx] = tim_props[:,tp[0]] - tim_props[:,tp[1]]

#            for tidx, tp in enumerate(tim_pairs):
#                tim_props[:,tidx] = uniq_feeds[0][tp[0]] - uniq_feeds[0][tp[1]]

        if 'EDur' in fd_spcs_kys:
            par_idx = self.get_idx('EDur', par_idx, fd_spcs, tim_props=tim_props) 

        if 'EOvl' in fd_spcs_kys:
            par_idx = self.get_idx('EOvl', par_idx, fd_spcs, tim_props=tim_props) 

        if 'EPrO' in fd_spcs_kys:
            par_idx = self.get_idx('EPrO', par_idx, fd_spcs, tim_props=tim_props) 

        if 'EPsO' in fd_spcs_kys:
            par_idx = self.get_idx('EPsO', par_idx, fd_spcs, tim_props=tim_props) 

        for idx in par_idx:
            fd = uniq_feeds[0][idx]
            par_pos.append(idx)
        # ('F22NH2', 'regular', 1000., 2000., 500., inf, 3, 5, 0, 1., 0.1, 'regular', 1037., 2037., 500., inf, 3, 5, 1, 1., 0.1) 
            fd_lbl.append('{!s}_{:03d}S{:03d}_DL{:04d}_{:04d}DR{:04d}_{:03d}A{:03d}_{:02d}B{:02d}'.
                format(fd[0], int(100*fd[9]), int(100*fd[19]), int(fd[2]-fd[12]), int(fd[3]-fd[2]), int(fd[13]-fd[12]), int(fd[4]), int(fd[14]), int(fd[6]), int(fd[16])))

        if not len(par_pos):
            print('No feed: {!s}'.format(par))    
        self.rel_feed = np.array(par_pos, dtype=int), np.array(fd_lbl), uniq_feeds[0][par_idx]

    def get_rel_net(self):
        par = self.rel_var
        uniq_nets = self.multclas.uniq_net_pars
        par_pos = []
        nt_lbl = []
        for i, nt in enumerate(uniq_nets):
            if nt[0][0] == par:
                par_pos.append(i)
                nt_lbl.append(None)
        if not len(par_pos):
            print('No net: {!s}'.format(par))    
        self.rel_net = np.array(par_pos, dtype=int), np.array(nt_lbl)

    def get_idx(self, spec, par_idx, fd_spcs, uniq_feeds=None, tim_props=None):  
        # ('F22NH2', 'regular', 1000., 2000., 500., inf, 3, 5, 0, 1., 0.1, 'regular', 1037., 2037., 500., inf, 3, 5, 1, 1., 0.1) 
        col_pos_dict = {
                        'Seg': ('f9', 'f19'),
                        'EInt': ('f4', 'f14'), 
                        'EDur': (6,7),
                        'EOvl': (8,),
                        'EPrO': (9,),
                        'EPsO': (10,),
                        }
        spec_props = fd_spcs[spec] 
        spec_add = col_pos_dict[spec]

        if not uniq_feeds is None:
            spec0 = uniq_feeds[0][par_idx][spec_add[0]]
            spec0_idx = np.where((spec0 >= spec_props[0][0]) & (spec0 <= spec_props[0][1]))[0] 
            par_idx = par_idx[spec0_idx]
            spec1 = uniq_feeds[0][par_idx][spec_add[1]]
            spec1_idx = np.where((spec1 >= spec_props[1][0]) & (spec1 <= spec_props[1][1]))[0] 
            par_idx = par_idx[spec1_idx]
        elif not tim_props is None:
            if spec == 'EDur':
                spec0 = tim_props[par_idx][:,spec_add[0]] 
                spec0_idx = np.where((spec0 >= spec_props[0][0]) & (spec0 <= spec_props[0][1]))[0] 
                par_idx = par_idx[spec0_idx]
                spec1 = tim_props[par_idx][:,spec_add[1]] 
                spec1_idx = np.where((spec1 >= spec_props[1][0]) & (spec1 <= spec_props[1][1]))[0] 
                par_idx = par_idx[spec1_idx]
            else:
                spec0 = tim_props[par_idx][:,spec_add[0]] 
                spec0_idx = np.where((spec0 >= spec_props[0]) & (spec0 <= spec_props[1]))[0] 
                par_idx = par_idx[spec0_idx]
            
        return par_idx

class Feeds():
    def __init__(self, multanacls):
        self.multana = multanacls
        self.N_fils = self.multana.N_fils
        self.N_feeds = self.multana.N_feeds
        self.N_subpop_idx = np.arange(4)
     #   self.filt_dict = filt_dict
        #self.filt_dict = {
        #                    'Low': [('fds', ['F22SH2', 'F22NH4']), ('Int1', [(125,250)]) ],
        #                    'High': [('Dur2', [(100,250)])],
        #                } 
        self.filt_dict = self.multana.filt_dict['feed']

        self.get_feed_arr()
        self.align_feeds()
        self.filter_feeds()
        self.get_uniq_feeds()

    def get_feed_arr(self):
        self.feed_arr = np.empty(shape=self.N_feeds, dtype=fed_dat)
        idx = 0 
        for filidx in np.arange(self.N_fils):
            mult_fil = self.multana.fil_ary[filidx,:]
            for fdidx, fd in enumerate(mult_fil[5]):
                self.feed_arr[idx]['fil'] = [filidx, fdidx, mult_fil[0].attrs['N_extfeed_trials'], mult_fil[0].attrs['N_network_trials'] ,0]    
                self.feed_arr[idx]['fd'] = fd[0]['spcs'] 
                self.feed_arr[idx]['algn']['TgtSub'] = fd[0]['TgtSub'] 
                idx += 1

        self.feed_arr['fil'][:,4] = self.feed_arr['fil'][:,2] * self.feed_arr['fil'][:,3]

    def align_feeds(self):
        # fed_spc = np.dtype('U16, U16, f4, f4, f4, f4, i4, i4, f4, f4, U16, f4, f4, f4, f4, i4, i4, f4, f4')
        shft_idx = np.where(self.feed_arr['fd']['f2'] > self.feed_arr['fd']['f11'])[0]
        non_shft_idx = np.delete(np.arange(self.N_feeds), shft_idx)
        self.feed_arr['algn']['shft'][shft_idx] = True 

        self.feed_arr['algn']['t0'][non_shft_idx,0] = self.feed_arr['fd']['f2'][non_shft_idx]
        self.feed_arr['algn']['t0'][non_shft_idx,1] = self.feed_arr['fd']['f11'][non_shft_idx]
        self.feed_arr['algn']['t0'][shft_idx,0] = self.feed_arr['fd']['f11'][shft_idx]
        self.feed_arr['algn']['t0'][shft_idx,1] = self.feed_arr['fd']['f2'][shft_idx]

        self.feed_arr['algn']['tend'][non_shft_idx,0] = self.feed_arr['fd']['f3'][non_shft_idx]
        self.feed_arr['algn']['tend'][non_shft_idx,1] = self.feed_arr['fd']['f12'][non_shft_idx]
        self.feed_arr['algn']['tend'][shft_idx,0] = self.feed_arr['fd']['f12'][shft_idx]
        self.feed_arr['algn']['tend'][shft_idx,1] = self.feed_arr['fd']['f3'][shft_idx]

        self.feed_arr['algn']['Int'][non_shft_idx,0] = self.feed_arr['fd']['f4'][non_shft_idx]
        self.feed_arr['algn']['Int'][non_shft_idx,1] = self.feed_arr['fd']['f13'][non_shft_idx] 
        self.feed_arr['algn']['Int'][shft_idx,0] = self.feed_arr['fd']['f13'][shft_idx]
        self.feed_arr['algn']['Int'][shft_idx,1] = self.feed_arr['fd']['f4'][shft_idx]

        self.feed_arr['algn']['TgtSub'][shft_idx,:] = np.roll(self.feed_arr['algn']['TgtSub'][shft_idx,:], 1)

        # Correct reverse matmul assignment
        corr = True
        if corr:
            self.feed_arr['algn']['TgtSub'] = np.roll(self.feed_arr['algn']['TgtSub'], 1)

        self.feed_arr['algn']['AlgSub'][:,:2] = self.feed_arr['algn']['TgtSub'] 

        self.feed_arr['algn']['Dur'] = self.feed_arr['algn']['tend'] - self.feed_arr['algn']['t0'] 
        self.feed_arr['algn']['TPart'][:,0] = self.feed_arr['algn']['tend'][:,0] - self.feed_arr['algn']['t0'][:,1]   
        self.feed_arr['algn']['TPart'][:,1] = self.feed_arr['algn']['t0'][:,1] - self.feed_arr['algn']['t0'][:,0]   
        self.feed_arr['algn']['TPart'][:,2] = self.feed_arr['algn']['tend'][:,1] - self.feed_arr['algn']['tend'][:,0]   

        for i in range(self.N_feeds):
            self.feed_arr['algn']['AlgSub'][i,2:] = np.delete(self.N_subpop_idx, self.feed_arr['algn']['TgtSub'][i,:])

    def filter_feeds(self):
        track_idx = {}
        def_idx = np.arange(self.N_feeds)
        for k in self.filt_dict.keys():
            track_idx[k] = def_idx 
            for cons, v in self.filt_dict[k]:
                arr = self.get_field(cons)
                keep_idx = []
                if type(v[0]) is not tuple:
                    for val in v:
                        tmp_idx = np.where(arr[track_idx[k]]==val)[0]
                        keep_idx.append(track_idx[k][tmp_idx])
                        track_idx[k] = track_idx[k][np.delete(np.arange(track_idx[k].shape[0]), tmp_idx)] 
                    track_idx[k] = reduce(np.union1d, keep_idx)
                else:
                    for val in v:
                        tmp_idx = np.where((arr[track_idx[k]]>=val[0]) & (arr[track_idx[k]]<=val[1]))[0]
                        track_idx[k] = track_idx[k][np.intersect1d(np.arange(track_idx[k].shape[0]), tmp_idx, assume_unique=True)] 

        filt_idx = reduce(np.union1d, [*track_idx.values()]) if len(track_idx.values()) else def_idx 
        self.filt_feed_arr = self.feed_arr[filt_idx]


    def get_uniq_feeds(self):
        uniq_feeds, uni_pos, pos_idx = np.unique(self.filt_feed_arr['fd'], return_index=True, return_inverse=True)
        N_feeds = uniq_feeds.shape[0]
        trck_lst = [[] for i in range(N_feeds)]
        for i, idx in enumerate(pos_idx):
            trck_lst[idx].append(i)


        self.uniq_feeds = np.empty(shape=(N_feeds), dtype=fed_unq)
        self.uniq_feeds['fd'] = self.filt_feed_arr['fd'][uni_pos] 
        self.uniq_feeds['algn'] = self.filt_feed_arr['algn'][uni_pos] 
        for fdidx, _ in enumerate(uni_pos):
           # self.uniq_feeds['fils'][fdidx] = np.array(trck_lst[fdidx])
            self.uniq_feeds['fils'][fdidx] = self.filt_feed_arr['fil'][trck_lst[fdidx],0]


    def get_field(self, fld):
        fld_dict = {
                    'fds': self.feed_arr['fd']['f0'],
                    'Int1': self.feed_arr['algn']['Int'][:,0],
                    'Int2': self.feed_arr['algn']['Int'][:,1],
                    'Dur1': self.feed_arr['algn']['Dur'][:,0],
                    'Dur2': self.feed_arr['algn']['Dur'][:,1],
                    'Ovlp': self.feed_arr['algn']['TPart'][:,0],
                    'Dlay': self.feed_arr['algn']['TPart'][:,1],
                    'PstO': self.feed_arr['algn']['TPart'][:,2],
                    }
        return fld_dict[fld]

class Params():
    def __init__(self, multanacls):
        self.multana = multanacls
        self.N_fils = self.multana.N_fils
        self.N_uniq_params = self.multana.uniq_param_comb.shape[0]
     #   self.filt_dict = filt_dict
      #  self.filt_dict = {
      #               #       'Ctx_STN_A': [7.33e-06, 3.666e-05],
      #                    #  'GPe_STN': [1e-5], 
      #                  } 

        self.filt_dict = self.multana.filt_dict['param']
        self.all_diff_keys = self.multana.all_diff_keys
        self.get_param_dat()
        self.get_filtered_param()

    def get_param_dat(self):
        self.dyn_param_dt = np.dtype([(i[:-7], np.float) for i, _ in self.all_diff_keys])
        self.param_unq = np.dtype([('param', self.dyn_param_dt), ('fils', np.ndarray)])
        self.uniq_param_combo = np.empty(shape=self.N_uniq_params, dtype=self.param_unq)

        self.uniq_param_combo['param'] = self.multana.uniq_param_comb[:,0]
        for idx in np.arange(self.N_uniq_params):
            self.uniq_param_combo[idx]['fils'] = np.stack([self.multana.uniq_param_comb[idx][1], self.multana.uniq_param_comb[idx][2]], axis=1)
            
    def get_filtered_param(self):
        track_idx = {}
        def_idx = np.arange(self.N_uniq_params)
        for k,v in self.filt_dict.items():
            track_idx[k] = def_idx 
            arr = self.uniq_param_combo['param'][k]
            keep_idx = []
            for val in v:
                tmp_idx = np.where(arr[track_idx[k]]==val)[0]
                keep_idx.append(track_idx[k][tmp_idx])
                track_idx[k] = track_idx[k][np.delete(np.arange(track_idx[k].shape[0]), tmp_idx)] 
            track_idx[k] = reduce(np.union1d, keep_idx)

        filt_idx = reduce(np.intersect1d, [*track_idx.values()]) if len(self.filt_dict) else def_idx 
        self.filt_param_arr = self.uniq_param_combo[filt_idx]

class Nets():
    def __init__(self, multanacls):
        self.multana = multanacls
        uniq_nets = self.multana.uniq_net_pars 
        self.N_nets = uniq_nets.shape[0]
        self.net_arr = np.empty(shape=self.N_nets, dtype=net_unq)
        for idx in np.arange(self.N_nets):
            self.net_arr[idx]['nt'] = uniq_nets[idx][0][0]
            self.net_arr[idx]['fils'] = uniq_nets[idx][1]

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))

def get_spectral_matrix(signal, freq, tim, sT= 0.025, N=2**13, s= 0.5, fix_Ncyc = False, Ncycl=7):
    B = np.zeros((len(freq), len(tim)))
    fs= 1000/sT
    S_trans = np.transpose(tim)
    sig = sps.detrend(signal)
#    b, a = sps.butter(2, 2*np.array([0.5, 64])/fs, btype='bandpass')
  #  sig = sps.lfilter(b, a, s)
 #   sig = sps.filtfilt(b, a, s)
    for j in range(0, len(freq)):
        B[j, :] += __energyvec(freq[j], sig, width=Ncycl)
    return B 

def __energyvec(f, s, fs=1/0.025e-3,width=7):
    dt = 1. /fs
    sf = f / width
    st = 1. / (2. * np.pi * sf)
    bnd = width/2
    t = np.arange(-bnd*st, bnd*st, dt)
    m = __morlet(f, t, width=width)
    y = sps.fftconvolve(s, m)
    y = (2. * abs(y) / fs)**2
    y = y[int(np.ceil(len(m)/2.)):int(len(y)-np.floor(len(m)/2.)+1)]
    return y

def __morlet(f, t, width=7):
    sf = f / width
    st = 1. / (2. * np.pi * sf)
    A = 1. / (st * np.sqrt(2.*np.pi))
    y = A * np.exp(-t**2. / (2. * st**2.)) * np.exp(1.j * 2. * np.pi * f * t)
    return y

def my_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] += spcpow

    return spcmat

def get_morlet_transmat(freqs, fs=4e4, N=2**19, s=3.5):
    N=2**19
    trans_mor_mat = np.empty(shape=(freqs.shape[0],  N), dtype=np.complex)
    for fidx, f in enumerate(freqs):
        trans_mor_mat[fidx, :] = np.fft.fft(sps.morlet(N+1, f*(N+1)*0.5/(s*fs), s, 'True')[:-1]*np.sqrt(s), norm='ortho')
    return trans_mor_mat

def new_spectral_matrix(signal, specmor, tim, normlz=None):
    N_f, N = specmor.shape
    N_tim = tim.shape[0]
    signal = sps.detrend(signal)
    res = np.fft.rfft(signal, n=N, norm='ortho') 
    # convert to full specs
    res = np.concatenate((res, np.conjugate(np.flip(res[1:-1])))) if res.shape[0]%2 else np.concatenate((res, np.conjugate(np.flip(res[1:]))))
    filttrans = res * specmor
    filttim = np.fft.ifft(filttrans, axis=0) 
    filttim = filttim[:,:N_tim]
    filtpow = np.abs(filttim)**2
    if normlz == 'full':
        for i in range(N_f):
            filtpow[i,:] = filtpow[i,:]/filtpow[i,:].sum(axis=1)
    if normlz == 'fullperc':
        for i in range(N_f):
            fpow = filtpow[i,:].sum(axis=1)
            filtpow[i,:] = (filtpow[i,:]-fpow)/fpow
    return filtpow

def sing_style_axis(ax, top=False, right=False, bottom=True, left=True, offset=5):
    spn = ['top', 'bottom', 'right', 'left']
    for sp in spn:
        ax.spines[sp].set_position(('outward', offset))
        ax.spines[sp].set_visible(eval(sp))
  #  ax.spines['left'].set_position(('outward', offset))

   # ax.spines['top'].set_visible(top)
   # ax.spines['right'].set_visible(right)

    return ax

def unpack_gather(lst, chore_ass, N_chores):
    new_lst = np.empty(shape=N_chores, dtype='O')
    for ridx, rnk in enumerate(chore_ass):
        if rnk.size:
            for r, p in zip (rnk, lst[ridx]):
                new_lst[r] = p 
    return new_lst        

def axis_create_align(axr, gl, fig, xlab='', ylab=''):
    gl_gem = gl.get_geometry()
    ax_shp = axr.shape
    if gl_gem == ax_shp:
        axr[0,0] = fig.add_subplot(gl[0,0])
        for i in range(1,gl_gem[1]):
            axr[0,i] = fig.add_subplot(gl[0,i], sharey=axr[0,0])
        for j in range(1,gl_gem[0]):
            axr[j,0] = fig.add_subplot(gl[j,0], sharex=axr[0,0], sharey=axr[0,0])
            axr[j,0].set_ylabel(ylab)
        for i,j in it.product(range(1,gl_gem[0]), range(1, gl_gem[1])):
            axr[i,j] = fig.add_subplot(gl[i,j], sharex=axr[0,j], sharey=axr[i,0])
        for j in range(gl_gem[1]):
            axr[-1,j].set_xlabel(xlab)
        axr[0,0].set_ylabel(ylab)
    else:
        print('Axis array and Gridspec geometry mismatch')

    return axr

def axis_create_inset(axr, xlab='', ylab=''):
    ax_shp = axr.shape
    for i, j in it.product(range(ax_shp[0]), range(ax_shp[1])):
        axr[i,j,1] = inset_axes(axr[i, j, 0], width="70%", height="60%", loc=1)
        axr[i,j,1].patch.set_alpha(0)
    axr[0,0,1].set_xlabel(xlab)
    axr[0,0,1].set_ylabel(ylab)
    return axr

if __name__ == '__main__':
    fils = sys.argv[1:]

    if fils[0].endswith('.hdf5'):

 #       exec_multiana(fils)

       # exec_GLM_ana(fils)    
       # exec_plot(fils[0], tbreaks= [500, 3500])
       # exec_analysis(fils[0], tbreaks=[1000, 1500, 2000, 2500, 4000]) 
        multana = MultiAnalysis(fils, tbreaks=[500, 2500]) 
      #  multana.track_uniq_extfd_pars()
      #  print(multana.uniq_feed_pars['fils'])
        multana.run_defaults()
    #    cls_fd = Feeds(multana) 
#        print(cls_fd.filt_feed_arr)
    #    cls_prm = Params(multana)
#        print(cls_prm.filt_param_arr)
        cls_nt = Nets(multana)
       # exec_V(fil)
       # exec_SS(fil)
       # consistency_check(fils)
       #MultiAnalysis(fils)
 #       Run_MultiAna(fils)
    else:
        print('Invalid HDF5 file... exiting')
