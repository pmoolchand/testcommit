# templatefeed.py - template dictionary structure for ext_feed
#
# ver: 0.0
# rev: 2017-01-17

ext_feed = {
    'Seed': [-1],
    'STN': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'uyuICulamp0': {
            'sig': [(500, 1000, 'flat', 25e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        },
        'tyiIClamp1': {
            'sig': [(2000,2500, 'lin', 0.00005e-5, 10e-5)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        }
    },
    'GPeP': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'ertyIClamp0': {
            'sig': [(000, 3000, 'flat', -0.65e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'ifdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },

    'GPeA': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'errevIClamp0': {
            'sig': [(000, 3000, 'flat', -0.8e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'fdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },
    'Ctx_STN': {
        'main_seed':-1 ,
       #  'subpop_ass': 'Match',
         'subpop_ass': 'Specific',
       # 'subpop_ass': 'RoundRobin',
       # 'subpop_ass': 'Random',

        'Feed0' : {
            'sig': [(1500, 1510, 'normal', 3, 2, 1, 2),
 (1525, 1535, 'normal', 3, 2, 1, 2),
 (1550, 1560, 'normal', 3, 2, 1, 2),
 (1575, 1585, 'normal', 3, 2, 1, 2),
 (1600, 1610, 'normal', 3, 2, 1, 2),
 (1625, 1635, 'normal', 3, 2, 1, 2),
 (1650, 1660, 'normal', 3, 2, 1, 2),
 (1675, 1685, 'normal', 3, 2, 1, 2),
 (1700, 1710, 'normal', 3, 2, 1, 2),
 (1725, 1735, 'normal', 3, 2, 1, 2),
 (1750, 1760, 'normal', 3, 2, 1, 2),
 (1775, 1785, 'normal', 3, 2, 1, 2)], 
            'f_seed': -1,
            'prop_ass_subpop': 0.25,
            'prop_oth_subpop': 0.25,
            'pos_gids': None,
            'target_subpop': 0,
        },
        'Feed1' : {
            'sig': [(1510, 1520, 'normal', 3, 2, 1, 2),
 (1535, 1545, 'normal', 3, 2, 1, 2),
 (1560, 1570, 'normal', 3, 2, 1, 2),
 (1585, 1595, 'normal', 3, 2, 1, 2),
 (1610, 1620, 'normal', 3, 2, 1, 2),
 (1635, 1645, 'normal', 3, 2, 1, 2),
 (1660, 1670, 'normal', 3, 2, 1, 2),
 (1685, 1695, 'normal', 3, 2, 1, 2),
 (1710, 1720, 'normal', 3, 2, 1, 2),
 (1735, 1745, 'normal', 3, 2, 1, 2),
 (1760, 1770, 'normal', 3, 2, 1, 2),
 (1785, 1795, 'normal', 3, 2, 1, 2)], 
            'f_seed': -1,
            'prop_ass_subpop': 0.25,
            'prop_oth_subpop': 0.25,
            'pos_gids': None,
            'target_subpop': 1,
        },
        'uFeed2' : {
            'sig': [(250, 260, 'regular', 25, 5, 1, 3)],
            'f_seed': -1,
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 1,
            'pos_gids': None,
            'target_subpop': 2,
        },
    },
}
