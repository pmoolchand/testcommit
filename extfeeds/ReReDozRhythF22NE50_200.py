# templatefeed.py - template dictionary structure for ext_feed
#
# ver: 0.0
# rev: 2017-01-17

ext_feed = {
    'Seed': [-1],
    'STN': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'uyuICulamp0': {
            'sig': [(500, 1000, 'flat', 25e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        },
        'tyiIClamp1': {
            'sig': [(2000,2500, 'lin', 0.00005e-5, 10e-5)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        }
    },
    'GPeP': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'ertyIClamp0': {
            'sig': [(000, 3000, 'flat', -0.65e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'ifdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },

    'GPeA': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'errevIClamp0': {
            'sig': [(000, 3000, 'flat', -0.8e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'fdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },
    'Ctx_STN': {
        'main_seed':-1 ,
       #  'subpop_ass': 'Match',
         'subpop_ass': 'Specific',
       # 'subpop_ass': 'RoundRobin',
       # 'subpop_ass': 'Random',

        'Feed0' : {
            'sig': [(1500, 1550, 'normal', 3, 2, 1, 2), (1750, 1800, 'normal', 3, 2, 1, 2), (2000, 2050, 'normal', 3, 2, 1, 2), (2250, 2300, 'normal', 3, 2, 1, 2), (2500, 2550, 'normal', 3, 2, 1, 2), (2750, 2800, 'normal', 3, 2, 1, 2), (3000, 3050, 'normal', 3, 2, 1, 2), (3250, 3300, 'normal', 3, 2, 1, 2), (3500, 3550, 'normal', 3, 2, 1, 2), (3750, 3800, 'normal', 3, 2, 1, 2), (4000, 4050, 'normal', 3, 2, 1, 2), (4250, 4300, 'normal', 3, 2, 1, 2)],
            'f_seed': -1,
            'prop_ass_subpop': 0.85,
            'prop_oth_subpop': 0.05,
            'pos_gids': None,
            'target_subpop': 0,
        },
        'Feed1' : {
            'sig': [(1505, 1555, 'normal', 3, 2, 1, 2), (1755, 1805, 'normal', 3, 2, 1, 2), (2005, 2055, 'normal', 3, 2, 1, 2), (2255, 2305, 'normal', 3, 2, 1, 2), (2505, 2555, 'normal', 3, 2, 1, 2), (2755, 2805, 'normal', 3, 2, 1, 2), (3005, 3055, 'normal', 3, 2, 1, 2), (3255, 3305, 'normal', 3, 2, 1, 2), (3505, 3555, 'normal', 3, 2, 1, 2), (3755, 3805, 'normal', 3, 2, 1, 2), (4005, 4055, 'normal', 3, 2, 1, 2), (4255, 4305, 'normal', 3, 2, 1, 2)],
            'f_seed': -1,
            'prop_ass_subpop': 0.85,
            'prop_oth_subpop': 0.05,
            'pos_gids': None,
            'target_subpop': 1,
        },
        'uFeed2' : {
            'sig': [(250, 260, 'regular', 25, 5, 1, 3)],
            'f_seed': -1,
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 1,
            'pos_gids': None,
            'target_subpop': 2,
        },
    },
}
