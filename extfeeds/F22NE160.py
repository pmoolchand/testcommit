# templatefeed.py - template dictionary structure for ext_feed
#
# ver: 0.0
# rev: 2017-01-17

ext_feed = {
    'Seed': [-1],
    'STN': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'uyuICulamp0': {
            'sig': [(500, 1000, 'flat', 25e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        },
        'tyiIClamp1': {
            'sig': [(2000,2500, 'lin', 0.00005e-5, 10e-5)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0.5,
        }
    },
    'GPeP': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'ertyIClamp0': {
            'sig': [(000, 3000, 'flat', -0.65e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'ifdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },

    'GPeA': {
        'main_seed': -1,
        'subpop_ass': 'random',
       # 'subpop_ass': 'RoundRobin',
        'errevIClamp0': {
            'sig': [(000, 3000, 'flat', -0.8e-5, None)],
            'Assign': 'Random',
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 0,
        },
        'fdIuClamp1': {
            'sig': [(10, 20, 'flat', -0.1e-5, None), (5,15.1, 'lin', 2, 0.1)],
            'Assign': 'Random',
            'prop_ass_subpop': 0.5,
            'prop_oth_subpop': 0.5,
        }
    },
    'Ctx_STN': {
        'main_seed':-1 ,
       #  'subpop_ass': 'Match',
         'subpop_ass': 'Specific',
       # 'subpop_ass': 'RoundRobin',
       # 'subpop_ass': 'Random',

        'Feed0' : {
            'sig': [(1500, 1660, 'normal', 15, 5, 1, 2)],
            'f_seed': -1,
            'prop_ass_subpop': 0.85,
            'prop_oth_subpop': 0.05,
            'pos_gids': None,
            'target_subpop': 0,
        },
        'Feed1' : {
            'sig': [(1600, 1760, 'normal', 15, 5, 1, 2)],
            'f_seed': -1,
            'prop_ass_subpop': 0.85,
            'prop_oth_subpop': 0.05,
            'pos_gids': None,
            'target_subpop': 1,
        },
        'uFeed2' : {
            'sig': [(250, 260, 'regular', 25, 5, 1, 3)],
            'f_seed': -1,
            'prop_ass_subpop': 1,
            'prop_oth_subpop': 1,
            'pos_gids': None,
            'target_subpop': 2,
        },
    },
}
