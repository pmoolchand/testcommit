# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
#    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
#         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.gridspec as gs
import matplotlib.figure as mpl_figure
import matplotlib.style as mpl_style
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

def add_labels(txt1, txt2, text, subfiglab=True, fontprop=None, offsetpad=None):
 
    ax1 = txt1[0]
    if type(txt1[1]) is mpl.text.Text:
        labtxt1 = txt1[1]
        txt1txt = True
    else:
        labtxt1 = getattr(ax1, '{!s}axis'.format(txt1[1])).get_label()    
        txt1txt = False 

    ax2 = txt2[0]
    if type(txt2[1]) is mpl.text.Text:
        labtxt2 = txt2[1]
        txt2txt = True
    else:
        labtxt2 = getattr(ax2, '{!s}axis'.format(txt2[1])).get_label()    
        txt2txt = False 

    fig = ax1.get_figure()

    if subfiglab:
        if labtxt1.get_text() != '' :
            xref = labtxt1 
        else:
            xref = ax1.get_ymajorticklabels()[0] 

        if labtxt2.get_text() != '' :
            yref = labtxt2 
        else:
            yref = ax2.get_xmajorticklabels()[0] 

        xcoords = xref.get_window_extent()
        ycoords = yref.get_window_extent()

        x,y = fig.transFigure.inverted().transform((xcoords.x0, ycoords.y1))
        font_prop = {'size': 14, 'weight': 'bold'} if fontprop is None else fontprop
        fig.text(x, y, s=text, ha='left', va='top', fontproperties=font_prop)
             
    else:
        if (txt1txt and txt2txt) or (txt1[1] == txt2[1]): 
            coords1 = labtxt1.get_window_extent() 
            coords2 = labtxt2.get_window_extent() 
            font_prop = labtxt1.get_font_properties() if fontprop is None else fontprop
            offset_pad = 3 if offsetpad is None else offsetpad

            if txt1txt: 
                x1mean = (coords1.x0 + coords1.x1)/2
                x2mean = (coords2.x0 + coords2.x1)/2
                xmean = (x1mean + x2mean)/2
                #xmean = (coords1.x1 + coords2.x0)/2
                ymean = coords1.y0
                x,y = fig.transFigure.inverted().transform((xmean, ymean))
                fig.text(x, y, s=text, ha='center', va='bottom', fontproperties=font_prop)
            elif txt1[1] == 'x': 
                xmean = (coords1.x1 + coords2.x0)/2
                ymean = coords1.y1 
                x,y = fig.transFigure.inverted().transform((xmean, ymean))
                y = y + offset_pad/(fig.get_figheight()*72)
                fig.text(x, y, s=text, ha='center', va='top', fontproperties=font_prop)
            elif txt1[1] == 'y': 
                xmean = coords1.x0
                ymean = (coords1.y0 + coords2.y1)/2
                x,y = fig.transFigure.inverted().transform((xmean, ymean))
                x = x + offset_pad/(fig.get_figwidth()*72)
                fig.text(x, y, s=text, rotation='vertical', ha='left', va='center', fontproperties=font_prop)
