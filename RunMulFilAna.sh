#!/bin/bash

#SBATCH -N 1
#SBATCH -n 48 
#SBATCH -t 2:00:00
#SBATCH --job-name=Full_Network_Swing
#SBATCH -p skx-normal 
#SBATCH --mail-type=ALL
#SBATCH --mail-user=prannath_moolchand@brown.edu

export I_MPI_EXTRA_FILESYSTEM_LIST=garbage
module load python3
module load phdf5/1.8.16

#ibrun python3 MulFilAna.py /scratch/04119/pmoolcha/STNGPe/ConflictDat.hdf5 
python3 -u MulFilAna.py /scratch/04119/pmoolcha/STNGPe/ConflictDat.hdf5 
