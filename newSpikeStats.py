#! python3

from mpi4py import MPI
import h5py, sys
import numpy as np
from AnalysisClass import Analysis



class AnalyzeSpikeStats():
    def __init__(self, fil):
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.N_hosts = MPI.COMM_WORLD.Get_size()
        datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

        tbreaks = [500, 2500]

        self.anacls = Analysis(datafile, tmin=500, tmax=2500, tbreaks=tbreaks)
        self.anacls.new_init(datafile, tmin=0., tmax=3000., tbreaks=[])

        self.get_chores(datafile)

        self.spkstat_dt = np.dtype([('sim', np.int, 1), ('means', np.float, 3),('STNPASS', np.bool, 1), ('GPPPASS', np.bool, 1), ('GPAPASS', np.bool, 1), ('AllPASS', np.bool, 1)])
        self.STN_means = [9,13]
        self.GPP_means = [29,31]
        self.GPA_means = [4.5,5.5]

        self.run_stats()

        datafile.close()

    def run_stats(self):
        N_chores = self.my_chores.shape[0]
        spk_stats = np.empty(shape=(N_chores), dtype=self.spkstat_dt) 
        spk_stats['STNPASS'] = False
        spk_stats['GPPPASS'] = False
        spk_stats['GPAPASS'] = False
        spk_stats['AllPASS'] = False
        for sim_idx, sim in enumerate(self.my_chores):
            spk_stats[sim_idx]['sim'] = sim 
            spk_stats[sim_idx]['means'] = self.spike_stats('S{:03d}'.format(sim))
             
        spk_stats['STNPASS'] = (spk_stats['means'][:,0] >= self.STN_means[0]) & (spk_stats['means'][:,0] <= self.STN_means[1])
        spk_stats['GPPPASS'] = (spk_stats['means'][:,1] >= self.GPP_means[0]) & (spk_stats['means'][:,1] <= self.GPP_means[1])
        spk_stats['GPAPASS'] = (spk_stats['means'][:,2] >= self.GPA_means[0]) & (spk_stats['means'][:,2] <= self.GPA_means[1])
        spk_stats['AllPASS'] = spk_stats['STNPASS'] & spk_stats['GPPPASS'] & spk_stats['GPAPASS'] 

        PASS_idx = np.where(spk_stats['AllPASS']==True)[0]
        for i in PASS_idx:
            print(spk_stats[i])


    def get_chores(self, datafile):
        self.sim_contents = np.array(datafile['sim_contents'])
        comp_sims = np.where(self.sim_contents['Complete']==True)[0]
        self.my_chores = comp_sims[self.rank:comp_sims.size:self.N_hosts] 

    def spike_stats(self, simpath):

        anacl = self.anacls
        N_timint = anacl.timeints.size
        expt_path = simpath 

        expt_path_txt = '' 
   #     for k,v in expt_path_att[1].items():
   #         expt_path_txt += '{!s}: {:.2e}\n'.format(k,v) 
        tim = np.array(anacl.file[expt_path]['props']['timesteps'])
        spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)
        
        all_idx_ranges = np.array(anacl.file[expt_path]['props']['gids_STN'], dtype=int)
        all_pop = np.empty(shape=(1, 2), dtype='O')
        all_pop[0,0] = all_idx_ranges
        all_pop[0,1] = all_idx_ranges
        N_gids = all_idx_ranges.size
        
        GPPgids = np.array(anacl.file[expt_path]['props']['gids_GPeP'], dtype=int)
        GPAgids = np.array(anacl.file[expt_path]['props']['gids_GPeA'], dtype=int)
        
        GPP_pop = np.empty(shape=(1, 2), dtype='O')
        GPP_pop[0,0] = np.arange(GPPgids.size) 
        GPP_pop[0,1] = GPPgids 
        GPA_pop = np.empty(shape=(1, 2), dtype='O')
        GPA_pop[0,0] = np.arange(GPAgids.size) 
        GPA_pop[0,1] = GPAgids 
        
        
        GPPpop_spks = anacl.get_gid_spk(expt_path, GPPgids)
        GPApop_spks = anacl.get_gid_spk(expt_path, GPAgids)
        STNpop_spks = anacl.get_gid_spk(expt_path, all_idx_ranges)
    
        STNfreq = anacl.get_gid_freq(STNpop_spks)  
        GPPfreq = anacl.get_gid_freq(GPPpop_spks)  
        GPAfreq = anacl.get_gid_freq(GPApop_spks)  
        
    #    print(expt_path, STNfreq[0], GPPfreq[0], GPAfreq[0])
        return STNfreq[0], GPPfreq[0], GPAfreq[0]


        

if __name__=='__main__':
    fil = sys.argv[1]
    if fil.endswith('.hdf5'):
        AnalyzeSpikeStats(fil)
