# class_net.py - establishes the Network class and related methods
#
# ver: 1.0
# rev: 2018-01-17 - Added feed combos and  assignments

import itertools as it
import numpy as np
import scipy as sp
import sys

from neuron import h as nrn
from fn.cells.GPeP import GPeP
from fn.cells.GPeA import GPeA
from fn.cells.STN import STN
from fn.simple_feed import ParSimpleFeed
import fn.extfeeds as ext_feeds
import fn.net_connect as ntcnt

# create Network class
class Network():
    def __init__(self, p, netwrk, ext_feed, RecDict):
        self.p = p

        # Parallel stuff
        self.pc = nrn.ParallelContext()
        self.n_hosts = int(self.pc.nhost())
        self.rank = int(self.pc.id())

        self.network = ntcnt.get_net_props(netwrk)
        self.ext_feed = ext_feed
        self.RecDict = RecDict

        # source (src) is defined as something that can send events elsewhere (other nodes)
        # this is N_src ON THIS NODE
        # cells are sources, but there are other sources
        self.N_src = 0
        self.N_cells = 0

        # numbers of sources
        self.N = {}

        # Source list of names
        # in particular order (cells, extinput, alpha names of unique inputs)
        self.src_list_new = self.__create_src_list()

        # cell position lists, also will give counts: must be known by ALL nodes
        self.pos_dict = dict.fromkeys(self.src_list_new)

        # create coords in pos_dict for all cells first
        self.__create_coords_STN()
        self.__create_coords_GPe()
        self.__count_cells()

        if self.N_cells:
          #  self.feed_list = ext_feeds.create_feed_list(self.ext_feed, self.network, self.p['tstop'])
          #  self.Ctx_Feed_Combos, self.STN_Ctx_Combo_idx, self.Ctx_STN_list = ext_feeds.create_feed_list(self.ext_feed, self.network, self.p['tstop'])
            self.Ctx_Feed_Combos, self.STN_Ctx_Combo_idx, self.Ctx_STN_list = ext_feeds.create_feed_list(self.ext_feed, self.network, self.p)
            self.STN_clamp_com, self.STN_clamp_idx = ext_feeds.get_Iclamp_matrix(self.network['STN']['gids'], self.ext_feed['STN'], self.p['tstop'], self.p['dt'])
            self.GPeP_clamp_com, self.GPeP_clamp_idx = ext_feeds.get_Iclamp_matrix(self.network['GPeP']['gids'], self.ext_feed['GPeP'], self.p['tstop'], self.p['dt'])
            self.GPeA_clamp_com, self.GPeA_clamp_idx = ext_feeds.get_Iclamp_matrix(self.network['GPeA']['gids'], self.ext_feed['GPeA'], self.p['tstop'], self.p['dt'])

        self.__count_extsrcs()

        # create dictionary of GIDs according to cell type
        # global dictionary of gid and cell type
        # example: self.gid_dict = {'STN': [0, 1, 2, 3], 'GPe': [4, 5]}
        self.gid_dict = self.__create_gid_dict()

        # assign gid to hosts, creates list of gids for this node in __gid_list
        # __gid_list length is number of cells assigned to this id()
        self.__gid_list = self.__gid_assign()

        # create cells
        self.cells_list = []

        # create lists of external inputs
        self.Ctx_input_list = []
        self.MSN_input_list = []

        # get connectivity matrix and feed list

        self.own_gid_dict = {'STNCell':[], 'GPePCell':[], 'GPeACell':[], 'CtxInput': [], 'MSNInput':[]}
        self.create_own_gid_dict()

        self.own_gid_obj = {'STNCell':[], 'GPePCell':[], 'GPeACell':[], 'CtxInput': [], 'MSNInput':[]}

        self.validate_own_gid_dict()

        # create sources and init
        self.__create_all_src()

        # parallel network connector
        self.__parnet_connect()

        # set to record spikes
        self.spiketimes = nrn.Vector()
        self.spikegids = nrn.Vector()
        self.__record_spikes()

    # creates the immutable source list along with corresponding numbers of cells
    def __create_src_list(self):
        # base source list of tuples, name and number, in this order
        self.cellname_list = [
            'STNCell',
            'GPePCell',
            'GPeACell',
        ]

        # list of external sources
        self.ext_src_list = [
            'CtxInput',
            'MSNInput',
        ]

        src_list = self.cellname_list + self.ext_src_list

        return src_list

    # Creates cells and grid
    # pyr grid is the immutable grid, origin now calculated in relation to feed
    def __create_coords_STN(self):
        #create list of tuples/coords, (x, y, z)
        self.pos_dict['STNCell'] = self.network['STN']['coords']

    def __create_coords_GPe(self):

        ## create list of tuples/coords, (x, y, z)
        self.pos_dict['GPePCell'] = self.network['GPeP']['coords']
        self.pos_dict['GPeACell'] = self.network['GPeA']['coords']

    # cell counting routine
    def __count_cells(self):
        # cellname list is used *only* for this purpose for now
        for src in self.cellname_list:
            # if it's a cell, then add the number to total number of cells

            self.N[src] = len(self.pos_dict[src])
            self.N_cells += self.N[src]

        return self.N_cells

    # general counting method requires pos_dict is correct for each source
    # and that all sources are represented
    def __count_extsrcs(self):
        # all src numbers are based off of length of pos_dict entry
        # generally done here in lieu of upstream changes

        self.N['CtxInput'] = self.Ctx_Feed_Combos.size * self.n_hosts
        self.N['MSNInput'] = self.n_hosts

    # creates gid dicts and pos_lists
    def __create_gid_dict(self):
        # initialize gid index gid_ind to start at 0
        gid_ind = [0]

        # append a new gid_ind based on previous and next cell count
        # order is guaranteed by self.src_list_new
        for i in range(len(self.src_list_new)):
            # N = self.src_list_new[i][1]
            # grab the src name in ordered list src_list_new
            src = self.src_list_new[i]

            # query the N dict for that number and append here to gid_ind, based on previous entry
            gid_ind.append(gid_ind[i]+self.N[src])

            # accumulate total source count
            self.N_src += self.N[src]

        # dictionary of gids for each source
        self.gid_dict = {}

        # now actually assign the ranges
        for i in range(len(self.src_list_new)):
            src = self.src_list_new[i]
            self.gid_dict[src] = range(gid_ind[i], gid_ind[i+1])

        return self.gid_dict

    # this happens on EACH node
    # creates gid_list for THIS node
    def __gid_assign(self):
        # round robin assignment of gids

        gid_list = []
        for gid in range(self.rank, self.N_cells, self.n_hosts):
            # set the cell gid
            self.pc.set_gid2node(gid, self.rank)
            gid_list.append(gid)

            # set the extInput gid
          #  self.pc.set_gid2node(gid+self.N_cells, self.rank)
          #  gid_list.append(gid+self.N_cells)
        for gid in np.array(self.gid_dict['CtxInput'][self.rank::self.n_hosts]):
            self.pc.set_gid2node(gid, self.rank)
            gid_list.append(gid)
        for gid in np.array(self.gid_dict['MSNInput'][self.rank::self.n_hosts]):
            self.pc.set_gid2node(gid, self.rank)
            gid_list.append(gid)

        # extremely important to get the gids in the right order
        gid_list.sort()

        return gid_list

    # reverse lookup of gid to type
    def gid_to_type(self, gid):
        for gidtype, gids in self.gid_dict.items():
            if gid in gids:
                return gidtype

    def create_own_gid_dict(self):
        for gid in self.__gid_list:
            self.own_gid_dict[self.gid_to_type(gid)].append(gid)

    def validate_own_gid_dict(self):
        for gid in self.__gid_list:
            if self.pc.gid_exists(gid):
            #    print(self.rank, gid, self.gid_to_type(gid))
                if gid not in self.own_gid_dict[self.gid_to_type(gid)]:
                    print('GID type mismatch')
                    exit()
            else:
                print('GID not found')
                exit()

    def __create_all_src(self):
        for gid in self.own_gid_dict['STNCell']:
            gid_idx = gid - self.gid_dict['STNCell'][0]
            pos = self.pos_dict['STNCell'][gid_idx]
            self.own_gid_obj['STNCell'].append((gid, STN(gid, pos, self.p, self.network['connectivity_matrix'], self.RecDict, self.network['STN']['ini_V'][gid_idx], self.STN_clamp_com[self.STN_clamp_idx[gid_idx],:])))
            self.pc.cell(gid, self.own_gid_obj['STNCell'][-1][1].connect_to_target(None))

        for idx, gid in enumerate(self.own_gid_dict['GPePCell']):
            gid_idx = gid - self.gid_dict['GPePCell'][0]
            pos = self.pos_dict['GPePCell'][gid_idx]
            self.own_gid_obj['GPePCell'].append((gid, GPeP(gid, pos, self.p, self.network['connectivity_matrix'], self.RecDict, self.network['GPeP']['ini_V'][gid_idx], self.GPeP_clamp_com[self.GPeP_clamp_idx[gid_idx],:])))
            self.pc.cell(gid, self.own_gid_obj['GPePCell'][-1][1].connect_to_target(None))

        for idx, gid in enumerate(self.own_gid_dict['GPeACell']):
            gid_idx = gid - self.gid_dict['GPeACell'][0]
            pos = self.pos_dict['GPeACell'][gid_idx]
            self.own_gid_obj['GPeACell'].append((gid, GPeA(gid, pos, self.p, self.network['connectivity_matrix'], self.RecDict, self.network['GPeA']['ini_V'][gid_idx], self.GPeA_clamp_com[self.GPeA_clamp_idx[gid_idx],:])))
            self.pc.cell(gid, self.own_gid_obj['GPeACell'][-1][1].connect_to_target(None))

        for idx, gid in enumerate(self.own_gid_dict['CtxInput']):
            self.own_gid_obj['CtxInput'].append((gid, ParSimpleFeed(self.Ctx_Feed_Combos[idx])))
            self.pc.cell(gid, self.own_gid_obj['CtxInput'][-1][1].connect_to_target())

        for idx, gid in enumerate(self.own_gid_dict['MSNInput']):
            self.own_gid_obj['MSNInput'].append((gid, ParSimpleFeed(self.Ctx_Feed_Combos[0])))
            self.pc.cell(gid, self.own_gid_obj['MSNInput'][-1][1].connect_to_target())

    # connections:
    # this NODE is aware of its cells as targets
    # for each syn, return list of source GIDs.
    # for each item in the list, do a:
    # nc = pc.gid_connect(source_gid, target_syn), weight,delay
    # Both for synapses AND for external inputs
    def __parnet_connect(self):
        # loop over target zipped gids and cells

        for gid, gid_cell in zip(self.own_gid_dict['STNCell'], self.own_gid_obj['STNCell']):
            gid_cell[1].parconnect(gid, self.gid_dict, self.p)
            gid_cell[1].connect_external(self.own_gid_dict['CtxInput'][self.STN_Ctx_Combo_idx[gid]])

        for gid, gid_cell in zip(self.own_gid_dict['GPePCell'], self.own_gid_obj['GPePCell']):
            gid_cell[1].parconnect(gid, self.gid_dict, self.p)
            gid_cell[1].connect_external(self.own_gid_dict['CtxInput'][0])

        for gid, gid_cell in zip(self.own_gid_dict['GPeACell'], self.own_gid_obj['GPeACell']):
            gid_cell[1].parconnect(gid, self.gid_dict, self.p)
            gid_cell[1].connect_external(self.own_gid_dict['CtxInput'][0])

    # setup spike recording for this node
    def __record_spikes(self):
        self.pc.spike_record(-1, self.spiketimes, self.spikegids)
