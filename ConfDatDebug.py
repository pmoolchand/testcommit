#! python3 

from mpi4py import MPI 
import h5py, os, sys, datetime, gc

import numpy as np 
import scipy.stats as sts
import itertools as it
from functools import reduce
from AnalysisClass import MultiAnalysis

sys_name = 'ANATAC'


fed_spc = np.dtype('S16, S16, f4, f4, f4, f4, i4, i4, f4, f4, S16, f4, f4, f4, f4, i4, i4, f4, f4')                                                                                                    
feed_dt = np.dtype([('spcs', fed_spc), ('TgtSub', np.int, 2)])
fed_alg = np.dtype([('shft', np.bool), ('t0', np.float, 2), ('tend', np.float, 2), ('Dur', np.float, 2), ('Int', np.int, 2), ('TPart', np.float, 3), ('TgtSub', np.int, 2), ('AlgSub', np.int, 4)])
fed_dat = np.dtype([('fil', np.int, 5), ('fd', fed_spc), ('algn', fed_alg)])

h5ints = h5py.special_dtype(vlen=np.int32)
h5flts = h5py.special_dtype(vlen=np.float32)
fed_unq = np.dtype([('fd', fed_spc), ('algn', fed_alg), ('fils', h5ints)])
net_unq = np.dtype([('nt', 'S16'), ('fils', h5ints)])

sim_pat = np.dtype('i4, S64, (3)i4, i4, i4')

class MultiAnaStats():
    def __init__(self, param_dict):

        # Set parallel machinery
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.N_hosts = MPI.COMM_WORLD.Get_size()
    
        for k,v in param_dict.items():
            setattr(self, k, v)
        
        # Load data order and create data array
        # Load Multiclass
        self.multclas = MultiAnalysis(self.fils, tmin=self.tmin, tmax=self.tmax, ad_fd=self.ad_fd, ad_nt=self.ad_nt, tbreaks=self.tbreaks, filt_dict=self.filt_dict)
        self.N_fils = self.multclas.N_fils
        self.dims = self.multclas.sim_path_mat.shape
        self.uniq_sims = self.multclas.param_feed_trk
        self.N_uniq_sims = self.uniq_sims.shape[0]
        self.N_subpop =  5
        self.N_fdcombo = 4
        self.N_stims = 2
   #     print(self.alltimpts)
#        print(self.multclas.sim_paths)

        self.param_combo = self.multclas.uniq_param_combo
        self.uniq_feeds = self.multclas.uniq_feed_pars
        self.uniq_nets = self.multclas.uniq_net_pars
        self.multclas.load_Ana_class()
    
        self.alltimpts = self.multclas.alltimpts
        self.N_alltimpts = self.alltimpts.shape[0]

        # Get relevant sims and assign chores
        # [self.chores, self.my_chores]
        self.get_multi_chores()
        self.N_trials = int(self.N_chores/self.N_uniq_sims)

        self.get_pop_stats()

        self.ini_datfil()

#        self.alt_ini_datfil()


        self.get_tripatts()
        self.del_vars()
        self.count_fdsb()

        for var in ['AMPA', 'NMDA', 'GABA', 'Iint']:
            self.get_timdat(var)
    
        self.get_netcurs()

        self.multclas.close_files()

    def del_vars(self):
        del self.pack
        del self.STN_subpop
        del self.STN_spk
        del self.GPP_spk
        del self.GPA_spk
        gc.collect()
        

    def ini_datfil(self):
        # Create file (parallel) & store details

        self.pack = [self.STN_subpop, self.STNfdsp, self.STN_spk, self.GPP_spk, self.GPA_spk]
        self.tmp_gather = MPI.COMM_WORLD.gather(self.pack, root=0) 

        test_file = h5py.File(self.datfilnam, 'w-', driver='mpio', comm=MPI.COMM_WORLD)
        test_file['sim_fils'] = np.array(self.multclas.fils, dtype='S')
        test_file['sim_paths'] = np.array(self.multclas.sim_paths, dtype=sim_pat) 
        test_file['uniq_combo'] = self.param_combo['param']
        test_file['uniq_sims'] = self.uniq_sims

        test_file.close()

        if self.rank == 0:
            test_file = h5py.File(self.datfilnam, 'r+')
            test_file.create_dataset('uniq_param_fils', shape=(self.param_combo['fils'].shape[0],2), dtype=h5ints)

            for idx, i in enumerate(self.param_combo['fils']):
                test_file['uniq_param_fils'][idx,0] = list(i[:,0])
                test_file['uniq_param_fils'][idx,1] = list(i[:,1])

            test_file['uniq_feeds'] = np.array(self.uniq_feeds, dtype=fed_unq) 
            test_file['uniq_nets'] = np.array(self.uniq_nets, dtype=net_unq)

            tmp_STN_spk = np.empty(shape=(self.N_uniq_sims, self.N_trials, self.N_STN), dtype=h5flts)
            tmp_GPP_spk = np.empty(shape=(self.N_uniq_sims, self.N_trials, self.N_GPP), dtype=h5flts)
            tmp_GPA_spk = np.empty(shape=(self.N_uniq_sims, self.N_trials, self.N_GPA), dtype=h5flts)

            tmp_STNsb = np.empty(shape=(self.N_uniq_sims, self.N_trials, self.N_subpop-1), dtype=h5ints)
            tmp_STNfdsp = np.empty(shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop), dtype=h5ints)

            for i in range(self.N_hosts):
                STN_subpop, STNfdsp, STN_spk, GPP_spk, GPA_spk = self.tmp_gather[i]
                host_coords =  self.chores[self.chore_ass[i]]
                coords = [(m, n) for m, n in zip(host_coords['f4'], host_coords['f3'])]
                for k, stn, gpp, gpa, ssb, fdsp in zip (coords, STN_spk, GPP_spk, GPA_spk, STN_subpop, STNfdsp):
                    tmp_STN_spk[k] = stn 
                    tmp_GPP_spk[k] = gpp 
                    tmp_GPA_spk[k] = gpa 
                    tmp_STNsb[k] = ssb
                    tmp_STNfdsp[k] = fdsp 

            test_file['STN_spk'] = tmp_STN_spk
            test_file['GPP_spk'] = tmp_GPP_spk
            test_file['GPA_spk'] = tmp_GPA_spk
            test_file['STN_sbp'] = tmp_STNsb
            test_file['STN_fdsp'] = tmp_STNfdsp
            test_file.close()

    def alt_ini_datfil(self):
        # Create file (parallel) & store details
        self.pack = None
        test_file = h5py.File(self.datfilnam, 'w-', driver='mpio', comm=MPI.COMM_WORLD)
        test_file['sim_fils'] = np.array(self.multclas.fils, dtype='S')
        test_file['sim_paths'] = np.array(self.multclas.sim_paths, dtype=sim_pat) 
        test_file['uniq_combo'] = self.param_combo['param']
        test_file['uniq_sims'] = self.uniq_sims

        test_file.create_dataset('STN_spk', shape=(self.N_uniq_sims, self.N_trials, self.N_STN), dtype=h5flts)
        test_file.create_dataset('GPP_spk', shape=(self.N_uniq_sims, self.N_trials, self.N_GPP), dtype=h5flts)
        test_file.create_dataset('GPA_spk', shape=(self.N_uniq_sims, self.N_trials, self.N_GPA), dtype=h5flts)

        test_file.create_dataset('STN_sbp', shape=(self.N_uniq_sims, self.N_trials, self.N_subpop-1), dtype=h5ints)
        test_file.create_dataset('STN_fdsp', shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop), dtype=h5ints)

        test_file.close()

        if self.rank == 0:
            test_file = h5py.File(self.datfilnam, 'r+')
            test_file.create_dataset('uniq_param_fils', shape=(self.param_combo['fils'].shape[0],2), dtype=h5ints)
        
            for idx, i in enumerate(self.param_combo['fils']):
                test_file['uniq_param_fils'][idx,0] = list(i[:,0])
                test_file['uniq_param_fils'][idx,1] = list(i[:,1])
        
            test_file['uniq_feeds'] = np.array(self.uniq_feeds, dtype=fed_unq) 
            test_file['uniq_nets'] = np.array(self.uniq_nets, dtype=net_unq)
    
            i_host = 0
            self.write_to_file(test_file)
            test_file.close()
            i_host += 1
        else:
            i_host = None    

        c_host = MPI.COMM_WORLD.bcast(i_host, root=0)
        i_host = c_host if c_host == self.rank else self.N_hosts + 1
        self.seq_write(i_host, c_host)


    def seq_write(self, i_host, c_host):
        if c_host < self.N_hosts:
            if self.rank == i_host:
                test_file = h5py.File(self.datfilnam, 'r+')
                self.write_to_file(test_file)
                test_file.close()
                i_host += 1
            
            c_host = MPI.COMM_WORLD.bcast(i_host, root=c_host) 
            i_host = c_host if c_host == self.rank else self.N_hosts + 1
            self.seq_write(i_host, c_host)

    def write_to_file(self, filobj):
        test_file = filobj
        for k, stn, gpp, gpa, ssb, fdsp in zip (self.my_coords, self.STN_spk, self.GPP_spk, self.GPA_spk, self.STN_subpop, self.STNfdsp):
            test_file['STN_spk'][k] = stn 
            test_file['GPP_spk'][k] = gpp 
            test_file['GPA_spk'][k] = gpa
            test_file['STN_sbp'][k] = ssb
            test_file['STN_fdsp'][k] = fdsp

    def test_datfil(self):
        test_file = h5py.File(self.datfilnam, 'r', driver='mpio', comm=MPI.COMM_WORLD)
        chore = self.my_chores[0] 
        u = chore['f4']
        t = chore['f3']

        test_file.close()
        
    def count_fdsb(self):       
        test_file = h5py.File(self.datfilnam, 'r+', driver='mpio', comm=MPI.COMM_WORLD)
        test_file.create_dataset('STN_fdsp_N', shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop), dtype=np.int32)
        for coord in self.my_coords:
            rel_mat = np.array(test_file['STN_fdsp'][coord])
            for idx, obj in np.ndenumerate(rel_mat):
                test_file['STN_fdsp_N'][coord+idx] = obj.shape[0]
        test_file.close()
  
    def get_netcurs(self):
        test_file = h5py.File(self.datfilnam, 'r+', driver='mpio', comm=MPI.COMM_WORLD)
        test_file.create_dataset('NetSyn', shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop, self.N_alltimpts), dtype=np.float32)
        test_file.create_dataset('NetCap', shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop, self.N_alltimpts), dtype=np.float32)
        for chidx, _ in enumerate(self.my_chores):
            coord = self.my_coords[chidx]
            AMPA = test_file['AMPA'][coord]
            NMDA = test_file['NMDA'][coord]
            GABA = test_file['GABA'][coord]
            Iint = test_file['Iint'][coord]
            NSyn = AMPA + NMDA + GABA
            NCap = NSyn + Iint
            test_file['NetSyn'][coord] = NSyn 
            test_file['NetCap'][coord] = NCap 
        test_file.close()

    def get_timdat(self, var):
        dat_dict = {
                    'AMPA': 'STNSyn_AMPA_Ctx',
                    'NMDA': 'STNSyn_NMDA_Ctx',
                    'GABA': 'STNSyn_GABA_GPe',
                    'Iint': 'STNSta_i_int',
                    }

        test_file = h5py.File(self.datfilnam, 'r+', driver='mpio', comm=MPI.COMM_WORLD)
        test_file.create_dataset(var, shape=(self.N_uniq_sims, self.N_trials, self.N_fdcombo, self.N_subpop, self.N_alltimpts), dtype=np.float32)
        for chidx, chore in enumerate(self.my_chores):
            raw_path = self.multclas.fil_ary[chore[0], 0].file[chore[1]]['raw']
            var_dat = np.array(raw_path[dat_dict[var]], dtype=np.float32)
            for idx, obj in np.ndenumerate(self.STNfdsp[chidx]):
                test_file[var][self.my_coords[chidx]+idx] = np.mean(var_dat[obj],axis=0) if obj.size else np.zeros(shape=self.N_alltimpts, dtype=np.float32)
        test_file.close()

    def get_pop_stats(self):
        self.N_mychores = self.my_chores.shape[0]

        self.STN_subpop = np.empty(shape=(self.N_mychores, self.N_subpop-1), dtype=h5ints)
        self.STNfdsp = np.empty(shape=(self.N_mychores, self.N_fdcombo, self.N_subpop), dtype=h5ints)

        self.STN_spk = np.empty(shape=(self.N_mychores), dtype=np.ndarray)
        self.GPP_spk = np.empty(shape=(self.N_mychores), dtype=np.ndarray)
        self.GPA_spk = np.empty(shape=(self.N_mychores), dtype=np.ndarray)

        fdcomb1 = np.array([1,2,3,0])
        fdcomb2 = np.array([2,1,3,0])
        for chidx, chore in enumerate(self.my_chores):
            fidx = chore[2][1]
            fd = self.multclas.uniq_feed_pars[fidx]
            AlgSub = fd['algn']['AlgSub']
            prop_path = self.multclas.fil_ary[chore[0], 0].file[chore[1]]['props']
            AlgSTNsp = np.array(prop_path['STN_subpop'])[AlgSub]
            self.STN_subpop[chidx] = AlgSTNsp
            fdcmAlg = fdcomb2 if fd['algn']['shft'] else fdcomb1
            Ctx_assgn = np.unique(prop_path['Ctx_asg_STN'], return_inverse=True) 
            tmp_lst =[[] for i in range(4)]
            for idx, i in enumerate(Ctx_assgn[1]):
                tmp_lst[Ctx_assgn[0][i]].append(idx)
            for fdci, sbpi in it.product(range(4), range(4)):
                self.STNfdsp[chidx, fdci, sbpi] = np.intersect1d(tmp_lst[fdcmAlg[fdci]], AlgSTNsp[sbpi]) 
            for i in range(4):
                self.STNfdsp[chidx, i, 4] = np.array(tmp_lst[fdcmAlg[i]]) 

            raw_path = self.multclas.fil_ary[chore[0], 0].file[chore[1]]['raw']

            raw_spk = np.array(raw_path['spikes'])
            N_cells = raw_spk.shape[0]
            idx_cells = np.array(N_cells* np.array([0.25, 0.75]), dtype=np.int)
            self.STN_spk[chidx] = raw_spk[:idx_cells[0]]
            self.GPP_spk[chidx] = raw_spk[idx_cells[0]:idx_cells[1]]
            self.GPA_spk[chidx] = raw_spk[idx_cells[1]:]

        self.N_STN = self.STN_spk[0].shape[0]
        self.N_GPP = self.GPP_spk[0].shape[0]
        self.N_GPA = self.GPA_spk[0].shape[0]


    def get_tripatts(self, b_intval=200, f_intval=400):
        test_file = h5py.File(self.datfilnam, 'r+', driver='mpio', comm=MPI.COMM_WORLD)
        test_file.create_dataset('STN_Sil', shape=(self.N_uniq_sims, self.N_trials, self.N_stims, self.N_STN), dtype=np.float32)
        test_file.create_dataset('STN_Bur', shape=(self.N_uniq_sims, self.N_trials, self.N_stims, self.N_STN), dtype=np.float32)
        test_file.create_dataset('STN_Fir', shape=(self.N_uniq_sims, self.N_trials, self.N_stims, self.N_STN), dtype=np.float32)

        STN_Sil = np.empty(shape=(self.N_mychores, self.N_stims, self.N_STN), dtype=np.float32)
        STN_Bur = np.empty(shape=(self.N_mychores, self.N_stims, self.N_STN), dtype=np.float32)
        STN_Fir = np.empty(shape=(self.N_mychores, self.N_stims, self.N_STN), dtype=np.float32)

        for chidx, chore in enumerate(self.my_chores):
            coord = self.my_coords[chidx]
            fidx = chore[2][1]
            fd = self.multclas.uniq_feed_pars[fidx]
            t0 = fd['algn']['t0']
            delay = fd['algn']['TPart'][0] 
            STN_spk = self.STN_spk[chidx]   

            for spidx, spk in enumerate(STN_spk): 
                t0_idx_all = np.searchsorted(spk, [t0, t0+f_intval])
                t0_idx = t0_idx_all[0] 
                if t0_idx[0] < spk.shape[0]:
                    algISI0 = spk[t0_idx+1]-spk[t0_idx]
                    STN_Sil[chidx,:, spidx] = algISI0 
                    bur_pos = np.searchsorted(spk[t0_idx[0]:], spk[t0_idx+1]+b_intval)
                    cnt = np.array([0,0])
                    cnt[0] = spk[t0_idx[0]:][1:bur_pos[0]].shape[0]
                    cnt[1] = spk[t0_idx[0]:][np.diff(t0_idx)[0]:bur_pos[1]].shape[0]
                    STN_Bur[chidx,:, spidx] = 1000*cnt/b_intval 
                    fir = np.array([0,0])
                    fir[0] = spk[t0_idx[0]:t0_idx_all[1,0]].shape[0]
                    fir[1] = spk[t0_idx[1]:t0_idx_all[1,1]].shape[0]
                    STN_Fir[chidx,:, spidx] = 1000*fir/f_intval 
                else:
                    STN_Sil[chidx,:, spidx] = [np.inf, np.inf] 
                    STN_Bur[chidx,:, spidx] = [0, 0] 
                    STN_Fir[chidx,:, spidx] = [0, 0] 
                    
        for chidx, coord in enumerate(self.my_coords):
            test_file['STN_Sil'][coord] = STN_Sil[chidx]
            test_file['STN_Bur'][coord] = STN_Bur[chidx]
            test_file['STN_Fir'][coord] = STN_Fir[chidx]

        test_file.close()

    def get_analysis_assgn(self):
        ana_dict = {} 
        ana_lst = []
#        rnk_arr = np.empty(self.N_hosts, dtype=np.ndarray)
        for idx, ana in enumerate(self.RunAnalysis):
            for i in self.data_dict[ana[0]][1]:
                ana_lst.append((i, ana[1], idx))
                ana_dict.update({i:[]})
#                rnk_arr[ana[1]] = np.append(rnk_arr[ana[1]], idx)

        for i, pref, idx in ana_lst:
            ana_dict[i].append(idx)

        for k, v in ana_dict.items():
            ana_dict[k] = np.sort(v)

        self.ana_dict = ana_dict
      #  self.my_analysis = self.RunAnalysis[self.rank:len(self.chores):self.N_hosts]

    def get_multi_chores(self):
        self.chores = self.multclas.sim_paths 
        # Assign chores
        self.N_chores = self.chores.shape[0]
    
        self.chore_ass = np.empty(shape=self.N_hosts, dtype=np.ndarray)
        if self.rank == 0:
            for i in range(self.N_hosts):
                self.chore_ass[i] =  np.arange(i,self.N_chores,self.N_hosts) 

        self.my_chores_idx = np.arange(self.rank, self.N_chores, self.N_hosts) 
        self.my_chores = self.chores[self.my_chores_idx]

        self.my_chorestrack = np.array([self.chores['f4'],self.chores['f3']]).T
        self.my_coords = [(m, n) for m, n in zip(self.my_chores['f4'], self.my_chores['f3'])]
        
    def get_theta_mnhs(self, cidx, anacl_obj, path, intval=300, preintval=300, intper=500):

        STN_pop = self.data_arr[self.data_position['STN_pop']][cidx][0]
        subpop_size = np.array([i.shape[0] for i in STN_pop[:,0]])
        curr, tim =  anacl_obj.get_subpop_cap_cur(STN_pop, path, Tsam=int(1))
        all_cur = np.empty(shape=(1, tim.shape[0]))
        all_cur[0,:] = np.matmul(subpop_size, curr)/subpop_size.sum()

        N_subpop = STN_pop.shape[0]
        N_axes = max(1, N_subpop) + int(N_subpop>1) 
        N_timint = 2

        T = tim[1]-tim[0]
#        freq = np.geomspace(2,8,15)
        freq = np.array([2,4])

        fidx = self.my_chores[cidx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        ft0 = fd['algn']['t0']
        ftgt = fd['algn']['TgtSub'] 
        
        rel_tidx = np.searchsorted(tim, np.concatenate([ft0, ft0-preintval, ft0+intval]))

        # syn_spec.shape = [subpop, freqs, tims]
        allpop_syn_spec = anacl_obj.get_subpop_spec(all_cur, freq, tim)
        subpop_syn_spec = anacl_obj.get_subpop_spec(curr, freq, tim)

        mnhs_mat = np.empty(shape=(N_axes, ftgt.shape[0]+1, 1), dtype=np.ndarray)
        for timidx in np.arange(N_timint):
            mnhs_mat[0, timidx, 0] = 10*np.log(np.sum(np.mean(allpop_syn_spec[0][timidx, rel_tidx[2+timidx]:rel_tidx[4+timidx]], axis=0))*1000/T)
        mnhs_mat[0, 2, 0] = 10*np.log(np.sum(np.mean(allpop_syn_spec[0][:, rel_tidx[2]:rel_tidx[5]], axis=0))*1000/T)

        if N_subpop> 1:
            non_target = np.delete(np.arange(N_subpop), ftgt)
            algn_pop = np.concatenate((ftgt, non_target))
            for pidx, _ in enumerate(algn_pop):
                for timidx in range(N_timint): 
                    mnhs_mat[pidx+1, timidx, 0] = 10*np.log10(np.sum(np.mean(subpop_syn_spec[pidx][:, rel_tidx[2+timidx]:rel_tidx[4+timidx]], axis=0))*1000/T)
                mnhs_mat[pidx+1, 2, 0] = 10*np.log10(np.sum(np.mean(subpop_syn_spec[pidx][:, rel_tidx[2]:rel_tidx[5]], axis=0))*1000/T)

        return mnhs_mat

    def gather_var(self, var, root=0):                                                                                                                                                                  
        var = np.array(MPI.COMM_WORLD.gather(var, root=root))
   #     if self.rank == root:
   #         self.unpack_gather(var)
    
    def unpack_gather(self, var):
        lst = var 
        new_lst = np.empty(shape=self.N_chores, dtype='O')
        for ridx, rnk in enumerate(self.chore_ass):
            if rnk.size:
                for r, p in zip (rnk, lst[ridx]):
                    new_lst[r] = p
        var = new_lst



def exec_multiana(fils):

    param_dict = dict(fils=fils, tmin=00, tmax=5000, ad_fd=4, ad_nt=4, tbreaks=[00,5000])
    
    param_dict['RunAnalysis'] = [
                                 ('All', 0),
#                                ('SilenceHist',0), ('SilenceBox',0), ('SilenceReg', 0),
#                                ('BurstHist',0), ('BurstBox',0), ('BurstReg', 0),
#                                ('FirHist',0), ('FirBox',0), ('FirReg', 0),
#                                ('ThetaBox',0), ('ThetaReg', 0),
                                ]

    test_dict = {
                'param': {},
#                'feed': {'Low': [('fds', ['F22SH2', 'F22NH4']), ('Int1', [(125,250)]) ],},
#                'feed': {'All': [('fds', ['F22NH2', ])]}
            }

    param_dict['datfilnam'] = 'DebugConfDat.hdf5'
    param_dict['filt_dict'] = test_dict 

    MultiAnaStats(param_dict)

 #   for k in param_set_dict.keys():
 #       param_dict['doc_prefix'] = k 
 #       param_dict['ParEffects'] = param_set_dict[param_dict['doc_prefix']] 
 #       MultiAnaStats(param_dict)

if __name__ == '__main__':
    fils = sys.argv[1:]

    if fils[0].endswith('.hdf5'):
        exec_multiana(fils) 
    else:
        print('Invalid HDF5 file... skipping')
