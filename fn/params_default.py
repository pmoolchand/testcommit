# params_default.py - master list of changeable params. most set to default val of active
#
# ver: 1.0 
# rev: 2018-01-17 (PM: Added fields for metadata, spiking threshold, master trials)

def get_params_default():
    # return {
    # set default params
    p = {
        'sim_prefix': 'default',
        'save_dir' : None,
        'sys_name': None,
        'comment': None,
        'fixed_hier': 'network',
        # simulation end time (ms)
        'tstop': 250.,
        'ResumeFile': None,
        'Resume': False,

        'dt': 0.0025,
        'trec': 0.025,
        'trec_ini': 1000,

        # external feed file
        'ext_feed_file': 'templatefeed',
        'net_file': 'templatenet',

        # update Feed props
        'qFeedx_up': None,

        # Recording options for cell_types
        'RecSTNSta_V': False,
        'RecSTNSta_m': False,
        'RecSTNSta_h': False,
        'RecSTNSta_n': False,
        'RecSTNSta_s': False,
        'RecSTNSta_a': False,
        'RecSTNSta_b': False,
        'RecSTNSta_r': False,
        'RecSTNSta_Cai': False,
        'RecSTNSta_i_int': False,
        'RecSTNSta_ina': False,
        'RecSTNSta_ik': False,
        'RecSTNSta_il': False,
        'RecSTNSta_ilca': False,
        'RecSTNSta_itca': False,
        'RecSTNSta_iahp': False,

        'RecSTNSyn_AMPA_Ctx': False,
        'RecSTNSyn_NMDA_Ctx': False,
        'RecSTNSyn_GABA_GPe': False,

        'RecSTNClm_IV': False,

        'RecGPPSta_V': False,
        'RecGPPSta_m': False,
        'RecGPPSta_h': False,
        'RecGPPSta_n': False,
        'RecGPPSta_s': False,
        'RecGPPSta_a': False,
        'RecGPPSta_r': False,
        'RecGPPSta_Cai': False,
        'RecGPPSta_i_int': False,
        'RecGPPSta_ina': False,
        'RecGPPSta_ik': False,
        'RecGPPSta_il': False,
        'RecGPPSta_ilca': False,
        'RecGPPSta_itca': False,
        'RecGPPSta_iahp': False,

        'RecGPPSyn_AMPA_STN': False,
        'RecGPPSyn_NMDA_STN': False,
        'RecGPPSyn_GABA_GPe': False,

        'RecGPPClm_IV': False,

        'RecGPASta_V': False,
        'RecGPASta_m': False,
        'RecGPASta_h': False,
        'RecGPASta_n': False,
        'RecGPASta_s': False,
        'RecGPASta_a': False,
        'RecGPASta_r': False,
        'RecGPASta_Cai': False,
        'RecGPASta_i_int': False,
        'RecGPASta_ina': False,
        'RecGPASta_ik': False,
        'RecGPASta_il': False,
        'RecGPASta_ilca': False,
        'RecGPASta_itca': False,
        'RecGPASta_iahp': False,

        'RecGPASyn_AMPA_STN': False,
        'RecGPASyn_NMDA_STN': False,
        'RecGPASyn_GABA_GPe': False,

        'RecGPAClm_IV': False,

        # Voltage Clamps
        'STN_VClamp': False,
        'GPP_VClamp': False,
        'GPA_VClamp': False,

        # numerics
        # N_trials of 0 means that seed is set by rank, will still run 1 trial (obviously)
        # 'N_trials': 1,
        'N_extfeed_trials': 1, 
        'N_network_trials': 1,

    }

    # grab cell-specific params and update p accordingly
    p_STN = get_STN_params_default()
    p_GPe = get_GPe_params_default()
    p.update(p_STN)
    p.update(p_GPe)

    return p

def get_STN_params_default():
    return {
        # Synapses

        'I_STN': 1.0,
        'STN_threshold': -12,

        # GABAA synapse (from GPe to STN)
        'GPe_STN_weight': 10.0e-3,
        'GPe_STN_e': -84.0,
        'GPe_STN_tau1': 0.875,
        'GPe_STN_tau2': 7.72,
        'GPe_STN_k': 0.125,
        'GPe_STN_v_half': -9.0,
        'GPe_STN_delay': 4.75,

        # AMPA synapse (from Cortex to STN)
        'Ctx_STN_A_weight': 3.666e-3,
        'Ctx_STN_A_e': 0.0,
        'Ctx_STN_A_tau1': 0.83,
        'Ctx_STN_A_tau2': 4.53,
        'Ctx_STN_A_k': 0.045,
        'Ctx_STN_A_v_half': 30.0,
        'Ctx_STN_A_delay': 5.0,

        # NMDA synapse (from Cortex to STN)
        'Ctx_STN_N_weight': 5.208e-3,
        'Ctx_STN_N_e': 0.0,
        'Ctx_STN_N_tau1': 5.5,
        'Ctx_STN_N_tau2': 48.0,
        'Ctx_STN_N_k': 0.06613,
        'Ctx_STN_N_v_half': -36.0,
        'Ctx_STN_N_delay': 5.0,

        # AMPA synapse (intranuclear STN)
        'STN_STN_weight': 0.0,
        'STN_STN_e': 0.0,
        'STN_STN_tau1': 0.5,
        'STN_STN_tau2': 5.0,
        'STN_STN_k': 0.05,
        'STN_STN_v_half': 30.0,
        'STN_STN_delay': 1.0,

        # Current clamp
        'IClamp_STN_delay': 0.,
        'IClamp_STN_dur': 0.,
        'IClamp_STN_amp': 0.,

        # Voltage clamp
        'STN_VClmp_dur1': 0.,
        'STN_VClmp_amp1': 0.,
        'STN_VClmp_dur2': 0.,
        'STN_VClmp_amp2': 0.,
        'STN_VClmp_dur3': 0.,
        'STN_VClmp_amp3': 0.,
        'STN_VClmp_rs': 0.,

        # Biophysics
        'STNbio_ena': 55.0,
        'STNbio_gnabar': 37.5e-3,
        'STNbio_ek': -80.0 ,
        'STNbio_gkbar': 45.0e-3,
        'STNbio_el': -60.0,
        'STNbio_gl': 2.25e-3,
        'STNbio_m_theta': -30.0,
        'STNbio_m_sigma': 15.0,
        'STNbio_h_theta': -39.0,
        'STNbio_h_sigma': -3.1,
        'STNbio_h_phi': 0.75,
        'STNbio_h_tau_theta': -57.0,
        'STNbio_h_tau_sigma': -3.0,
        'STNbio_h_tau_naught': 1.0,
        'STNbio_h_tau_prime': 500.0,
        'STNbio_n_theta': -32.0,
        'STNbio_n_sigma': 8.0,
        'STNbio_n_phi': 0.75,
        'STNbio_n_tau_theta': -80.0,
        'STNbio_n_tau_sigma': -26.0,
        'STNbio_n_tau_naught': 1.0,
        'STNbio_n_tau_prime': 100.0,
        'STNbio_eca': 140.0,
        'STNbio_glcabar': 0.5e-3,
        'STNbio_gtcabar': 0.5e-3,
        'STNbio_gahp': 9.0e-3,
        'STNbio_s_theta': -39.0,
        'STNbio_s_sigma': 8.0,
        'STNbio_r_theta': -67.0,
        'STNbio_r_sigma': -2.0,
        'STNbio_r_phi': 0.5,
        'STNbio_r_tau_theta': 68.0,
        'STNbio_r_tau_sigma': -2.2,
        'STNbio_r_tau_naught': 7.1,
        'STNbio_r_tau_prime': 17.5,
        'STNbio_a_theta': -63.0,
        'STNbio_a_sigma': 7.8,
        'STNbio_b_theta': 0.25,
        'STNbio_b_sigma': -0.07,
        'STNbio_k1': 15.0,
        'STNbio_kCa': 22.5,
        'STNbio_epsilon': 5.0e-5,
        'STNbio_Ca_phi': 0.75,
        'STNbio_Ca_ini': 5.2e-5,
        'STNbio_Ca_scl': -1.0e3,
    }

def get_GPe_params_default():
    return {

        # Synapses
        'GPeP_threshold': 0,
        'GPeA_threshold': 0,

        # AMPA synapse (from STN to GPe)
        'STN_GPe_A_weight': 3.66e-3,
        'STN_GPe_A_e': 0.0,
        'STN_GPe_A_tau1': 0.83,
        'STN_GPe_A_tau2': 4.53,
        'STN_GPe_A_k': 0.045,
        'STN_GPe_A_v_half': 30.0,
        'STN_GPe_A_delay': 4.9,

        # NMDA synapse (from STN to GPe)
        'STN_GPe_N_weight': 5.208e-3,
        'STN_GPe_N_e': 0.0,
        'STN_GPe_N_tau1': 5.5,
        'STN_GPe_N_tau2': 48.0,
        'STN_GPe_N_k': 0.06613,
        'STN_GPe_N_v_half': -36.0,
        'STN_GPe_N_delay': 4.9,

        # GABAA synapse (intranuclear GPe)
        'GPe_GPe_weight': 36.76e-3,
        'GPe_GPe_e': -84.0,
        'GPe_GPe_tau1': 0.89,
        'GPe_GPe_tau2': 4.75,
        'GPe_GPe_k': 0.5,
        'GPe_GPe_v_half': -39.0,
        'GPe_GPe_delay': 1.0,

        # GABAA synapse (from MSN to GPe)
        'MSN_GPe_weight': 0.0,
        'MSN_GPe_e': -65.0,
        'MSN_GPe_tau1': 0.5,
        'MSN_GPe_tau2': 5.0,
        'MSN_GPe_k': 0.005,
        'MSN_GPe_v_half': -39.0,
        'MSN_GPe_delay': 5.0,

        # Current clamps
        'IClamp_GPe_delay': 0.,
        'IClamp_GPe_dur': 0.,
        'IClamp_GPe_amp': 0,

        # Voltage clamp
        'GPe_VClmp_dur1': 0.,
        'GPe_VClmp_amp1': 0.,
        'GPe_VClmp_dur2': 0.,
        'GPe_VClmp_amp2': 0.,
        'GPe_VClmp_dur3': 0.,
        'GPe_VClmp_amp3': 0.,
        'GPe_VClmp_rs': 0.,

        # Biophysics
        'GPePbio_ena': 55.0,
        'GPePbio_gnabar': 120.0e-3,
        'GPePbio_ek': -80.0,
        'GPePbio_gkbar': 30.0e-3,
        'GPePbio_el': -55.0,
        'GPePbio_gl': 0.1e-3,
        'GPePbio_m_theta': -37.0,
        'GPePbio_m_sigma': 10.0,
        'GPePbio_h_theta': -58.0,
        'GPePbio_h_sigma': -12.0,
        'GPePbio_h_phi': 0.05,
        'GPePbio_h_tau_theta': -40.0,
        'GPePbio_h_tau_sigma': -12.0,
        'GPePbio_h_tau_naught': 0.05,
        'GPePbio_h_tau_prime': 0.27,
        'GPePbio_n_theta': -50.0,
        'GPePbio_n_sigma': 14.0,
        'GPePbio_n_phi': 0.1,
        'GPePbio_n_tau_theta': -40.0,
        'GPePbio_n_tau_sigma': -12.0,
        'GPePbio_n_tau_naught': 0.05,
        'GPePbio_n_tau_prime': 0.27,
        'GPePbio_eca': 120.0,
        'GPePbio_glcabar': 0.1e-3,
        'GPePbio_gtcabar': 0.5e-3,
        'GPePbio_gahp': 30.0e-3,
        'GPePbio_s_theta': -35.0,
        'GPePbio_s_sigma': 2.0,
        'GPePbio_r_theta': -70.0,
        'GPePbio_r_sigma': -2.0,
        'GPePbio_r_phi': 1.0,
        'GPePbio_r_tau': 30.0,
        'GPePbio_a_theta': -57.0,
        'GPePbio_a_sigma': 2.0,
        'GPePbio_k1': 30.0,
        'GPePbio_kCa': 15.0,
        'GPePbio_epsilon': 1.0e-4,
        'GPePbio_Ca_ini': 5.2e-5,
        'GPePbio_Ca_scl': -1.0e3,

        # Biophysics
        'GPeAbio_ena': 55.0,
        'GPeAbio_gnabar': 120.0e-3,
        'GPeAbio_ek': -80.0,
        'GPeAbio_gkbar': 30.0e-3,
        'GPeAbio_el': -55.0,
        'GPeAbio_gl': 0.1e-3,
        'GPeAbio_m_theta': -37.0,
        'GPeAbio_m_sigma': 10.0,
        'GPeAbio_h_theta': -58.0,
        'GPeAbio_h_sigma': -12.0,
        'GPeAbio_h_phi': 0.05,
        'GPeAbio_h_tau_theta': -40.0,
        'GPeAbio_h_tau_sigma': -12.0,
        'GPeAbio_h_tau_naught': 0.05,
        'GPeAbio_h_tau_prime': 0.27,
        'GPeAbio_n_theta': -50.0,
        'GPeAbio_n_sigma': 14.0,
        'GPeAbio_n_phi': 0.1,
        'GPeAbio_n_tau_theta': -40.0,
        'GPeAbio_n_tau_sigma': -12.0,
        'GPeAbio_n_tau_naught': 0.05,
        'GPeAbio_n_tau_prime': 0.27,
        'GPeAbio_eca': 120.0,
        'GPeAbio_glcabar': 0.1e-3,
        'GPeAbio_gtcabar': 0.5e-3,
        'GPeAbio_gahp': 30.0e-3,
        'GPeAbio_s_theta': -35.0,
        'GPeAbio_s_sigma': 2.0,
        'GPeAbio_r_theta': -70.0,
        'GPeAbio_r_sigma': -2.0,
        'GPeAbio_r_phi': 1.0,
        'GPeAbio_r_tau': 30.0,
        'GPeAbio_a_theta': -57.0,
        'GPeAbio_a_sigma': 2.0,
        'GPeAbio_k1': 30.0,
        'GPeAbio_kCa': 15.0,
        'GPeAbio_epsilon': 1.0e-4,
        'GPeAbio_Ca_ini': 5.2e-5,
        'GPeAbio_Ca_scl': -1.0e3,
    }
