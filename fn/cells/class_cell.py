# class_cell.py - establish class def for general cell features
#
# ver: 1.0
# rev: 2018-01-17 - Made spiking threshold assignable

import numpy as np
import itertools as it
from neuron import h as nrn

# Units for e: mV
# Units for gbar: S/cm^2

# Create a cell class
class Cell():
    def __init__(self, cell_type):
        # Parallel methods
        self.pc = nrn.ParallelContext()

        # make cell type an attribute of cell
        self.cell_type = cell_type

        # create soma and set geometry
        self.soma = nrn.Section(cell=self, name=cell_type)
        self.soma.L = 1./np.pi
        self.soma.diam = 1.
        self.soma.cm = 1.

    # Add IClamp to soma
    def insert_IClamp(self):
        stim = nrn.IClamp(self.soma(0.5))

        # object must exist for NEURON somewhere and needs to be saved
        return stim

    # Add SEClamp to soma
    def insert_SEClamp(self):
        VClmp = nrn.SEClamp(self.soma(0.5))

        # object must exist for NEURON somewhere and needs to be saved
        return VClmp

    # simple function to record current---for now only at the soma
    def record_current_soma(self):
        # a soma exists at self.soma
        self.rec_i = nrn.Vector()

        try:
            # assumes that self.synapses is a dict that exists
            list_syn_soma = [key for key in self.synapses.keys() if key.startswith('soma_')]

            # matching dict from the list_syn_soma keys
            self.dict_currents = dict.fromkeys(list_syn_soma)

            # iterate through keys and record currents appropriately
            for key in self.dict_currents.iterkeys():
                self.dict_currents[key] = nrn.Vector()
                self.dict_currents[key].record(self.synapses[key]._ref_i)

        except:
            print("Warning in Cell(): record_current_soma() was called, but no self.synapses dict was found")
            pass

    # General fn that creates any Exp2syn synapse type
    # requires dictionary of synapse properties
    def syn_create(self, secloc, p):
        syn = nrn.Exp2Syn(secloc)
        syn.g = p['g']
        syn.e = p['e']
        syn.tau1 = p['tau1']
        syn.tau2 = p['tau2']

        return syn

    # connect_to_target created for pc, used in Network()
    # these are SOURCES of spikes
    def connect_to_target(self, target):
        nc = nrn.NetCon(self.soma(0.5)._ref_v, target, sec=self.soma)
        nc.threshold = self.threshold
        # print 'nc in connect_to_target', dir(nc), '\n'
        return nc

    # parallel receptor-centric connect FROM presyn TO this cell, based on GID
    def parconnect_from_src(self, gid_presyn, nc_dict, postsyn):
        # nc_dict keys are: {pos_src, A_weight, A_delay, lamtha}
        nc = self.pc.gid_connect(gid_presyn, postsyn)

        # print 'nc in parconnect', dir(nc), '\n'
        # calculate distance between cell positions with pardistance()
        # d = self.__pardistance(nc_dict['pos_src'])

        # set props here
        nc.weight[0] = nc_dict['A_weight']
        nc.delay = nc_dict['A_delay']

        return nc

    # pardistance function requires pre position, since it is calculated on POST cell
    def __pardistance(self, pos_pre):
        dx = np.abs(self.pos[0] - pos_pre[0])
        dy = np.abs(self.pos[1] - pos_pre[1])
        dz = np.abs(self.pos[2] - pos_pre[2])

        return np.sqrt(dx**2 + dy**2)

    # Define 3D shape of soma -- is needed for gui representation of cell
    # DO NOT need to call nrn.define_shape() explicitly!!
    def shape_soma(self):
        nrn.pt3dclear(sec=self.soma)

        # nrn.ptdadd(x, y, z, diam) -- if this function is run, clobbers
        # self.soma.diam set above
        nrn.pt3dadd(0, 0, 0, self.diam, sec=self.soma)
        nrn.pt3dadd(0, self.L, 0, self.diam, sec=self.soma)

    # Define 3D shape and position of cell. By default neuron uses xy plane for
    # height and xz plane for depth. This is opposite for model as a whole, but
    # convention is followed in this function ease use of gui.
    def __shape_change(self):
        self.shape_soma()

        self.soma.push()
        for i in range(0, int(nrn.n3d())):
            nrn.pt3dchange(i, self.pos[0]*100 + nrn.x3d(i), -self.pos[2] + nrn.y3d(i),
                self.pos[1] * 100 + nrn.z3d(i), nrn.diam3d(i))

        nrn.pop_section()

if __name__ == '__main__':
    TestCell = Cell('Testing')
    print(TestCell.soma.nseg)
