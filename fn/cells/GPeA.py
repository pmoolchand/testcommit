# GPe.py - establish class def for GPe cells
#
# ver: 1.0
# rev: 2018-01-17 - Added spiking threshold

import itertools as it
from neuron import h as nrn
from .class_cell import Cell

# Units for e: mV
# Units for gbar: S/cm^2 unless otherwise noted

# class GPe(Cell):
class GPeA(Cell):
    def __init__(self, gid, pos, p, con_mat, RecDict, v_ini, Iclamp):
        # Note: Cell properties are set in Cell()
        Cell.__init__(self, cell_type='GPeCell')

        # Assigning gid and position to cell
        self.gid = gid
        self.pos = pos
        self.p = p
        self.con_mat = con_mat
        self.RecDict = {key:RecDict[key] for key in ['GPASta', 'GPASyn', 'GPeClm']}
        self.v_ini = v_ini

        self.threshold = p['GPeA_threshold']

        # create IClamp in cell
        self.iclamp = nrn.Vector()
        self.iclamp.from_python(Iclamp)
        self.stim = self.insert_IClamp()

        # create SEClamp in cell
        self.VClmp = self.insert_SEClamp()

        self.__biophysics()
        self.__synapse_create()
        self.__set_iclamp_props()

        if p['GPA_VClamp'] is True:
            self.__set_seclamp_props()
            if '_IV' in self.RecDict['GPAClm']:
                self.i_VClmp = nrn.Vector()
                self.i_VClmp.record(self.VClmp._ref_i)

        if self.p['RecGPASta_V']:
            # create empty array to store membrane potentials
            self.v_soma = nrn.Vector()
            self.v_soma.record(self.soma(0.5)._ref_v)

        # create and store state variables

        for state in RecDict['GPASta']:
            setattr(self, '{!s}_soma'.format(state), nrn.Vector())
            nrnVec = getattr(self, '{!s}_soma'.format(state))
            nrnVec.record(getattr(self.soma(0.5), '_ref{!s}_GPe'.format(state)))

        for synap in RecDict['GPASyn']:
            setattr(self, 'i{!s}_soma'.format(synap), nrn.Vector())
            nrnVec = getattr(self, 'i{!s}_soma'.format(synap))
            synobj = getattr(self, 'soma{!s}'.format(synap))
            nrnVec.record(getattr(synobj, '_ref_i'))

        # par: create arbitrary lists of connections FROM other cells
        # TO this cell instantiation
        # these lists are allowed to be empty
        self.ncfrom_GPeP = []
        self.ncfrom_GPeA = []
        self.ncfrom_STN_A = []
        self.ncfrom_STN_N = []
        self.ncfrom_MSN = []

    def __set_iclamp_props(self):
        # sets the properties of the IClamp
        self.stim.delay = 0
        self.stim.dur = 1e9
        self.iclamp.play(self.stim._ref_amp, self.p['dt'])

    def __set_seclamp_props(self):
        # sets the properties of the SEClamp
        self.VClmp.dur1 = self.p['GPe_VClmp_dur1']
        self.VClmp.amp1 = self.p['GPe_VClmp_amp1']
        self.VClmp.dur2 = self.p['GPe_VClmp_dur2']
        self.VClmp.amp2 = self.p['GPe_VClmp_amp2']
        self.VClmp.dur3 = self.p['GPe_VClmp_dur3']
        self.VClmp.amp3 = self.p['GPe_VClmp_amp3']
        self.VClmp.rs = self.p['GPe_VClmp_rs']

    def __synapse_create(self):
        # ceates synaptic channels onto this cell
        self.soma_AMPA_STN = self.syn_AMPA_STN_create(self.soma(0.5))
        self.soma_NMDA_STN = self.syn_NMDA_STN_create(self.soma(0.5))
        self.soma_GABA_GPe = self.syn_GABA_GPe_create(self.soma(0.5))
        self.soma_GABA_MSN = self.syn_GABA_MSN_create(self.soma(0.5))

    def __biophysics(self):
        # Insert mechanisms
        self.soma.insert('GPe')

        # Update biophysical parameters from defaults
        p_GPe_bio = {key[8:]:val for key,val in self.p.items() if key.startswith('GPeAbio')}
        for prop, val in p_GPe_bio.items():
            setattr(self.soma, prop+'_GPe', val)
        self.soma.v = self.v_ini

    # For all synapses, section location 'secloc' is being explicitly supplied
    # for clarity, even though they are (right now) always 0.5. Might change in future
    # creates a RECEIVING inhibitory synapse at secloc
    # Intranuclear GPe innervations
    def syn_GABA_GPe_create(self, secloc):
        syn_GABA_GPe = nrn.Exp2Syn_vg(secloc)
        syn_GABA_GPe.e = self.p['GPe_GPe_e']
        syn_GABA_GPe.tau1 = self.p['GPe_GPe_tau1']
        syn_GABA_GPe.tau2 = self.p['GPe_GPe_tau2']
        syn_GABA_GPe.k = self.p['GPe_GPe_k']
        syn_GABA_GPe.v_half = self.p['GPe_GPe_v_half']

        syn_GABA_GPe.a = 1
        syn_GABA_GPe.b = -1

        return syn_GABA_GPe

    # STN innervations
    # creates a RECEIVING excitatory AMPA synapse at secloc
    def syn_AMPA_STN_create(self, secloc):
        syn_AMPA_STN = nrn.Exp2Syn_vg(secloc)
        syn_AMPA_STN.e = self.p['STN_GPe_A_e']
        syn_AMPA_STN.tau1 = self.p['STN_GPe_A_tau1']
        syn_AMPA_STN.tau2 = self.p['STN_GPe_A_tau2']
        syn_AMPA_STN.k = self.p['STN_GPe_A_k']
        syn_AMPA_STN.v_half = self.p['STN_GPe_A_v_half']
        syn_AMPA_STN.a = 1
        syn_AMPA_STN.b = -1

        return syn_AMPA_STN

    # creates a RECEIVING excitatory NMDA synapse at secloc
    def syn_NMDA_STN_create(self, secloc):
        syn_NMDA_STN = nrn.Exp2Syn_vg(secloc)
        syn_NMDA_STN.e = self.p['STN_GPe_N_e']
        syn_NMDA_STN.tau1 = self.p['STN_GPe_N_tau1']
        syn_NMDA_STN.tau2 = self.p['STN_GPe_N_tau2']
        syn_NMDA_STN.k = self.p['STN_GPe_N_k']
        syn_NMDA_STN.v_half = self.p['STN_GPe_N_v_half']

        return syn_NMDA_STN

    # MSN innervations
    # creates a RECEIVING inhibitory synapse at secloc
    def syn_GABA_MSN_create(self, secloc):
        syn_GABA_MSN = nrn.Exp2Syn_vg(secloc)
        syn_GABA_MSN.e = self.p['MSN_GPe_e']
        syn_GABA_MSN.tau1 = self.p['MSN_GPe_tau1']
        syn_GABA_MSN.tau2 = self.p['MSN_GPe_tau2']
        syn_GABA_MSN.k = self.p['MSN_GPe_k']
        syn_GABA_MSN.v_half = self.p['MSN_GPe_v_half']

        return syn_GABA_MSN

    def connect_external(self, gid_src):
        nc_dict = {
            'pos_src': None,
            'A_weight':self.p['MSN_GPe_weight'],
            'A_delay': self.p['MSN_GPe_delay'],
            'lamtha': 3,
        }
        self.ncfrom_MSN.append(self.parconnect_from_src(gid_src, nc_dict, self.soma_GABA_MSN))


    # Parallel connection function from all cells TO this cell
    def parconnect(self, gid, gid_dict, p):
        # init dict of dicts, FROM_TO properties
        nc_dict = {
            'GPe_GPe': {
                'A_weight': p['GPe_GPe_weight'],
                'A_delay': p['GPe_GPe_delay'],
                'lamtha': 3.,
                'pos_src' : None,
            },
            'STN_GPe_A': {
                'A_weight': p['STN_GPe_A_weight'],
                'A_delay': p['STN_GPe_A_delay'],
                'lamtha': 3.,
                'pos_src' : None,
            },
            'STN_GPe_N': {
                'A_weight': p['STN_GPe_N_weight'],
                'A_delay': p['STN_GPe_N_delay'],
                'lamtha': 3.,
                'pos_src' : None
            },
        }

        # FROM GPeA cells (autosynapse possible)
        for gid_src in gid_dict['GPeACell']:
            # Check if cell connects to THIS based on connectivity matrix
            if self.con_mat[gid_src, self.gid] != 0.:
               self.ncfrom_GPeA.append(self.parconnect_from_src(gid_src, nc_dict['GPe_GPe'], self.soma_GABA_GPe))

        # FROM STN cells
        for gid_src in gid_dict['STNCell']:
            # Check if cell connects to THIS based on connectivity matrix
            if self.con_mat[gid_src, self.gid] != 0.:
               self.ncfrom_STN_A.append(self.parconnect_from_src(gid_src, nc_dict['STN_GPe_A'], self.soma_AMPA_STN))
               self.ncfrom_STN_N.append(self.parconnect_from_src(gid_src, nc_dict['STN_GPe_N'], self.soma_NMDA_STN))

        # FROM GPeP cells
        for gid_src in gid_dict['GPePCell']:
            # Check if cell connects to THIS based on connectivity matrix
            if self.con_mat[gid_src, self.gid] != 0.:
               self.ncfrom_GPeP.append(self.parconnect_from_src(gid_src, nc_dict['GPe_GPe'], self.soma_GABA_GPe))
