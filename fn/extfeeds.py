# extfeeds.py - returns the feed_list
#
# ver: 1.0 
# rev: 2018-01-17 - Created Ctx_feed_combos and seperated assignments 

import os, sys
import importlib as implb
import numpy as np
import itertools as it
from scipy.stats import bernoulli as bern
from functools import reduce 
#import new_net_connect as nenet

####**** SPIKES ***####

def generate_event_times(feedsig, fseed, ststop):

    t0 = feedsig[0]
    tstop = min(feedsig[1], ststop)
    dist = feedsig[2]
    arg0 =  feedsig[3]
    arg1 = feedsig[4]
    spkpb = feedsig[5]
    spk_int = feedsig[6]

    np.random.seed(seed=fseed)

    # Generate Inter-Burst-Intervals based on distribution and parameters
    if dist == 'regular':
        event_times = np.arange(t0, tstop+arg0, arg0)
    else:
        if dist == 'normal':
            IBI = np.random.normal(arg0, arg1, (1,1000))
        elif dist == 'exponential':
            IBI = np.random.exponential(arg0, (1,1000))
        elif dist == 'uniform':
            IBI = np.random.uniform(arg0, arg1, (1,1000))
        else:
            print("\nDistribution not defined!\n")

        # Add t0 as initial occurrence of burst
        IBI = np.insert(IBI, 0, t0)

        # Create burst onset times and reject those greater than feed stop time
        event_times = np.cumsum(IBI)
        event_times = event_times[event_times>=t0]
        event_times = event_times[event_times<=tstop]
        event_times = np.around(event_times, 2)

    # Adding constituent spike times in bursts
    ISIs = np.array([])

    for n_spk in np.arange(spkpb-1):
        ISIs = np.append(ISIs, event_times + (n_spk+1)* spk_int)

    event_times = np.append(event_times, ISIs)
    event_times = event_times[event_times<=tstop]
    event_times.sort()

    return event_times

def generate_aggregate_times(feed, tstop):
    feedtimes = np.array([]) 
    for feedsig in feed['sig']:
        feedtimes = np.append(feedtimes, generate_event_times(feedsig, feed['f_seed'], tstop))
    feedtimes.sort()
    return feedtimes

def generate_feedcombos(feed_arr):
    FeedCombos = np.empty(shape=2**feed_arr.size, dtype='O')
    powerset = [x for length in range(feed_arr.size+1) for x in it.combinations(feed_arr, length)]
    # Empty Set
    FeedCombos[0] = np.array([])

    Solo_idx = 2**np.arange(feed_arr.size)
    Combo_idx = np.setdiff1d(np.arange(1,2**feed_arr.size), Solo_idx, assume_unique=True)

    # Solo Feeds
    for i in Solo_idx: 
        FeedCombos[i] = powerset[i][0]

    # Combo Feeds
    for i in Combo_idx:
        FeedCombos[i] = reduce(np.union1d, powerset[i])

    return FeedCombos

def create_feed_list(ext_feed, netwrk, p):
    tstop = p['tstop']
    up_feed = p['upfeed']

    np.random.seed(seed=ext_feed['Seed'])
    main_seeds = np.random.randint(999999999, size = 10)

    # Cortical to STN feeds
    Ctx_STN_feed = ext_feed['Ctx_STN']

    Ctx_feeds = [key for key in Ctx_STN_feed.keys() if key.startswith('Feed')]
    Ctx_feeds.sort()
    N_Ctx_feeds = len(Ctx_feeds)

    Ctx_feed_arr = np.empty(shape=N_Ctx_feeds, dtype='O')

    if N_Ctx_feeds:

        # generate seeds for feeds
        Ctx_STN_feed['main_seed'] = main_seeds[0]
        np.random.seed(seed=Ctx_STN_feed['main_seed'])
        Ctx_feed_seeds = np.random.randint(999999999, size = N_Ctx_feeds)

        if not up_feed['sig'] is None:
            prse_up = up_feed['sig'].split('(')[1:]
        #    print(prse_up)
            N_ps = len(prse_up)
            fd_up = np.empty(shape=(N_ps,8), dtype='O')
            for idx in range(N_ps):
                fd_up[idx,:] = [convert_to_type(item) for item in prse_up[idx].split(',')] 
                
            N_min = min(N_ps, N_Ctx_feeds)

            for idx, Ctx_f in enumerate(Ctx_feeds[:N_min]):
                Ctx_STN_feed[Ctx_f]['prop_ass_subpop'] = fd_up[idx,6]
                Ctx_STN_feed[Ctx_f]['prop_oth_subpop'] = fd_up[idx,7]
                ori_sig = Ctx_STN_feed[Ctx_f]['sig'][0]
                if ori_sig[2] == 'regular':
                    fd_up[idx, 2] = ori_sig[3]
                    fd_up[idx, 3] = None
                elif ori_sig[2] == 'normal':
                    dif = ori_sig[1] - ori_sig[0]
            #        fd_up[idx, 0] = ori_sig[0]
            #        fd_up[idx, 1] = ori_sig[1] 
                    fd_up[idx, 1] = fd_up[idx, 0] + dif
                elif ori_sig[2] == 'uniform':
                    fd_up[idx,2] = ori_sig[3]  
                    fd_up[idx,3] = ori_sig[4]

                Ctx_STN_feed[Ctx_f]['sig'][0] = (fd_up[idx,0], fd_up[idx,1], ori_sig[2], fd_up[idx,2], fd_up[idx,3], fd_up[idx,4], fd_up[idx,5])

        for idx, Ctx_f in enumerate(Ctx_feeds):
            # update parameters
            Ctx_STN_feed[Ctx_f]['f_seed'] = Ctx_feed_seeds[idx]
            Ctx_feed_arr[idx] = generate_aggregate_times(Ctx_STN_feed[Ctx_f], tstop)

    Ctx_Feed_Combos = generate_feedcombos(Ctx_feed_arr)

    STN_Ctx_Combo_idx, Ctx_STN_list = get_feed_cell_assignment(netwrk['STN']['subpop_list'], Ctx_STN_feed, Ctx_feeds)

    return Ctx_Feed_Combos, STN_Ctx_Combo_idx, Ctx_STN_list

def convert_to_type(var):
    try:
        return int(var)
    except ValueError:
        return float(var)

####**** CURRENT CLAMPS ***####

def get_Iclamp(cell_clamp, tstop, dt):
    tim = np.arange(0, tstop+dt, dt)
    clmps_list = [key for key  in cell_clamp.keys() if key.startswith('IClamp')]
    clmps_list.sort()
    clmps = [(cell_clamp[key]['sig'], np.zeros(tim.shape), key) for key in clmps_list]
    for clmp in clmps:
        create_clamp(clmp, tstop, dt)
    return clmps

def create_clamp(Iclamp, tstop, dt):
    for props in Iclamp[0]:
        t0 = max(0, props[0])
        t_end = min(props[1], tstop)
        t0_idx = int(t0/dt)
        tend_idx = int(t_end/dt)
        if props[2] == 'flat':
            Iclamp[1][t0_idx:tend_idx] += props[3]
        elif props[2] == 'lin':
            Iclamp[1][t0_idx:tend_idx] += props[3]*np.arange(tend_idx-t0_idx)+props[4] 

def get_Iclamp_matrix(gid_list, feed, tstop, dt):
    
    Clamplist = get_Iclamp(feed, tstop, dt)
    N_feed = len(Clamplist)
    N_tim = int(-(-tstop//dt))
    Feed_matrix = np.zeros(shape=(N_feed, N_tim+1))
    for idx, clmp in enumerate(Clamplist):
        Feed_matrix[idx,:] = clmp[1] 

    Feed_tt = np.array([i for i in it.product([0,1], repeat=N_feed)])
    Feed_combos = np.matmul(Feed_tt,Feed_matrix)

    N_cells = len(gid_list)
    Cell_gid_list = np.arange(N_cells, dtype=int)
    
    Feed_Cell_matrix = np.zeros(shape=(N_cells, N_feed), dtype=int)
    Cell_Feed_idx = np.zeros(N_cells, dtype=int)

    if N_feed:
        if feed['subpop_ass'] == 'RoundRobin':

            # Segregated RoundRobin Assignment of gids to subpopulations
            Feed_gids = np.tile(np.arange(N_feed), -(-N_cells//N_feed))

        elif feed['subpop_ass'] == 'random':

            # Segregated RoundRobin Assignment of gids to subpopulations
            Feed_gids = np.tile(np.arange(N_feed), -(-N_cells//N_feed))

            np.random.seed(seed=feed['main_seed'])

            # Randomize assignment of gids to Cortical drives
            np.random.shuffle(Feed_gids)

        # Trim to number of cells
        Feed_gids = Feed_gids[range(N_cells)]

        # Gather cell gids
        Feed_Cell_list = []
        for i in np.arange(N_feed):
            Feed_Cell_list.append((i, np.where(Feed_gids==i)[0]))

        for idx, Feed_f in enumerate(Clamplist):
            pos_gids = Feed_Cell_list[idx][1]
            pos_gids_oth = np.delete(Cell_gid_list, pos_gids)
            Feed_Cell_matrix[pos_gids,idx] = bern.rvs(feed[Feed_f[-1]]['prop_ass_subpop'], size=len(pos_gids))
            Feed_Cell_matrix[pos_gids_oth,idx] = bern.rvs(feed[Feed_f[-1]]['prop_oth_subpop'], size=len(pos_gids_oth))


        bin_weights = np.logspace(N_feed-1, 0, N_feed, base=2, dtype=int)

        Cell_Feed_idx = np.matmul(Feed_Cell_matrix, bin_weights)

    return Feed_combos, Cell_Feed_idx

def get_feed_cell_assignment(subpop_list, feed_dict, feed_keys):

    N_subpop = subpop_list.size
    N_cells = 0
    for subpop in subpop_list:
        N_cells += subpop.size

    N_feed = len(feed_keys)
    Cell_gid_list = np.arange(N_cells, dtype=int)
    
    Feed_Cell_matrix = np.zeros(shape=(N_cells, N_feed), dtype=int)
    Cell_Feed_idx = np.zeros(N_cells, dtype=int)


    if N_feed:

        if feed_dict['subpop_ass'] == 'Match' and N_feed != N_subpop:
            print('Cannot match Feeds to Subpopulations, defaulting to Specific assignment!')
            feed_dict['subpop_ass'] = 'Specific'

        if feed_dict['subpop_ass'] == 'Match':
            Feed_gids = np.zeros(N_cells)
            for fidx, feed in enumerate(feed_keys):
                Feed_gids[subpop_list[int(feed_dict[feed]['target_subpop'])]] = fidx
        
        elif feed_dict['subpop_ass'] == 'Specific':
            Feed_gids = np.full(N_cells, np.nan)
            for fidx, feed in enumerate(feed_keys):
                Feed_gids[subpop_list[int(feed_dict[feed]['target_subpop'])]] = fidx

        elif feed_dict['subpop_ass'] == 'RoundRobin':

            # Segregated RoundRobin Assignment of gids to subpopulations
            Feed_gids = np.tile(np.arange(N_feed), -(-N_cells//N_feed))

        elif feed_dict['subpop_ass'] == 'Random':

            # Segregated RoundRobin Assignment of gids to subpopulations
            Feed_gids = np.tile(np.arange(N_feed), -(-N_cells//N_feed))

            np.random.seed(seed=feed_dict['main_seed'])

            # Randomize assignment of gids to Cortical drives
            np.random.shuffle(Feed_gids)

        # Trim to number of cells
        Feed_gids = Feed_gids[range(N_cells)]


    # Gather cell gids
    Feed_Cell_list = []
    for i in np.arange(N_feed):
        Feed_Cell_list.append((i, np.where(Feed_gids==i)[0]))


    for idx, Feed_f in enumerate(feed_keys):
        pos_gids = Feed_Cell_list[idx][1]
        pos_gids_oth = np.delete(Cell_gid_list, pos_gids)
        Feed_Cell_matrix[pos_gids,idx] = bern.rvs(feed_dict[Feed_f]['prop_ass_subpop'], size=len(pos_gids))
        Feed_Cell_matrix[pos_gids_oth,idx] = bern.rvs(feed_dict[Feed_f]['prop_oth_subpop'], size=len(pos_gids_oth))

    bin_weights = np.logspace(N_feed-1, 0, N_feed, base=2, dtype=int)

    Cell_Feed_idx = np.matmul(Feed_Cell_matrix, bin_weights)

    return Cell_Feed_idx, Feed_Cell_list

if __name__ == "__main__":

    ext_feed = {
        'Seed': 5678787,
        'STN': {
            'main_seed': 10050,
            'subpop_ass': 'random',
           # 'subpop_ass': 'RoundRobin',
            'IClamp0': {
                'sig': [(5, 25, 'flat', -25, None)],  
                'Assign': 'Random',
                'prop_ass_subpop': 1,
                'prop_oth_subpop': 0.5,
            },
            'IClamp1': {
                'sig': [(5,15.1, 'lin', 2, 0.1)],  
                'Assign': 'Random',
                'prop_ass_subpop': 0,
                'prop_oth_subpop': 1.,
            }
        },
        'GPe': {
            'main_seed': 1050,
            'subpop_ass': 'random',
           # 'subpop_ass': 'RoundRobin',
            'IClamp0': {
                'sig': [(5, 10, 'flat', 25, None)],  
                'Assign': 'Random',
                'prop_ass_subpop': 0.8,
                'prop_oth_subpop': 0.2,
            },
            'IClamp1': {
                'sig': [(10, 20, 'flat', -25, None), (5,15.1, 'lin', 2, 0.1)],  
                'Assign': 'Random',
                'prop_ass_subpop': 0.5,
                'prop_oth_subpop': 0.5,
            },
            'IClamp2': {
                'sig': [(5, 10, 'flat', -4, None)],  
                'Assign': 'Random',
                'prop_ass_subpop': 0.8,
                'prop_oth_subpop': 0.2,
            },
        },
        'Ctx_STN': {
            'main_seed':-1 ,
            'subpop_ass': 'Match',
#            'subpop_ass': 'Specific',
           # 'subpop_ass': 'RoundRobin',
#            'subpop_ass': 'Random',
        
            'Feed0' : {
                'sig': [(1000, 2000, 'regular', 250, 10, 1, 2), (500, 600, 'normal', 15, 5, 3, 1.5)],
                'f_seed': -1,
                'prop_ass_subpop': 1,
                'prop_oth_subpop': 0.5,
                'pos_gids': None,
                'target_subpop': 0,
            },
            'Feed1' : {
                'sig': [(1000, 1500, 'normal', 20, 5, 1, 2)],
                'f_seed': -1,
                'prop_ass_subpop':0.5,
                'prop_oth_subpop': 0.5,
                'pos_gids': None,
                'target_subpop': 1,
            },
            'Feed2' : {
                'sig': [(100, 150, 'normal', 25, 5, 1, 3)],
                'f_seed': -1,
                'prop_ass_subpop': 0.5,
                'prop_oth_subpop': 0.5,
                'pos_gids': None,
                'target_subpop': 2,
            },
        },
    }
    net_params = {
        'Cell_STN': {
            'N' : 30,
            'gid_rank': 0,
            'shape': 'grid',
            'N_subpop': 3,
            'Assign': 'Random',
            'Int_Con_p': 0.,
            'Ext_Con_p': 0,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0), 
            },
        'Cell_GPeP':{
            'N': 6,
            'gid_rank': 1,
            'shape': 'grid',
            'N_subpop': 1,
            'Assign': 'Random',
            'Int_Con_p': 0.2,
            'Ext_Con_p': 0.,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0), 
            },
        'Cell_GPeA':{
            'N': 6,
            'gid_rank': 2,
            'shape': 'grid',
            'N_subpop': 1,
            'Assign': 'Random',
            'Int_Con_p': 0.5,
            'Ext_Con_p': 0.,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0), 
            },
        'Con_STN_GPeP': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_STN_GPeA': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeP_STN': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeA_STN': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeP_GPeA': {'p': 0.01, 'reci': False, 'seed': None},
        'Con_GPeA_GPeP': {'p': 0.01, 'reci': False, 'seed': None},
        'Seed': 6723162,
     #    'Seed': -1,
        'architecture': 'LocalInh',
        'con_mat': False,
        }
