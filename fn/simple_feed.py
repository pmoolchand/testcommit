# class_feed.py - establishes FeedExt(), ParFeedAll()
#
# ver: 0.0
# rev: 2017-01-17

from neuron import h as nrn

class ParSimpleFeed():
    # p_ext has a different structure for the extinput
    # usually, p_ext is a dict of cell types
    def __init__(self, event_vec):
        # VecStim setup
        self.eventvec = nrn.Vector()
        self.vs = nrn.VecStim()
        self.eventvec.from_python(event_vec)
        self.vs.play(self.eventvec)

    def connect_to_target(self):
        nc = nrn.NetCon(self.vs, None)
        nc.threshold = 0

        return nc
