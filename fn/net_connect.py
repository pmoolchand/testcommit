# net_connect - appends the network connectivity
#
# ver: 1.01
# rev: 2018-01-18 - Corrected network fields and seeds

import sys
import numpy as np
import itertools as it
from scipy.stats import bernoulli as bern

def grid_factors(N):
    f1 = int(np.floor(np.sqrt(N)))
    while N%f1 != 0:
        f1 = f1-1
    f2 = int(N/f1)
    x = max(f1,f2)
    y = min(f1,f2)
    return x,y


def connect_intranucleus(nuc_pop):

    nuc_pop['matrix'] = np.zeros((nuc_pop['N'], nuc_pop['N']), dtype=int)

    # Segregated RoundRobin Assignment of gids to subpopulations
    gid_subpop = np.tile(np.arange(nuc_pop['N_subpop']), -(-nuc_pop['N']//nuc_pop['N_subpop']))
    np.random.seed(seed=nuc_pop['seed'])

    # Randomize assignment of subpopulation
    if nuc_pop['Assign'] == 'Random':
        np.random.shuffle(gid_subpop)

    # Trim to number of cells
    gid_subpop = gid_subpop[nuc_pop['gids']]

    # Gather members of subpopulations
    subpop_list = np.empty(shape=nuc_pop['N_subpop'], dtype='O')
    for idx in np.arange(subpop_list.size):
        subpop_list[idx] = np.where(gid_subpop==idx)[0]

    # Within nuclear subpopulation connections
    for nuc_group in subpop_list:
        for cell_cell in it.product(nuc_group, repeat=2):
            nuc_pop['matrix'][cell_cell] = bern.rvs(nuc_pop['Int_Con_p'])

    if nuc_pop['N_subpop'] > 1:
        # Across subpopulation connections
        for idx, jdx in it.product(range(subpop_list.size), repeat=2):
            if idx != jdx:
                for cell_cell in it.product(subpop_list[idx], subpop_list[jdx]):
                    nuc_pop['matrix'][cell_cell] = bern.rvs(nuc_pop['Ext_Con_p'])

    nuc_pop['matrix'][np.diag_indices(nuc_pop['N'])] = 0.
    nuc_pop['subpop_list'] = subpop_list

def connect_GPes(nuc_pop):

    nuc_pop['matrix'] = np.zeros((nuc_pop['N'], nuc_pop['N']))

    for i, GPe1 in enumerate(nuc_pop['coords']):
        for j, GPe2 in enumerate(nuc_pop['coords']):
            coord_diff = np.subtract(GPe1,GPe2)
            if coord_diff.dot(coord_diff) <= 2.:
                nuc_pop['matrix'][i,j] = 1.

    nuc_pop['matrix'][np.diag_indices(nuc_pop['N'])] = 0.

    return nuc_pop


def connect_STNstoGPes(netwrk):

    np.random.seed(seed=netwrk['STN_GPe']['seed'])
    netwrk['STN_GPe']['matrix'] = bern.rvs(netwrk['STN_GPe']['Con_p'], size=(netwrk['GPe']['N'], netwrk['STN']['N']))

    return netwrk

def connect_GPestoSTNs(netwrk):

    netwrk['GPe_STN']['matrix'] = netwrk['STN_GPe']['matrix'].T

    return netwrk

def connect_GPeintra(nuc_pop):
    np.random.seed(seed=nuc_pop['seed'])
    nuc_pop['matrix'] = np.random.binomial(1, nuc_pop['Int_Con_p'], size=(nuc_pop['N'], nuc_pop['N']))

def connect_nuclei(network, eff, aff):
    comb = '{!s}_{!s}'.format(eff, aff)
    comb_rev = '{!s}_{!s}'.format(aff, eff)
    if network[comb]['reci'] and 'matrix' in network[comb_rev].keys():
        network[comb]['matrix'] = network[comb_rev]['matrix'].T
    else:
        np.random.seed(seed=network[comb]['seed'])
        network[comb]['matrix'] = bern.rvs(network[comb]['p'], size=(network[aff]['N'], network[eff]['N']))


def poget_net_props(netwrk):

    STN_Vini = netwrk['STN']['V_ini']
    GPeP_Vini = netwrk['GPeP']['V_ini']
    GPeA_Vini = netwrk['GPeA']['V_ini']

    netwrk['STN_GPe']['seed'] = connect_seeds[2]

    if netwrk['architecture'] == 'Unconnected':
        pass

    # connect ALL cells
    elif netwrk['architecture']  == 'AlltoAll':
        netwrk['connectivity_matrix'] += 1.

    elif netwrk['architecture'] == 'AllNoSTNSTN':
        netwrk['connectivity_matrix'] += 1.
        netwrk['connectivity_matrix'][0:netwrk['STN']['N'], 0:netwrk['STN']['N']] = 0.

    elif netwrk['architecture'] == 'LocalInh':

        netwrk['STN'] = connect_intranucleus(netwrk['STN'])
        netwrk['GPe'] = connect_GPes(netwrk['GPe'])
        netwrk = connect_STNstoGPes(netwrk)
        netwrk = connect_GPestoSTNs(netwrk)

        preSTN = np.concatenate((netwrk['STN']['matrix'], netwrk['GPe_STN']['matrix']),1)
        preGPe = np.concatenate((netwrk['STN_GPe']['matrix'], netwrk['GPe']['matrix']),1)
        netwrk['connectivity_matrix'] = np.concatenate((preSTN, preGPe))

    else:
        print("Unrecognized architecture, defaulting to Unconnected")

    if netwrk['con_mat'] is not False:
        netwrk['connectivity_matrix'] = np.array(netwrk['con_mat'])

    STN_Vini['vals'] = np.random.normal(STN_Vini['arg0'], STN_Vini['arg1'], netwrk['STN']['N'])
    GPeP_Vini['vals'] = np.random.normal(GPeP_Vini['arg0'], GPeP_Vini['arg1'], netwrk['GPeP']['N'])
    GPeA_Vini['vals'] = np.random.normal(GPeA_Vini['arg0'], GPeA_Vini['arg1'], netwrk['GPeA']['N'])

    return netwrk

def get_net_props(net):

    network = net

    # Get cell list
    Cell_list = [key[5:] for key in network.keys() if key.startswith('Cell_')]
    for cell in Cell_list:
        network[cell] = network['Cell_{!s}'.format(cell)]

    # Get connectivity list
    Conn_list = [key[4:] for key in network.keys() if key.startswith('Con_')]
    for con in Conn_list:
        network[con] = network['Con_{!s}'.format(con)]

    # Validate connectivity pairs
    for eff, aff in it.permutations(Cell_list,2):
        comb = '{!s}_{!s}'.format(eff, aff)
        if comb not in Conn_list:
            network[comb] = {'p': 0.0, 'reci': False, 'seed': None}
            Conn_list.append(comb)

    np.random.seed(seed=network['Seed'])
    connect_seeds = np.random.randint(999999999, size=len(Cell_list)+len(Conn_list))

    gid_rank = []

    for cell in Cell_list:
        gid_rank.append((network[cell]['gid_rank'], network[cell]['N'], cell))

    gid_rank.sort()

    network['gid_rank'] = gid_rank

    # Validate gid_rank
    N_cells = 0
    for idx, cell in enumerate(gid_rank):
        network[cell[2]]['gids'] = range(N_cells, N_cells+cell[1])
        network[cell[2]]['seed'] = connect_seeds[idx]
        N_cells += cell[1]
        # Create coordinates
        x, y = grid_factors(network[cell[2]]['N'])
        network[cell[2]]['coords'] = [pos for pos in it.product(np.arange(x), np.arange(y), [idx])]

    network['N_cells'] = N_cells

    for idx, con in enumerate(Conn_list):
        network[con]['seed'] = connect_seeds[idx+len(gid_rank)]

    connect_intranucleus(network['STN'])
    connect_GPeintra(network['GPeP'])
    connect_GPeintra(network['GPeA'])

    connect_nuclei(network, 'STN', 'GPeP')
    connect_nuclei(network, 'STN', 'GPeA')
    connect_nuclei(network, 'GPeP', 'STN')
    connect_nuclei(network, 'GPeA', 'STN')
    connect_nuclei(network, 'GPeP', 'GPeA')
    connect_nuclei(network, 'GPeA', 'GPeP')

    for cell in Cell_list:
        cell_vini = network[cell]
        cell_vini['ini_V'] = np.random.normal(cell_vini['V_ini'][1], cell_vini['V_ini'][2], cell_vini['N'])

    preSTN = np.concatenate((network['STN']['matrix'], network['GPeP_STN']['matrix'], network['GPeA_STN']['matrix']), 1)
    preGPeP = np.concatenate((network['STN_GPeP']['matrix'], network['GPeP']['matrix'], network['GPeA_GPeP']['matrix']), 1)
    preGPeA = np.concatenate((network['STN_GPeA']['matrix'], network['GPeP_GPeA']['matrix'], network['GPeA']['matrix']), 1)
    network['connectivity_matrix'] = np.concatenate((preSTN, preGPeP, preGPeA))

    network['connectivity_matrix'][np.diag_indices(network['N_cells'])] = int(0)

    if network['con_mat'] is not False:
        network['connectivity_matrix'] = np.array(netwrk['con_mat'])

    network['cells'] = Cell_list

    return(network)

if __name__ == '__main__':

    net_par = {
        'Cell_STN': {
            'N' : 300,
            'gid_rank': 0,
            'shape': 'grid',
            'N_subpop': 4,
            'Assign': 'Random',
            'Int_Con_p': 0.,
            'Ext_Con_p': 0,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0),
            },
        'Cell_GPeP':{
            'N': 6,
            'gid_rank': 1,
            'shape': 'grid',
            'N_subpop': 1,
            'Assign': 'Random',
            'Int_Con_p': 0.2,
            'Ext_Con_p': 0.,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0),
            },
        'Cell_GPeA':{
            'N': 6,
            'gid_rank': 2,
            'shape': 'grid',
            'N_subpop': 1,
            'Assign': 'Random',
            'Int_Con_p': 0.5,
            'Ext_Con_p': 0.,
            'autosyn': False,
            'V_ini': ('normal', -65.0, 3.0),
            },
        'Con_STN_GPeP': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_STN_GPeA': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeP_STN': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeA_STN': {'p': 0.02, 'reci': False, 'seed': None},
        'Con_GPeP_GPeA': {'p': 0.01, 'reci': False, 'seed': None},
        'Con_GPeA_GPeP': {'p': 0.01, 'reci': False, 'seed': None},
        'Seed': 6723162,
     #    'Seed': -1,
        'architecture': 'LocalInh',
        'con_mat': False,
        }

    net = get_net_props(net_par)
    print(net['STN']['subpop_list'])
   # print(net)
