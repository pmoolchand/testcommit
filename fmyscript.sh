#!/bin/bash

#SBATCH -N 1
#SBATCH -n 48 
#SBATCH -t 00:20:00
#SBATCH --job-name=ParameterSweeps
#SBATCH -p skx-normal 

export I_MPI_EXTRA_FILESYSTEM_LIST=garbage
module load phdf5
ibrun python3 runsim.py param/facdef.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
