: Hodgkin Huxley mechanisms with fixed ENa, EK, i.e., independent of ion flux, not using Nernst equation
: Models and parameters as per 10.1007/s10827-011-0366-4 (So, Grill, 2011)
: m - instantaneous; h, n - 1st order kinetics
: L-type Ca2+, T-type Ca2+ and [Ca2+] dependent K+, independent of ion flux, not using Nernst equation
: Models  and parameters as per 10.1007/s10827-011-0366-4
: a, b (T-type), s (L-type)  - instantaneous; r - 1st order kinetics

: 0.3
: rev 2017-03-10 (PM: paramters updated based on XPPAUT file)

NEURON {
    SUFFIX GPe 
    NONSPECIFIC_CURRENT ina
    NONSPECIFIC_CURRENT ik
    NONSPECIFIC_CURRENT il
    NONSPECIFIC_CURRENT ilca
    NONSPECIFIC_CURRENT itca
    NONSPECIFIC_CURRENT iahp

    : Parameter assignments possible through models
    RANGE gl, el
    RANGE gnabar, ena
    RANGE gkbar, ek
    RANGE eca, glcabar, gtcabar, gahp
    RANGE m, h, n
    RANGE s, a, r, Cai

    RANGE m_theta, m_sigma
    RANGE h_theta, h_sigma, h_phi, h_tau_theta, h_tau_sigma, h_tau_naught, h_tau_prime
    RANGE n_theta, n_sigma, n_phi, n_tau_theta, n_tau_sigma, n_tau_naught, n_tau_prime
    RANGE s_theta, s_sigma
    RANGE r_theta, r_sigma, r_phi, r_tau_naught, r_tau_prime, r_tau
    RANGE a_theta, a_sigma
    RANGE k1, kCa, epsilon, Ca_ini, Ca_scl

    RANGE i_int
}

UNITS {
    (S)  = (siemens)
    (mV) = (millivolt)
    (mA) = (milliamp)
}

PARAMETER {
    : leak
    gl      = 0.1e-3 (S/cm2)
    el      = -55.0   (mV)

    : Na+
    gnabar  = 120.0e-3 (S/cm2)
    ena     = 55.0    (mV)

        : activation gating variable, m
        m_theta = -37.0 (mV)
        m_sigma = 10.0 (mV) 

        : inactivation gating variable, h
        h_theta = -58.0 (mV)
        h_sigma = -12.0 (mV)
        h_phi = 0.05
        h_tau_theta = -40.0 (mV)
        h_tau_sigma = -12.0 (mV)
        h_tau_naught = 0.05 (ms)
        h_tau_prime = 0.27 (ms)

    : K+
    gkbar   = 30.0e-3 (S/cm2)
    ek      = -80.0   (mV)

        : non-inactivation gating variable, n
        n_theta = -50.0 (mV)
        n_sigma = 14.0 (mV)
        n_phi = 0.1
        n_tau_theta = -40.0 (mV)
        n_tau_sigma = -12.0 (mV)
        n_tau_naught = 0.05 (ms)
        n_tau_prime = 0.27 (ms)

    : Ca2+
    eca     = 120.0 (mV)

    : L-type Ca2+
    glcabar = 0.1e-3  (S/cm2)

        : non-inactivation gating variable, s
        s_theta = -35.0 (mV)
        s_sigma = 2.0 (mV)

    :   T-type Ca2+
    gtcabar = 0.5e-3  (S/cm2)

        : a
        a_theta = -57.0 (mV)
        a_sigma = 2.0 (mV)

	    : r
        r_theta = -70.0 (mV)
        r_sigma = -2.0 (mV)
        r_phi = 1.0
        r_tau = 30.0 (ms)

    : [Ca2+] dependent K+
    gahp    = 30.0e-3  (S/cm2)

    : [Ca2+] dynamics
    k1 = 30.0
    kCa = 15.0
    epsilon = 1.0e-4
    Ca_scl   = -10
    Ca_ini   = 5.2e-5
}

ASSIGNED {
    v       (mV)

    il      (mA/cm2)

    m
    gna     (S/cm2)
    ina     (mA/cm2)

    gk      (S/cm2)
    ik      (mA/cm2)

    s
    glca    (S/cm2)
    ilca    (mA/cm2)

    a
    gtca    (S/cm2)
    itca    (mA/cm2)

    iahp    (mA/cm2)

    i_int (mA/cm2)
}

STATE {
    h
    n
    r
    Cai
}

UNITSOFF

BREAKPOINT {
    SOLVE states METHOD cnexp

    : leak current
    il      =   gl * (v - el)

    : Na+ current
    m       =   1 / ( 1+ exp(-(v-m_theta)/m_sigma) )
    gna     =   gnabar * m * m * m * h
    ina     =   gna * (v - ena)

    : K+ current
    gk      =   gkbar * n * n * n * n
    ik      =   gk * (v - ek)

    : L-type Ca2+ current
    s       =   1/ ( 1+ exp(-(v-s_theta)/s_sigma) )
    glca    =   glcabar * s * s
    ilca    =   glca*(v-eca)

    : T-type Ca2+ current
    a       =   1/ ( 1+ exp(-(v-a_theta)/a_sigma) )
    gtca    =   gtcabar * a * a * a * r
    itca    =   gtca * (v - eca)

    : [Ca2+] dependent K+ current
    iahp    =   gahp * (Cai / (Cai + k1)) * (v - ek)

    : net intrinsic current
    i_int = il + ina + ik + ilca + itca + iahp 

}

INITIAL {
    h  =   1 /( 1+ exp(-(v-h_theta)/h_sigma) )
    n  =   1 /( 1+ exp(-(v-n_theta)/n_sigma) )
    r  =   1 /( 1+ exp(-(v-r_theta)/r_sigma) )
    Cai  =   Ca_ini
}

DERIVATIVE states {
    h'  =   h_phi * ((1/(1+exp(-(v-h_theta)/h_sigma)))-h) / (h_tau_naught+h_tau_prime/(1+exp(-(v-h_tau_theta)/h_tau_sigma)))
    n'  =   n_phi * ((1/(1+exp(-(v-n_theta)/n_sigma)))-n) / (n_tau_naught+n_tau_prime/(1+exp(-(v-n_tau_theta)/n_tau_sigma)))
    r'  =   r_phi * ((1/(1+exp(-(v-r_theta)/r_sigma)))-r) /r_tau
    Cai'=   epsilon  * ( Ca_scl *(ilca + itca) - kCa * Cai)
}

UNITSON
