#!/usr/bin/env python3
#
# runsim.py - main program to run simulations, optimized for HDF5 storage
#
# ver: 2.3 
# rev: 2019-02-06 - Explicit Feed attrs 

import sys, datetime, os, shutil, time
import importlib as implb
import itertools as it
from mpi4py import MPI
from neuron import h as nrn
nrn.load_file("stdrun.hoc")
from fn.params_default import get_params_default
import numpy as np
from class_net import Network
import gc
import h5py
import pprint




#def run_simulation(default_dict, network, ext_feed, RecDict, sim_file, sim, simidx):
def run_simulation(default_dict, nettup, feedtup, RecDict, sim_file, sim, simidx, param_updict):
    t0 = time.time()

    sim_path = sim['sim_id'].decode()
    ext_feed = feedtup[1][0]
    network = nettup[1][0]

    pc = nrn.ParallelContext()
    pc.barrier()
    pc.gid_clear()
    rank = int(pc.id())
    host_list = range(int(pc.nhost()))

    p = default_dict
    nrn.tstop = p['tstop']
    nrn.dt = p['dt']
    net = Network(p, network, ext_feed, RecDict)

    if rank == 0:
        # set t vec to record
        t_vec = nrn.Vector()
        t_vec.record(nrn._ref_t)

    # sets the default max solver step in ms (purposefully large)
    pc.set_maxstep(10)

    nrn.finitialize()
    nrn.fcurrent()
    nrn.frecord_init()

    # actual simulation
    pc.psolve(nrn.tstop)

    spktimes = np.array(net.spiketimes.to_python())
    spkgids = np.array(net.spikegids.to_python(), dtype=int)

    N = {}
    Ntot= {}
    N['STN'] = len(net.own_gid_dict['STNCell'])
    N['GPP'] = len(net.own_gid_dict['GPePCell'])
    N['GPA'] = len(net.own_gid_dict['GPeACell'])
    N['cells'] = N['STN'] + N['GPP'] + N['GPA']
    N['Ctx'] = len(net.own_gid_dict['CtxInput'])
    N['MSN'] = len(net.own_gid_dict['MSNInput'])
    Ntot['STN'] = network['STN']['N']
    Ntot['GPP'] = network['GPeP']['N']
    Ntot['GPA'] = network['GPeA']['N']
    Ntot['cells'] = Ntot['STN'] + Ntot['GPP'] + Ntot['GPA']
    Ntot['Ctx'] = len(net.gid_dict['CtxInput'])
    Ntot['MSN'] = len(net.gid_dict['MSNInput'])

    unique_gids = np.unique(spkgids)
    spkdict = {gid: [] for gid in unique_gids}

    cell_gid_spikes = unique_gids[unique_gids<Ntot['cells']]

    for gid, spktim in zip(spkgids, spktimes):
        spkdict[gid].append(spktim)

    for gid, spks in spkdict.items():
        trec_idx  = np.searchsorted(spks, p['trec_ini'])
        spkdict[gid] = spks[trec_idx:] 

    # share the spike data
    mpicom = MPI.COMM_WORLD

    if rank != 0:
        mpicom.send(spkdict, dest=0, tag=101)
        mpicom.send(cell_gid_spikes, dest=0, tag=102)

    if rank == 0:
        for src in host_list:
            if src != 0:
                spkdict.update(mpicom.recv(source=src, tag=101))
                cell_gid_spikes = np.append(cell_gid_spikes, mpicom.recv(source=src, tag=102))

    # create indices of time points to be written
    t_write = np.arange(p['trec_ini']/p['dt'], (p['tstop']/p['dt'])+p['dt'], p['trec']/p['dt'], dtype = int)

    N_col = t_write.size

    RecList = []
    RecValDict = {}
    StoreDict = {}

    # Create lists
    for key, val in RecDict.items():
        for var in val:
            RecList.append(key+var)
            RecValDict[key+var] = np.full((N[key[0:3]], N_col), np.NaN)

    if p['RecSTNSta_V']:
        RecList.append('STNSta_V')
        RecValDict['STNSta_V'] = np.full((N['STN'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['STNCell']):
            RecValDict['STNSta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    if p['RecGPPSta_V']:
        RecList.append('GPPSta_V')
        RecValDict['GPPSta_V'] = np.full((N['GPP'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['GPePCell']):
            RecValDict['GPPSta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    if p['RecGPASta_V']:
        RecList.append('GPASta_V')
        RecValDict['GPASta_V'] = np.full((N['GPA'], N_col), np.NaN)
        for idx, gid_cell in enumerate(net.own_gid_obj['GPeACell']):
            RecValDict['GPASta_V'][idx,:] = np.array(gid_cell[1].v_soma.to_python())[t_write]

    RecList.sort()
    RecListSTN = [var for var in RecList if var.startswith('STN')]
    RecListGPP = [var for var in RecList if var.startswith('GPP')]
    RecListGPA = [var for var in RecList if var.startswith('GPA')]

    for idx, gid_cell in enumerate(net.own_gid_obj['STNCell']):
        for state in RecDict['STNSta']:
            RecValDict['STNSta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['STNSyn']:
            RecValDict['STNSyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

   # for idx, gid_cell in enumerate(net.own_gid_obj['GPeCell']):
    for idx, gid_cell in enumerate(net.own_gid_obj['GPePCell']):
        for state in RecDict['GPPSta']:
            RecValDict['GPPSta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['GPPSyn']:
            RecValDict['GPPSyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

    for idx, gid_cell in enumerate(net.own_gid_obj['GPeACell']):
        for state in RecDict['GPASta']:
            RecValDict['GPASta{!s}'.format(state)][idx,:] =  np.array(getattr(gid_cell[1], '{!s}_soma'.format(state)).to_python())[t_write]
        for synap in RecDict['GPASyn']:
            RecValDict['GPASyn{!s}'.format(synap)][idx,:] =  np.array(getattr(gid_cell[1], 'i{!s}_soma'.format(synap)).to_python())[t_write]

    writefile = h5py.File(sim_file, 'a', driver='mpio', comm=MPI.COMM_WORLD, libver='latest')
    #writefile.atomic = True
    writefile.create_group(sim_path)

    for dat in RecList:
        StoreDict[dat] = writefile.create_dataset(os.path.join(sim_path, 'raw', dat), (Ntot[dat[0:3]], N_col), dtype='f4')

    if net.own_gid_dict['STNCell']:
        STNcoord = np.array(net.own_gid_dict['STNCell'])
        for dat in RecListSTN:
            StoreDict[dat][STNcoord,:] = RecValDict[dat]

    if net.own_gid_dict['GPePCell']:
        GPePcoord = np.array(net.own_gid_dict['GPePCell']) - Ntot['STN']
        for dat in RecListGPP:
            StoreDict[dat][GPePcoord,:] = RecValDict[dat]

    if net.own_gid_dict['GPeACell']:
        GPeAcoord = np.array(net.own_gid_dict['GPeACell']) - Ntot['STN'] - Ntot['GPP']
        for dat in RecListGPA:
            StoreDict[dat][GPeAcoord,:] = RecValDict[dat]

    writefile.close()
    mpicom.barrier()

    if rank == 0:

        writefile = h5py.File(sim_file, 'a', libver='latest')

        spkdt = h5py.special_dtype(vlen=np.dtype('f4'))
        giddt = h5py.special_dtype(vlen=np.dtype('u2'))
        spk = writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'raw', 'spikes')), (Ntot['cells'],), dtype=spkdt, fillvalue=None)

        for gid in cell_gid_spikes:
            spk[gid] = np.array(spkdict[gid])

        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'cells')), data = np.string_(net.network['cells']))
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'timesteps')), data = np.array(t_vec.to_python())[t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'con_mat')), data = net.network['connectivity_matrix'], dtype='u1')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_STN')), data = net.STN_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_GPeP')), data = net.GPeP_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clamps_GPeA')), data = net.GPeA_clamp_com[:, t_write], dtype='f4')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_STN')), data = net.STN_clamp_idx, dtype='u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_GPeP')), data = net.GPeP_clamp_idx, dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'clm_asg_GPeA')), data = net.GPeA_clamp_idx, dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_STN')), data = net.gid_dict['STNCell'], dtype= 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_GPeP')), data = net.gid_dict['GPePCell'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_GPeA')), data = net.gid_dict['GPeACell'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'gids_Ctx')), data = net.own_gid_dict['CtxInput'], dtype = 'u2')
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'feeds_Ctx')), data = net.Ctx_Feed_Combos, dtype = spkdt)
        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'Ctx_asg_STN')), data = net.STN_Ctx_Combo_idx, dtype = 'u2')

        writefile.create_dataset('{!s}'.format(os.path.join(sim_path, 'props', 'STN_subpop')), data = net.network['STN']['subpop_list'], dtype=giddt)

        tfin = time.time()
        writefile[sim_path]['props'].attrs['UTC_start'] = '{!s}'.format(time.asctime(time.gmtime(t0)))
        writefile[sim_path]['props'].attrs['UTC_end'] = '{!s}'.format(time.asctime(time.gmtime(tfin)))
        writefile[sim_path]['props'].attrs['LOC_start'] = '{!s}'.format(time.asctime(time.localtime(t0)))
        writefile[sim_path]['props'].attrs['LOC_end'] = '{!s}'.format(time.asctime(time.localtime(tfin)))
        writefile[sim_path]['props'].attrs['duration_s'] = '{:.3f}'.format(tfin-t0)


        write_attr(writefile[sim_path], nettup[2])
        write_attr(writefile[sim_path], flat_extfeed(ext_feed))
        write_attr(writefile[sim_path], flat_param(param_updict))

        writefile[sim_path].attrs['net_seed'] = network['Seed']
        writefile[sim_path].attrs['feed_seed'] = ext_feed['Seed']


        simcontsfile = np.array(writefile['sim_contents'])
        simcontsfile['Complete'][simidx] = True 
        simcontsfile['cores'][simidx] = int(pc.nhost()) 
        simcontsfile['dur'][simidx] = time.time() - t0 
        writefile['sim_contents'][...] = simcontsfile 

        writefile.close()

    pc.barrier()
    pc.gid_clear()
    del net
    del spkdict
    gc.collect()

    if rank == 0:
        print('Completion time: {:.3f} s'.format(time.time()-t0))

class Simulation():
    def __init__(self, paramfile, sys_det=('NA', 'NA', 'NA', 'NA')):

        # Set up MPI and Parallel Context
        self.t0ini = time.time()
        self.mpicom = MPI.COMM_WORLD
        self.N_hosts = self.mpicom.Get_size()
        self.__set_parallel()
        self.sys_det = sys_det

        # Get parameters 
        param_att_lst = ['param_dict', 'cart_prod_keys', 'rec_dict', 'RecDict', 'factor_keys', 'upfeeddict']
        param_res = self.get_sim_params(paramfile)
        for att, res in zip(param_att_lst, param_res):
            setattr(self, att, res)

        # Validate param_file 
        self.default_dict = get_params_default()
        self.validate_paramfile(self.default_dict, self.param_dict)
        self.update_feedparams(self.default_dict, self.upfeeddict)

        self.default_dict.update(self.param_dict)

        # Get param combinations
        self.param_combo = self.get_param_combo(self.cart_prod_keys, self.param_dict, self.factor_keys)
        
        # Get seeds
        self.extfeed_trial_seeds = self.get_seeds(self.param_dict['N_extfeed_trials']) 
        self.network_trial_seeds = self.get_seeds(self.param_dict['N_network_trials']) 

        # Get extfeed, net list & seed_list
        self.extfeed_list, self.extfeed_seed_arr = self.get_feednet_lists(self.param_dict, self.extfeed_trial_seeds, 'extfeed')
        self.network_list, self.network_seed_arr = self.get_feednet_lists(self.param_dict, self.network_trial_seeds, 'net')

        self.object_counts_dict = self.get_object_counts()

        self.sim_contents = self.get_sim_contents(self.object_counts_dict)

        # Create file & write contents
        if self.default_dict['Resume']:
            self.Resume = True
            self.resume_file(self.default_dict['ResumeFile'])
        else:
            self.Resume = False 
            self.create_file()

        self.runsim()

        self.complete_sim()

    def __set_parallel(self): 
        # Set the Parallel Machinery
        self.mpicom = MPI.COMM_WORLD
        self.pc = nrn.ParallelContext()
        self.rank = int(self.pc.id())
        self.host_list = range(int(self.pc.nhost()))

    def get_sim_params(self, paramfile):
        f = open(paramfile, 'r')
        fl = f.readlines()
        cart_prod_keys = []
        RecList = []
        RecDict = {'STNSta': [], 'STNSyn': [], 'GPPSta': [], 'GPPSyn': [], 'GPASta': [], 'GPASyn': [], 'STNClm': [], 'GPeClm': []}
        rec_dict = {}
        testdict = {}
        upfeeddict = {'sig': None, 'glcon': None}
        newdict = {line.split(':')[0].strip():line.split(':')[1][1:-1].strip() for line in fl if not (line.startswith('#') or line.startswith('\n'))}
    
        if 'qFeedx_up' in newdict.keys():
            upfeeddict['sig'] = newdict['qFeedx_up']
            newdict.pop('qFeedx_up')
    
        factor_keys = []
    
        for key, val in newdict.items():
            if (val.startswith('{') and val.endswith('}')):
                tmp = []
                for item in val[1:-1].split(','):
                    tmp.append(self.validate_string(item))
                tmp = list(set(tmp))
                if len(tmp) > 1:
                    cart_prod_keys.append(key)
                    newdict[key] = np.sort(tmp)
                else:
                    newdict[key] = tmp[0]
            elif val.startswith('*'):
                splt = val.split('*')
                factor_keys.append((key, self.validate_string(splt[1]), self.validate_string(splt[2])))
            else:
                newdict[key] = self.validate_string(val)
    
            if key.startswith('Rec') and newdict[key] is True:
                rec_dict.update({key[3:]:None})
                RecList.append(key[3:])
    
        factor_keys.sort()
        for fac in factor_keys:
            if fac[2] not in newdict.keys():
                print('Invalid taget key for multiplier')
                factor_keys.remove(fac)
            else:
                if not isinstance(fac[1], (int, float)):
                    print('Invalid value as factor')
                    factor_keys.remove(fac)
    
        for rec in RecList:
            RecDict[rec[0:6]].append(rec[6:])
    
        cart_prod_keys.sort()
    
        # Convert to list
        for key in ['ext_feed_file', 'net_file']:
            if key not in cart_prod_keys:
                newdict[key] = newdict[key].split()
            else:
                cart_prod_keys.remove(key)
    
        if '_V' in RecDict['STNSta']:
            newdict['RecSTNSta_V'] = True
            RecDict['STNSta'].remove('_V')
    
        if '_V' in RecDict['GPPSta']:
            newdict['RecGPPSta_V'] = True
            RecDict['GPPSta'].remove('_V')
    
        if '_V' in RecDict['GPASta']:
            newdict['RecGPASta_V'] = True
            RecDict['GPASta'].remove('_V')

        return newdict, cart_prod_keys, rec_dict, RecDict, factor_keys, upfeeddict

    def validate_string(self, s):
        if s == 'True':
            return True
        elif s == 'False':
            return False
        else:
            try:
                return int(s)
            except ValueError:
                try:
                    return float(s)
                except ValueError:
                    return s.strip()

    def get_dir_path(self, param_dict):
        # get simulation path
        datenow = datetime.datetime.now()
        datepath = os.path.join(param_dict['save_dir'], datenow.strftime("%Y-%m-%d"))
        sys_name = param_dict['sys_name']
        fil_pre = '{!s}{!s}_{!s}'.format(sys_name, datenow.strftime("%Y%m%d"), param_dict['sim_prefix'])
    
        # check for existing in datpath
        if os.path.isdir(datepath):
            dirlist = os.listdir(datepath)
            tmplist = [item for item in dirlist if item.startswith(fil_pre)]
            if tmplist:
                tmplist.sort()
                n = int((tmplist[-1].split('-')[1]).split('.')[0])
            else:
                n = -1
        else:
            n = -1
    
        dirpath = os.path.join(datepath, '{!s}-{:03d}'.format(fil_pre,n+1))
        return datepath, dirpath

    def __makedir(self):
        if self.rank == 0:
            os.makedirs(self.datepath, exist_ok=True)


    def validate_paramfile(self, default_dict, param_dict):
        if self.rank == 0:
            def_dict_keys =  set(default_dict.keys())
            param_dict_keys = set(param_dict.keys())
            alien_keys =  param_dict_keys.difference(def_dict_keys)

            if len(alien_keys):
                print(alien_keys, ' not in params_defaulit')
                sys.exit(1)

    def update_feedparams(self, param_dict, upfeeddict):
        param_dict['upfeed'] = upfeeddict

    def get_param_combo(self, cart_prod_keys, param_dict, factor_keys):
        # Get params_default and set up Cartesian product of parameters
        cart_prod_vals = []
        for key in cart_prod_keys:
            cart_prod_vals.append(param_dict[key])

        # create param combo
        param_combo = []
        for p_idx, comb in enumerate(it.product(*cart_prod_vals)):
            comb_dict = {key:val for key, val in zip(cart_prod_keys, comb)}
            for fac in factor_keys:
                comb_dict[fac[0]] = fac[1] * comb_dict[fac[2]]
            param_combo.append((comb_dict, p_idx))
        
        N_parcom = len(param_combo)
        if N_parcom: 
            parcom_keys = list(param_combo[0][0].keys())
            parcom_keys.sort()
            parcom_dt = np.dtype([('param_id', 'S8', 1)] + [(k, np.float, 1) for k in parcom_keys])
            param_combo_arr = np.empty(shape=(N_parcom), dtype = parcom_dt)
            for idx, parc in enumerate(param_combo):
                param_combo_arr['param_id'][idx] = 'P{:03d}'.format(idx) 
                for nam in parcom_keys:
                    param_combo_arr[nam][idx] = param_combo[idx][0][nam]
        else:
            param_combo_arr = np.array([])

        return param_combo_arr

    def get_seeds(self, N):
        N_trials = max(1, N) 
        if self.rank == 0:
            N_trial_seeds = np.random.randint(1e7, size=(N_trials), dtype=np.int)
        else:
            N_trial_seeds = None
        N_trial_seeds = self.mpicom.bcast(N_trial_seeds, root = 0)
        return N_trial_seeds 

    def get_feednet_lists(self, param_dict, trial_seeds, obj):
        obj_list = []
        obj_seed_list = []
    
        if obj == 'extfeed':
            for idx, files in enumerate(param_dict['ext_feed_file']):
                if not os.path.isfile(os.path.join(os.getcwd(), 'extfeeds', files+'.py')):
                    print('\nExternal feed file {!s} does not exist! Exiting gracefully...\n'.format(files))
                    sys.exit(1)
                else:
                    extfeeditem = implb.import_module('extfeeds.'+files).ext_feed
                 #   extfeed_list.append((files, (extfeeditem, idx), flat_extfeed(extfeeditem)))
                    obj_list.append((files, (extfeeditem, idx), None))
                    file_seeds = [int(seed) for seed in extfeeditem['Seed'][:param_dict['N_extfeed_trials']] if seed > 0 ]
                    obj_seed_list.append(np.concatenate((file_seeds, trial_seeds[len(file_seeds):])).astype(np.int))
    
        elif obj == 'net':
            for idx, files in enumerate(param_dict['net_file']):
                if not os.path.isfile(os.path.join(os.getcwd(), 'networks', files+'.py')):
                    print('\nNetwork file {!s} does not exist! Exiting gracefully...\n'.format(files))
                    sys.exit(1)
                else:
                    networkitem = implb.import_module('networks.'+files).net_params
                    obj_list.append((files, (networkitem, idx), flat_network(networkitem)))
                    file_seeds = [int(seed) for seed in networkitem['Seed'][:param_dict['N_network_trials']] if seed > 0 ]
                    obj_seed_list.append(np.concatenate((file_seeds, trial_seeds[len(file_seeds):])).astype(np.int))

        N_objs = len(obj_list)
        N_trials = len(obj_seed_list[0])
        obj_dt = np.dtype([(obj, 'S32', 1), ('Seeds', np.uint32, (1, N_trials))]) 
        obj_seed_arr = np.empty(shape=N_objs, dtype=obj_dt)
        for idx, seed in enumerate(obj_seed_list):
            obj_seed_arr[idx] = obj_list[idx][0], seed 

        return obj_list, obj_seed_arr

    def get_object_counts(self):
        N_params = self.param_combo.shape[0]
        N_network = len(self.network_list)
        N_extfeed = len(self.extfeed_list)
        N_nsd = self.network_seed_arr['Seeds'][0].shape[1]
        N_esd = self.extfeed_seed_arr['Seeds'][0].shape[1]
        return {'params': N_params, 'networks': N_network, 'extfeeds': N_extfeed, 'net_trials': N_nsd, 'efd_trials': N_esd}
         
    def get_sim_contents(self, count_dict):
        len_par = count_dict['params']
        len_ext = count_dict['extfeeds']
        len_net = count_dict['networks']
        len_esd = count_dict['efd_trials']
        len_nsd = count_dict['net_trials']

        sim_dtype = np.dtype([('sim_id', 'S8', 1), ('Complete', np.bool), ('param', np.uint32, 1), ('network', np.uint32, 1), ('extfeed', np.uint32,1), 
                    ('network_seed', np.uint32, 1), ('extfeed_seed', np.uint32, 1), ('dur', np.float, 1), ('cores', np.uint32, 1)])
        N_sims = len_par * len_net * len_ext * len_nsd * len_esd
        
        sim_contents = np.empty(shape=(N_sims), dtype=sim_dtype) 
    
        idx = 0
        for i in range(len_par):
            for j in range(len_net):
                for k in range(len_ext):
                    for l in range(len_nsd):
                        for m in range(len_esd):
                          #  sim_contents[idx] = ('S{:03d}'.format(idx), False, i, network_list[j][1][-1], network_seed[j][k], extfeed_list[l][1][-1], extfeed_seed[l][m])
                            sim_contents[idx] = ('S{:03d}'.format(idx), False, i, j, k, l, m, 0., self.N_hosts)
                            idx += 1
        return sim_contents 

    def get_param_upddict(self, param_arr):
        N_parcom = param_arr.shape[0]    
        if N_parcom > 1:
            param_dt_lst = list(param_arr.dtype.names)[1:]
            param_com_dict_obj = np.empty(shape=N_parcom, dtype='O')
            for idx, parcom in enumerate(param_arr):
                param_com_dict_obj[idx] = dict([(k, parcom[k]) for k in param_dt_lst])
        else:
            param_com_dict_obj[0] = {}
        return param_com_dict_obj
                    
    def create_file(self):
        # Get & create directory path
        self.datepath, self.dirpath = self.get_dir_path(self.param_dict)
        self.__makedir()

        # Create and close HDF5 file, sync
        self.sim_file = self.dirpath+'.hdf5'
    
        if self.rank == 0:
            # Write filename to text file to access via shell
            with open('{!s}_{!s}.txt'.format(self.sys_det[0], self.sys_det[3]), 'a') as f:
                f.write(self.sim_file)
    
        self.sim_fil = h5py.File(self.sim_file, 'w-', driver='mpio', comm=MPI.COMM_WORLD, libver='latest')

        # Write sim contents
        self.sim_fil['sim_contents'] = self.sim_contents 
        self.sim_fil['param_combo'] = self.param_combo
        self.sim_fil['network_seed'] = self.network_seed_arr
        self.sim_fil['extfeed_seed'] = self.extfeed_seed_arr

        # Write creation attributes
        self.sim_fil.attrs['n_hosts'] = int(self.pc.nhost())
        self.sim_fil.attrs['UTC_start'] = '{!s}'.format(time.asctime(time.gmtime()))
        self.sim_fil.attrs['LOC_start'] = '{!s}'.format(time.asctime(time.localtime()))
        self.sim_fil.attrs['JOB_ID'] = self.sys_det[0]
        self.sim_fil.attrs['NODE_ID'] = self.sys_det[1]
        self.sim_fil.attrs['QOS'] = self.sys_det[2]
        self.sim_fil.attrs['CLUSTER'] = self.sys_det[3]
        self.sim_fil.attrs['N_resume'] = 0 

        self.sim_fil.close()

    def resume_file(self, sim_file):
        self.sim_file = sim_file 
        self.sim_fil = h5py.File(self.sim_file, 'a', driver='mpio', comm=MPI.COMM_WORLD, libver='latest')

        self.sim_run_arr = np.where(self.sim_fil['sim_contents']['Complete']==False)[0]
        sim_dirs = self.sim_fil['sim_contents']['sim_id'][ self.sim_run_arr] 
        N_resume = self.sim_fil.attrs['N_resume'] + 1
        respre = 'Res{:02d}_'.format(N_resume)

        self.sim_fil.attrs[respre + 'n_hosts'] = int(self.pc.nhost())
        self.sim_fil.attrs[respre + 'UTC_start'] = '{!s}'.format(time.asctime(time.gmtime()))
        self.sim_fil.attrs[respre + 'LOC_start'] = '{!s}'.format(time.asctime(time.localtime()))
        self.sim_fil.attrs[respre + 'JOB_ID'] = self.sys_det[0]
        self.sim_fil.attrs[respre + 'NODE_ID'] = self.sys_det[1]
        self.sim_fil.attrs[respre + 'QOS'] = self.sys_det[2]
        self.sim_fil.attrs[respre + 'CLUSTER'] = self.sys_det[3]
        self.sim_fil.attrs[respre + 'ResumeArr'] = self.sim_run_arr 
        self.sim_fil.attrs['N_resume'] = N_resume

        self.Res_sim_contents = np.array(self.sim_fil['sim_contents'][self.sim_run_arr])
        self.Res_param_combo = np.array(self.sim_fil['param_combo'])

        for simdir in sim_dirs:
            if simdir in self.sim_fil:
                del self.sim_fil[simdir]    

        self.sim_fil.close()

    def runsim(self):

        if self.Resume:
            sim_run_arr = self.sim_run_arr
            sim_contents = self.Res_sim_contents
            param_combo = self.Res_param_combo
        else:
            sim_contents = self.sim_contents
            sim_run_arr = np.arange(sim_contents.shape[0])
            param_combo = self.param_combo
            
        N_parcom = param_combo.shape[0]     
        if N_parcom > 1:
            parcom_status = True
            parcom_updict = self.get_param_upddict(param_combo)
        else:
            parcom_status = False
            parcom_updict = {} 
            
        if parcom_status: 
            for simidx, sim in zip(sim_run_arr, sim_contents):
                param_update_dict = parcom_updict[sim['param']]
                self.default_dict.update(param_update_dict)
                netwrk = self.network_list[sim['network']]
                extfd = self.extfeed_list[sim['extfeed']]
                netwrk[1][0].update({'Seed': self.network_seed_arr['Seeds'][sim['network']][0, sim['network_seed']]})
                extfd[1][0].update({'Seed': self.extfeed_seed_arr['Seeds'][sim['extfeed']][0, sim['extfeed_seed']]})
             #   if self.rank == 0:
             #       print('Sim: {:2d}/{:d}, ParameterSet: {:d}/{:d}, FeedSet: {:d}/{:d}, FeedTrial: {:d}/{:d}, NetworkSet: {:d}/{:d}, NetworkTrial: {:d}/{:d}'.\
             #       format(sim_idx, len_sim, pidx+1, len_par, fidx+1, len_ext, ftri+1, N_extfeed_trials, nidx+1, len_net, ntri+1, N_network_trials ), end=' ')
                self.pc.barrier()
                run_simulation(self.default_dict, netwrk, extfd, self.RecDict, self.sim_file, sim, simidx, param_update_dict)
                self.pc.barrier()
        else:
            for simidx, sim in zip(sim_run_arr, sim_contents):
                netwrk = self.network_list[sim['network']]
                extfd = self.extfeed_list[sim['extfeed']]
                netwrk[1][0].update({'Seed': self.network_seed_arr['Seeds'][sim['network']][0, sim['network_seed']]})
                extfd[1][0].update({'Seed': self.extfeed_seed_arr['Seeds'][sim['extfeed']][0, sim['extfeed_seed']]})
             #   if self.rank == 0:
             #       print('Sim: {:2d}/{:d}, ParameterSet: {:d}/{:d}, FeedSet: {:d}/{:d}, FeedTrial: {:d}/{:d}, NetworkSet: {:d}/{:d}, NetworkTrial: {:d}/{:d}'.\
             #       format(sim_idx, len_sim, pidx+1, len_par, fidx+1, len_ext, ftri+1, N_extfeed_trials, nidx+1, len_net, ntri+1, N_network_trials ), end=' ')
                self.pc.barrier()
                run_simulation(self.default_dict, netwrk, extfd, self.RecDict, self.sim_file, sim, simidx, {})
                self.pc.barrier()
    

    def complete_sim(self):
        if self.rank == 0:
            f = h5py.File(self.sim_file, 'a', libver='latest')
            def_dict_list = [('defd_'+key, val) for key, val in self.default_dict.items()]
            param_def_list = [('pard_'+key, val) for key, val in  self.param_dict.items()]
            write_attr(f, param_def_list)
            write_attr(f, def_dict_list)
            if not self.Resume:
                f.attrs['total_dur'] = time.time() - self.t0ini
            f.close()

        if self.pc.nhost > 1:
            self.pc.runworker()
            self.pc.done()
        nrn.quit()
            

def popo():
    if rank == 0:

        writefile = h5py.File(sim_file, 'a', libver='latest')
        write_attr(writefile, def_dict_list)
        write_attr(writefile, param_def_list)

        writefile.attrs['net_file'] = [np.string_(i) for i in param_dict['net_file']] 
        writefile.attrs['ext_feed_file'] = [np.string_(i) for i in param_dict['ext_feed_file']] 

        if param_dict['fixed_hier'] == 'extfeed':
            for param, feed, fidx, network, nidx in it.product(param_combo, extfeed_list, range(N_extfeed_trials), network_list, range(N_network_trials)):
                write_attr(writefile['P{:03d}'.format(param[1])], flat_param(param[0]))
              #  write_attr(writefile['P{:03d}'.format(param[1])][feed[0]], feed[2])
                write_attr(writefile['P{:03d}'.format(param[1])][feed[0]], flat_extfeed(feed[1][0]))
                writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)].attrs['Seed'] = int(extfeed_seed_list[feed[1][1]][fidx])
                write_attr(writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)][network[0]], network[2])
                writefile['P{:03d}'.format(param[1])][feed[0]]['F{:03d}'.format(fidx)][network[0]]['N{:03d}'.format(nidx)].attrs['Seed'] = int(network_seed_list[network[1][1]][nidx])
        else:
            for param, network, nidx, feed, fidx in it.product(param_combo, network_list, range(N_network_trials), extfeed_list, range(N_extfeed_trials)):
                write_attr(writefile['P{:03d}'.format(param[1])], flat_param(param[0]))
                write_attr(writefile['P{:03d}'.format(param[1])][network[0]], network[2])
                writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)].attrs['Seed'] = int(network_seed_list[network[1][1]][nidx])
              #  write_attr(writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]], feed[2])
                write_attr(writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]], flat_extfeed(feed[1][0]))
                writefile['P{:03d}'.format(param[1])][network[0]]['N{:03d}'.format(nidx)][feed[0]]['F{:03d}'.format(fidx)].attrs['Seed'] = int(extfeed_seed_list[feed[1][1]][fidx])

        writefile.close()

    if pc.nhost > 1:
        pc.runworker()
        pc.done()
    nrn.quit()

def flat_extfeed(extfeed_dict):
    flat_list = []
    for key, val in extfeed_dict.items():
        if type(val) is dict:
            for kkey, vval in val.items():
                if type(vval) is dict:
                    for kkkey, vvval in vval.items():
                        flat_list.append(('{!s}_{!s}_{!s}'.format(key, kkey, kkkey), vvval))
                else:
                    flat_list.append(('{!s}_{!s}'.format(key, kkey), vval))
        else:
            flat_list.append((key, val))
    return flat_list

def flat_network(network_dict):
    flat_list = []
    for key, val in network_dict.items():
        if type(val) is dict:
            for kkey, vval in val.items():
                flat_list.append(('{!s}_{!s}'.format(key, kkey), vval))
        else:
            flat_list.append((key, val))
    return flat_list

def flat_param(param_dic):
    param_dic_list = []
    for key, val in param_dic.items():
        param_dic_list.append(('param_'+key, val))
    return param_dic_list

def write_attr(obj, attlist):
    for key, val in attlist:
        try:
            obj.attrs[key] = val
        except:
            obj.attrs[key] = np.string_(val)



if __name__ == "__main__":
    # reads the specified param file

    if sys.argv[1].endswith('.param'):
        paramfile = sys.argv[1]

        if len(sys.argv) == 6:
            sys_det = (sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])  # JOB_ID, NODE_NAME, QOS, CLUSTER
        else:
            sys_det = ('NA', 'NA', 'NA', 'NA')

       # exec_sim(paramfile, sys_det)
        Simulation(paramfile, sys_det)

    else:
        print('Usage: {!s} param_input incorrect'.format(sys.argv[0]))
        sys.exit(1)
