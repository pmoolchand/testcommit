#!/bin/bash

#SBATCH -N 7
#SBATCH -n 336 
#SBATCH -t 1:15:00
#SBATCH --job-name=Sing_Pop_Reg
#SBATCH -p skx-normal 
#SBATCH --mail-type=ALL
#SBATCH --mail-user=prannath_moolchand@brown.edu

export I_MPI_EXTRA_FILESYSTEM_LIST=garbage
module load python3
module load phdf5/1.8.16 
#ibrun python3 runsim.py param/QuadPopNoSEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopNoSReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
ibrun python3 runsim.py param/newtunesingpop.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopIntrin.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
