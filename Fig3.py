#! python3

from mpi4py import MPI
import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
#    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
#         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.gridspec as gs
import matplotlib.figure as mpl_figure
import matplotlib.style as mpl_style
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it
import plot_utils as plut

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])



def reso_freq():


#    filRH='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_OneCell-000.hdf5' 
#    filLE='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_OneCell-000.hdf5' 
#    filHE='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_OneCell-000.hdf5' 


    filRH='/scratch/04119/pmoolcha/STNGPe/2021-05-01/TAC20210501_Spiking_SingPop_Reg_AMPANMDA-000.hdf5'
    filLE='/scratch/04119/pmoolcha/STNGPe/2021-05-01/TAC20210501_Spiking_SingPop_Evt_AMPANMDALoInt-000.hdf5'
    filHE='/scratch/04119/pmoolcha/STNGPe/2021-05-01/TAC20210501_Spiking_SingPop_Evt_AMPANMDAHiInt-000.hdf5'

    timrng=[1250, 2500]
    anRH=Plots(filRH, None, tmin=timrng[0], tmax=timrng[1]) 
    anLE=Plots(filLE, None, tmin=timrng[0], tmax=timrng[1]) 
    anHE=Plots(filHE, None, tmin=timrng[0], tmax=timrng[1]) 

    N_NMDA_pos = [0,1,2] 
    N_NMDA = 3 
    NM_lab = ['Low', 'Med', 'High']

    RH = [(2,'2'), (4,'4'), (8,'8'), (12.5,'13'), (20,'20'), (32,'32')]
#    RH = [(2,'2'), (4,'4')]
    RH_plot=np.array([2,4,8,12.5,20,32],dtype=int)
#    RH_plot=np.array([2,4])

    EV = np.array([10, 31, 50, 80, 125, 250, 500],dtype=int)
#    EV = np.array([10, 125])

    figprops = {                                                                                                                                                                                    
               # 'fig_siz': (5, 6),
           #     'fig_siz': (8, 4),
                'fig_siz': (9,8),
             #   'fig_siz': (12,9),
                'glshap': (1,2),
                'wratio': [2,1],
                }

    fig, gl = anRH.create_fig(figprops, constrained_layout=True)
    gl.tight_layout(fig)

    def set_axes(): 

        def freq_axes():
            hratio = [1,2,4, 1,2,4]
            freqspec = gs.GridSpecFromSubplotSpec(6, 2, subplot_spec=gl[:, 0], height_ratios=hratio)
            axr_arr = np.empty(shape=freqspec.get_geometry(), dtype='O')
            tit_arr = np.empty(shape=freqspec.get_geometry(), dtype='O')

            for i, _ in np.ndenumerate(axr_arr[:,:3]):
                axr_arr[i] = fig.add_subplot(freqspec[i])
                anRH.style_axis(axr_arr[i], form=False)
    
            for _, ax in np.ndenumerate(axr_arr[:,:2]): 
                axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], ax) 
    
    
            for i in [0,3]:
                for j in range(2):
                    axr_arr[i,j].axis('off')
                 #   axr_arr[i+1,j].get_xaxis().set_visible(False)
                    axr_arr[i+1,j].xaxis.set_visible(False)
                    axr_arr[i+1,j].xaxis.tick_bottom()
                    axr_arr[i,j].set_ylim([0,1])
                    axr_arr[0,0].get_shared_y_axes().join(axr_arr[0,0], axr_arr[i,j]) 
    #                anRH.style_axis(axr_arr[i,j], bottom=False, left=False, form=True)
                    anRH.style_axis(axr_arr[i+1,j], bottom=False, form=True)
    
            R250  = np.arange(0, 1000, 250)
            R50   = np.arange(0, 1000, 50)
    
            E10 =  np.array([0, 9.68303722]) # 6*np.random.random(size=10)+9
            E80 =  np.array([0, 3.29163264,   6.36586215,  10.5493598 ,  14.09140449,
                        18.12508383,  22.09151704,  25.16457864,  29.19506302,
                        33.10430607,  36.72128906,  39.83802528,  43.52563456,
                        48.4949767 ,  52.72494864,  57.40025165,  61.66294238,
                        64.80185772,  68.87735447,  72.69786357,  76.85119477])
    
            arrows = False    
    
            if arrows:
                durarrow250 = mpl.patches.FancyArrowPatch((0, 0.9), (250, 0.9), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
                durarrow50 = mpl.patches.FancyArrowPatch((0,0.9), (50, 0.9), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
                durarrow10 = mpl.patches.FancyArrowPatch((0, 0.2), (10, 0.2), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
                durarrow80 = mpl.patches.FancyArrowPatch((0, 0.2), (80, 0.2), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
        
                durarrow10int = mpl.patches.FancyArrowPatch((0, 0.9), (E10[1], 0.9), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
                durarrow80int = mpl.patches.FancyArrowPatch((0, 0.9), (E80[1], 0.9), arrowstyle='<|-|>', shrinkA=0, shrinkB=0, mutation_scale=5, facecolor='k', edgecolor='k', capstyle='butt', joinstyle='miter')
       
                axr_arr[0,0].add_patch(durarrow250)
                axr_arr[0,1].add_patch(durarrow50)
        
                axr_arr[3,0].add_patch(durarrow10)
                axr_arr[3,0].add_patch(durarrow10int)
        
                axr_arr[3,1].add_patch(durarrow80)
                axr_arr[3,1].add_patch(durarrow80int)
    
            tit_arr[0,0] = axr_arr[0,0].set_title('\n4 Hz, Mid NMDA$^1$')
            tit_arr[0,1] = axr_arr[0,1].set_title('\n20 Hz, Mid NMDA$^2$')
    
            tit_arr[3,0] = axr_arr[3,0].set_title('\n10 ms, Low Burst, Mid NMDA$^3$')
            tit_arr[3,1] = axr_arr[3,1].set_title('\n50 ms, High Burst, High NMDA$^4$')

            axr_arr[1,0].set_ylabel(r'I$_{syn}$ [nA]')
            axr_arr[4,0].set_ylabel(r'I$_{syn}$ [nA]')
    
            axr_arr[2,0].set_ylabel('Frequency [Hz]')
            axr_arr[5,0].set_ylabel('Frequency [Hz]')
    
            axr_arr[5,0].set_xlabel('time [ms]')
            axr_arr[5,1].set_xlabel('time [ms]')

            axr_arr[0,0].eventplot(R250, lineoffsets=0.5, linelengths=0.7, color='k')
            axr_arr[0,1].eventplot(R50, lineoffsets=0.5, linelengths=0.7, color='k')
            axr_arr[3,0].eventplot(E10, lineoffsets=0.5, linelengths=0.7, color='k')
            axr_arr[3,1].eventplot(E80, lineoffsets=0.5, linelengths=0.7, color='k')

            return axr_arr, tit_arr

        def firing_axes():
    
            firingspec = gs.GridSpecFromSubplotSpec(2, 1, subplot_spec=gl[:, 1])
            fir_arr = np.empty(shape=firingspec.get_geometry(), dtype='O')
            firtit_arr = np.empty(shape=firingspec.get_geometry(), dtype='O')

            for i in range(2):
                fir_arr[i,0] = fig.add_subplot(firingspec[i,0])
                anRH.style_axis(fir_arr[i,0], form=False)

            #for _, ax in np.ndenumerate(fir_arr): 
            #    fir_arr[0,0].get_shared_x_axes().join(fir_arr[0,0], ax) 
            #    fir_arr[0,0].get_shared_y_axes().join(fir_arr[0,0], ax) 
    
            fir_arr[1,0].set_xscale('log')
            fir_arr[1,0].xaxis.set_major_formatter(ticker.ScalarFormatter())
            fir_arr[1,0].plot([],[], marker='D', label='Low', color='k')
            fir_arr[1,0].plot([],[], marker='o', label='High', color='k')
            fir_arr[1,0].legend(title='Burst\nIntensity', frameon=False)
#            fir_arr[1,0].axhspan(4, 8, xmin=0, xmax=1, color='k', alpha=0.2, lw=None) 
#            fir_arr[0,0].axhspan(4, 8, xmin=0, xmax=1, color='k', alpha=0.2, lw=None) 
            fir_arr[0,0].set_ylabel('Resonant Freq. [Hz]')
            fir_arr[1,0].set_ylabel('Resonant Freq. [Hz]')
            fir_arr[0,0].set_xlabel('Driving freq. [Hz]')
            fir_arr[1,0].set_xlabel('Duration [ms]')

            firtit_arr[0,0] = fir_arr[0,0].set_title('Rhythmic Stimulation')
            firtit_arr[1,0] = fir_arr[1,0].set_title('Burst Stimulation')

            return fir_arr, firtit_arr


        axr_arr, tit_arr = freq_axes()            
        fir_arr, firtit_arr = firing_axes()            

        return axr_arr, tit_arr, fir_arr, firtit_arr

    axr_arr, tit_arr, fir_arr, firtit_arr  = set_axes()

   #`` fig.savefig('{!s}.pgf'.format(filnam),  bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.pdf'.format(filnam) )



    RH_rat = np.zeros(shape=(len(RH), N_NMDA)) 
    EL_rat = np.zeros(shape=(len(EV), N_NMDA)) 
    EH_rat = np.zeros(shape=(len(EV), N_NMDA)) 

    RH_spec_idx = [1,4]
    RHspec=RH_plot[RH_spec_idx]
    EV_spec_idx = [0,2]

    Icap=False

    for jidx, j in enumerate(RHspec):
        expt = 'P001/F1H{!s}/F000/SimpleNet4/N000'.format(j)
        STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
        ctxSTNgids = np.where(STNgids==1)[0]
        jpos = 1
        axr_arr = plot_spec_cur(anRH, expt, ctxSTNgids, axr_arr, (jpos, jidx), timrng, Icap=Icap)

    expt = 'P001/F1E10/F000/SimpleNet4/N000'
    STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
    ctxSTNgids = np.where(STNgids==1)[0]
    axr_arr = plot_spec_cur(anLE, expt, ctxSTNgids, axr_arr, (4,0), timrng, Icap=Icap)

    expt = 'P002/F1E80/F000/SimpleNet4/N000'
    STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
    ctxSTNgids = np.where(STNgids==1)[0]
    axr_arr = plot_spec_cur(anHE, expt, ctxSTNgids, axr_arr, (4,1), timrng, Icap=Icap)

    axr_arr[4,0].legend(['Net', 'GABA', 'NMDA', 'AMPA'], loc=4, frameon=False, fontsize='small', labelspacing=0.25)


    cmapmpl = mpl.cm.get_cmap('tab10')
    colors = cmapmpl(np.arange(10))

    for idx, i in enumerate(N_NMDA_pos):
        for jidx, j in enumerate(RH):
            expt = 'P{:03d}/F1H{!s}/F000/SimpleNet4/N000'.format(i, j[1])
            STNgids = np.array(anRH.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            RH_rat[jidx,idx] = get_resfreq(anRH, expt, ctxSTNgids, timrng, Icap=Icap) 

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anLE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            EL_rat[jidx,idx] = get_resfreq(anLE, expt, ctxSTNgids, timrng, Icap=Icap) 

        for jidx, j in enumerate(EV):
            expt = 'P{:03d}/F1E{!s}/F000/SimpleNet4/N000'.format(i, j)
            STNgids = np.array(anHE.filobj[expt]['props']['Ctx_asg_STN'])
            ctxSTNgids = np.where(STNgids==1)[0]
            EH_rat[jidx,idx] = get_resfreq(anHE, expt, ctxSTNgids, timrng, Icap=Icap) 


        fir_arr[0,0].plot(RH_plot, RH_rat[:,idx], label=NM_lab[idx], color=colors[idx], marker='D')
        fir_arr[0,0].legend(NM_lab, title='NMDA', frameon=False)
        fir_arr[1,0].plot(EV, EL_rat[:,idx], marker='D', color=colors[idx])
        fir_arr[1,0].plot(EV, EH_rat[:,idx], marker='o', color=colors[idx])

    fir_arr[0,0].text(x=4, y=RH_rat[RH_spec_idx[0], 1], s='1', va='center', ha='center', weight='bold', size='large')
    fir_arr[0,0].text(x=20, y=RH_rat[RH_spec_idx[1], 1], s='2', va='center', ha='center' , weight='bold', size='large')

    fir_arr[1,0].text(x=10, y=EL_rat[EV_spec_idx[0], 1], s='3', va='center', ha='center' , weight='bold', size='large')
    fir_arr[1,0].text(x=50, y=EH_rat[EV_spec_idx[1], 2], s='4', va='center', ha='center' , weight='bold', size='large')

    filnam='NewFig3'
    fig.align_ylabels([axr_arr[1,0], axr_arr[2,0], axr_arr[4,0], axr_arr[5,0]])
    fig.savefig('{!s}.pdf'.format(filnam), transparent=True, bbox_inches='tight', pad_inches=0)

    plut.add_labels((axr_arr[2,0],'y'), (axr_arr[0,0], tit_arr[0,0]), 'A') 
    plut.add_labels((axr_arr[2,0],'y'), (axr_arr[3,0], tit_arr[3,0]), 'B') 
    plut.add_labels((axr_arr[0,0], tit_arr[0,0]), (axr_arr[0,1], tit_arr[0,1]), 'Rhythmic Single Spike Drive [RSSD]\n', False) 
    plut.add_labels((axr_arr[3,0], tit_arr[3,0]), (axr_arr[3,1], tit_arr[3,1]), 'Single Burst Event Drive [SBED]\n', False) 

    plut.add_labels((fir_arr[0,0],'y'), (fir_arr[0,0], firtit_arr[0,0]), 'C') 
    plut.add_labels((fir_arr[1,0],'y'), (fir_arr[0,0], firtit_arr[1,0]), 'D') 


    fig.savefig('{!s}.pdf'.format(filnam), transparent=True, bbox_inches='tight', pad_inches=0)
    fig.savefig('{!s}.pgf'.format(filnam), transparent=True, bbox_inches='tight', pad_inches=0)

def run_mulfil(ancls, pres, ax, otherstim=False, idx=None, confunits=None):
    for chore, res in ancls.my_chores:
        chor_seg = [ancls.uniq_combo[ancls.uniq_sims[chore[1],0]], ancls.uniq_feeds[ancls.uniq_sims[chore[1],1]]]
    if pres=='LFOplot':
        ax, arr = ancls.run_timser(ax, chore, 'STN_LFO_Amp', chor_seg, sqr=True, otherstim=otherstim, cumsum=True, confunits=confunits)
    elif pres=='NetSyn':
        ax, arr = ancls.run_timser(ax, chore, 'NetSyn', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits)
    elif res=='STN_pp':
        cell_spk = ancls.get_spk(chore, 'STN_spk', ancls.tmin_ana, ancls.tmax_ana)
        cell_pp, tbin = ancls.get_pp(cell_spk, tbin=ancls.tbin)
        fdsp_pp = ancls.run_pp(chore, 'STN_spk', chor_seg)
        ax = ancls.get_STNpp_ax(ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0) 
        if idx is not None:
            t0_idx = int((1500-ancls.tmin_ana)/tbin)
            t_idx = int(idx/tbin) 
            avgspkr = 1000*np.sum(fdsp_pp[:,:,2, t0_idx:t_idx], axis=-1)/idx 
        arr = avgspkr 
    return ax, arr

def get_spk_rate(pp, T, typ, ofset=10, tbin=5):
    tbin_of = int(ofset//5)
    # pp = avg
    nnz_idx = np.where(pp[tbin_of:]>0)[0][0]
    T_idx = int(T//tbin)
    if typ == 'R':
        rat = np.sum(pp[tbin_of+nnz_idx:T_idx])*1000/(tbin*(T_idx-nnz_idx)) if nnz_idx < T_idx else 0
    elif typ == 'E':
        rat = np.sum(pp[tbin_of+nnz_idx:tbin_of+nnz_idx+T_idx])*1000/T if nnz_idx < T_idx else 0
    return rat 

def plot_spec_cur(ancls, exp_path, gids, axr_arr, coord, timrng, Icap=False):

    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 

    if Icap:
        icap = np.mean(ancls.filobj[exp_path]['raw/STNSta_i_int'][gids,:], axis=0)[tidx[0]:tidx[1]]
        
    I_cap = I_syn + icap if Icap else I_syn

    i,j = coord
    axr_arr[i,j].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_cap, freq, tim) 

    img=axr_arr[i+1,j].imshow(STNspec, cmap='plasma', origin='lower', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[i+1,j].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    return axr_arr

def get_resfreq(ancls, exp_path, gids, timrng, Icap=False):
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    if Icap:
        icap = np.mean(ancls.filobj[exp_path]['raw/STNSta_i_int'][gids,:], axis=0)[tidx[0]:tidx[1]]
        
    I_cap = I_syn + icap if Icap else I_syn
# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_cap, freq, tim) 
    ind = np.unravel_index(np.argmax(STNspec, axis=None), STNspec.shape)
    maxfreq = freq[ind[0]]
    return maxfreq
    


def get_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] = spcpow
    return spcmat

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))


if __name__ == '__main__':
    reso_freq()
