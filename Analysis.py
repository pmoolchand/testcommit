#! python3
from mpi4py import MPI
import h5py
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import matplotlib.ticker as ticker
import scipy as sp
import scipy.signal as sps
from functools import reduce
import itertools as it
import os 


class Analysis():
    def __init__(self, datafile, tmin=0., tmax=3000., tbreaks=[]): 
      #  self.datafile = datafile
      #  self.file = h5py.File(self.datafile, 'r', driver='mpio', comm=MPI.COMM_WORLD)
        self.file = datafile 
        self.tstop = self.file.attrs['tstop']
        self.trec = self.file.attrs['trec']
        self.trec_ini = self.file.attrs['trec_ini']
        self.tmin = max(self.trec_ini, tmin)
        self.tmax = min(tmax, self.tstop)
        self.tbreaks = np.sort(tbreaks)
        self.tbreaks = self.tbreaks[np.where((self.tbreaks>=self.tmin) & (self.tbreaks<=self.tmax))] 
        self.timeposts =  reduce(np.union1d, (np.array([self.tmin, self.tmax]), self.tbreaks))

    # Interperse spike_times between timeposts
    # [timeposts_intervals: spk_times]
    def trim_spike_range(self, tvec, timeposts):
        splidx = np.searchsorted(tvec, timeposts)
        spk_post_arr = np.empty(shape=timeposts.size-1, dtype='O')
        spk_post_arr[:] = np.split(tvec, splidx)[1:-1] 
        return spk_post_arr 

    # Get subpop spikes array, row = gid, col = interpersed spike times between timeposts 
    # [gid, timpst_int: spk_times]
    def get_gids_trim_spike(self, gid_nplist, spk_data):
        subpop_spk_arr = np.empty(shape=(gid_nplist.size, self.timeposts.size-1), dtype='O')
        for idx, gid in enumerate(gid_nplist):
            subpop_spk_arr[idx,:] = self.trim_spike_range(spk_data[gid], self.timeposts)
        return subpop_spk_arr

    # Create correspondence between gids and gid_arrays based on subpop membership
    # [subpop: gid_ref_idx]
    def get_subpop_idx_ranges(self, spk_subp):
        subpop_idx_ranges = np.empty(shape=(spk_subp.shape[0]), dtype='O')
        idx_ini = 0
        N_gids = 0
        for sidx, subpop in enumerate(spk_subp):
            subpop_idx_ranges[sidx] = idx_ini + np.arange(subpop[0].size)    
            idx_ini = subpop_idx_ranges[sidx][-1] + 1
            N_gids += subpop[0].size
        return subpop_idx_ranges, N_gids 

    # Get array of subpop spikes array 
    # [gid, timpst_int: spk_times (subpop based gid_ref_idx)]
    def get_gid_spk(self, expt_path, spk_subp, N_gids):
        self.spk_data = self.file[expt_path]['raw']['spikes']
        gid_spk_arr = np.empty(shape=(N_gids, self.timeposts.size-1), dtype='O')
       # for sidx, subpop in enumerate(spk_subp):
        for subpop in spk_subp:
            gid_spk_arr[subpop[1]] = self.get_gids_trim_spike(subpop[0], self.spk_data)
        return gid_spk_arr

    # Converts spike trains to point processes
    # [gid, tim_bins: spk (subpop based gid_ref_idx)]
    def get_gid_pp(self, expt_path, spk_subp, subpop_idx_ranges, N_gids, tbin=0.025):
        prec = tbin/self.trec
        self.spk_data = self.file[expt_path]['raw']['spikes']
        prec = max(1,prec)
        gid_pp_tmp = np.zeros(shape=(N_gids, int((self.tmax-self.tmin)/self.trec)+1), dtype=int)
        
       # for sidx, subpop in enumerate(spk_subp):
       #     for gidc, gidx in enumerate(subpop_idx_ranges[sidx]):
       #         spk_arr = np.array((self.trim_spike_range(self.spk_data[subpop[0][gidc]], np.array([self.tmin, self.tmax]))[0]-self.tmin)/self.trec, dtype=int) 
       #         gid_pp_tmp[gidx, spk_arr] = int(1)
        for sidx in range(spk_subp.shape[0]):
            for gidc, gidx in zip(spk_subp[sidx,0], spk_subp[sidx,1]):
                spk_arr = np.array((self.trim_spike_range(self.spk_data[gidc], np.array([self.tmin, self.tmax]))[0]-self.tmin)/self.trec, dtype=int) 
                gid_pp_tmp[gidx, spk_arr] = int(1)

        if prec > 1:
            tposts = np.rint(np.arange(0, gid_pp_tmp.shape[1], prec)).astype(int) 
            gid_pp_arr = np.zeros(shape=(N_gids, tposts.size), dtype=int)
            for i,_ in enumerate(tposts[:-1]):
                gid_pp_arr[:,i] = np.sum(gid_pp_tmp[:, tposts[i]:tposts[i+1]], axis=1)
            gid_pp_arr[:,-1] = np.sum(gid_pp_tmp[:, tposts[-1]:], axis=1)
        else: 
            gid_pp_arr = gid_pp_tmp
        return gid_pp_arr, tbin

    # Get ISIs per gid between timeposts
    # [gid, timpst_int: ISI_times (subpop based gid_ref_idx)]
    def get_gid_ISI(self, gid_spk_arr):
        gsar_shape = gid_spk_arr.shape
        gid_ISI_arr = np.empty(shape=gsar_shape, dtype='O')
        for gidx, tidx in it.product(np.arange(gsar_shape[0]), np.arange(gsar_shape[1])): 
            gid_ISI_arr[gidx, tidx] = np.diff(gid_spk_arr[gidx, tidx])
        return gid_ISI_arr        

    # Get mean of cap curr for each subpop
    # [subpop, timesteps], time
    def get_subpop_cap_cur(self, spk_subpop, expt_path, Tsam=int(1)):
        tim = np.array(self.file[expt_path]['props']['timesteps'])
        t_idx = np.arange((self.tmin-self.trec_ini)/self.trec, (self.tmax-self.trec_ini)/self.trec+1, Tsam, dtype=int)
        tim = tim[t_idx]
        subpop_curr_arr = np.zeros(shape=(spk_subpop.shape[0], tim.size))

        intrin = self.file[expt_path]['raw']['STNSta_i_int'][:, t_idx]
        ampa = self.file[expt_path]['raw']['STNSyn_AMPA_Ctx'][:, t_idx]
        nmda = self.file[expt_path]['raw']['STNSyn_NMDA_Ctx'][:, t_idx]
        gaba = self.file[expt_path]['raw']['STNSyn_GABA_GPe'][:, t_idx]
        capa = intrin + ampa + nmda + gaba

        for sidx, subpop in enumerate(spk_subpop):
            subpop_curr_arr[sidx,:]  = np.mean(capa[subpop[0], :], axis = 0) 
        return subpop_curr_arr, tim

    def get_subpop_spec(self, subpop_cap_cur, freq, tim):
        N_subpop = subpop_cap_cur.shape[0]
        subpop_spec_mat = np.empty(shape=(N_subpop, freq.size, tim.size), dtype='float') 
        for sidx in range(N_subpop):
            subpop_spec_mat[sidx,:,:] = get_spectral_matrix(subpop_cap_cur[sidx], freq, tim)
        return subpop_spec_mat
        


    # Plot spike raster on subpop basis
    # [subpop: axis] 
    def plot_subpop_spikes(self, spk_subp, gid_spk_arr, idx_ranges, subpop_axis):
        # spk_subp = [(gidlist, offset, color)]
        subp_count = 0
      #  for sidx, subpop in enumerate(spk_subp):
      #      for gidx, gid in enumerate(idx_ranges[sidx]):
      #          tvec = gid_spk_arr[gid] 
      #          subpop_axis[sidx].eventplot(np.concatenate(tvec), lineoffsets=subp_count+subpop[1]+gidx, linelengths=0.5, linewidths=0.5 ,color=subpop[2])
        for sidx in range(spk_subp.shape[0]):
            for gidx, gid in enumerate(spk_subp[sidx,1]):
                tvec = gid_spk_arr[gid] 
                subpop_axis[sidx].eventplot(np.concatenate(tvec), lineoffsets=subp_count+gidx, linelengths=0.5, linewidths=0.5, color='k')
          #  subp_count += subpop[0].size
        return subpop_axis

    # Plot ISI histograms on subpop basis
    # [subpop, timpst_int: axis]
    def plot_subpop_ISIs(self, spk_subp, gid_ISI_arr, idx_ranges, axis_arr, bins=50, orien='horizontal'):
        for sidx in range(spk_subp.shape[0]):
            for tidx in np.arange(gid_ISI_arr.shape[1]):
              #  print(np.mean(np.concatenate(gid_ISI_arr[self.idx_ranges[sidx], tidx])))
                concat_subpop_ISI = np.concatenate((gid_ISI_arr[spk_subp[sidx,1], tidx]))
                axis_arr[sidx, tidx, 0].hist(concat_subpop_ISI, bins=bins, orientation=orien, color='k', alpha=0.3)
                axis_arr[sidx, tidx, 1].boxplot(concat_subpop_ISI, showmeans=True, vert=True)
                mean = np.mean(concat_subpop_ISI) 
                median = np.median(concat_subpop_ISI) 
                std = np.std(concat_subpop_ISI) 
                axis_arr[sidx, tidx, 1].text(0.7, 0.5, '{:>6}: {:.2f}\n{:>6}: {:.2f}\n{:>6}: {:.2e}\n{:>6}: {:8f}\n{:>6}: {:.2f}'\
                .format('mean', mean, 'median', median, 'std', std, 'cv', std/mean, 'mean_f', 1000/mean,\
                fontsize='xx-small'), verticalalignment='center', family='monospace', transform=axis_arr[sidx, tidx, 1].transAxes)

        return axis_arr

    # Plot spike counts
    def plot_subpop_spkcounts(self, spk_subp, gid_spk_cnt, tbin, idx_ranges, axis_arr):
        tposts = np.arange(self.tmin, self.tmax+tbin, tbin)
        for sidx in range(spk_subp.shape[0]):
            axis_arr[sidx].bar(tposts, np.sum(gid_spk_cnt[spk_subp[sidx,1],:], axis=0), width=tbin, align='edge', alpha=0.1, color='k')
        return axis_arr

    def plot_subpop_specs(self, subpop_spec_mat, axis_arr, freq, tim, subpop_curr):
        N_subplot = subpop_spec_mat.shape[0]
        formatter = ticker.ScalarFormatter(useMathText=True)
        formatter.set_powerlimits((-2,2))
        for sidx in range(N_subplot):

            self.plot_specs(subpop_spec_mat[sidx,:,:], axis_arr[sidx], freq, tim, subpop_curr[sidx,:], formatter)
        #    img = axis_arr[sidx,0].imshow(subpop_spec_mat[sidx,:,:], cmap='YlOrRd', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        #    axis_arr[sidx,0].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
        #    axis_arr[sidx,0].yaxis.set_label_position('right')
        #    curax = axis_arr[sidx,0].twinx()
        #    curax.plot(tim, subpop_curr[sidx,:], 'k', lw=0.2)
        #    curax.yaxis.set_major_formatter(formatter)
        #    curax.yaxis.set_label_position('left')
        #    cbar = plt.colorbar(img, cax=axis_arr[sidx,1], format=formatter)
        #    cbar.ax.yaxis.set_offset_position('left')
        #    cbar.update_ticks()

        return axis_arr

    def plot_specs(self, spec_mat, axis, freq, tim, subpop_curr, formatter):
        img = axis.imshow(spec_mat, cmap='YlOrRd', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        axis.yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))
        axis.yaxis.set_label_position('right')
        cbar = plt.colorbar(img, ax=axis, orientation='vertical', format=formatter)
        cbar.ax.yaxis.set_offset_position('left')
        cbar.update_ticks()
        curax = axis.twinx()
        curax.plot(tim, subpop_curr, 'k', lw=0.2)
        curax.yaxis.set_major_formatter(formatter)
        curax.yaxis.set_label_position('left')
        return axis

    def get_STN_subpop(self, expt_path):
        STN_subpop = self.file[expt_path]['props']['STN_subpop']
        subpop = np.empty(shape=(len(STN_subpop), 2), dtype='O') 
        idx_ini = 0
        N_gids = 0
        for sidx, STNpop in enumerate(STN_subpop):
            subpop[sidx,0] = np.array(STNpop)
            subpop[sidx,1] = idx_ini + np.arange(subpop[sidx,0].size)    
            idx_ini = subpop[sidx, 1][-1] + 1
            N_gids += subpop[sidx,0].size
        return subpop, N_gids

#    def get_subpop_spec_pow(self, spk_subp, gid_pp_arr, dt):
        

 #   def 


#        subpop_tim_ISI = np.empty(shape=(spk_subp.size, self.timeposts.size-1), dtype='O')
#        for sbidx, subpop in enumerate(spk_subp):
#            for gidx, gid in enumerate(subpop[0]):
#                tvec = gid_spk_arr[sbidx][gidx] 
#                for tintidx, ISI in enumerate(tvec):
#                    axis_arr
#
def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))

def get_spectral_matrix(signal, freq, tim, sT= 0.025, N=2**13, s= 0.5, fix_Ncyc = False, Ncycl=7):
   B = np.zeros((len(freq), len(tim)))
   fs= 1/sT
   S_trans = np.transpose(tim)
   s = sps.detrend(signal)
   for j in range(0, len(freq)):
       B[j, :] += __energyvec(freq[j], s, width=Ncycl)
   return B 

def __energyvec(f, s, fs=1/0.025e-3,width=7):
    dt = 1. /fs
    sf = f / width
    st = 1. / (2. * np.pi * sf)
    t = np.arange(-3.5*st, 3.5*st, dt)
    m = __morlet(f, t)
    y = sps.fftconvolve(s, m)
    y = (2. * abs(y) / fs)**2
    y = y[int(np.ceil(len(m)/2.)):int(len(y)-np.floor(len(m)/2.)+1)]
    return y

def __morlet(f, t, width=7):
    sf = f / width
    st = 1. / (2. * np.pi * sf)
    A = 1. / (st * np.sqrt(2.*np.pi))
    y = A * np.exp(-t**2. / (2. * st**2.)) * np.exp(1.j * 2. * np.pi * f * t)
    return y

#def my_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17, s= 3.5):
#
#    spcmat = np.zeros((len(freq), len(tim)))
#    signal = np.transpose(signal)
#    signal = sps.detrend(signal)
#
#    for idx, f in enumerate(freq):
#        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
#        conv = sps.fftconvolve(signal, wvlt, 'same')
#        spcpow = (2.* abs(conv))**2
#        spcmat[idx,:] += spcpow
#
#    return spcmat
#
def stats(pol, expt_path, timint, filnam=None):
    # get subpop
    N_timint = timint.size
    all_idx_ranges = np.array(pol.file[expt_path]['props']['gids_STN'], dtype=int)
    all_pop = np.empty(shape=(1, 2), dtype='O')
    all_pop[0,0] = all_idx_ranges
    all_pop[0,1] = all_idx_ranges
    N_gids = all_idx_ranges.size

    allpop_spks = pol.get_gid_spk(expt_path, all_pop, N_gids)
    allpop_ISI = pol.get_gid_ISI(allpop_spks)
    allgid_pp, tbin = pol.get_gid_pp(expt_path, all_pop, all_idx_ranges, N_gids, tbin=5)
    all_cap_cur, tim = pol.get_subpop_cap_cur(all_pop, expt_path, Tsam=int(1))

    freq = np.geomspace(2,64, 156)
    all_specs = pol.get_subpop_spec(all_cap_cur, freq, tim)

    h_ratios = np.array([6,3,3])
    spk_subpop, N_gids = pol.get_STN_subpop(expt_path)
    N_subpop = spk_subpop.shape[0]
    if N_subpop > 1: 
        idx_ranges, N_gids = pol.get_subpop_idx_ranges(spk_subpop)
#    gid_spk_arr = pol.get_gid_spk(expt_path, spk_subpop, N_gids)
#    gid_ISI_arr = pol.get_gid_ISI(gid_spk_arr)
#    gid_pp, tbin = pol.get_gid_pp(expt_path, spk_subpop, idx_ranges, N_gids, tbin=0.025)
        subpop_cap_cur, tim = pol.get_subpop_cap_cur(spk_subpop, expt_path, Tsam=int(1))
        specs_mat = pol.get_subpop_spec(subpop_cap_cur, freq, tim)
        h_ratios = np.concatenate(([6,3,3], np.tile([4,3,3], N_subpop)))

    fig = plt.figure(figsize=(20, np.sum(h_ratios)))
    gl = gs.GridSpec(h_ratios.size, N_timint, width_ratios=timint, height_ratios=h_ratios)

    allspks = np.empty(shape=(1), dtype='O')
    allspks[0] = fig.add_subplot(gl[0,:])
    allISI = np.empty(shape=(1, timint.size, 2), dtype='O') 
    allISI[0, 0, 0] = fig.add_subplot(gl[2, 0])
    for i in range(1,N_timint):
        allISI[0, i, 0] = fig.add_subplot(gl[2, i],sharey=allISI[0,0,0])
    for i in range(N_timint):
        allISI[0, i, 1] = allISI[0, i ,0].twiny() 
    allspecs = np.empty(shape=(1), dtype='O')
    allspecs[0] = fig.add_subplot(gl[1,:], sharex=allspks[0])

    allspks = pol.plot_subpop_spikes(all_pop, allpop_spks, all_idx_ranges, allspks)
    allspks = pol.plot_subpop_spkcounts(all_pop, allgid_pp, tbin, all_idx_ranges, allspks)
    allspecs = pol.plot_subpop_specs(all_specs, allspecs, freq, tim, all_cap_cur)

    allISI = pol.plot_subpop_ISIs(all_pop, allpop_ISI, all_idx_ranges, allISI, bins=50, orien='horizontal')

#    subspks = np.empty(shape=(N_subpop), dtype='O')
#    subspecs = np.empty(shape=(N_subpop), dtype='O')
#    subISIs = np.empty(shape=(N_subpop, N_timint, 2), dtype='O') 
#
#    for i in range(N_subpop):
#        subspks[i] = fig.add_subplot(gl[(i+1)*3,:])
#        subspecs[i] = fig.add_subplot(gl[(i+1)*3+1,:])
#        subISIs[i,0,0] = fig.add_subplot(gl[(i+1)*3+2,0])
#        for j in range(1,N_timint):
#            subISIs[i, j, 0] = fig.add_subplot(gl[(i+1)*3+2,j],sharey=subISIs[i,0,0])
#        for j in range(N_timint):
#            subISIs[i, j, 1] = subISIs[i, j ,0].twiny() 
#
#    subspks = pol.plot_subpop_spikes(spk_subpop, allpop_spks, all_idx_ranges, subspks)
#    subspks = pol.plot_subpop_spkcounts(spk_subpop, allgid_pp, tbin, all_idx_ranges, subspks)
#    subspecs = pol.plot_subpop_specs(specs_mat, subspecs, freq, tim, subpop_cap_cur)
#    subISIs = pol.plot_subpop_ISIs(spk_subpop, allpop_ISI, idx_ranges, subISIs, bins=50, orien='horizontal')

    
    filnamplt = expt_path.split('/')

    plt.tight_layout()
    filtit='{!s}_{!s}_{!s}'.format(filnamplt[1], filnamplt[2], filnamplt[4], filnamplt[-1])
    plt.suptitle(filtit)
#    fig.savefig('{!s}_AMPANMDAHi.pdf'.format(filnam))
#    fig.savefig('{!s}_AMPALo.pdf'.format(filnam))
    fig.savefig('AnaPlots/{!s}_{!s}.pdf'.format(filnam, filtit))
    plt.close()



def runAna(fil):

    datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)
    filnam = datafile.filename.split('/')[-1][:-5]

#    tbreaks=[500, 1000, 2000, 2250, 2500]
    tbreaks=[500, 1500, 2500]
    pol = Analysis(datafile, tmin=500, tmax=3500, tbreaks=tbreaks)
    timint = np.diff(pol.timeposts)
    timint_n = timint.size 
    
   # expt_path='/P000/F1H4/F000/SimpleNet/N000'
    net='SimpleNet'
    chores = []
    lenparm = len(datafile)
    for param in range(lenparm):
        parstr = 'P{:03d}'.format(param)
        for ext_feed in datafile[parstr]:
            chores.append(os.path.join('/', parstr, ext_feed, 'F000', net, 'N000'))
        
    chores.sort()

    rank = MPI.COMM_WORLD.Get_rank()
    N_hosts = MPI.COMM_WORLD.Get_size()

    my_chores = chores[rank:len(chores):N_hosts]

    for chore in my_chores:

   # expt_path='/P000/F22SH4/F000/SimpleNet4/N000'
    #    stats(pol, chore, timint)
        stats(pol, chore, timint, filnam)

    datafile.close()

if __name__ == '__main__':
    
    fil_lst= [
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Intrin-000.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Reg_AMPANMDA-000.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Reg_AMPA-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Reg_NMDA-000.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_AMPANMDAHiInt-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_AMPAHiInt-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_NMDAHiInt-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_AMPANMDALoInt-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_AMPALoInt-001.hdf5',
    '/scratch/04119/pmoolcha/STNGPe/2020-09-19/TAC20200919_Spiking_SingPop_Evt_NMDALoInt-002.hdf5',
    ]
    
    for fil in fil_lst:
        runAna(fil)



#    subpop_spk_arr= np.empty(shape=(spk_subpop.shape[0]), dtype='O')
#    figcnt, subpop_spk_arr = plt.subplots(spk_subpop.shape[0], 1, sharex=True)

#    gid_spk_arr = pol.get_gid_spk(expt_path, spk_subpop, idx_ranges, N_gids)
 #   subpop_spk_arr = pol.plot_spikes(spk_subpop, gid_spk_arr, idx_ranges, subpop_spk_arr)

  #  plt.savefig('gidspkcounts10.pdf') 
 #   plt.savefig('gidspksraster.pdf') 
 #   plt.close()
    
#    specs_arr = np.empty(shape=(spk_subpop.shape[0]), dtype='O')
#   # figspecs, specs_arr = plt.subplots(spk_subpop.shape[0],2, sharex=True, figsize=(8,12), gridspec_kw = {'width_ratios':[24, 1]})
#    figspecs = plt.figure(figsize=(8,12))
#    for i, _ in enumerate(specs_arr):
#        specs_arr[i] = figspecs.add_subplot(spk_subpop.shape[0], 1, i+1)
#    specs_arr = pol.plot_subpop_specs(subp_specs, specs_arr, freq, tim, subpop_cap_cur)
#
#    plt.savefig('ifigspecs.pdf')
#    plt.close()

#    subpopISI = np.empty(shape=(spk_subpop.shape[0], timint_n, 2), dtype='O')
   # figISI = plt.figure()
  #  for i in range(spk_subpop.shape[0]):
  #      for j in range(timint_n):
  #          subpopISI[i,j,0] = figISI.add_subplot(gl[i,j])
  #          subpopISI[i,j,1] = subpopISI[i,j,0].twiny()

 #   subpopISI = pol.plot_subpop_ISIs(spk_subpop, gid_ISI_arr, idx_ranges, subpopISI, bins=50, orien='horizontal')
 #   plt.savefig('histISI.pdf') 
 #   plt.close()
    
   # subpop_spk_arr = pol.plot_subpop_spkcounts(spk_subpop, gid_pp, tbin, idx_ranges, subpop_spk_arr)
   # figcnt.savefig('spkscount.pdf')
   # plt.close()

    




##  #  print(gid_pp.shape)
#    sumgid = np.sum(gid_pp[idx_ranges[0]], 0)
#    four = np.fft.rfft(sumgid)
#    freq = np.fft.rfftfreq(sumgid.size,d=tbin*1e-3 ) 
#    f1 = np.searchsorted(freq, 60)
#    Sspks = np.absolute(four)**2
##
##
##    qwe, tim = pol.get_subpop_cap_cur(spk_subpop, expt_path, Tsam=int(tbin/pol.trec))
##  #  arr = pol.get_gid_spk(expt_path, spk_subpop, idx_ranges, N_gids)
###    print(arr)
##   # fig = mpl.figure.Figure()
#    fouq = np.fft.rfft(qwe[0])
#    freqq = np.fft.rfftfreq(qwe[0].size, d=pol.trec*1e-3)
#    Sfld = np.absolute(fouq**2)
#    f2 = np.searchsorted(freqq, 60)
##    print(f1,f2)
#    coher = four * np.conj(fouq) /np.sqrt(Sspks* Sfld) 
#    fig = plt.Figure()
###    ax_ar = np.empty(shape=(3,1), dtype='O')
##    ax_ar[0,0] = fig.add_subplot(311)
##    ax_ar[1,0] = fig.add_subplot(312)
##    ax_ar[2,0] = fig.add_subplot(313)
##    for i in np.arange(spk_subpop.shape[0]):
##        plt.plot(tim, qwe[i, :], lw=0.1)
#    pf, poherence = sps.coherence(sumgid, qwe[0], fs=40e3, nperseg=sumgid.size)
#
#   # plt.plot(freq[:f1], np.abs(four[:f1]), freqq[:f2], np.abs(fouq[:f2]), lw=0.1)
# #   ax = plt.plot(freq[:f1], np.absolute(coher[:f1]), 'r', lw=0.2)
# #   ax1 = plt.twinx()
# #   ax1.plot(freq[:f1], np.angle(coher[:f1]), 'g', lw=0.2)
#    fcut = np.searchsorted(pf,60)
#    plt.plot(pf[:fcut], poherence[:fcut], lw=0.2)
#
##   # ax = fig.add_su:)
##  #  ax = pol.plot_s:_subpop, arr, ax)
##    diffISI = pol.get_gid_ISI(arr)
##    ax_ar = pol.plot_subpop_ISIs(spk_subpop, diffISI, ax_ar)
##    print(ax_ar)
#    plt.savefig('cohere.pdf') 
#    plt.close()
