#! python3

import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.style as mpl_style
#mpl_style.use('paper')
#mpl_style.use('presentation')
import matplotlib.ticker as ticker

from AnalysisClass import Analysis 

import numpy as np
import scipy as sp
import scipy.signal as sps

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)


class Plots():
    def __init__(self, fil, exp_path, tmin=500, tmax=2500, param={}):
        self.file = fil
        self.filobj = h5py.File(fil, 'r')
        self.anacl = Analysis(self.filobj, tmin, tmax)
        self.N_gids = 1200
        qgd = self.N_gids//4
        self.qgd = qgd
        self.slc = [slice(qgd), slice(qgd, 3*qgd), slice(3*qgd, self.N_gids)]

        self.gids = np.arange(self.N_gids)
        if exp_path is not None:
            STNgids = np.array(self.filobj[exp_path]['props']['Ctx_asg_STN'])
            self.STNgids = np.where(STNgids==1)[0]

   #     self.STNgids = self.gids[self.slc[0]] 
        self.GPPgids = self.gids[self.slc[1]] 
        self.GPAgids = self.gids[self.slc[2]] 

     #   self.filobj.close()

# Syn Current
    def get_curr(self, exp_path, net=True, cap=False):
        tim = np.array(self.filobj[exp_path]['props/timesteps'])-1500

        if net:
            gaba = np.mean(self.filobj[exp_path]['raw/STNSyn_GABA_GPe'], axis=0)
            ampa = np.mean(self.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'], axis=0) 
            nmda = np.mean(self.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'], axis=0)
            Iint = np.mean(self.filobj[exp_path]['raw/STNSta_i_int'], axis=0)
        else:
            gaba = np.array(self.filobj[exp_path]['raw/STNSyn_GABA_GPe'][0,:])
            ampa = np.array(self.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][0,:]) 
            nmda = np.array(self.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][0,:])
            Iint = np.array(self.filobj[exp_path]['raw/STNSta_i_int'][0,:])

        I_syn = gaba + ampa + nmda
        I_cap = I_syn + Iint if cap else I_syn

        return (gaba, ampa, nmda, I_cap, tim)

    def get_curr_axr(self, curs, axr):
        gaba, ampa, nmda, I_cap, tim = curs
        axr.plot(tim, gaba, tim, I_syn, tim, nmda, tim, ampa) 
        axr.set_ylabel('Average synaptic currents [nA]')
        axr.set_xlabel('time [ms]')
        axr.legend(['GABA', 'NetSyn', 'NMDA', 'AMPA'])

        freq = np.geomspace(2,64, 63)
#        LFO_idx = np.searchsorted(freq, [2,8])
#        LFO_slc = slice(LFO_idx[0], LFO_idx[1])

        syn_spec = self.get_spectral_matrix(I_cap, freq, tim)
#        cap_spec = self.get_spectral_matrix(I_cap, freq, tim)
        

#        LFO_syn = np.mean(syn_spec[LFO_slc], axis=0)
#        LFO_cap = np.mean(cap_spec[LFO_slc], axis=0)

        self.plot_spec(axr_arr[1,0], syn_spec, freq, tim)
#        self.plot_spec(axr_arr[1,1], cap_spec, freq, tim)

#        twin_syn = axr_arr[1,0].twinx()
#        self.style_axis(twin_syn, bottom=False, right=True)
#        twin_syn.plot(tim, LFO_syn, color='w')
#
#        twin_cap = axr_arr[1,1].twinx()
#        self.style_axis(twin_cap, bottom=False, right=True)
#        twin_cap.plot(tim, LFO_cap, color='w')
        return axr

    def plot_curr(self, exp_path, filnam):
        figprops = {
                    'fig_siz': (4,8),
                    'glshap': (2,1),
                    }
        fig, gl = self.create_fig(figprops)
        axr_arr = self.get_axs_arr(fig, gl)

        I_currs = self.get_curr(exp_path, net=True, cap=False)

        axr_arr[0,0] = self.get_curr_axr(I_currs, axr_arr[0,0])

        fig.tight_layout()
        fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
        fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

# Ind V
    def get_indV(self, exp_path, axr_arr, idx={'STN':[0,10,25,50],'GPP':[0,100],'GPA':[0,100]}, timrng=None):
    #    STN_idx = [0, 100]

        idx_lst = idx['STN']
        GPP_idx = idx['GPP']
        GPA_idx = idx['GPA']

#        idx_lst = [0, 10, 25, 50]
#        GPP_idx = [0, 100]
#        GPA_idx = [0, 100]

        STN_idx = self.gids[idx_lst] 
#        GPP_idx = [self.qgd, self.qgd+100]
#        GPA_idx = [3*self.qgd, 3*self.qgd+100]
        fch_idx = STN_idx
        STN_arr = np.array(self.filobj[exp_path]['raw/STNSta_V'][STN_idx,:])
        GPP_arr = np.array(self.filobj[exp_path]['raw/GPPSta_V'][GPP_idx,:])
        GPA_arr = np.array(self.filobj[exp_path]['raw/GPASta_V'][GPA_idx,:])
        
        tim = np.array(self.filobj[exp_path]['props/timesteps'])
        if timrng != None:
            tmin=timrng[0]
            tmax=timrng[1]
            tidx = np.searchsorted(tim, timrng)    
            tim=tim[tidx[0]:tidx[1]]-1500
            STN_arr=STN_arr[:,tidx[0]:tidx[1]]
            GPP_arr=GPP_arr[:,tidx[0]:tidx[1]]
            GPA_arr=GPA_arr[:,tidx[0]:tidx[1]]


        if len(idx_lst) > 1:
            axr_arr[0,0].plot(tim, STN_arr.T)
            axr_arr[1,0].plot(tim, GPP_arr.T)
            axr_arr[2,0].plot(tim, GPA_arr.T)
        else:
            axr_arr[0,0].plot(tim, STN_arr.T, color='r')
            axr_arr[1,0].plot(tim, GPP_arr.T, color='b')
            axr_arr[2,0].plot(tim, GPA_arr.T, color='g')
            

        axr_arr[1,0].plot(tim, GPP_arr.T, lw=0.5)
        axr_arr[2,0].plot(tim, GPA_arr.T, lw=0.5)
        axr_arr[1,0].set_ylabel('Membrane Potential [mV]')
        return axr_arr

    def plot_v(self, exp_path, filnam):
        figprops = {
                    'fig_siz': (5,4),
                  #  'glshap': (3,1),
                    'glshap': (1,1),
                    }
        fig, gl = self.create_fig(figprops)
        axr_arr = self.get_axs_arr(fig, gl, form=False)
        axr_arr = self.get_indV(exp_path, axr_arr)
        axr_arr[0,0].set_ylabel('Membrane Potential [mV]')
        fig.tight_layout()
        fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
        fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

# Specs
    def plot_spec(self, axis, spcmat, freq, tim):
        img = axis.imshow(spcmat, cmap='plasma', aspect='auto', origin='bottom', extent=[tim[0], tim[-1], 1, 6]) 
        axis.yaxis.set_major_formatter(ticker.FuncFormatter(self.major_formatter))
        axis.set_ylabel('Frequency [Hz]')
       #  axis.yaxis.set_label_position('right')
        axis.yaxis.grid(b=True, lw=0.1, ls =':', alpha=0.3)
       #  cbarax, kw = mpl.colorbar.make_axes(axis, location='bottom', fraction=0.05, pad=0)
        
        cbarax, kw = mpl.colorbar.make_axes_gridspec(axis, orientation='horizontal', fraction=0.05, pad=0.1) 
        cbar = mpl.colorbar.Colorbar(cbarax, img, format=formatter, **kw)

    def get_spectral_matrix(self, signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
        spcmat = np.zeros((len(freq), len(tim)))
        signal = np.transpose(signal)
        signal = sps.detrend(signal)
    
        for idx, f in enumerate(freq):
           # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
            wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
            conv = sps.fftconvolve(signal, wvlt, 'same')
            spcpow = abs(conv)**2
            spcmat[idx,:] = spcpow
        return spcmat

# Get data
    def get_spk_arr(self, exp_path, gids):
        return self.anacl.get_gid_spk(exp_path, gids) 
    
    def get_gid_ISI(self, spk_arr):
        return self.anacl.get_gid_ISI(spk_arr) 

    def get_gid_freq(self, spk_arr):
        return self.anacl.get_gid_freq(spk_arr) 

# Hist
    def get_hist_spk_arr(self, exp_path, axr_arr):
        N_gids = self.N_gids 
        spk_arr = self.get_spk_arr(exp_path, self.gids)
        spk_ISI = self.get_gid_ISI(spk_arr)
        spk_freq = self.get_gid_freq(spk_arr)
        
        col = ['r', 'b', 'g']
        tit = ['STN', 'GPeP', 'GPeA']
        binsiz = [10,15,5]
    
        for i in range(3):
            isi_con = np.concatenate(spk_ISI[self.slc[i],0])
            isi_mean = np.mean(isi_con)
            isi_median = np.median(isi_con)
            isi_std = np.std(isi_con)
    
            axr_arr[0,i].hist(isi_con, color=col[i], bins=binsiz[i]) 
      #      axr_arr[0,i].yaxis.set_major_locator(tikloc)
    
            twin_ax = axr_arr[0,i].twinx()
            twin_ax.boxplot(isi_con, showmeans=True, vert= False, showfliers=False, meanprops={'color':'k'})
            twin_ax.axis('off')
       #     axr_arr[0,i].text(0.5, 0.7, '{:.2f}\n{:.2f}\n{:.2f}\n{:.2f}'.format(isi_mean, isi_median, isi_std,  isi_std/isi_mean, fontsize='xx-small'), 
       #                         verticalalignment='center', family='monospace', transform=axr_arr[0,i].transAxes)
    
    
            fre_con = np.concatenate(spk_freq[self.slc[i]])
            fre_mean = np.mean(fre_con)
            print(fre_mean)
            fre_median = np.median(fre_con)
            fre_std = np.std(fre_con)
    
       #     axr_arr[0,i].set_title(tit[i], pad=15)
            axr_arr[0,i].set_title(tit[i], color=col[i], pad=1)
    
            axr_arr[1,i].hist(fre_con, color=col[i], bins=binsiz[i])
            twin_ax = axr_arr[1,i].twinx()
            twin_ax.boxplot(fre_con, showmeans=True, vert= False, showfliers=False, meanprops={'color':'k'})
            twin_ax.axis('off')
       #     axr_arr[1,i].text(0.5, 0.7, '{:.2f}\n{:.2f}\n{:.2f}\n{:.2f}'.format(fre_mean, fre_median, fre_std,  fre_std/fre_mean, fontsize='xx-small'), 
       #                         verticalalignment='center', family='monospace', transform=axr_arr[1,i].transAxes)

        axr_arr[0,0].set_ylabel('ISI count') 
        axr_arr[0,1].set_xlabel('ISI [ms]') 
        axr_arr[1,0].set_ylabel('Frequency count') 
        axr_arr[1,1].set_xlabel('Spike Frequency [Hz]') 

        for ax in axr_arr.flatten():
            t = ax.yaxis.get_offset_text()
            t.set_x(-0.3)

        return axr_arr
    
    def plot_hist_spk(self, exp_path, filnam):
        figprops = {
                    'fig_siz': (10, 8),
                    'glshap': (1,2),
                    }
        fig, gl = self.create_fig(figprops)
        axr_arr = self.get_axs_arr(fig, gl)

        axr_arr = self.get_hist_spk_arr(exp_path, axr_arr) 

        fig.align_ylabels(axr_arr[:,0])
    
        fig.tight_layout()
        axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
            xytext=(0., 1), textcoords='figure fraction',
            horizontalalignment='left', verticalalignment='top',
            )

        fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
        fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

# Spk Cnt
    def get_spkcnt_axr(self, exp_path, axr_arr, timalgn=1500, spkcntaxis=True, spkaxis=None):
        spk_arr = self.get_spk_arr(exp_path, self.gids) 
        STNpop_spks = spk_arr[self.slc[0]]
    #    STNpop_spks = spk_arr[self.STNgids]
        self.STNgids = np.arange(300) 
    
        STN_pp, tbin = self.anacl.get_gid_pp(exp_path, self.STNgids, tbin=5) 
        GPP_pp, tbin = self.anacl.get_gid_pp(exp_path, self.GPPgids, tbin=5) 
        GPA_pp, tbin = self.anacl.get_gid_pp(exp_path, self.GPAgids, tbin=5) 
        STN_sum = np.sum(STN_pp, axis=0)
        GPP_sum = np.sum(GPP_pp, axis=0) #[:-1]
        GPA_sum = np.sum(GPA_pp, axis=0) #[:-1]

        tposts = np.arange(self.anacl.tmin-timalgn, self.anacl.tmax+tbin-timalgn, tbin)

        self.style_axis(axr_arr[0,0], left=True, form=False)
        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[1,0])
    
        for gidx, tvec in enumerate(STNpop_spks):
            axr_arr[0,0].eventplot(np.concatenate(tvec)-timalgn, lineoffsets=gidx, linelengths=0.95, linewidths=1, alpha=1, color='r')
    
        if spkcntaxis:
            spk_cnt_ax = axr_arr[0,0].twinx()
            spk_cnt_ax = self.style_axis(spk_cnt_ax, top=False, right=True, bottom=False, left=False, form=False)
            spk_cnt_ax.bar(tposts, STN_sum, width=tbin, align='center', alpha=0.75, color='k')
 #           spk_cnt_ax.yaxis.tick_left()
            spk_cnt_ax.yaxis.set_label_text('STN Spike Count', color='k')

 #       gpe_cnt_ax = axr_arr[0,0].twinx()
 #       gpe_cnt_ax = self.style_axis(spk_cnt_ax, top=False, right=True, bottom=False, left=False, form=False)
 #       gpe_cnt_ax.plot(tposts, GPP_sum, 'b', ls=':')
 #      # gpe_cnt_ax.plot(tposts, GPP_sum, 'b', ':', tposts, GPA_sum, 'g', ':', tposts, GPP_sum+GPA_sum, 'm', ':') 
 #       gpe_cnt_ax.yaxis.tick_right()
 #       gpe_cnt_ax.yaxis.set_label_text('GPe Spike Count', color='k')

 #       axr_arr[0,0].axis('off')
        axr_arr[0,0].spines['bottom'].set_visible(False)
        axr_arr[0,0].xaxis.set_visible(False)

        tpst=tposts[:-1]

     #   axr_arr[1,0].plot(tpst, STN_sum[:-1], 'r', tpst, GPP_sum, 'b', tpst, GPA_sum, 'g')
        axr_arr[1,0].plot(tpst, STN_sum[:-1], 'r')
        axr_arr[1,0].plot(tpst, GPP_sum[:-1], 'b')
        axr_arr[1,0].plot(tpst, GPA_sum[:-1], 'g')
        axr_arr[1,0].set_ylabel('Spike counts')
     #   axr_arr[1,0].legend(['STN', 'GPeP', 'GPeA', 'AllGPe'])


        axr_arr[1,0].set_xlabel('time [ms]')
        axr_arr[0,0].set_ylabel('STN unit', color='r')
        if spkcntaxis:
            axr_arr[0,0].set_title('STN Raster and Spike Counts')
        else:
            axr_arr[0,0].set_title('STN Raster')

        axr_arr[1,0].set_title('Synchronous Population Spiking', pad=-0.5)


#        axr_arr[2,0].set_title('STN, GPeP, GPeA')

#        axr_arr[1,0].set_title('GPe Spike Counts')
    
    #    axr_arr[1,0].set_axis_off()
        axr_arr[2,0].set_axis_off()

        STN_cntfft = np.fft.rfft(STN_sum, n=10*STN_sum.size) 
        GPP_cntfft = np.fft.rfft(GPP_sum, n=10*STN_sum.size) 
        GPA_cntfft = np.fft.rfft(GPA_sum, n=10*STN_sum.size) 
        fftfreq = np.fft.rfftfreq(n=10*STN_sum.size, d=tbin/1000) 
        freq_cut = 125
        freq_cutoff = np.searchsorted(fftfreq, freq_cut)
        fftSTN_abs = np.abs(STN_cntfft[:freq_cutoff]) 
        fftGPP_abs = np.abs(GPP_cntfft[:freq_cutoff]) 
        fftGPA_abs = np.abs(GPA_cntfft[:freq_cutoff]) 
        fftSTN_dB = 20 * np.log10(fftSTN_abs/fftSTN_abs[0])
        fftGPP_dB = 20 * np.log10(fftGPP_abs/fftGPP_abs[0])
        fftGPA_dB = 20 * np.log10(fftGPA_abs/fftGPA_abs[0])
#        fftSTN_dB = fftSTN_abs/fftSTN_abs[0]
         
        axr_arr[3,0].plot(fftfreq[:freq_cutoff], fftGPA_dB, color='g')
        axr_arr[3,0].plot(fftfreq[:freq_cutoff], fftGPP_dB, color='b')
        axr_arr[3,0].plot(fftfreq[:freq_cutoff], fftSTN_dB, color='r')

        axr_arr[3,0].set_xlabel('frequency [Hz]')
        axr_arr[3,0].set_ylabel('Norm. Power [dB]')
        axr_arr[3,0].set_title('Spike train spectral power')

        return axr_arr

    def plot_spk_cnt(self, exp_path, filnam, timalgn=1500):
        figprops = {
                   # 'fig_siz': (5, 6),
                    'fig_siz': (10, 8),
                    'glshap': (2,1),
                    'hratio': (3, 1)
                    }
        fig, gl = self.create_fig(figprops)
        axr_arr = self.get_axs_arr(fig, gl, form=False)

        axr_arr[0,0].get_shared_x_axes().join(axr_arr[0,0], axr_arr[1,0])

        axr_arr = self.get_spkcnt_axr(exp_path, axr_arr, timalgn=1500)    

        fig.align_ylabels(axr_arr[:,0])
        fig.tight_layout()

  #      axr_arr[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
  #          xytext=(0., 1), textcoords='figure fraction',
  #          horizontalalignment='left', verticalalignment='top',
  #          )

  #      axr_arr[1,0].annotate('PosB', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
  #          xytext=(0., 0.32), textcoords='figure fraction',
  #          horizontalalignment='left', verticalalignment='top',
  #          )

        fig.savefig('{!s}.pdf'.format(filnam[0]),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
        fig.savefig('{!s}.pgf'.format(filnam[0]),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    

#        figprops1 = {
#                    'fig_siz': (4, 4),
#                    'glshap': (1,2),
#                    }
#        fig1, gl1 = self.create_fig(figprops1)
#        axs = self.get_axs_arr(fig1, gl1)
#    
#        axs[0,0].plot(tposts, STN_sum, 'r', tposts, GPP_sum, 'b', tposts, GPA_sum, 'g', tposts, GPP_sum+GPA_sum, 'c') 
#        axs[0,0].set_xlabel('time [ms]')
#        axs[0,0].set_ylabel('Spike counts')
#        axs[0,0].legend(['STN', 'GPeP', 'GPeA', 'AllGPe'])
#        NFFT=128
#    
#        axs[0,1].cohere(STN_sum, GPP_sum, Fs=1000/tbin, NFFT=NFFT)
#        axs[0,1].cohere(STN_sum, GPA_sum, Fs=1000/tbin, NFFT=NFFT)
#        axs[0,1].cohere(STN_sum, GPP_sum+GPA_sum, Fs=1000/tbin, NFFT=NFFT)
#        axs[0,1].cohere(GPP_sum, GPA_sum, Fs=1000/tbin, NFFT=NFFT)
#        axs[0,1].grid(False)
#        axs[0,1].legend(['STN-GPeP', 'STN-GPeA', 'STN-AllGPe', 'GPeA-GPeP'])
#    
#        fig1.tight_layout()
#        fig1.savefig('{!s}.pdf'.format(filnam[1], transparent=True, rasterized=True))
#        fig1.savefig('{!s}.pgf'.format(filnam[1], transparent=True, rasterized=True))


# Figures
    def create_fig(self, figprops, constrained_layout=False):
        fig_dict = {
                    'fig_siz': (5,4),
                    'glshap': (1,1),
                    'hratio': None,
                    'wratio': None,
                    }
        fig_dict.update(figprops)
        fig = mpl_figure.Figure(figsize=fig_dict['fig_siz'], constrained_layout=constrained_layout)
        fignamcanv = FigureCanvas(fig)
        gl = fig.add_gridspec(*fig_dict['glshap'], width_ratios=fig_dict['wratio'], height_ratios=fig_dict['hratio'])
        return fig, gl 

    def get_axs_arr(self, fig, gl, form=True):
        axs_arr = np.empty(shape=gl.get_geometry(), dtype='O')
        for i, _ in np.ndenumerate(axs_arr):
            axs_arr[i] = fig.add_subplot(gl[i])
            self.style_axis(axs_arr[i], form=form)
        return axs_arr

    def style_axis(self, ax, top=False, right=False, bottom=True, left=True, offset=5, form=True):
        spn = ['top', 'bottom', 'right', 'left']
        for sp in spn:
            ax.spines[sp].set_position(('outward', offset))
            ax.spines[sp].set_visible(eval(sp))
      #  ax.spines['left'].set_position(('outward', offset))
        if form:
            ax.yaxis.set_major_formatter(formatter)
#            ax.get_yaxis().get_offset_text().set_position((0,1))
       # ax.spines['top'].set_visible(top)
       # ax.spines['right'].set_visible(right)
        return ax

    def major_formatter(self, x, pos):                                                                                             
        return '{:d}'.format(int(2**x))

if __name__ == '__main__':
    #fil='/Volumes/Work/LabData/STNGPe/2019-04-10/MBP20190410_RegularSpiking-000.hdf5' 
     filF0='/scratch/04119/pmoolcha/STNGPe/2019-05-28/TAC20190528_Spiking_SingPop_curr_spikes-000.hdf5'
#     filRhy='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_Spiking_SingPop_curr_spikes-000.hdf5'
   #  filEhi='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighInt-000.hdf5'
     filELo='/scratch/04119/pmoolcha/STNGPe/2019-05-13/TAC20190513_SingPop_Reg_Event_HighLow-000.hdf5' 

     stnfil='/scratch/04119/pmoolcha/STNGPe/2019-06-04/TAC20190604_OneCell-000.hdf5'
     ampafil='/scratch/04119/pmoolcha/STNGPe/2019-06-04/TAC20190604_Spiking_SingPop_curr_spikes_H4_AMPA-000.hdf5'   
     nmdafil='/scratch/04119/pmoolcha/STNGPe/2019-06-04/TAC20190604_Spiking_SingPop_curr_spikes_H4_NMDA-000.hdf5'   
     hihi25='/scratch/04119/pmoolcha/STNGPe/2019-06-01/TAC20190601_HiHi25-000.hdf5'
     hihi55='/scratch/04119/pmoolcha/STNGPe/2019-06-01/TAC20190601_HiHi55-000.hdf5'
     hihi85='/scratch/04119/pmoolcha/STNGPe/2019-06-01/TAC20190601_HiHi85-000.hdf5'

#        exp_path='P000/F1E50/F000/SimpleNet4/N000'
      #  exp_path='P004/F1H20/F000/SimpleNet4/N000'
#        exp_path='P002/F1H4/F000/SimpleNet/N000'
   #     exp_path='P000/F22NE50/F000/SimpleNet4/N000'
     exp_path='P000/F0intrin/F000/SimpleNet4/N001'

     ancls = Plots(filF0, exp_path)
     ancls.plot_v(exp_path, 'F0IndVpre')

  #  hist_spk()
#    spk_cnt()



#    filF0='/scratch/04119/pmoolcha/STNGPe/2019-05-28/TAC20190528_Spiking_SingPop_curr_spikes-000.hdf5'
#    Plots(filF0)
