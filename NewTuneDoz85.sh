#!/bin/bash

#SBATCH -N 8
#SBATCH -n 384 
#SBATCH -t 3:00:00
#SBATCH --job-name=Doz_BigDelta
#SBATCH -p skx-normal 
#SBATCH --mail-type=ALL
#SBATCH --mail-user=prannath_moolchand@alumni.brown.edu

export I_MPI_EXTRA_FILESYSTEM_LIST=garbage
module load python3
module load phdf5/1.8.16
#ibrun python3 runsim.py param/QuadPopNoSEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopNoSReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/QuadPopSegReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopEvnt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopReg.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME

ibrun python3 runsim.py param/NewTuningRhyth_seg085.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/NewNetTuneSingPopEvtAMPANMDALoInt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/NewNetTuneSingPopEvtAMPAHiInt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/NewNetTuneSingPopEvtNMDAHiInt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/NewNetTuneSingPopEvtAMPALoInt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/NewNetTuneSingPopEvtNMDALoInt.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME

#ibrun python3 newrunsim.py param/NewPostRes.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
#ibrun python3 runsim.py param/SingPopIntrin.param $SLURM_JOB_ID $SLURMD_NODENAME $SLURM_JOB_QOS $SLURM_CLUSTER_NAME
