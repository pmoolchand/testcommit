#! python3

import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.preamble': [
         '\\usepackage{units}',         # load additional packages
         '\\usepackage{metalogo}',
         '\\usepackage{unicode-math}',  # unicode math setup
         r'\setmathfont{xits-math.otf}',
         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.style as mpl_style
import matplotlib.gridspec as gs
import matplotlib.text as mpl_txt
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])



def main():
#    filF0 ='/Volumes/Work/sandbox/Code/SGP35N75HD/TAC20200709_Spiking_SingPop_F0intrin-001.hdf5'
#    F0expt_path='P000/F0intrin/F000/SimpleNet4/N001'

#    filF0 ='/Volumes/Work/LabData/STNGPe/2020-07-15/MBP20200715_Spiking_SingPop-000.hdf5'
#    F0expt_path='P000/F0intrin/F000/SimpleNet4/N000'

#    filSing ='/Volumes/Work/LabData/STNGPe/2020-09-14/MBP20200914_Spiking_OneCell-002.hdf5'
#    filSing ='/Volumes/Work/LabData/STNGPe/2020-09-14/MBP20200914_Spiking_OneCell-009.hdf5'
    filSing ='/Volumes/Work/LabData/STNGPe/2020-11-28/MBP20201128_OneCell-001.hdf5' 
    Singexpt_path='P000/F0intrin/F000/OneCell/N000'

###### Restest
  #  filF0 ='/Volumes/Work/LabData/STNGPe/2020-09-14/MBP20200914_Spiking_SingPop-024.hdf5'
    filF0 ='/Volumes/Work/LabData/STNGPe/2020-09-14/MBP20200914_Spiking_SingPop-042.hdf5'
    F0expt_path='P000/F0intrin/F000/SimpleNet4/N000'

    ancls = Plots(filF0, F0expt_path, tmax=1500) 
    anclsing = Plots(filSing, Singexpt_path)

    figprops = {                                                                                                                                                                                    
                'fig_siz': (8, 10),
         #       'fig_siz': (10, 8),
                'glshap': (3,3),
                'hratio': (3,0.5,1.5),
                'wratio': (1,0.25,1),
                }
    fig, gl = ancls.create_fig(figprops)
    gl.update(wspace=0, hspace=0)
    gl.tight_layout(fig, pad=0)

    
    gl_V = gs.GridSpecFromSubplotSpec(10, 1, subplot_spec=gl[:, 2], wspace=0.0, hspace=0.1, height_ratios=(1,1,1,0.1,1,1,1,0.7,1.5,1.8))

    axr_arrV = ancls.get_axs_arr(fig, gl_V, form=False)
#    axs_props = [(0,0,3,1), (1,0,3,1), (2,0,3,1)]



#    axs_arr = np.empty(shape=gl.get_geometry(), dtype='O')
#    for i, _ in np.ndenumerate(axs_arr):
#        axs_arr[i] = fig.add_subplot(gl[i])
#        ancls.style_axis(axs_arr[i], form=form)


    for i in [1,2,4,5,6]: 
        axr_arrV[0,0].get_shared_x_axes().join(axr_arrV[0,0], axr_arrV[i,0])


    for i in range(6): 
        axr_arrV[i,0].spines['bottom'].set_visible(False)
        axr_arrV[i,0].xaxis.set_visible(False)

    axr_arrV[4:,:] = ancls.get_indV(F0expt_path, axr_arrV[4:,:], timrng=[500,1500])
    axr_arrV[:3,:] = anclsing.get_indV(Singexpt_path, axr_arrV[:3,:], idx={'STN':[0],'GPP':[0],'GPA':[0]}, timrng=[500,1500])

#    axr_arrV[0,0].set_title('Single Cells Autonomous Activity [mV]', pad=0)
    axr_arrV[0,0].set_title('Single Cells Autonomous Activity [mV]')
    axr_arrV[4,0].set_title('Single Cells in STN-GPe network')
    axr_arrV[6,0].set_xlabel('time [ms]')

    axr_arrV[0,0].set_ylabel('$V_{STN}$', color='r')
    axr_arrV[4,0].set_ylabel('$V_{STN}$', color='r')
    axr_arrV[1,0].set_ylabel('$V_{GPeP}$', color='b')
    axr_arrV[5,0].set_ylabel('$V_{GPeP}$', color='b')
    axr_arrV[2,0].set_ylabel('$V_{GPeA}$', color='g')
    axr_arrV[6,0].set_ylabel('$V_{GPeA}$', color='g')

    axr_arrV[3,0].set_axis_off()
    axr_arrV[7,0].set_axis_off()

    V_rng = np.arange(-100, 50, 0.05)
    AMPA_mod = 1 + -1/(1+np.exp(-0.045*(V_rng-30)))
    NMDA_mod = 1/(1+np.exp(-0.06613*(V_rng+36)))
    GAGG_mod = 1 - 1/(1+np.exp(-0.5*(V_rng+39)))
    GAGS_mod = 1 - 1/(1+np.exp(-0.125*(V_rng+9)))

    axr_arrV[8,0].plot(V_rng, AMPA_mod, V_rng, NMDA_mod, V_rng, GAGG_mod, V_rng, GAGS_mod)
    axr_arrV[8,0].set_ylabel('$f_{scale}$')
    axr_arrV[8,0].set_title('Voltage dependent synaptic conductances')
    axr_arrV[8,0].legend(['AMPA', 'NMDA', 'GABA_{STN}', 'GABA_{GP}'], loc='best', frameon=False, fontsize='x-small')
    axr_arrV[8,0].spines['bottom'].set_visible(False)
    axr_arrV[8,0].xaxis.set_visible(False)

    pnorm_AMPA = V_rng*AMPA_mod
    pnorm_NMDA = V_rng*NMDA_mod
    pnorm_GAGG = (-84-V_rng)*GAGG_mod
    pnorm_GAGS = (-84-V_rng)*GAGS_mod

    N_AMPA80 = 20*20
    N_NMDA40 = 140*20 
    N_GAGA60 = 40 *20 
    N_GAGS50 = 50*20 

    norm_AMPA = pnorm_AMPA/abs(pnorm_AMPA[N_AMPA80])
    norm_NMDA = pnorm_NMDA/abs(pnorm_NMDA[N_NMDA40])
    norm_GAGG = pnorm_GAGG/abs(pnorm_GAGG[N_GAGA60])
    norm_GAGS = pnorm_GAGS/abs(pnorm_GAGS[N_GAGS50])

    axr_arrV[9,0].set_title('Voltage dependent peak currents', pad=-1)
#    axr_arrV[9,0].plot(V_rng, norm_AMPA, V_rng, norm_NMDA) #, V_rng, norm_GAGG, V_rng, norm_GAGS) 
    axr_arrV[9,0].plot(V_rng, norm_AMPA, V_rng, norm_NMDA, V_rng, norm_GAGG, V_rng, norm_GAGS) 
    axr_arrV[9,0].set_xlabel('membrane potential [mV]')
    axr_arrV[9,0].set_ylabel('Normalized peak current')


    gl_raster = gs.GridSpecFromSubplotSpec(4, 1, subplot_spec=gl[0, 0], wspace=0.0, hspace=0.05, height_ratios=(3,1,0.8,1.5))
    axr_arrraster = ancls.get_axs_arr(fig, gl_raster, form=False)

    axr_arrraster = ancls.get_spkcnt_axr(F0expt_path, axr_arrraster, timalgn=1500, spkcntaxis=False)


    gl_hist = gs.GridSpecFromSubplotSpec(2, 3, subplot_spec=gl[2, 0], wspace=0.5, hspace=0.8)
    axr_hist = ancls.get_axs_arr(fig, gl_hist, form=True)
    axr_hist = ancls.get_hist_spk_arr(F0expt_path, axr_hist)
    



 #   fig.add_artist(txt_LT)
    fig.tight_layout()
    fig.align_ylabels(np.concatenate((axr_arrraster[:,0], axr_hist[:,0]), axis=0))
    fig.align_ylabels(axr_arrV)
    
#    axr_hist[0,0].annotate('PosA', xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='axes fraction',
#        xytext=(0., 1), textcoords='axes fraction',
#        horizontalalignment='left', verticalalignment='top',
#        )

#    bots, tops, lefs, rigs =  gl.get_grid_positions(fig, raw=False)
 #   print(gl_raster.get_grid_positions(fig))
 #   print(gl_hist.get_grid_positions(fig))

#    axr_arrV[0,0].annotate('PosLT', xy=(0,1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0,1), textcoords='figure fraction', horizontalalignment='left', verticalalignment='top', )
#       
#    axr_arrraster[0,0].annotate('PosRT', xy=(0.5, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0.5, 1), textcoords='figure fraction', horizontalalignment='left', verticalalignment='top',)
#
#    axr_hist[0,0].annotate('PosRB', xy=(0.5, 0.35), fontsize='large', fontweight='bold',  xycoords='figure fraction',
#        xytext=(0.5, 0.35), textcoords='figure fraction', horizontalalignment='left', verticalalignment='top',)



    filnam='PaperFig1Restest12'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)

if __name__=='__main__':
    main()
