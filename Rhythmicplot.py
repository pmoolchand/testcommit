#! python3

import h5py

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': True,    # use inline math for ticks
  #  'text.usetex': False,    # use inline math for ticks
#    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': True,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
#         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Sans}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
#rcParams['font.sans-serif'] = ['Tahoma', 'DejaVu Sans',
#                               'Lucida Grande', 'Verdana']

# Choose Backend, Create Canvas
#mpl.use('Agg')
#from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

mpl.use('pgf')
from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.gridspec as gs
import matplotlib.style as mpl_style
import matplotlib.image as mpimg
mpl_style.use('paper')
#mpl_style.use('presentation')
#mpl.rcParams['lines.linewidth'] = 1.5 
import matplotlib.ticker as ticker
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
import plot_utils as plut

#from AnalysisClass import Analysis 
from PlotSpikes import Plots
from MulFilAna import GLM_Analysis

import numpy as np
import scipy.signal as sps
import scipy.stats as sts
import itertools as it

formatter = ticker.ScalarFormatter(useMathText=True)
#formatter = ticker.ScalarFormatter()
formatter.set_powerlimits((-1,1))
tikloc = ticker.MaxNLocator(nbins=4)

alpbt = np.array(['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z'])

def conflict():

    filConf='/scratch/04119/pmoolcha/STNGPe/ConflictPlots/ConflictDozE50SingleParam.hdf5'
    filSing='/scratch/04119/pmoolcha/STNGPe/BigDozSing.hdf5'
    fildum='/scratch/04119/pmoolcha/STNGPe/2021-03-19/TAC20210319_Spiking_Conflictp_Evt_AMPANMDAHiInt_BigDelta_350-001.hdf5'

    timrng=[1250, 8000]
    andum=Plots(fildum, None, tmin=timrng[0], tmax=timrng[1]) 

    figprops = {
                'fig_siz': (8, 10),
             #   'glshap': (2,1),
             #   'hratio': [3,2], 
                'glshap': (5,1),
                }

    fig, gl = andum.create_fig(figprops, constrained_layout=True)
    gl.tight_layout(fig)

    param_dict={}
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (5, 5)),# '--'
                            (0, (1, 5)), #':'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]
    param_dict['tbin'] = 5
    param_dict['ylimSTNpp'] = None 
    param_dict['GLM_Ana'] = ['LFOplot', 'NetSynplot', 'STN_pp'] 


    mainparam = {
            'param': {
                      #  'NMDAGLV':  {'Ctx_STN_A': [1.833e-05], }, 
                        'NMDAGLV':  {'Ctx_STN_A': [3.666e-06], }, 
                },
            'feed': {
                    'Conf': [('fds', ['RhythF22NE50_150']),] 
                },
            'cond': {
                    'RhythmicEvent' : ('NMDAGLV','Conf'),
                    },
        }             

    IEI = np.array([75, 125, 200, 350, 450])
    N_IEI = IEI.shape[0]

    seg = [25,55,85]
    N_seg = len(seg)
    seglab = [str(sg) for sg in seg]
  #  delay = [5,10,37]
    delay = [5,]
    stimdur = 50

    IEIdur = (np.array(IEI) + stimdur) * 11 + stimdur
    filtfds = [['ReReDozRhythF22NE50_{:d}'.format(ie)] for ie in IEI]

    pres = ['LFOplot', 'NetSyn', 'STN_spk']
    N_pres = len(pres)

    tit_lst = []

    def plot_timser():

        def set_axes():
            global Rhythaxs_arr
            Rhythspec = gs.GridSpecFromSubplotSpec(N_pres, N_IEI, subplot_spec=gl[0:N_pres, :]) 
            Rhythaxs_arr = np.empty(shape=Rhythspec.get_geometry(), dtype='O') 

            for idx, _  in np.ndenumerate(Rhythaxs_arr):
                Rhythaxs_arr[idx] = fig.add_subplot(Rhythspec[idx])
                andum.style_axis(Rhythaxs_arr[idx], form=False)

            ref_ax = Rhythaxs_arr[0,0]
            for _, ax in np.ndenumerate(Rhythaxs_arr):
                ref_ax.get_shared_x_axes().join(ref_ax, ax)

            for ax in Rhythaxs_arr[1,:]:
                andum.style_axis(ax, form=True)
    
            ylabels = [
                    'Avg. theta power\n[(nA)$^2$]',
                    '\nAvg. $I_{syn}$ [nA]',
                    'Cumm. Avg.\nSpike Count',
                    ]

            for i in range(3):
                Rhythaxs_arr[i, 0].get_shared_y_axes().join(*Rhythaxs_arr[i, :])
                Rhythaxs_arr[i, 0].set_ylabel(ylabels[i])
                for j in range(1, N_IEI):
                    Rhythaxs_arr[i,j].axes.get_yaxis().set_visible(False)
                    Rhythaxs_arr[i,j].spines['left'].set_visible(False)

            for _, ax in np.ndenumerate(Rhythaxs_arr[:2, :]):
                ax.axes.get_xaxis().set_visible(False)
                ax.spines['bottom'].set_visible(False)

            for j in range(N_IEI):
                titA = Rhythaxs_arr[0,j].set_title(r'$\Delta$ = {:d} ms'.format(IEI[j]))

            titB = Rhythaxs_arr[1,0].set_title(' ')
            titC = Rhythaxs_arr[2,0].set_title(' ')
            tit_lst.append(titA)
            tit_lst.append(titB)
            tit_lst.append(titC)

            Rhythaxs_arr[-1,2].set_xlabel(r'time [ms]')
            return Rhythaxs_arr

        def plot_data(Rhythaxs_arr):
            global cumsum_arr
            cumsum_arr = np.empty(shape=(3,N_IEI,3,4))

            for j in range(N_IEI):
                mainparam['feed']={'Conf': [('fds', filtfds[j]),]} 
                param_dict['filt_dict'] = mainparam
                anobj=GLM_Analysis(filConf, param_dict, tmin=timrng[0], tmax=timrng[1])
                for i in range(2):
                    Rhythaxs_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], Rhythaxs_arr[i,j], idx=None)
                for i in [2]:
                    Rhythaxs_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], Rhythaxs_arr[i,j], idx=IEIdur[j])

                anobj.filobj.close() 

            for i in range(3):
                Rhythaxs_arr[0, 0].plot([],[], label='{:d}%'.format(seg[i]), color=param_dict['colors'][i])
            Rhythaxs_arr[0, 0].legend(title='Seg', frameon=False, )
            return Rhythaxs_arr


        Rhythaxs_arr = set_axes()
        Rhythaxs_arr = plot_data(Rhythaxs_arr)
        return Rhythaxs_arr

    def plot_stats(cumsum_arr):
    
        def set_axes():
            wratio = [N_seg for i in range(N_IEI)] + [N_IEI for i in range(N_seg)]
        #    Statsspec = gs.GridSpecFromSubplotSpec(2, N_IEI+N_seg, subplot_spec=gl[1, :], width_ratios=wratio) 
            Statsspec = gs.GridSpecFromSubplotSpec(2, N_seg, subplot_spec=gl[N_pres:, :])
            global Statsaxs_arr
            Statsaxs_arr = np.empty(shape=Statsspec.get_geometry(), dtype='O') 
    
            for idx, _  in np.ndenumerate(Statsaxs_arr):
                Statsaxs_arr[idx] = fig.add_subplot(Statsspec[idx])
                andum.style_axis(Statsaxs_arr[idx], form=False)
    
            ref_ax = Statsaxs_arr[0,0]
            for _, ax in np.ndenumerate(Statsaxs_arr):
                ref_ax.get_shared_x_axes().join(ref_ax, ax)
#            ref_ax = Statsaxs_arr[0,N_IEI]
#            for _, ax in np.ndenumerate(Statsaxs_arr[:, N_IEI:]):
#                ref_ax.get_shared_x_axes().join(ref_ax, ax)

          #  non_main_y = np.delete(np.arange(N_IEI+N_seg), [0, N_IEI])
            non_main_y = np.array([1,2])
            ylabels = [
                    'Avg. theta energy\n[(nA)$^2$ms]',
                    'Avg. spike rate\n[spkes/s]',
                    ]


            for i in range(2):
                Statsaxs_arr[i,0].get_shared_y_axes().join(*Statsaxs_arr[i,:])
                Statsaxs_arr[i,0].set_ylabel(ylabels[i])
                for j in non_main_y: 
                    Statsaxs_arr[i,j].axes.get_yaxis().set_visible(False)
                    Statsaxs_arr[i,j].spines['left'].set_visible(False)
            for ax in Statsaxs_arr[0,:]:
                ax.axes.get_xaxis().set_visible(False)
                ax.spines['bottom'].set_visible(False)
#            Statsaxs_arr[-1,2].set_xlabel('Segregation [%]')     
            Statsaxs_arr[-1,1].set_xlabel(r'$\Delta$ [ms]')     

#            for j in range(N_IEI):
#                Statsaxs_arr[0,j].set_title(r'$\Delta$ = {:d} ms'.format(IEI[j]))
            for j in range(N_seg):
            #    Statsaxs_arr[0,N_IEI+j].set_title(r'Seg = {:d} \%'.format(seg[j]))
                Statsaxs_arr[0,j].set_title('\n\nSeg = {:d}\%'.format(seg[j]))
            titD = Statsaxs_arr[0,1].set_title('\n\nSeg = {:d}\%'.format(seg[1]))
            titE = Statsaxs_arr[1,1].set_title(' ')
             
            tit_lst.append(titD)
            tit_lst.append(titE)
            return Statsaxs_arr

        def plot_stats(Statsaxs_arr, cumsum_arr):
            cumsum_tmp = cumsum_arr[0,:,:,:] 
   
            for j in range(N_IEI):
                cumsum_tmp[j,:,:] = cumsum_tmp[j,:,:]/IEIdur[j]
            
            t_IEI_pow = np.zeros(shape=(N_IEI,N_IEI,N_seg,2)) 

            for j,i in it.combinations(range(N_IEI), 2):
                for k in range(N_seg):
                    t_IEI_pow[i,j,k,:] = sts.ttest_ind(cumsum_tmp[i,k,:], cumsum_tmp[j,k,:], equal_var=False) 

    #        for i in range(N_IEI):
    #            Statsaxs_arr[0,i].boxplot(cumsum_tmp[i,:,:].T, showmeans=True, labels=seg) 
            for i in range(N_seg):
            #    Statsaxs_arr[0,N_IEI+i].boxplot(cumsum_tmp[:,i,:].T, showmeans=True, labels=IEI) 
                Statsaxs_arr[0,i].boxplot(cumsum_tmp[:,i,:].T, showmeans=True, labels=IEI) 


            spksum_tmp = cumsum_arr[2,:,:,:] 

            t_IEI_spk = np.zeros(shape=(N_IEI,N_IEI,N_seg,2)) 

            for j,i in it.combinations(range(N_IEI), 2):
                for k in range(N_seg):
                    t_IEI_spk[i,j,k,:] = sts.ttest_ind(spksum_tmp[i,k,:], spksum_tmp[j,k,:], equal_var=False) 

#            for i in range(N_IEI):
#                Statsaxs_arr[1,i].boxplot(spksum_tmp[i,:,:].T, showmeans=True, labels=seg) 
            for i in range(N_seg):
            #    Statsaxs_arr[1,N_IEI+i].boxplot(spksum_tmp[:,i,:].T, showmeans=True, labels=IEI) 
                Statsaxs_arr[1,i].boxplot(spksum_tmp[:,i,:].T, showmeans=True, labels=IEI) 
            return Statsaxs_arr
            






#            axr_arr[3,1].boxplot(cumsum_tmp[2,:,:].T, showmeans=True, labels=['25%', '55%', '85%'])
#            axr_arr[3,1].set_ylim(bottom=10)
 #           labpos = axr_arr[3,1].get_xticks()






        Statsaxs_arr = set_axes()
        Statsaxs_arr = plot_stats(Statsaxs_arr, cumsum_arr)
        return Statsaxs_arr




    Rhythaxs_arr = plot_timser()
    Statsaxs_arr = plot_stats(cumsum_arr)

    

    filnam='Fig8'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, bbox_inches = 'tight', pad_inches = 0)

    if True:
        plut.add_labels((Statsaxs_arr[0,0],'y'), (Rhythaxs_arr[0,0], tit_lst[0]), 'A')
        plut.add_labels((Statsaxs_arr[0,0],'y'), (Rhythaxs_arr[1,0], tit_lst[1]), 'B')
        plut.add_labels((Statsaxs_arr[0,0],'y'), (Rhythaxs_arr[2,0], tit_lst[2]), 'C')
        plut.add_labels((Statsaxs_arr[0,1],tit_lst[3]), (Statsaxs_arr[0,1], tit_lst[3]), 'Theta Energy during Stimulation\n\n', False)
        plut.add_labels((Statsaxs_arr[0,0],'y'), (Statsaxs_arr[0,0], tit_lst[3]), 'D\n\n')
        plut.add_labels((Statsaxs_arr[1,1],tit_lst[4]), (Statsaxs_arr[1,1], tit_lst[4]), '\nSpiking Rate during Stimulation', False)
        plut.add_labels((Statsaxs_arr[0,0],'y'), (Statsaxs_arr[1,0], tit_lst[4]), 'E')

    #    plut.add_labels((HiHiaxr_arr[0,0],'y'), (HiHiaxr_arr[0,0], tit_lst[2]), 'D')
    #    plut.add_labels((Detcaxr_arr[0,0],'y'), (Detcaxr_arr[0,0], tit_lst[3]), 'E')
    #    plut.add_labels((Detcaxr_arr[0,2],'y'), (Detcaxr_arr[0,2], tit_lst[3]), 'F')
    #    plut.add_labels((Confaxr_arr2[0,0],'x'), (Confaxr_arr2[0,1], 'x'), 'time [ms]', False)
    #    plut.add_labels((Detcaxr_arr[1,0],'x'), (Detcaxr_arr[1,1], 'x'), 'time [ms]', False)
    #    plut.add_labels((Detcaxr_arr[0,0],'y'), (Detcaxr_arr[1,0], 'y'), 'Avg. $I_{syn}$ [nA]\n', False)
    #    plut.add_labels((Detcaxr_arr[0,2],'y'), (Detcaxr_arr[1,2], 'y'), 'q$_{NMDA}$:q$_{AMPA}$ ratio', False)
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, bbox_inches = 'tight', pad_inches = 0) 


def popo():




#ReReDozRhythF22NE50_125.py  ReReDozRhythF22NE50_200.py  ReReDozRhythF22NE50_25.py  ReReDozRhythF22NE50_275.py  ReReDozRhythF22NE50_350.py  ReReDozRhythF22NE50_450.py  ReReDozRhythF22NE50_75.py

    

    cumsum_arr = np.empty(shape=(3,N_IEI,3,4))

  #  filtset= [parmsetSing25, parmsetSing150, parmsetConf25, parmsetConf150]
    plotpos = [0, 2, 3]
    fils = [filSing, filConf]
 #   filidx= [0,0,1,1]
    filidx= [1,1,0,1]

#    IEI = ['25', '75', '150', '350']
#    IEIdur = np.array([2380, 2930, 3755, 5955]) - 1500

#    IEI = ['75', '125', '150', '200']
#    IEIdur = np.array([2930, 3480, 3755, 4305]) - 1500

 #   IEI = ['150', '200', '275', '450']
 #   IEI = ['25', '75', '150', '350']
 #   IEIdur = np.array([3755, 4305, 5130, 7055]) - 1500

    

    for j in range(N_IEI):
     #   param_dict['filt_dict'] = filtset[j] 
        mainparam['feed']={'Conf': [('fds', filtfds[j]),]} 
        param_dict['filt_dict'] = mainparam
    #    anobj=GLM_Analysis(fils[filidx[j]], param_dict)
    #    anobj=GLM_Analysis(fils[filidx[j]], param_dict, tmin=timrng[0], tmax=timrng[1])
        anobj=GLM_Analysis(filConf, param_dict, tmin=timrng[0], tmax=timrng[1])
        for i in range(2):
            axr_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], axr_arr[i,j], idx=None)
        for i in [2]:
            axr_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], axr_arr[i,j], idx=IEIdur[j])
       #     axr_arr[i,j], cumsum_arr[i,j,:,:] = run_mulfil(anobj, pres[i], axr_arr[i,j], idx=None)

        anobj.filobj.close() 
       
    # cumsum: [LFO, IEI, seg, tri]
#    print(cumsum_arr[0,0,0,:])


    axr_arr[0,0].set_ylabel('Avg. theta power [(nA)$^2$]')
    axr_arr[1,0].set_ylabel('Avg. $I_{syn}$ [nA]')
    axr_arr[2,0].set_ylabel('Cumm. Avg. Spike Count')


    cumsum_tmp = cumsum_arr[0,:,:,:] 
   
    for j in range(N_IEI):
        cumsum_tmp[j,:,:] = cumsum_tmp[j,:,:]/IEIdur[j]


    #for i, iei in enumerate(IEI):
   # axr_arr[3,0].bar(cumsum_tmp[:,0,:].T, orientation='vertical', yerr=cumsum_err[:,0])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, positions=[0,4,8,11], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][0]), labels=[25, 75,150,350]) 
#    axr_arr[3,0].boxplot(cumsum_tmp[:,1,:].T, positions=[1,5,9,12], showmeans=False, patch_artist=True, boxprops=dict(facecolor=param_dict['colors'][1], color=param_dict['colors'][1]), labels=[25,75,150,350])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,2,:].T, positions=[2,6,10,13], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][2]), labels=[25,75,150,350])
#    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, positions=[1,4,7,10], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][0], facecolor=None), labels=[25, 75,150,350]) 
#    axr_arr[3,0].boxplot(cumsum_tmp[:,2,:].T, positions=[2,5,8,11], showmeans=False, patch_artist=True, boxprops=dict(color=param_dict['colors'][2], facecolor=None), labels=[25,75,150,350])


    t_IEI = np.zeros(shape=(N_IEI,N_IEI,2)) 

    for j,i in it.combinations(range(N_IEI), 2):
        t_IEI[i,j,:] = sts.ttest_ind(cumsum_tmp[i,0,:], cumsum_tmp[j,0,:], equal_var=False) 
    print(t_IEI[:,:,-1])

    axr_arr[3,0].boxplot(cumsum_tmp[:,0,:].T, showmeans=True, labels=IEI) 

    for k in [1,2]:
        for j,i in it.combinations(range(N_IEI), 2):
            t_IEI[i,j,:] = sts.ttest_ind(cumsum_tmp[i,k,:], cumsum_tmp[j,k,:], equal_var=False) 
        print(t_IEI[:,:,-1])



    
    t_seg = np.zeros(shape=(3,2,2)) 

    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(cumsum_tmp[2,i,:], cumsum_tmp[2,j,:], equal_var=False) 
    print(t_seg[:,:,-1])





    axr_arr[3,1].boxplot(cumsum_tmp[2,:,:].T, showmeans=True, labels=['25%', '55%', '85%'])
#    axr_arr[3,1].set_ylim(bottom=10)
    labpos = axr_arr[3,1].get_xticks()
#    axr_arr[3,1].plot([labpos[0], labpos[2]], [11,11], color='k')
#    axr_arr[3,1].annotate('*', (0.5*(labpos[0]+labpos[2]), 11.5), xycoords='data', color='k')

#    axr_arr[0,0].set_title('$\Delta$= 25 ms')
#    axr_arr[0,1].set_title('$\Delta$= 75 ms')
#    axr_arr[0,2].set_title('$\Delta$= 150 ms')
#    axr_arr[0,3].set_title('$\Delta$= 350 ms')

#    axr_arr[0,0].set_title('$\Delta$= 75 ms')
#    axr_arr[0,1].set_title('$\Delta$= 125 ms')
#    axr_arr[0,2].set_title('$\Delta$= 150 ms')
#    axr_arr[0,3].set_title('$\Delta$= 200 ms')

#    axr_arr[0,0].set_title('$\Delta$= 150 ms')
#    axr_arr[0,1].set_title('$\Delta$= 200 ms')
#    axr_arr[0,2].set_title('$\Delta$= 275 ms')
#    axr_arr[0,3].set_title('$\Delta$= 450 ms')
    axr_arr[0,0].set_title('$\Delta$= 25 ms')
    axr_arr[0,1].set_title('$\Delta$= 75 ms')
    axr_arr[0,2].set_title('$\Delta$= 150 ms')
    axr_arr[0,3].set_title('$\Delta$= 350 ms')

    for k in [0,1,3]:
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(cumsum_tmp[k,i,:], cumsum_tmp[k,j,:], equal_var=False) 
        print(t_seg[:,:,-1])


    print('spikes')

    spksum_tmp = cumsum_arr[2,:,:,:] 
    t_IEI = np.zeros(shape=(4,3,2)) 

    for j,i in it.combinations(range(4), 2):
        t_IEI[i,j,:] = sts.ttest_ind(spksum_tmp[i,0,:], spksum_tmp[j,0,:], equal_var=False) 
    print(t_IEI[:,:,-1])

    axr_arr[3,2].boxplot(spksum_tmp[:,0,:].T, showmeans=True, labels=IEI) 

    for k in [1,2]:
        for j,i in it.combinations(range(4), 2):
            t_IEI[i,j,:] = sts.ttest_ind(spksum_tmp[i,k,:], spksum_tmp[j,k,:], equal_var=False) 
        print(t_IEI[:,:,-1])


    t_seg = np.zeros(shape=(3,2,2)) 

    for j,i in it.combinations(range(3), 2):
        t_seg[i,j,:] = sts.ttest_ind(spksum_tmp[2,i,:], spksum_tmp[2,j,:], equal_var=False) 
    print(t_seg[:,:,-1])

    axr_arr[3,3].boxplot(spksum_tmp[2,:,:].T, showmeans=True, labels=['25%', '55%', '85%'])


    for k in [0,1,3]:
        for j,i in it.combinations(range(3), 2):
            t_seg[i,j,:] = sts.ttest_ind(spksum_tmp[k,i,:], spksum_tmp[k,j,:], equal_var=False) 
        print(t_seg[:,:,-1])



    for i in range(4):
        axr_arr[-2,i].set_xlabel('time [ms]')




    axr_arr[-1,0].set_xlabel('Inter Event Interval, $\Delta$ [ms]')
    axr_arr[-1,0].set_ylabel('Avg. theta power during Stim, [$nA^2$]')
    axr_arr[-1,0].annotate('All Sig.\np<0.05', (0.95, 0.2), xycoords='axes fraction', ha='right')
    axr_arr[-1,0].set_title('Seg = 25%')
    axr_arr[-1,2].set_xlabel('Inter Event Interval, $\Delta$ [ms]')
    axr_arr[-1,2].set_title('Seg = 25%')
    axr_arr[-1,1].set_xlabel('Segregation Level')
    axr_arr[-1,2].set_ylabel('Avg. Spk. rate during Stim, [spk/s]')
    axr_arr[-1,1].set_title('$\Delta$ = 150 ms')
    axr_arr[-1,2].annotate('All Sig.\np<0.05', (0.95, 0.2), xycoords='axes fraction', ha='right')
    axr_arr[-1,3].set_xlabel('Segregation Level')
    axr_arr[-1,3].set_title('$\Delta$ = 150 ms')

#    fig.text(0.25, 0.95, 'No Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
#    fig.text(0.75, 0.95, 'Conflict', fontsize='large', fontweight='bold', transform=fig.transFigure, horizontalalignment='center')
  #  br = param_dict['colors'][5]
  #  ctx_lw=3
  #  ypos1= np.array([4.4e1, 4.4e1])
  #  ypos2 = ypos1-0.2e1
  #  ypos1mod= np.array([1.1e1, 1.1e1])

  #  axr_arr[0,0].plot([0,50], ypos1mod, [75,125], ypos1mod, [150,200], ypos1mod, color='k', lw=ctx_lw)
  #  axr_arr[0,2].plot([0,50], ypos1, [75,125], ypos1, [150,200], ypos1, color='k', lw=ctx_lw)

  #  axr_arr[0,1].plot([0,50], ypos1mod, [200,250], ypos1mod, [400,450], ypos1mod, color='k', lw=ctx_lw)
  #  axr_arr[0,3].plot([0,50], ypos1, [200,250], ypos1, [400,450], ypos1, color='k', lw=ctx_lw)

  #  axr_arr[0,2].plot([5,55], ypos2, [80,130], ypos2, [155,205], ypos2, color=br, lw=ctx_lw)
  #  axr_arr[0,3].plot([5,55], ypos2, [205,255], ypos2, [405,455], ypos2, color=br, lw=ctx_lw)

  #  stim25 = [0, 75, 150]
  #  stim150 = [0, 200, 400]
  #  for i in [0,2]:
  #      for j in stim25:
  #          axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
  #  for i in [1,3]:
  #      for j in stim150:
  #          axr_arr[0,i].axvline(j, color='k', ls='--', lw=0.1, alpha=0.5)
  #          axr_arr[0,i].axvline(j+5, color=br, ls='--', lw=0.1, alpha=0.5)

    verpos = [1, 3/4, 1/2, 1/4]
    horpos = [0, 1/4, 1/2, 3/4]

    seglev=[25, 55, 85]
    for i in range(3):
        axr_arr[0,1].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
        axr_arr[2,2].plot([],[], label='{:d}%'.format(seglev[i]), color=param_dict['colors'][i])
   # axr_arr[-1,2].legend(title='Seg', fontsize='x-large', frameon=False)
    axr_arr[0,1].legend(title='Seg', frameon=False)
    axr_arr[2,2].legend(title='Seg', frameon=False)


#    for j,i in it.product(range(1,4), range(2)):
#        axr_arr[i,j].set_axis_off()

    #for j in range(1,4):
    #    axr_arr[1,j].set_axis_off()
    #axr_arr[0,-1].set_axis_off()
    #axr_arr[0,1].set_axis_off()

    #for i in range(1,4):
    #    axr_arr[-1,i].get_yaxis().set_visible(False)
    #    axr_arr[-1,i].spines['left'].set_visible(False)
    #for i in range(2):
    #    axr_arr[i,0].get_xaxis().set_visible(False)
    #    axr_arr[i,0].spines['bottom'].set_visible(False)

    #axr_arr[0,2].get_xaxis().set_visible(False)
    #axr_arr[0,2].spines['bottom'].set_visible(False)

    

    fig.align_ylabels(axr_arr[:, 0])
    fig.align_ylabels(axr_arr[:, 2])
  #  alp_rep = np.reshape(alpbt[:axr_arr.size], axr_arr.shape)
  #  for idx, ax in np.ndenumerate(axr_arr):
  #  #    ax.annotate('P{:d}{:d}'.format(idx[0], idx[1]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
  #  #    xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
  #  #    horizontalalignment='left', verticalalignment='top'
  #  #    )

  #      ax.annotate('{!s}'.format(alp_rep[idx]), xy=(0, 1), fontsize='large', fontweight='bold',  xycoords='figure fraction',
  #      xytext=(horpos[idx[1]], verpos[idx[0]]), textcoords='figure fraction',
  #      horizontalalignment='left', verticalalignment='top'
  #      )

 #   filnam='quiLowDozConfAll'
    filnam='quiHighDozSingAllMIdFocus'
    fig.savefig('{!s}.pdf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
    fig.savefig('{!s}.pgf'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)
#    fig.savefig('{!s}.svg'.format(filnam),  transparent=True, rasterized=True, bbox_inches = 'tight', pad_inches = 0)


def run_mulfil(ancls, pres, ax, otherstim=False, idx=None, confunits=None):
    print('idx', idx)
    for chore, res in ancls.my_chores:
        chor_seg = [ancls.uniq_combo[ancls.uniq_sims[chore[1],0]], ancls.uniq_feeds[ancls.uniq_sims[chore[1],1]]]
    if pres=='LFOplot':
        print(chor_seg)
        ax, arr = ancls.run_timser(ax, chore, 'STN_LFO_Amp', chor_seg, sqr=True, otherstim=otherstim, cumsum=True, confunits=confunits)
    elif pres=='NetSyn':
        ax, arr = ancls.run_timser(ax, chore, 'NetSyn', chor_seg, sqr=False, otherstim=otherstim, confunits=confunits)
    elif res=='STN_pp':
     #   cell_spk = ancls.get_spk(chore, 'STN_spk', ancls.tmin_ana, ancls.tmax_ana)
        cell_spk = ancls.get_spk(chore, 'STN_spk', tmin=1250, tmax=6500)
        print('tbin', ancls.tbin)
        cell_pp, tbin = ancls.get_pp(cell_spk, tbin=ancls.tbin)
        fdsp_pp = ancls.run_pp(chore, 'STN_spk', chor_seg)

        print(fdsp_pp)

        ax = ancls.get_STNpp_ax(ax, fdsp_pp, tbin, chor_seg, pers='fd', ylim=None, sbpop=0) 
        if idx is not None:
            t0_idx = int((1500-ancls.tmin_ana)/tbin)
            t_idx = int(idx/tbin) 
            print('popo', t0_idx, t_idx)
            avgspkr = 1000*np.sum(fdsp_pp[:,:,2, t0_idx:t_idx], axis=-1)/idx 
        arr = avgspkr 
    return ax, arr


def get_spk_rate(pp, T, typ, ofset=10, tbin=5):
    tbin_of = int(ofset//5)
    # pp = avg
    nnz_idx = np.where(pp[tbin_of:]>0)[0][0]
    T_idx = int(T//tbin)
    if typ == 'R':
        rat = np.sum(pp[tbin_of+nnz_idx:T_idx])*1000/(tbin*(T_idx-nnz_idx)) if nnz_idx < T_idx else 0
    elif typ == 'E':
        rat = np.sum(pp[tbin_of+nnz_idx:tbin_of+nnz_idx+T_idx])*1000/T if nnz_idx < T_idx else 0
    return rat 

def plot_spec_cur(ancls, exp_path, gids, axr_arr, coord, timrng):

    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
    i,j = coord
    axr_arr[i,j+3].plot(tim, I_syn, tim, gaba, tim, nmda, tim, ampa) 

# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 

    img=axr_arr[i,j].imshow(STNspec, cmap='plasma', origin='bottom', aspect='auto', extent=[tim[0], tim[-1], 1, 6])
    axr_arr[i,j].yaxis.set_major_formatter(ticker.FuncFormatter(major_formatter))

    return axr_arr

def get_resfreq(ancls, exp_path, gids, timrng):
    tim = np.array(ancls.filobj[exp_path]['props/timesteps'])
    tmin=timrng[0]
    tmax=timrng[1]
    tidx = np.searchsorted(tim, timrng)    
    tim=tim[tidx[0]:tidx[1]]-1500

    gaba = np.mean(ancls.filobj[exp_path]['raw/STNSyn_GABA_GPe'][gids,:], axis=0)[tidx[0]:tidx[1]]
    ampa = np.mean(ancls.filobj[exp_path]['raw/STNSyn_AMPA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]] 
    nmda = np.mean(ancls.filobj[exp_path]['raw/STNSyn_NMDA_Ctx'][gids,:], axis=0)[tidx[0]:tidx[1]]
    I_syn = gaba + nmda + ampa 
   
# specs
    freq = np.geomspace(2,64,66)
    STNspec = get_spectral_matrix(I_syn, freq, tim) 
    ind = np.unravel_index(np.argmax(STNspec, axis=None), STNspec.shape)
    maxfreq = freq[ind[0]]
    return maxfreq
    


def get_spectral_matrix(signal, freq, tim, sT= 0.025e-3, N=2**17+1, s= 3.5):
    spcmat = np.zeros((len(freq), len(tim)))
    signal = np.transpose(signal)
    signal = sps.detrend(signal)

    for idx, f in enumerate(freq):
       # wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')/np.sqrt(s * (np.exp(-f**2)-2*np.exp(-0.75*f**2)+1))
        wvlt = sps.morlet(N, f*N*0.5*sT/s, s, 'True')*np.sqrt(s)
        conv = sps.fftconvolve(signal, wvlt, 'same')
        spcpow = abs(conv)**2
        spcmat[idx,:] = spcpow
    return spcmat

def major_formatter(x, pos):                                                                                             
    return '{:d}'.format(int(2**x))



if __name__ == '__main__':
    conflict()
