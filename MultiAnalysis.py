#! python3 

from mpi4py import MPI 
import h5py, os, sys, datetime

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': False,    # use inline math for ticks
    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Serif}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl

# Choose Backend, Create Canvas
mpl.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

#mpl.use('pgf')
#from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)
import matplotlib.figure as mpl_figure
import matplotlib.ticker as ticker
import matplotlib.text as text
#import matplotlib.markers as markers
#import matplotlib.lines as lines
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import numpy as np 
import scipy.stats as sts
import itertools as it
from functools import reduce
from AnalysisClass import MultiAnalysis, VarEffects, sing_style_axis 

sys_name = 'ANATAC'

class MultiAnaStats():
    def __init__(self, param_dict, filt_dict={}):

        # Set parallel machinery
        self.rank = MPI.COMM_WORLD.Get_rank()
        self.N_hosts = MPI.COMM_WORLD.Get_size()
    
        for k,v in param_dict.items():
            setattr(self, k, v)
        
        # Load data order and create data array
        # Load Multiclass
        self.multclas = MultiAnalysis(self.fils, tmin=self.tmin, tmax=self.tmax, ad_fd=self.ad_fd, ad_nt=self.ad_nt, tbreaks=self.tbreaks, filt_dict=self.filt_dict)
        self.N_fils = self.multclas.N_fils
        self.param_combo = self.multclas.uniq_param_combo
        self.uniq_feeds = self.multclas.uniq_feed_pars
        self.uniq_nets = self.multclas.uniq_net_pars
        self.multclas.load_Ana_class()
    
        # Get relevant sims and assign chores
        self.get_multi_chores()

        # Create data array
        self.get_data_arr()

        print(self.chores)

        # Get data
        for cidx, sim in enumerate(self.my_chores):
            anacl_obj = self.multclas.ana_class[sim[0]]
            self.data_arr[0][cidx] = (sim[2], anacl_obj) 
            for _, var in self.dep_list[1:]:       
                self.data_arr[self.data_position[var]][cidx] = self.proc_dict(anacl_obj, var, sim[1], cidx)

        self.get_lbl_mat()
     #   self.feed_lbls = [obj[2][0] for obj in self.effects_mat[1]]
        self.feed_lbls = [obj[0] for obj in self.uniq_feeds['fd']]
        self.fd_tit = self.format_feeds(self.feed_lbls, styl='Seg')
    
#        print(self.effects_mat[1])
        # Prepare data for analysis
        self.get_analysis_assgn()
        for var in self.ana_dict.keys():
            self.gather_var(var)

        if self.rank == 0:
            for var in self.ana_dict.keys():
                self.data_arr[self.data_position[var]] = self.reshape_sim_arr(var)

        self.data_arr[1:] = MPI.COMM_WORLD.bcast(self.data_arr[1:], root=0)

        self.my_ana = self.RunAnalysis[self.rank:len(self.RunAnalysis):self.N_hosts]

      #  for ana in self.my_ana:
      #      self.run_ana(ana[0])
#       self.plot_IS0_reg()

        self.multclas.close_files()


    def plot_prop_reg(self, prop, proptit, proplab):
        prop_hist = self.data_arr[self.data_position[prop]]
      #  prop_hist = self.data_arr[self.data_position['STN_ISI0_mnhs']]
        N_c, N_f, N_n = prop_hist.shape
        N_trials, N_axes, N_timint, _ = prop_hist[0,0,0].shape
        N_subpop = self.data_arr[self.data_position['STN_pop']][0][0].shape[0]

        N_rows = 1 

        axr_arr = np.empty(shape=(N_rows, N_timint), dtype='O')
        fig = mpl_figure.Figure(figsize=(6*N_timint,6*N_rows))
        canvas = FigureCanvas(fig)
        gl = fig.add_gridspec(N_rows, N_timint)
    
        c_vals = []
        for var in self.ParEffects:
            c_var = var[0]
            c_vals.append(np.float(var[1][var[0]][0]))
        c_vals = np.array(c_vals)

        if c_var == 'Ctx_STN_A':
            c_var = 'STN_NMDA'
            c_vals = 1.402 * np.array(c_vals)
        lwy = .1e-5
        c_valmin = min(c_vals) - lwy
        c_valmax = max(c_vals) + lwy

        poplab = ['AGGR']
        for i in range(N_subpop):
            poplab.append('SB{:02d}'.format(i))

        axr_arr = self.axis_create_align(axr_arr, gl, fig, xlab=c_var, ylab='Mean {!s} [{!s}]'.format(proplab[0], proplab[1]))

        formatter = ticker.ScalarFormatter(useMathText=False)
        formatter.set_powerlimits((-2,2))

        for (pidx, tidx), ax in np.ndenumerate(axr_arr):
            reg_res = np.empty(shape=(N_axes,N_f,2))
            for popidx in range(N_axes):
                for fidx, nidx in it.product(range(N_f), range(N_n)):
                    col_mn = []
                    col_tl = []
                    for cidx, arr in enumerate(prop_hist[:,fidx, nidx]):
                        mns = arr[:, popidx, tidx, 0]
                        c_tile = np.tile(c_vals[cidx], mns.shape[0])
                        col_mn.append(np.mean(mns))
                        col_tl.append(c_tile[0])
                        ax.scatter(c_tile, mns, color=self.colors[popidx], marker=self.markers[fidx], alpha=0.8)


                    res = sts.linregress(col_tl, col_mn)
#                    ax.errorbar(c_vals, res[0]*c_vals+res[1], color=self.colors[popidx], marker=self.markers[fidx], ls = self.lnstyl[fidx], 
#                        label='{!s} $r:$ {: 5.2f} $p:$ {: 5.2f}\n{!s}'.format(poplab[popidx], res[2], res[3], self.effects_mat[1][fidx,2])) 
                    ax.errorbar(c_vals, res[0]*c_vals+res[1], color=self.colors[popidx], marker=self.markers[fidx], ls = self.lnstyl[fidx]) 
                    reg_res[popidx, fidx,:] = [res[2], res[3]]
#            ax.legend(loc='best', fontsize = 'xx-small', framealpha=0, prop={'family': 'monospace'})
            # simpler legend
            for i in range(N_axes):
                res_vals = ['({:.2f},{:.2})'.format(res[0], res[1]) for res in reg_res[i,:,:]]
                ax.plot([],[], color=self.colors[i], label='{!s} {!s}'.format(poplab[i],  ' '.join(res_vals)))
            #    ax.plot([],[], color=self.colors[i], label=self.lbl_mat[i,0,0].split('\n')[1]) 
            for j in range(N_f):
                ax.plot([],[], color='k', ls=self.lnstyl[j], marker=self.markers[j], label=self.lbl_mat[0,j,0].split('\n')[0][2:-1]) 
            ax.legend(loc='best', fontsize = 'xx-small', framealpha=0, handlelength=5)


            ax.set_xlim([c_valmin, c_valmax])
            ax.xaxis.set_major_formatter(formatter)
            ax.set_title('Effects of {!s} on {!s}'.format(c_var, proplab[0]))

        pers = self.pers[0]
        pers_pre = pers[5]
        var = pers[0]
        var_lbl, fix_lbl = ('feed', 'param') if var else ('param', 'feed')

        fig.tight_layout()
        datenow = datetime.datetime.now()
        fil_pre = '{!s}{!s}_{!s}'.format(sys_name, datenow.strftime("%Y%m%d"), pers_pre)
        sufix = '_'.join((self.doc_prefix, fix_lbl, self.fd_tit))
        fig.savefig('{!s}_{!s}_{!s}_{:03d}.pdf'.format(fil_pre, proptit, sufix, self.rank), transparent=True)

    def plot_prop_hist(self, prop, proptit, proplab):
        prop_hist = self.data_arr[self.data_position[prop]]
        N_c, N_f, N_n = prop_hist.shape
        N_trials, N_axes, N_timint, _ = prop_hist[0,0,0].shape
        N_subpop = self.data_arr[self.data_position['STN_pop']][0][0].shape[0]
        axr_arr = np.empty(shape=(N_axes, N_timint), dtype='O')
        fig = mpl_figure.Figure(figsize=(15*N_timint,4*N_axes))
        canvas = FigureCanvas(fig)
        gl = fig.add_gridspec(N_axes, N_timint)
        axr_arr = self.axis_create_align(axr_arr, gl, fig, xlab='{!s} [{!s}]'.format(proplab[0], proplab[1]), ylab='Density')

        for (pidx, tidx), ax in np.ndenumerate(axr_arr):
            for ijk, arr in np.ndenumerate(prop_hist):
             #   mns = []
                for tri in arr[:,pidx,tidx]:
                    mn, hst = tri
              #      mns.append(mn)
                    ax.plot(hst[0], hst[1], color=self.colors[ijk[0]], ls=self.lnstyl[ijk[1]], lw=0.3)
                    ax.axvline(mn, color=self.colors[ijk[0]], ls=self.lnstyl[ijk[1]], lw=0.3)
              #  ax.axvline(np.mean(arr[:,pidx,tidx,0]), lw=1.0, color=self.colors[ijk[0]], ls=self.lnstyl[ijk[1]], label=self.lbl_mat[ijk])
                ax.axvline(np.mean(arr[:,pidx,tidx,0]), lw=1.0, color=self.colors[ijk[0]], ls=self.lnstyl[ijk[1]])
          #  ax.legend(loc='best', fontsize = 'xx-small', framealpha=0)
          #  ax.legend(loc='best', fontsize = 'xx-small', framealpha=0, title='col: param, ls: feed')
            # easier legend
            for i in range(N_c):
                ax.plot([],[], color=self.colors[i], label=self.lbl_mat[i,0,0].split('\n')[1]) 
            for j in range(N_f):
                ax.plot([],[], color='k', ls=self.lnstyl[j], label=self.lbl_mat[0,j,0].split('\n')[0][2:-1]) 
            ax.legend(loc='best', fontsize = 'xx-small', framealpha=0, handlelength=6)

        fig.tight_layout()
     #   fig.savefig('pufpuf')
        pers = self.pers[0]
        pers_pre = pers[5]
        var = pers[0]
        var_lbl, fix_lbl = ('feed', 'param') if var else ('param', 'feed')
        datenow = datetime.datetime.now()
        fil_pre = '{!s}{!s}_{!s}'.format(sys_name, datenow.strftime("%Y%m%d"), pers_pre)
        sufix = '_'.join((self.doc_prefix, fix_lbl, self.fd_tit))
        fig.savefig('{!s}_{!s}_{!s}_{:03d}.pdf'.format(fil_pre, proptit, sufix, self.rank), transparent=True)

    def plot_prop_box(self, prop, proptit, proplab, vew=0):
        # Load data
        prop_means = self.data_arr[self.data_position[prop]]
        N_trials, N_axes, N_timint, _ = prop_means[0,0,0].shape
        N_subpop = self.data_arr[self.data_position['STN_pop']][0][0].shape[0]

        subpop_dict = {'all': (np.arange(N_axes), ['Aggregate']+['SB_{:02d}'.format(i) for i in range(1, N_axes)]), 
                       'fst': (np.array([2]), ['First(Ctx)']), 
                       'scd': (np.array([1]), ['Second(Ctx)']), 
                       'fnd': (np.array([1,2]), ['Second(Ctx)', 'First(Ctx)']),
                        } 

        pers = self.pers[0]
      #  var = pers[0]
        var = vew
        fix = int(not(var)) 
        cat = pers[2]
        p_sbpop = subpop_dict[pers[3]][0]
        N_pspop = p_sbpop.size
        styl = pers[4]
        pers_pre = pers[5]

       # feed_lbls, fxfd, fd_tit = self.format_feeds([obj[2][0] for obj in self.effects_mat[1]], styl=styl)

        var_lbl, fix_lbl = ('feed', 'param') if var else ('param', 'feed')
        cat_dict = {'subpop': (p_sbpop, subpop_dict[pers[3]][1]),
                    'feed': (np.arange(prop_means.shape[1]), self.feed_lbls),
                    'param': (np.arange(prop_means.shape[0]), [obj[2][0] for obj in self.effects_mat[0]]),
                    }

        # Define order of boxes
        N_vars = cat_dict[var_lbl][0].shape[0]
        N_fix = cat_dict[fix_lbl][0].shape[0] 
        N_cat = cat_dict[cat][0].shape[0] 
        sep = 1
        N_catsep = N_vars+sep

        axr_arr = np.empty(shape=(N_fix, N_timint), dtype='O')
        STN_prop_mean_mat = np.empty(shape=prop_means.shape+(N_axes, N_timint), dtype=np.ndarray)
        for ijk, _ in np.ndenumerate(prop_means):
            for pidx, tidx in it.product(range(N_axes), range(N_timint)):
                STN_prop_mean_mat[ijk+(pidx,tidx)] = prop_means[ijk][:,pidx,tidx][:,0]

        # Box organization
        boxpos = np.concatenate([np.arange(i*N_catsep, (i+1)*N_catsep-1) for i in range(N_cat)]) 
        boxorder = np.arange(N_vars*N_cat).reshape(N_vars,N_cat).flatten('F') 
        tick_pos = np.mean(boxpos.reshape(N_cat,N_vars), axis=1)
        tick_label = cat_dict[cat][1] 

        # Box props
        medianprops = dict(linestyle=':', color='black')
        meanlineprops = dict(linestyle='-', color='black', marker='x')
        fig = mpl_figure.Figure(figsize=(12*N_timint,10*N_fix))
        canvas = FigureCanvas(fig)
        gl = fig.add_gridspec(N_fix, N_timint)
        axr_arr = self.axis_create_align(axr_arr, gl, fig, xlab='Subpopulation', ylab='{!s} [{!s}]'.format(proplab[0], proplab[1]))

        if var == 0:
            var_tit = cat_dict[var_lbl][1][0].split(':')[0]
            for pidx, timidx in it.product(range(N_fix), range(N_timint)): 
              #  pltobj = axr_arr[pidx,timidx].boxplot(STN_prop_mean_mat[:,:,:,:,timidx].flatten()[boxorder], widths=0.95, 
                pltobj = axr_arr[pidx,timidx].boxplot(STN_prop_mean_mat[:,pidx,:,p_sbpop,timidx].flatten()[boxorder], widths=0.95, 
                        patch_artist=True, showmeans=True, meanline=True, positions=boxpos, medianprops=medianprops, meanprops=meanlineprops)
                for bidx, patch in enumerate(pltobj['boxes']):
                    patch.set_facecolor(self.colors[bidx%N_vars])
                axr_arr[pidx,timidx].set_xticks(tick_pos)
                axr_arr[pidx,timidx].set_xticklabels(tick_label)
            for j,i in it.product(range(N_fix), range(N_timint)):
              #  axr_arr[j,i].set_title('Effects of {!s} on silence, fixed = {!s}, \n{!s}, time[ms]: {:.0f}-{:.0f}'
              #                #  .format(cat_dict[var_lbl][0], cat_dict[fix_lbl][pidx][:6], self.tbreaks[i], self.tbreaks[i+1]))
              #                  .format(var_tit, cat_dict[fix_lbl][1][j], fxfd, self.tbreaks[i], self.tbreaks[i+1]))
              #  axr_arr[j,i].set_title('Effects of {!s} on silence, fixed = {!s}, \ntime[ms]: {:.0f}-{:.0f}'
              #                  .format(var_tit, cat_dict[fix_lbl][1][j], self.tbreaks[i], self.tbreaks[i+1]))
                axr_arr[j,i].set_title('Effects of {!s} on {!s}, fixed = {!s}, \nAligned to Feed {:1d}'
                                .format(var_tit, proplab[0], cat_dict[fix_lbl][1][j], timidx))
        elif var == 1:
            for pidx, timidx in it.product(range(N_fix), range(N_timint)): 
                pltobj = axr_arr[pidx,timidx].boxplot(STN_prop_mean_mat[pidx,:,:,p_sbpop,timidx].flatten('F')[boxorder], widths=0.95, 
                        patch_artist=True, showmeans=True, meanline=True, positions=boxpos, medianprops=medianprops, meanprops=meanlineprops)
                for bidx, patch in enumerate(pltobj['boxes']):
                    patch.set_facecolor(self.colors[bidx%N_vars])
                axr_arr[pidx,timidx].set_xticks(tick_pos)
                axr_arr[pidx,timidx].set_xticklabels(tick_label)
            for j,i in it.product(range(N_fix), range(N_timint)):
               # axr_arr[j,i].set_title('Effects of {!s} on silence, fixed = {!s}, \ntime[ms]: {:.0f}-{:.0f}'.format(lbl[0], self.FeedEffects[j], self.tbreaks[i], self.tbreaks[i+1]))
              #  axr_arr[j,i].set_title('Effects of {!s} on silence, fixed = {!s}, \ntime[ms]: {:.0f}-{:.0f}'
              #                  .format('Feeds', cat_dict[fix_lbl][1][j], self.tbreaks[i], self.tbreaks[i+1]))
                axr_arr[j,i].set_title('Effects of {!s} on {!s}, fixed = {!s}, \nAligned to Feed {:1d}'
                                .format('Feeds', proplab[0], cat_dict[fix_lbl][1][j], i))
            
        for ij, obj in np.ndenumerate(axr_arr):
            for cidx, lbl in enumerate(cat_dict[var_lbl][1]):
               # axr_arr[0,-1].plot([],[], color=self.colors[cidx%N_vars], label=lbl)
                obj.plot([],[], color=self.colors[cidx%N_vars], label=lbl)
            obj.legend(loc='best', fontsize = 'xx-small', framealpha=0)

##            axr_arr[j,i].set_title('Effects of {!s} on silence, fixed = {!s}, \ntime[ms]: {:.0f}-{:.0f}'.format(lbl[0], self.FeedEffects[j], self.tbreaks[i], self.tbreaks[i+1]))
        pers = self.pers[0]
        pers_pre = pers[5]
        var_lbl, fix_lbl = ('feed', 'param') if var else ('param', 'feed')

        fig.tight_layout()
        datenow = datetime.datetime.now()
        fil_pre = '{!s}{!s}_{!s}'.format(sys_name, datenow.strftime("%Y%m%d"), pers_pre)
        sufix = '_'.join((self.doc_prefix, fix_lbl, self.fd_tit))
        fig.savefig('{!s}_{!s}_{!s}_{:03d}.pdf'.format(fil_pre, proptit, sufix, self.rank), transparent=True)
        
    def get_perspective(self):
        # Perspective params
 #       pers_arr = np.empty(shape=len(self.pers), dtype=np.ndarray)
        subpop_dict = {'all': (np.arange(N_axes), ['Aggregate']+['SB_{:02d}'.format(i) for i in range(N_subpop)]), 
                       'fst': (np.array([2]), ['First(Ctx)']), 
                       'scd': (np.array([1]), ['Second(Ctx)']), 
                       'fnd': (np.array([1,2]), ['Second(Ctx)', 'First(Ctx)']),
                        } 
        pers = self.pers[0]
        var = pers[0]
        fix = pers[1]
        cat = pers[2]
        p_sbpop = subpop_dict[pers[3]][0]
        
    def format_feeds(self, feedlbls, styl=None):
        lbl_ana = np.empty(shape=(len(feedlbls), 6), dtype='O')
        for idx, lbl in enumerate(feedlbls):
            lbl_ana[idx,:] = lbl.split('_')
        uniq_arr = np.empty(6, dtype=np.ndarray)
        for i in range(6):
            uniq_arr[i] = '_'.join(np.unique(lbl_ana[:,i]))

        fd_tit = '-'.join(uniq_arr)
        
        return fd_tit 

    def popo(self):
        varlbls = []
        fixlbls = []

        # 'F22NH2_100S085_DL037_1000DR1000_500A500_03B03
        styl_dict = {'Seg': 1, 'Off': 2, 'Int': 3, 'Bur': 4,}
        styl_keep = [0]
        if styl is not None:
            styl_expand = styl.split('_')
            for add_syl in styl_expand:
                styl_keep.append(styl_dict[add_syl])

        styl_keep = np.unique(styl_keep)
        styl_fix = np.setdiff1d(np.arange(5), styl_keep)
        fd_tit = ''

        for fdl in feedlbls:
            fd_cmp = np.array(fdl.split('_'))
         #   varlbls.append('_'.join(fd_cmp[styl_keep])) 
            varlbls.append(fd_cmp[styl_keep]) 
            fixlbls.append('_'.join(fd_cmp[styl_fix])) 
            fd_tit += '{!s}_'.format(fd_cmp[0])

        fd_tit=fd_tit[:-1]
        uniq_fix = np.unique(fixlbls)
        if uniq_fix.size > 1:
            print('Warning: Fixed components of feed labels not unique')
        return varlbls, uniq_fix[0], fd_tit

    def get_burst_rate(self, idx, intval=200):
        fidx = self.my_chores[idx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        t0 = fd['algn']['t0']

        ft0 = np.concatenate((t0, t0+intval))
        spk = self.data_arr[self.data_position['STN_spk']][idx]
        BurSpk = np.empty(shape=(spk.shape[0],3))
        for ijk, obj in np.ndenumerate(spk):
            if obj.size:
                fidx = np.searchsorted(obj, ft0)
                cnt0 = obj[fidx[0]+1:fidx[2]]
                cnt1 = obj[fidx[1]+1:fidx[3]]
                cntA = obj[fidx[0]+1:fidx[3]] 
                BurSpk[ijk[0],0] = 1000*cnt0.shape[0]/intval 
                BurSpk[ijk[0],1] = 1000*cnt1.shape[0]/intval
                BurSpk[ijk[0],2] = 1000*cntA.shape[0]/(ft0[3]-obj[fidx[0]+1])
            else:
                BurSpk[ijk[0],:] = [0, 0, 0] 
        return BurSpk 

    def get_fir_rate(self, idx, intval=400):
        fidx = self.my_chores[idx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        t0 = fd['algn']['t0']
        ft0 = np.concatenate((t0, t0+intval))
        spk = self.data_arr[self.data_position['STN_spk']][idx]
        FirSpk = np.empty(shape=(spk.shape[0],3))
        for ijk, obj in np.ndenumerate(spk):
            if obj.size:
                fidx = np.searchsorted(obj, ft0)
                cnt0 = obj[fidx[0]:fidx[2]] 
                cnt1 = obj[fidx[1]:fidx[3]] 
                cntA = obj[fidx[0]:fidx[3]]
                FirSpk[ijk[0],0] = 1000*cnt0.shape[0]/intval
                FirSpk[ijk[0],1] = 1000*cnt1.shape[0]/intval
                FirSpk[ijk[0],2] = 1000*cntA.shape[0]/(ft0[3]-ft0[0])
            else:
                FirSpk[ijk[0],:] = [0, 0, 0] 
        return FirSpk 

    def get_prop_pop_mnhs(self, cidx, var, t_bins=25, density=True):
    #    anacl_obj = self.data_arr[0][cidx][1]
#        N_timint = anacl_obj.timeints.size
        STN_subpop = self.data_arr[self.data_position['STN_pop']][cidx][0]
        N_subpop = STN_subpop.shape[0]
        N_axes = max(1, N_subpop) + int(N_subpop>1) 
        t_bins = self.hist_tbins

        fidx = self.my_chores[cidx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        t0 = fd['algn']['t0']
        ftgt = fd['algn']['TgtSub'] 

        prop = self.data_arr[self.data_position[var]][cidx]
        N_timint = prop.shape[1]

        mnhs_mat = np.empty(shape=(N_axes, N_timint,2), dtype=np.ndarray)
        for timidx in np.arange(N_timint):
            all_sil = prop[:,timidx]
            all_sil = all_sil[np.isfinite(all_sil)]
            mnhs_mat[0, timidx, 0] = np.mean(all_sil) 
          #  h,b = np.histogram(all_sil, bins=t_bins, density=density)
            h,b = np.histogram(all_sil, density=density)
            bs = (b[1:] + b[:-1])/2
            mnhs_mat[0, timidx, 1] = np.array([bs,h]) 

        if N_subpop> 1:
            non_target = np.delete(np.arange(N_subpop), ftgt)
            algn_pop = np.concatenate((ftgt, non_target))
            for pidx, _ in enumerate(algn_pop):
                for timidx in range(N_timint): 
                    all_sil = prop[STN_subpop[pidx][0],timidx]
                    all_sil = all_sil[np.isfinite(all_sil)]
                    mnhs_mat[pidx+1, timidx,0] = np.mean(all_sil) 
             #       h,b = np.histogram(all_sil, bins=t_bins, density=density)
                    h,b = np.histogram(all_sil, density=density)
                    bs = (b[1:] + b[:-1])/2
                    mnhs_mat[pidx+1, timidx, 1] = np.array([bs,h]) 
        return mnhs_mat 

    def run_ana(self, var):
        run_dict = {'SilenceHist': "self.plot_prop_hist('STN_ISI0_mnhs', 'SilHist', ('Silence', 'ms'))",
                    'SilenceBox': "self.loop_plot_box('STN_ISI0_mnhs', 'SilBox', ('Silence', 'ms'))",
                    'SilenceReg': "self.plot_prop_reg('STN_ISI0_mnhs', 'SilReg', ('Silence', 'ms'))",
                    'BurstHist': "self.plot_prop_hist('STN_Bur_mnhs', 'BurstHist', ('Burst Rate', 'spk/s'))",
                    'BurstBox': "self.loop_plot_box('STN_Bur_mnhs', 'BurstBox', ('Burst Rate', 'spk/s'))",
                    'BurstReg': "self.plot_prop_reg('STN_Bur_mnhs', 'BurstReg', ('Burst Rate', 'spk/s'))",
                    'FirHist': "self.plot_prop_hist('STN_Fir_mnhs', 'FirHist', ('Firing Rate', 'spk/s'))",
                    'FirBox': "self.loop_plot_box('STN_Fir_mnhs', 'FirBox', ('Firing Rate', 'spk/s'))",
                    'FirReg': "self.plot_prop_reg('STN_Fir_mnhs', 'FirReg', ('Firing Rate', 'spk/s'))",
                    'ThetaBox': "self.loop_plot_box('STN_Theta_mnhs', 'ThetaBox', ('Integral of Theta Power', '$log_{10} (nA^2/s)$'))",
                    'ThetaReg': "self.plot_prop_reg('STN_Theta_mnhs', 'ThetaReg', ('Integral of Theta Power', '$log_{10} (nA^2/s)$'))",
                    }
        return eval(run_dict[var])

    def loop_plot_box(self, prop, proptit, proplab):
        for i in [0,1]:
            self.plot_prop_box(prop, proptit, proplab, vew=i) 

    def get_analysis_assgn(self):
        ana_dict = {} 
        ana_lst = []
#        rnk_arr = np.empty(self.N_hosts, dtype=np.ndarray)
        for idx, ana in enumerate(self.RunAnalysis):
            for i in self.data_dict[ana[0]][1]:
                ana_lst.append((i, ana[1], idx))
                ana_dict.update({i:[]})
#                rnk_arr[ana[1]] = np.append(rnk_arr[ana[1]], idx)

        for i, pref, idx in ana_lst:
            ana_dict[i].append(idx)

        for k, v in ana_dict.items():
            ana_dict[k] = np.sort(v)

        self.ana_dict = ana_dict
      #  self.my_analysis = self.RunAnalysis[self.rank:len(self.chores):self.N_hosts]

    def get_multi_chores(self):
    #    ParEffects = self.ParEffects
    #    FeedEffects = self.FeedEffects
    #    NetEffects = self.NetEffects
    #
    #    par_efct_mat = np.empty(shape=(len(ParEffects),3), dtype='O')
    #    for cidx, c in enumerate(ParEffects):
    #        par_efct_mat[cidx,0] = VarEffects(self.multclas, 'param', c[0], c[1]) 
    #        par_efct_mat[cidx,1:] = par_efct_mat[cidx,0].rel_param 
    #
    #    fd_efct_mat_tmp = np.empty(shape=(len(FeedEffects),4), dtype='O')
    #    collect_fds = []
    #    for fidx, f in enumerate(FeedEffects):
    #        fd_efct_mat_tmp[fidx,0] = VarEffects(self.multclas, 'feed', f, []) 
    #        fd_efct_mat_tmp[fidx,1:] = fd_efct_mat_tmp[fidx,0].rel_feed
    #        for i, j, k in zip(fd_efct_mat_tmp[fidx,1], fd_efct_mat_tmp[fidx,2], fd_efct_mat_tmp[fidx,3]): 
    #           # print(i,j)
    #            collect_fds.append((fidx, i, j, k))

    #    fd_efct_mat = np.empty(shape=(len(collect_fds),4), dtype='O')
    #    for idx, obj in enumerate(collect_fds):
    #        fd_efct_mat[idx,0] = fd_efct_mat_tmp[obj[0],0]
    #        fd_efct_mat[idx,1] = np.array([obj[1]]) 
    #        fd_efct_mat[idx,2] = np.array([obj[2]]) 
    #        fd_efct_mat[idx,3] = np.array([obj[3]]) 

    #    nt_efct_mat = np.empty(shape=(len(NetEffects),3), dtype='O')
    #    for nidx, n in enumerate(NetEffects):
    #        nt_efct_mat[nidx,0] = VarEffects(self.multclas, 'net', n, []) 
    #        nt_efct_mat[nidx,1:] = nt_efct_mat[nidx,0].rel_net
    #
    #    self.effects_mat = np.array([par_efct_mat, fd_efct_mat, nt_efct_mat])
    #    self.chores = []
    #  #  self.cfn_effects = np.empty(shape=(len(ParEffects), len(FeedEffects), len(NetEffects)), dtype='O')
    #    self.cfn_effects = np.empty(shape=tuple([self.effects_mat[i].shape[0] for i in range(3)])+(2,), dtype='O')

    #    # (c,f,n) here does NOT correspond to (c,f,n) in param_fdnt_mat and param_feed_trk
    #    for (cidx,fidx,nidx), _ in np.ndenumerate(self.cfn_effects[:,:,:,0]): 
    #        self.cfn_effects[cidx,fidx,nidx,0] = self.get_trim_simpaths(cidx, fidx, nidx)
    #        self.cfn_effects[cidx,fidx,nidx,1] = fd_efct_mat[fidx,3]
#   #         self.cfn_effects[cidx,fidx,nidx,1] = fd_efct_mat[fidx,3]]
    #        for sidx, sim in enumerate(self.cfn_effects[cidx,fidx,nidx,0]):
    #            self.chores.append((sim[0][0], sim[0][1], sim[1], (cidx, fidx, nidx, sidx), self.cfn_effects[cidx,fidx,nidx,1])) 

        self.chores = np.concatenate((self.multclas.sim_paths, self.arange(self.multclas.sim_paths.shape[0])))

        # Assign chores
        self.chore_ass = np.empty(shape=self.N_hosts, dtype=np.ndarray)
        self.N_chores = len(self.chores)
        if self.rank == 0:
            for i in range(self.N_hosts):
                self.chore_ass[i] =  np.arange(i,self.N_chores,self.N_hosts)       
    
        self.my_chores = self.chores[self.rank:len(self.chores):self.N_hosts]
        
    def get_data_arr(self):
        load_order = {'STN_pop': 0, 'STN_spk': 1, 'STN_ISI0': 2, 'STN_gids': 0, 'STN_ISI0_mnhs': 3, 'STN_Bur':2, 'STN_Bur_mnhs':3, 'STN_Fir':2, 'STN_Fir_mnhs':3, 'STN_Theta_mnhs':2}
        data_dict = {'SilenceHist': (['STN_pop', 'STN_spk',  'STN_ISI0', 'STN_gids', 'STN_ISI0_mnhs'], ['STN_ISI0_mnhs']),
                     'SilenceBox': (['STN_pop', 'STN_spk', 'STN_ISI0', 'STN_gids', 'STN_ISI0_mnhs'], ['STN_ISI0_mnhs']),
                     'SilenceReg': (['STN_pop', 'STN_spk',  'STN_ISI0', 'STN_gids', 'STN_ISI0_mnhs'], ['STN_ISI0_mnhs']),
                     'BurstBox': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Bur', 'STN_Bur_mnhs'], ['STN_Bur_mnhs']),
                     'BurstHist': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Bur', 'STN_Bur_mnhs'], ['STN_Bur_mnhs']),
                     'BurstReg': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Bur', 'STN_Bur_mnhs'], ['STN_Bur_mnhs']),
                     'FirReg': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Fir', 'STN_Fir_mnhs'], ['STN_Fir_mnhs']),
                     'FirBox': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Fir', 'STN_Fir_mnhs'], ['STN_Fir_mnhs']),
                     'FirHist': (['STN_pop', 'STN_spk', 'STN_gids', 'STN_Fir', 'STN_Fir_mnhs'], ['STN_Fir_mnhs']),
                     'ThetaBox': (['STN_pop', 'STN_Theta_mnhs'], ['STN_Theta_mnhs']),
                     'ThetaReg': (['STN_pop', 'STN_Theta_mnhs'], ['STN_Theta_mnhs']),
#                     'GLMAnalysis': (['STN_pop', 'STN_Theta_mnhs'], ['STN_Theta_mnhs']),
                    }
        self.data_dict = data_dict 
        data_load = reduce(np.union1d, [data_dict[par[0]][0] for par in self.RunAnalysis])
        dep_list = []
        for par in data_load:
            dep_list.append((load_order[par], par))
        dep_list.sort()
        dep_list.insert(0, (0, 'sim_lbl'))
        self.dep_list = dep_list

        self.data_position = {par[1]:idx for idx, par in enumerate(dep_list)}
        self.data_arr = np.empty(shape=len(dep_list), dtype=np.ndarray) 
        for idx, _ in enumerate(self.data_arr):
            self.data_arr[idx] = np.empty(shape=len(self.my_chores), dtype=np.ndarray)

    def proc_dict(self, anacl_obj, var, path, idx):
        func_dict ={'STN_pop': "anacl_obj.get_STN_subpop('{!s}'.format(path))",
                    'STN_gids': "np.array(anacl_obj.file['{!s}'.format(path)]['props']['gids_STN'], dtype=int)",
                    'STN_spk': "anacl_obj.get_gid_spk('{!s}'.format(path), self.data_arr[self.data_position['STN_gids']][idx])",
                    'STN_ISI0': "self.get_ISI0(idx)",
                    'STN_ISI0_mnhs': "self.get_prop_pop_mnhs(idx, 'STN_ISI0')",
                    'STN_Bur': "self.get_burst_rate(idx, intval=200)", 
                    'STN_Bur_mnhs': "self.get_prop_pop_mnhs(idx, 'STN_Bur')",
                    'STN_Fir': "self.get_fir_rate(idx, intval=400)", 
                    'STN_Fir_mnhs': "self.get_prop_pop_mnhs(idx, 'STN_Fir')",
                    'STN_Theta_mnhs': "self.get_theta_mnhs(idx, anacl_obj, path)",
                    }
 #       print(var)
    #    STN_ctx[cidx] = np.array(anacl_obj.file[chore[1]]['props']['Ctx_asg_STN'], dtype=int)
 #  #     STN_freq[cidx] = anacl_obj.get_gid_freq(STNpop_spks[cidx])
        return eval(func_dict[var]) 

#    def get_theta(self, idx):
    def get_theta_mnhs(self, cidx, anacl_obj, path, intval=300, preintval=300, intper=500):

        STN_pop = self.data_arr[self.data_position['STN_pop']][cidx][0]
        subpop_size = np.array([i.shape[0] for i in STN_pop[:,0]])
        curr, tim =  anacl_obj.get_subpop_cap_cur(STN_pop, path, Tsam=int(1))
        all_cur = np.empty(shape=(1, tim.shape[0]))
        all_cur[0,:] = np.matmul(subpop_size, curr)/subpop_size.sum()

        N_subpop = STN_pop.shape[0]
        N_axes = max(1, N_subpop) + int(N_subpop>1) 
        N_timint = 2

        T = tim[1]-tim[0]
#        freq = np.geomspace(2,8,15)
        freq = np.array([2,4])

        fidx = self.my_chores[cidx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        ft0 = fd['algn']['t0']
        ftgt = fd['algn']['TgtSub'] 
        
        rel_tidx = np.searchsorted(tim, np.concatenate([ft0, ft0-preintval, ft0+intval]))

        # syn_spec.shape = [subpop, freqs, tims]
        allpop_syn_spec = anacl_obj.get_subpop_spec(all_cur, freq, tim)
        subpop_syn_spec = anacl_obj.get_subpop_spec(curr, freq, tim)

        mnhs_mat = np.empty(shape=(N_axes, ftgt.shape[0]+1, 1), dtype=np.ndarray)
        for timidx in np.arange(N_timint):
            mnhs_mat[0, timidx, 0] = 10*np.log(np.sum(np.mean(allpop_syn_spec[0][timidx, rel_tidx[2+timidx]:rel_tidx[4+timidx]], axis=0))*1000/T)
        mnhs_mat[0, 2, 0] = 10*np.log(np.sum(np.mean(allpop_syn_spec[0][:, rel_tidx[2]:rel_tidx[5]], axis=0))*1000/T)

        if N_subpop> 1:
            non_target = np.delete(np.arange(N_subpop), ftgt)
            algn_pop = np.concatenate((ftgt, non_target))
            for pidx, _ in enumerate(algn_pop):
                for timidx in range(N_timint): 
                    mnhs_mat[pidx+1, timidx, 0] = 10*np.log10(np.sum(np.mean(subpop_syn_spec[pidx][:, rel_tidx[2+timidx]:rel_tidx[4+timidx]], axis=0))*1000/T)
                mnhs_mat[pidx+1, 2, 0] = 10*np.log10(np.sum(np.mean(subpop_syn_spec[pidx][:, rel_tidx[2]:rel_tidx[5]], axis=0))*1000/T)

        return mnhs_mat
            

    def get_ISI0(self, idx):
        fidx = self.my_chores[idx][2][1]
        fd = self.multclas.uniq_feed_pars[fidx]
        ft0 = fd['algn']['t0']

        ovlp = ft0[1]-ft0[0] 

        ISI_spk = self.data_arr[self.data_position['STN_spk']][idx]
        ISI0 = np.empty(shape=(ISI_spk.shape[0],3))
        for ijk, obj in np.ndenumerate(ISI_spk):
            if obj.size:
                fidx = np.searchsorted(obj, ft0)
                ISI0[ijk[0],:2] = obj[fidx+1] - obj[fidx]
                ISI0[ijk[0],2] = np.sum(ISI0[ijk[0],:2]) if ISI0[ijk[0],0] <= ovlp else np.sum(ISI0[ijk[0],:2])-obj[fidx[0]+1]+ft0[1]
            else:
                ISI0[ijk[0],:] = [np.inf, np.inf, np.inf] 
        return ISI0

    def get_lbl_mat(self):
    #    dim = tuple([len(self.effects_mat[i]) for i in range(3)])
        dim = (self.param_combo.shape[0], self.uniq_feeds.shape[0], self.uniq_nets.shape[0])
        param_fields = self.param_combo['param'].dtype.fields
        chnl_lab = {'AMPA': 'Ctx_STN_A', 'NMDA': 'Ctx_STN_N', 'GABA': 'GPe_STN'}

        uniq_param_lbl = [] 
        for i in range(dim[0]):
          #  uniq_param_lbl[i] = ['-'.join(('{!s}: {:.2e}'.format(j, self.param_combo['param'][chnl_lab[j]][i]))) for j in ['NMDA', 'GABA']]    
            tmp = ''
            for j in ['NMDA', 'GABA']: 
                tmp += '{!s}: {:.2e} '.format(j, self.param_combo['param'][chnl_lab[j]][i])
            tmp.strip()
            uniq_param_lbl.append(tmp)

        uniq_param_lbl = np.array(uniq_param_lbl)

        lbl_mat = np.empty(shape=dim, dtype='O')
        for sim in self.chores:
        #    lbl_mat[sim[3][:3]] = '\n'.join(sim[2].split('] -'))
            prm_idx = sim[2][0]
            lbl_mat[prm_idx,:,:] = lbl_mat[prm_idx]
        self.lbl_mat = lbl_mat 

    def gather_var(self, var, root=0):
        self.data_arr[self.data_position[var]] = np.array(MPI.COMM_WORLD.gather(self.data_arr[self.data_position[var]], root=root))
        if self.rank == root:
            self.unpack_gather(var)

    def unpack_gather(self, var):
        lst = self.data_arr[self.data_position[var]]
        new_lst = np.empty(shape=self.N_chores, dtype='O')
        for ridx, rnk in enumerate(self.chore_ass):
            if rnk.size:
                for r, p in zip (rnk, lst[ridx]):
                    new_lst[r] = p 
        self.data_arr[self.data_position[var]] = new_lst        

    def reshape_sim_arr(self,var):
        #dim = tuple([len(self.effects_mat[i]) for i in range(3)])
        dim = (self.param_combo.shape[0], self.uniq_feeds.shape[0], self.uniq_nets.shape[0])
    
        var_mat = np.empty(shape=dim, dtype='O')
        for ijk, obj in np.ndenumerate(var_mat):
            var_mat[ijk] = []
    
        for v, sim in zip(self.data_arr[self.data_position[var]], self.chores):
            var_mat[sim[2]].append(v)
    
        for ijk, obj in np.ndenumerate(var_mat):
            var_mat[ijk] = np.array(obj)
    
        return var_mat 

    def get_trim_simpaths(self, cidx, fidx, nidx): 
        relv_lst = []

        for c,f,n in it.product(enumerate(self.effects_mat[0][cidx,1]), self.effects_mat[1][fidx,1], self.effects_mat[2][nidx,1]):
            for tri in self.multclas.sim_path_mat[c[1],f,n]:
                relv_lst.append((int(tri[0]), os.path.join(*tri[1]), '{!s} - {!s}'.format(self.effects_mat[1][fidx,2], self.effects_mat[0][cidx,2][c[0]])))
        uniq_sim = list(set(relv_lst))
        uniq_sim.sort()
    
        uniq_sp = np.empty(shape=(len(uniq_sim),2), dtype='O') 
    
        for i, sim in enumerate(uniq_sim):
            uniq_sp[i,0] = tuple((sim[0], sim[1]))
            uniq_sp[i,1] = sim[2] 
        
        return uniq_sp

    def axis_create_align(self, axr, gl, fig, xlab='', ylab='', style=True):
        gl_gem = gl.get_geometry()
        ax_shp = axr.shape
        if gl_gem == ax_shp:
            axr[0,0] = fig.add_subplot(gl[0,0])
            for i in range(1,gl_gem[1]):
                axr[0,i] = fig.add_subplot(gl[0,i], sharey=axr[0,0])
            for j in range(1,gl_gem[0]):
                axr[j,0] = fig.add_subplot(gl[j,0], sharex=axr[0,0], sharey=axr[0,0])
                axr[j,0].set_ylabel(ylab)
            for i,j in it.product(range(1,gl_gem[0]), range(1, gl_gem[1])):
                axr[i,j] = fig.add_subplot(gl[i,j], sharex=axr[0,j], sharey=axr[i,0])
            for j in range(gl_gem[1]):
                axr[-1,j].set_xlabel(xlab)
            axr[0,0].set_ylabel(ylab)
            if style:
                for i, ax in np.ndenumerate(axr):
                    self.style_axis(ax)

        else:
            print('Axis array and Gridspec geometry mismatch')
        return axr
    
    def axis_create_inset(self, axr, xlab='', ylab=''):
        ax_shp = axr.shape
        for i, j in it.product(range(ax_shp[0]), range(ax_shp[1])):
            axr[i,j,1] = inset_axes(axr[i, j, 0], width="70%", height="60%", loc=1)
            axr[i,j,1].patch.set_alpha(0)
        axr[0,0,1].set_xlabel(xlab)
        axr[0,0,1].set_ylabel(ylab)
        return axr

    def style_axis(self, ax, top=False, right=False, offset=5):
        ax.spines['top'].set_visible(top)
        ax.spines['right'].set_visible(right)

        ax.spines['bottom'].set_position(('outward', offset))
        ax.spines['left'].set_position(('outward', offset))

def exec_multiana(fils):

    param_dict = dict(fils=fils, tmin=00, tmax=2250, ad_fd=2, ad_nt=2, tbreaks=[500,2250])
    
    # Feed options {dur:'', }

    param_dict['NetEffects'] = ['SimpleNet4']
#    param_dict['RunAnalysis'] = [('SilenceHist',0)]
  #  param_dict['RunAnalysis'] = [('SilenceHist',0), ('SilenceBox',0), ('SilenceReg', 0)]
  #  param_dict['RunAnalysis'] = [('SilenceHist', 0), ('BurstHist',0)]#, ('SilenceBox',0), ('SilenceReg', 0)]
   # param_dict['RunAnalysis'] = [('BurstHist',0), ('BurstBox', 0), ('BurstReg', 0)]#, ('SilenceBox',0), ('SilenceReg', 0)]
  #  param_dict['RunAnalysis'] = [('FirHist',0), ('FirBox', 0), ('FirReg', 0)]#, ('SilenceBox',0), ('SilenceReg', 0)]
#    param_dict['RunAnalysis'] = [('ThetaBox',0), ('ThetaReg', 0)]#, ('SilenceBox',0), ('SilenceReg', 0)]
    param_dict['RunAnalysis'] = [
#                                ('SilenceHist',0), ('SilenceBox',0), ('SilenceReg', 0),
                                ('BurstHist',0), ('BurstBox',0), ('BurstReg', 0),
#                                ('FirHist',0), ('FirBox',0), ('FirReg', 0),
#                                ('ThetaBox',0), ('ThetaReg', 0),
                                ]
#     cmap = plt.get_cmap('tab10')
    cmapmpl = mpl.cm.get_cmap('tab10')
    param_dict['markers'] = ['x', 'v', 'o', 'D', '+', 's']
    param_dict['colors'] = cmapmpl(np.arange(10))
    param_dict['lnstyl'] = [
                            (0, ()), #'-', 
                            (0, (1, 5)), #':'
                            (0, (5, 5)),# '--'
                            (0, (3, 5, 1, 5)), #dashed-dotted
                            (0, (1, 1)), #densely dotted
                            (0, (3, 1, 1, 1, 1, 1)), #densely-dashdotted                         
                            ]
    param_dict['hist_tbins'] = 25,
#     param_dict['pers'] = (0, 1, 'subpop', 'all', 'Seg', 'EffParFed'), # (var, fix, subpops)
#     param_dict['pers'] = (0, 1, 'subpop', 'fnd'), # (var, fix, subpops)

    param_set_dict = {
        'GPeLow': [
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [4.0e-06], 'Ctx_STN_A_weight': [3.666e-6]}), 
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [4.0e-06], 'Ctx_STN_A_weight': [9.165e-6]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [4.0e-06], 'Ctx_STN_A_weight': [1.833e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [4.0e-06], 'Ctx_STN_A_weight': [3.666e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [4.0e-06], 'Ctx_STN_A_weight': [4.583e-5]})],
        'GPeDef': [        
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [8.0e-06], 'Ctx_STN_A_weight': [3.666e-6]}), 
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [8.0e-06], 'Ctx_STN_A_weight': [9.165e-6]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [8.0e-06], 'Ctx_STN_A_weight': [1.833e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [8.0e-06], 'Ctx_STN_A_weight': [3.666e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [8.0e-06], 'Ctx_STN_A_weight': [4.583e-5]})],
        'GPeHig': [    
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [3.666e-6]}), 
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [9.165e-6]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [1.833e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [3.666e-5]}),
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [4.583e-5]})],
        'NMDALow': [   
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-6], 'GPe_STN_weight': [4.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-6], 'GPe_STN_weight': [8.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-6], 'GPe_STN_weight': [1.00e-05]})],
        'NMDALMi': [        
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [9.165e-6], 'GPe_STN_weight': [4.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [9.165e-6], 'GPe_STN_weight': [8.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [9.165e-6], 'GPe_STN_weight': [1.00e-05]})],
        'NMDAMid': [        
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [1.833e-5], 'GPe_STN_weight': [4.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [1.833e-5], 'GPe_STN_weight': [8.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [1.833e-5], 'GPe_STN_weight': [1.00e-05]})],
        'NMDADef': [        
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-5], 'GPe_STN_weight': [4.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-5], 'GPe_STN_weight': [8.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [3.666e-5], 'GPe_STN_weight': [1.00e-05]})],
        'NMDAHig': [        
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [4.583e-5], 'GPe_STN_weight': [4.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [4.583e-5], 'GPe_STN_weight': [8.00e-06]}),
            ('GPe_STN_weight', {'Ctx_STN_N_weight':None, 'Ctx_STN_A_weight': [4.583e-5], 'GPe_STN_weight': [1.00e-05]})],
    
    }
 #   param_dict['FeedEffects'] = ['F22NE10']
#    FeedSpecs = {'EInt':None, 'EDur':None, 'EOvl':None, 'EPrO':None, 'EPsO':None, 'Seg':None} 
  #  FeedSpecs = {'EInt':((2,4),(2,4)), 'EDur':((150,2000),(150,1500)), 'EOvl': (0, 2000), 'Seg':((0.,1.01),(0.,1,.01))}
#    FeedSpecs = {'EInt':((2,4),(2,4)), 'Seg':((0.,1.01),(0.,1,.01)), 'EPsO':(100,110)}
  #  FeedSpecs = {'Seg':((0.,1.01),(0.,1.01))}
    FeedSpecs = {}
    param_dict['FeedEffects'] = [('F22NH2', FeedSpecs) ]
    param_dict['pers'] = [(0, 1, 'subpop', 'all', 'Seg', 'SegEffectsLag103E10')] # (var, fix, subpops)

    param_dict['doc_prefix'] = 'GPeDef' 
    param_dict['FeedEffects'] = [('F22NH2', FeedSpecs) ]
    #param_dict['ParEffects'] = param_set_dict[param_dict['doc_prefix']] 
    param_dict['ParEffects'] = [ 
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [7.33e-6]}), 
            ('Ctx_STN_A_weight', {'Ctx_STN_N_weight':None, 'GPe_STN_weight': [1.0e-05], 'Ctx_STN_A_weight': [3.666e-5]}),
                                ]

    popof = {
                'param': {},
                'feed': {'Low': [('fds', ['F22SH2', 'F22NH4']), ('Int1', [(125,250)]) ],},
            }

    param_dict['filt_dict'] = popof

    MultiAnaStats(param_dict, filt_dict=popof)

 #   for k in param_set_dict.keys():
 #       param_dict['doc_prefix'] = k 
 #       param_dict['ParEffects'] = param_set_dict[param_dict['doc_prefix']] 
 #       MultiAnaStats(param_dict)

if __name__ == '__main__':
    fils = sys.argv[1:]

    if fils[0].endswith('.hdf5'):
        exec_multiana(fils) 
    else:
        print('Invalid HDF5 file... skipping')
