#! python3 

from mpi4py import MPI 
import h5py, sys

# Set PGF preferences
pgf_with_custom_preamble = {
    'font.family': 'sans-serif', # use serif/main font for text elements
    'font.size' : 10,

    'text.usetex': False,    # use inline math for ticks
    'text.latex.unicode': True,

    'pgf.texsystem': 'lualatex',

    'pgf.rcfonts': False,   # don't setup fonts from rc parameters
    'pgf.preamble': [
#         '\\usepackage{units}',         # load additional packages
#         '\\usepackage{metalogo}',
         '\\usepackage{unicode-math}',  # unicode math setup
#         r'\setmathfont{xits-math.otf}',
#         r'\setmainfont{DejaVu Serif}', # serif font via preamble
         ]
    }

# Matplotlib
import matplotlib as mpl

# Choose Backend, Create Canvas
mpl.use('Agg')
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas

#mpl.use('pgf')
#from matplotlib.backends.backend_pgf import FigureCanvasPgf as FigureCanvas
#mpl.rcParams.update(pgf_with_custom_preamble)
#mpl.backend_bases.register_backend('pdf', FigureCanvas)

import matplotlib.figure as mpl_figure
#import matplotlib.ticker as ticker
import matplotlib.text as text
#import matplotlib.markers as markers
#import matplotlib.lines as lines
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import numpy as np 
from AnalysisClass import Analysis, sing_style_axis 

def stats(anacl, expt_path_att):
    # get subpop
    N_timint = anacl.timeints.size
    expt_path = expt_path_att[0]
#    expt_path_txt = expt_path_att[1]

    expt_path_txt = '' 
    for k,v in expt_path_att[1].items():
        expt_path_txt += '{!s}: {:.2e}\n'.format(k,v) 

    all_idx_ranges = np.array(anacl.file[expt_path]['props']['gids_STN'], dtype=int)
    all_pop = np.empty(shape=(1, 2), dtype='O')
    all_pop[0,0] = all_idx_ranges
    all_pop[0,1] = all_idx_ranges
    N_gids = all_idx_ranges.size
    spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)

    Ctx_feeds, N_Ctx_feeds = anacl.get_ctx_feed(expt_path)
    STN_ctx = np.array(anacl.file[expt_path]['props']['Ctx_asg_STN'], dtype=int)

#    STNpop_spks = anacl.get_gid_spk(expt_path, all_pop, N_gids)
    STNpop_spks = anacl.get_gid_spk(expt_path, all_idx_ranges)

    allpop_ISI = anacl.get_gid_ISI(STNpop_spks)
    allgid_pp, tbin = anacl.get_gid_pp(expt_path,  all_idx_ranges, tbin=5)
    all_cap_cur, tim = anacl.get_subpop_cap_cur(all_pop, expt_path, Tsam=int(1))
    all_all_cur, tim = anacl.get_subpop_allcurr(all_pop, expt_path, Tsam=int(1))
    all_cap_cur = all_all_cur[:,4,:]

  #  all_cap_cur_fil = anacl.get_filtered_sig(all_cap_cur, anacl.bandthe)
    

    freq = anacl.freq
    band_posts = anacl.band_posts
    all_specs = anacl.get_subpop_spec(all_cap_cur, freq, tim)
    all_specs_band = anacl.get_bands_avg(all_specs, anacl.band_posts_idx, tim.size)
    
    GPPgids = np.array(anacl.file[expt_path]['props']['gids_GPeP'], dtype=int)
    GPAgids = np.array(anacl.file[expt_path]['props']['gids_GPeA'], dtype=int)
    GPP_spks = anacl.get_gid_spk(expt_path, GPPgids)
    GPA_spks = anacl.get_gid_spk(expt_path, GPAgids)
    GPP_pp, GPbin = anacl.get_gid_pp(expt_path,  GPPgids, tbin=5)
    GPA_pp, GAbin = anacl.get_gid_pp(expt_path,  GPAgids, tbin=5)
    GPtposts = np.arange(anacl.tmin-0.5*GPbin, anacl.tmax+0.5*GPbin, GPbin)
    GPP_f = np.sum(GPP_pp, axis=0)    
    GPA_f = np.sum(GPA_pp, axis=0)    
    GPe_f = GPP_f + GPA_f


    gid_freq = anacl.get_gid_freq(STNpop_spks)

    h_ratios = np.array([3,3,6,4,4,4])
    N_plots = h_ratios.size
    N_subpop = spk_subpop.shape[0]
    if N_subpop > 1: 
        idx_ranges = spk_subpop[:,0]
        subpop_cap_cur, tim = anacl.get_subpop_cap_cur(spk_subpop, expt_path, Tsam=int(1))
        subpop_all_cur, tim = anacl.get_subpop_allcurr(spk_subpop, expt_path, Tsam=int(1))
        subpop_cap_cur = subpop_all_cur[:,4,:]
        specs_mat = anacl.get_subpop_spec(subpop_cap_cur, freq, tim)
        specs_band = anacl.get_bands_avg(specs_mat, anacl.band_posts_idx, tim.size)
        h_ratios = np.concatenate(([3,3,6,4,4,4], np.tile([3,3,5,4,4,4], N_subpop)))

    fig = mpl_figure.Figure(figsize=(30, np.sum(h_ratios)))
    canvas = FigureCanvas(fig)
    gl = fig.add_gridspec(h_ratios.size, N_timint, width_ratios=anacl.timeints, height_ratios=h_ratios)


    # Axis creation
    # Freq Axes
    allfreq = np.empty(shape=(1, N_timint, 2), dtype='O') 
    allfreq[0, 0, 0] = fig.add_subplot(gl[0, 0])
    # ISI Axes
    allISI = np.empty(shape=(1, N_timint, 2), dtype='O') 
    allISI[0, 0, 0] = fig.add_subplot(gl[1, 0])

    # Freq Power
    allpowfreq = np.empty(shape=(1, N_timint, 2), dtype='O')
    allpowfreq[0,0,0] = fig.add_subplot(gl[5, 0])

    for i in range(1,N_timint):
        allfreq[0, i, 0] = fig.add_subplot(gl[0, i],sharey=allfreq[0,0,0])
        allISI[0, i, 0] = fig.add_subplot(gl[1, i],sharey=allISI[0,0,0])
        allpowfreq[0,i,0] = fig.add_subplot(gl[5, i], sharey=allpowfreq[0,0,0])
    for i in range(N_timint):
        allfreq[0, i, 1] = allfreq[0, i ,0].twiny() 
        allISI[0, i, 1] = allISI[0, i ,0].twiny() 
        allpowfreq[0, i, 1] = allpowfreq[0, i ,0].twinx() 

###### NOTE IMPORTANT: 20210319 commenting out anacl.upd ticks 141,145,150 
    # Raster Axes
    allspks = np.empty(shape=(1), dtype='O')
    allspks[0] = fig.add_subplot(gl[2,:])
    anacl.upd_axis_ticks(allspks[0].axes.xaxis, Ctx_feeds, N_Ctx_feeds)
    # Current Axes
    allcurr = np.empty(shape=(1), dtype='O')
    allcurr[0] = fig.add_subplot(gl[3,:])
    anacl.upd_axis_ticks(allcurr[0].axes.xaxis, Ctx_feeds, N_Ctx_feeds)

    # Specs Axes
    allspecs = np.empty(shape=(1), dtype='O')
    allspecs[0] = fig.add_subplot(gl[4,:])
    anacl.upd_axis_ticks(allspecs[0].axes.xaxis, Ctx_feeds, N_Ctx_feeds)


    # Plotting
    gpallx = allspks[0].twinx()
    gpallx.plot(GPtposts[1:], GPP_f[1:], lw=0.5, ls='-.', alpha=0.5)
    gpallx.plot(GPtposts[1:], GPA_f[1:], lw=0.5, ls='-.', alpha=0.5)
    gpallx.plot(GPtposts[1:], GPe_f[1:], lw=0.5, ls='-.', alpha=0.5)
    gpallx.tick_params(axis='y', colors='g')

    allfreq = anacl.plot_subpop_freq(all_pop, gid_freq, allfreq, bins=50, orien='horizontal')
    allISI = anacl.plot_subpop_ISIs(all_pop, allpop_ISI, allISI, bins=50, orien='horizontal', co=(False, 50))
    allspks = anacl.plot_subpop_spikes(all_pop, STNpop_spks, allspks, STN_ctx)
    allspks = anacl.plot_subpop_spkcounts(all_pop, allgid_pp, tbin, allspks)
    allcurr = anacl.plot_subpop_curr(allcurr, tim, all_all_cur)
    allspecs = anacl.plot_subpop_specs(all_specs, allspecs, freq, tim, all_cap_cur, all_specs_band)
  #  allpowfreq = anacl.plot_powspec_curr(allpowfreq, tim, all_all_cur[:,4,:])


    if N_subpop > 1: 

        subfreq = np.empty(shape=(N_subpop, N_timint, 2), dtype='O') 
        subISIs = np.empty(shape=(N_subpop, N_timint, 2), dtype='O') 
        subpowf = np.empty(shape=(N_subpop, N_timint, 2), dtype='O') 
        subspks = np.empty(shape=(N_subpop), dtype='O')
        subcurr = np.empty(shape=(N_subpop), dtype='O')
        subspecs = np.empty(shape=(N_subpop), dtype='O')
    
        for i in range(N_subpop):
            subfreq[i,0,0] = fig.add_subplot(gl[(i+1)*N_plots,0])
            subISIs[i,0,0] = fig.add_subplot(gl[(i+1)*N_plots+1,0])
            subpowf[i,0,0] = fig.add_subplot(gl[(i+1)*N_plots+5,0])
            for j in range(1,N_timint):
                subfreq[i, j, 0] = fig.add_subplot(gl[(i+1)*N_plots,j],sharey=subfreq[i,0,0])
                subISIs[i, j, 0] = fig.add_subplot(gl[(i+1)*N_plots+1,j],sharey=subISIs[i,0,0])
                subpowf[i, j, 0] = fig.add_subplot(gl[(i+1)*N_plots+5,j],sharey=subpowf[i,0,0])
            for j in range(N_timint):
                subISIs[i, j, 1] = subISIs[i, j ,0].twiny() 
                subfreq[i, j, 1] = subfreq[i, j ,0].twiny() 
                subpowf[i, j, 1] = subpowf[i, j ,0].twinx() 
    
            subspks[i] = fig.add_subplot(gl[(i+1)*N_plots+2,:])
            subcurr[i] = fig.add_subplot(gl[(i+1)*N_plots+3,:])
            subspecs[i] = fig.add_subplot(gl[(i+1)*N_plots+4,:])

            gpsub = subspks[i].twinx()
            gpsub.plot(GPtposts[1:], GPP_f[1:], lw=0.5, ls='-.', alpha=0.5)
            gpsub.plot(GPtposts[1:], GPA_f[1:], lw=0.5, ls='-.', alpha=0.5)
            gpsub.plot(GPtposts[1:], GPe_f[1:], lw=0.5, ls='-.', alpha=0.5)
            gpsub.tick_params(axis='y', colors='g')


        for i in range(N_subpop):
            anacl.upd_axis_ticks(subspks[i].axes.xaxis, Ctx_feeds, N_Ctx_feeds)
            anacl.upd_axis_ticks(subcurr[i].axes.xaxis, Ctx_feeds, N_Ctx_feeds)
            anacl.upd_axis_ticks(subspecs[i].axes.xaxis, Ctx_feeds, N_Ctx_feeds)

        subISIs = anacl.plot_subpop_ISIs(spk_subpop, allpop_ISI, subISIs, bins=50, orien='horizontal', co=(False, 50))
        subfreq = anacl.plot_subpop_freq(spk_subpop, gid_freq, subfreq, bins=50, orien='horizontal')

        subspks = anacl.plot_subpop_spikes(spk_subpop, STNpop_spks, subspks, STN_ctx)
        subspks = anacl.plot_subpop_spkcounts(spk_subpop, allgid_pp, tbin, subspks)

        subcurr = anacl.plot_subpop_curr(subcurr, tim, subpop_all_cur)

        subspecs = anacl.plot_subpop_specs(specs_mat, subspecs, freq, tim, subpop_cap_cur, specs_band)

   #     subpowf = anacl.plot_powspec_curr(subpowf, tim, subpop_all_cur[:,4,:])


 #   filnamfig.= expt_path.split('/')

    fig.tight_layout()
#    filnam='{!s}_{!s}_{!s}'.format(filnamplt[2], filnamplt[4], filnamplt[-1])
    filnam = expt_path.replace('/', '_')
    fig.suptitle('{!s}\n{!s}'.format(filnam, expt_path_txt))
    fig.savefig('crossxheck{!s}_{!s}.pdf'.format(anacl.pre_file, filnam), rasterized=True)

def plot_V(anacl, expt_path_att):

    expt_path = expt_path_att[0]
#    expt_path_txt = expt_path_att[1]

    expt_path_txt = '' 
    for k,v in expt_path_att[1].items():
        expt_path_txt += '{!s}: {:.2e}\n'.format(k,v) 


    spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)
    tim = np.array(anacl.file[expt_path]['props']['timesteps'])
    STN_V = np.array(anacl.file[expt_path]['raw']['STNSta_V'])

    GPPgids = np.array(anacl.file[expt_path]['props']['gids_GPeP'], dtype=int)
    GPAgids = np.array(anacl.file[expt_path]['props']['gids_GPeA'], dtype=int)
#    GPP_spks = anacl.get_gid_spk(expt_path, GPPgids)
#    GPA_spks = anacl.get_gid_spk(expt_path, GPAgids)
    GPP_pp, GPbin = anacl.get_gid_pp(expt_path,  GPPgids, tbin=5)
    GPA_pp, GAbin = anacl.get_gid_pp(expt_path,  GPAgids, tbin=5)
    GPtposts = np.arange(anacl.tmin-0.5*GPbin, anacl.tmax+0.5*GPbin, GPbin)
    GPP_f = np.sum(GPP_pp, axis=0)    
    GPA_f = np.sum(GPA_pp, axis=0)    
    GPe_f = GPP_f + GPA_f
    Ctx_feeds, N_Ctx_feeds = anacl.get_ctx_feed(expt_path)

    N_subpop = spk_subpop.shape[0]
    avg_subpop = N_gids//N_subpop
    N_ax_subpop = 10 if avg_subpop > 10 else 1 
    N_axes = N_ax_subpop * N_subpop 

    fig = mpl_figure.Figure(figsize=(30, 2*N_axes))
    canvas = FigureCanvas(fig)
    gl = fig.add_gridspec(N_axes, 1)
    axgrid = np.empty(shape=(N_axes, 2), dtype='O') 
    axgrid[0, 0] = fig.add_subplot(gl[0, 0])

    for i in range(1, N_axes):
        axgrid[i] = fig.add_subplot(gl[i, 0], sharex=axgrid[0, 0])

    for s in range(N_subpop):
        cell_split = np.array_split(spk_subpop[s,0], N_ax_subpop)    
        for idx, arr in enumerate(cell_split):
            axgrid[s*N_ax_subpop + idx, 1] = arr

    for i in range(N_axes):
        anacl.upd_axis_ticks(axgrid[i,0].axes.xaxis, Ctx_feeds, N_Ctx_feeds)
        axgrid[i,0].plot(tim, STN_V[axgrid[i,1], :].T, lw=0.5, alpha=0.5)


    fig.tight_layout()
    filnam = expt_path.replace('/', '_')
    fig.suptitle('{!s}\n{!s}'.format(filnam, expt_path_txt))
    fig.savefig('V_{!s}_{!s}.pdf'.format(anacl.pre_file, filnam))

def spikes_stats(anacl, expt_path_att):

    N_timint = anacl.timeints.size
    expt_path = expt_path_att[0]
    expt_path_txt = '' 
    for k,v in expt_path_att[1].items():
        expt_path_txt += '{!s}: {:.2e}\n'.format(k,v) 
    spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)
    tim = np.array(anacl.file[expt_path]['props']['timesteps'])
    spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)

    all_idx_ranges = np.array(anacl.file[expt_path]['props']['gids_STN'], dtype=int)
    all_pop = np.empty(shape=(1, 2), dtype='O')
    all_pop[0,0] = all_idx_ranges
    all_pop[0,1] = all_idx_ranges
    N_gids = all_idx_ranges.size

    GPPgids = np.array(anacl.file[expt_path]['props']['gids_GPeP'], dtype=int)
    GPAgids = np.array(anacl.file[expt_path]['props']['gids_GPeA'], dtype=int)

    GPP_pop = np.empty(shape=(1, 2), dtype='O')
    GPP_pop[0,0] = np.arange(GPPgids.size) 
    GPP_pop[0,1] = GPPgids 
    GPA_pop = np.empty(shape=(1, 2), dtype='O')
    GPA_pop[0,0] = np.arange(GPAgids.size) 
    GPA_pop[0,1] = GPAgids 


    GPPpop_spks = anacl.get_gid_spk(expt_path, GPPgids)
    GPApop_spks = anacl.get_gid_spk(expt_path, GPAgids)
    STNpop_spks = anacl.get_gid_spk(expt_path, all_idx_ranges)

    h_ratios = np.array([3,3,3])
    N_subpop = spk_subpop.shape[0]
    if N_subpop > 1: 
        idx_ranges = spk_subpop[:,0]
        h_ratios = np.concatenate(([3,3,3], np.tile([3], N_subpop)))

    fig = mpl_figure.Figure(figsize=(20, np.sum(h_ratios)))
    canvas = FigureCanvas(fig)
    gl = fig.add_gridspec(h_ratios.size, N_timint, width_ratios=anacl.timeints, height_ratios=h_ratios)

    allSTNfreq = np.empty(shape=(1, N_timint, 2), dtype='O') 
    allGPPfreq = np.empty(shape=(1, N_timint, 2), dtype='O') 
    allGPAfreq = np.empty(shape=(1, N_timint, 2), dtype='O') 
    for idx, i in enumerate([allGPPfreq, allGPAfreq, allSTNfreq]):
        i[0, 0, 0] = fig.add_subplot(gl[idx, 0])
        for j in range(1,N_timint):
            i[0, j, 0] = fig.add_subplot(gl[idx, j],sharey=i[0,0,0])

    for i in range(N_timint):
        allSTNfreq[0, i, 1] = allSTNfreq[0, i ,0].twiny() 
        allGPPfreq[0, i, 1] = allGPPfreq[0, i ,0].twiny() 
        allGPAfreq[0, i, 1] = allGPAfreq[0, i ,0].twiny() 

    STN_gid_freq = anacl.get_gid_freq(STNpop_spks)
    GPP_gid_freq = anacl.get_gid_freq(GPPpop_spks)
    GPA_gid_freq = anacl.get_gid_freq(GPApop_spks)
#    print(STN_gid_freq.shape, GPP_gid_freq.shape, GPA_gid_freq.shape)
#    print('S', all_pop, '\nP', GPP_pop, '\nA', GPA_pop)

    allGPPfreq = anacl.plot_subpop_freq(GPP_pop, GPP_gid_freq, allGPPfreq, bins=50, orien='horizontal')
    allGPPfreq[0,0,0].set_title('GPeP')
    allGPAfreq = anacl.plot_subpop_freq(GPA_pop, GPA_gid_freq, allGPAfreq, bins=50, orien='horizontal')
    allGPAfreq[0,0,0].set_title('GPeA')
    allSTNfreq = anacl.plot_subpop_freq(all_pop, STN_gid_freq, allSTNfreq, bins=50, orien='horizontal')
    allSTNfreq[0,0,0].set_title('STN')

    if N_subpop > 1: 
        subfreq = np.empty(shape=(N_subpop, N_timint, 2), dtype='O') 

        for i in range(N_subpop):
            subfreq[i,0,0] = fig.add_subplot(gl[i+3,0])
            for j in range(1,N_timint):
                subfreq[i, j, 0] = fig.add_subplot(gl[i+3,j],sharey=subfreq[i,0,0])
            for j in range(N_timint):
                subfreq[i, j, 1] = subfreq[i, j ,0].twiny() 

        subfreq = anacl.plot_subpop_freq(spk_subpop, STN_gid_freq, subfreq, bins=50, orien='horizontal')

    fig.tight_layout()
    filnam = expt_path.replace('/', '_')
    fig.suptitle('{!s}\n{!s}'.format(filnam, expt_path_txt))
    fig.savefig('SS_{!s}_{!s}.pdf'.format(anacl.pre_file, filnam))

def plot_specs_single(anacl, expt_path_att):

    # get subpop
    N_timint = anacl.timeints.size
    expt_path = expt_path_att[0]
#    expt_path_txt = expt_path_att[1]

    expt_path_txt = '' 
    for k,v in expt_path_att[1].items():
        expt_path_txt += '{!s}: {:.2e}\n'.format(k,v) 

    spk_subpop, N_gids = anacl.get_STN_subpop(expt_path)

    Ctx_feeds, N_Ctx_feeds = anacl.get_ctx_feed(expt_path)
    STN_ctx = np.array(anacl.file[expt_path]['props']['Ctx_asg_STN'], dtype=int)

    freq = anacl.freq
    band_posts = anacl.band_posts

    # Subpop 1
    idx_ranges = spk_subpop[1,0]
    subpop_cap_cur, tim = anacl.get_subpop_cap_cur(spk_subpop, expt_path, Tsam=int(1))
    subpop_all_cur, tim = anacl.get_subpop_allcurr(spk_subpop, expt_path, Tsam=int(1))
    subpop_cap_cur = subpop_all_cur[:,4,:]
    specs_mat = anacl.get_subpop_spec(subpop_cap_cur, freq, tim)
    specs_band = anacl.get_bands_avg(specs_mat, anacl.band_posts_idx, tim.size)


    fig = mpl_figure.Figure(figsize=(5,4))
    canvas = FigureCanvas(fig)
    gl = fig.add_gridspec(1,1)

    # Specs Axes
    subspecs = np.empty(shape=(1), dtype='O')
    subspecs[0] = fig.add_subplot(gl[0,0])

    subspecs[0] = sing_style_axis(subspecs[0])
    
    anacl.upd_axis_ticks(subspecs[0].axes.xaxis, Ctx_feeds, N_Ctx_feeds)
    
    subspecs = anacl.plot_subpop_specs(specs_mat, subspecs, freq, tim, subpop_cap_cur, specs_band, orientation='horizontal')
    subspecs[0].set_xlabel('time [ms]')
    subspecs[0].set_ylabel('Frequency [Hz]')

    fig.tight_layout()
    filnam = expt_path.replace('/', '_')
#    fig.suptitle('{!s}\n{!s}'.format(filnam, expt_path_txt))
    fig.savefig('SingleSpec{!s}_{!s}.pdf'.format(anacl.pre_file, filnam))
    fig.savefig('SingleSpec{!s}_{!s}.pgf'.format(anacl.pre_file, filnam), rasterized=True, dpi=450)

def exec_spec(fil, tbreaks=[500, 3500]):
    # Set parallel machinery
    rank = MPI.COMM_WORLD.Get_rank()
    N_hosts = MPI.COMM_WORLD.Get_size()

    datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

#    tbreaks=[500, 1000, 1500, 2500, 2750]

#    anacl = Analysis(datafile, tmin=500, tmax=5000, tbreaks=tbreaks)
    anacl = Analysis(datafile, tmin=500, tmax=3500, tbreaks=tbreaks)
    anacl.get_hier()
    anacl.band_posts = [(2,8, 'theta', 0.7, '_'), (12, 30, 'beta', 0.7, '_')]
#    timint = np.diff(anacl.timeposts)
#    timint_n = timint.size 
    chores = anacl.comb_paths

    # Initialize Analysis Specific
    anacl.freq = np.geomspace(2,64, 156)
    freq_list = []
    for f in anacl.band_posts:
        freq_list.extend([f[0], f[1]])

    uniq_f, ret_inv = np.unique(freq_list, return_inverse=True)
    uniq_f_idx = np.searchsorted(anacl.freq, uniq_f, side='right')

    for fidx, f in enumerate(uniq_f):
        if uniq_f_idx[fidx] > 0:
            f_low = anacl.freq[uniq_f_idx[fidx]-1]    
            f_hig = anacl.freq[uniq_f_idx[fidx]]    
            if f-f_low < f_hig-f:
                uniq_f_idx[fidx] -= 1

    anacl.band_posts_idx = np.empty(shape=(len(anacl.band_posts),2), dtype=int)
    for idx, i in enumerate(anacl.band_posts):
        anacl.band_posts_idx[idx,0] = int(uniq_f_idx[ret_inv[idx*2]])
        anacl.band_posts_idx[idx,1] = int(uniq_f_idx[ret_inv[idx*2+1]])
        

#    order = 2
#    anacl.band012 = sps.butter(order, [0.5, 64]/anacl.nyq, btype='bandpass')
#    anacl.banddel = sps.butter(order, [0.5, 3.5]/anacl.nyq, btype='bandpass')
#    anacl.bandthe = sps.butter(order, [3.5, 8.5]/anacl.nyq, btype='bandpass')
#    anacl.bandalp = sps.butter(order, [8.5, 16.5]/anacl.nyq, btype='bandpass')
#    anacl.bandbet = sps.butter(order, [16.5, 32.5]/anacl.nyq, btype='bandpass')
#    anacl.bandgam = sps.butter(order, [32.5, 128]/anacl.nyq, btype='bandpass')
    

    
    my_chores = chores[rank:len(chores):N_hosts]

    for chore in my_chores:
   #     stats(anacl, chore, timint)
        if chore[0].endswith('N000'):
            ch_r_e = chore[0].split('/')
            if ch_r_e[0] in ['P001', 'P007']:
                plot_specs_single(anacl, chore)
       # print(rank, chore)
    datafile.close()

def exec_V(fil):
    # Set parallel machinery
    rank = MPI.COMM_WORLD.Get_rank()
    N_hosts = MPI.COMM_WORLD.Get_size()

    datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

    tbreaks=[000, 10000]
    anacl = Analysis(datafile, tmin=00, tmax=5000, tbreaks=tbreaks)
    anacl.get_hier()
#    timint = np.diff(anacl.timeposts)
#    timint_n = timint.size 
    chores = anacl.comb_paths
    
    my_chores = chores[rank:len(chores):N_hosts]

    for chore in my_chores:
        if chore[0].startswith('P000') and chore[0].endswith('F000/SimpleNet4/N000'):
            plot_V(anacl, chore)
    datafile.close()

def exec_stats(fil, tbreaks=[500, 1500, 2000, 2500]):

    # Set parallel machinery
    rank = MPI.COMM_WORLD.Get_rank()
    N_hosts = MPI.COMM_WORLD.Get_size()

    datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

#    tbreaks=[500, 1000, 1500, 2500, 2750]

#    anacl = Analysis(datafile, tmin=500, tmax=5000, tbreaks=tbreaks)
    anacl = Analysis(datafile, tmin=500, tmax=3500, tbreaks=tbreaks)
    anacl.get_hier()
    anacl.band_posts = [(2,8, 'theta', 0.7, '_'), (12, 30, 'beta', 0.7, '_')]
#    timint = np.diff(anacl.timeposts)
#    timint_n = timint.size 
    chores = anacl.comb_paths

    # Initialize Analysis Specific
    anacl.freq = np.geomspace(2,64, 156)
#    anacl.freq = np.geomspace(2,64, 61)
  #  N = anacl.N_timpts
  #  N = int(2**(np.ceil(np.log2(N)))) if np.log2(N)%1 else N
  #  print(N)
  #  anacl.transmorlet = get_morlet_transmat(anacl.freq, fs=anacl.fs, N=N, s=2) 

    freq_list = []
    for f in anacl.band_posts:
        freq_list.extend([f[0], f[1]])

    uniq_f, ret_inv = np.unique(freq_list, return_inverse=True)
    uniq_f_idx = np.searchsorted(anacl.freq, uniq_f, side='right')

    for fidx, f in enumerate(uniq_f):
        if uniq_f_idx[fidx] > 0:
            f_low = anacl.freq[uniq_f_idx[fidx]-1]    
            f_hig = anacl.freq[uniq_f_idx[fidx]]    
            if f-f_low < f_hig-f:
                uniq_f_idx[fidx] -= 1

    anacl.band_posts_idx = np.empty(shape=(len(anacl.band_posts),2), dtype=int)
    for idx, i in enumerate(anacl.band_posts):
        anacl.band_posts_idx[idx,0] = int(uniq_f_idx[ret_inv[idx*2]])
        anacl.band_posts_idx[idx,1] = int(uniq_f_idx[ret_inv[idx*2+1]])
        

#    order = 2
#    anacl.band012 = sps.butter(order, [0.5, 64]/anacl.nyq, btype='bandpass')
#    anacl.banddel = sps.butter(order, [0.5, 3.5]/anacl.nyq, btype='bandpass')
#    anacl.bandthe = sps.butter(order, [3.5, 8.5]/anacl.nyq, btype='bandpass')
#    anacl.bandalp = sps.butter(order, [8.5, 16.5]/anacl.nyq, btype='bandpass')
#    anacl.bandbet = sps.butter(order, [16.5, 32.5]/anacl.nyq, btype='bandpass')
#    anacl.bandgam = sps.butter(order, [32.5, 128]/anacl.nyq, btype='bandpass')
    

    
    my_chores = chores[rank:len(chores):N_hosts]

    for chore in my_chores:
#        stats(anacl, chore, timint)
        stats(anacl, chore)

#        if chore[0].startswith('P000') and chore[0].endswith('F000/SimpleNet4/N000'):
#            stats(anacl, chore)
       # print(rank, chore)
    datafile.close()

def exec_SS(fil):
    # Set parallel machinery
    rank = MPI.COMM_WORLD.Get_rank()
    N_hosts = MPI.COMM_WORLD.Get_size()

    datafile = h5py.File(fil, 'r', driver='mpio', comm=MPI.COMM_WORLD)

    tbreaks=[500, 1000, 1500, 2500, 2750]
    anacl = Analysis(datafile, tmin=00, tmax=5000, tbreaks=tbreaks)
    anacl.get_hier()
#    timint = np.diff(anacl.timeposts)
#    timint_n = timint.size 
    chores = anacl.comb_paths
    
    my_chores = chores[rank:len(chores):N_hosts]

    for chore in my_chores:
#       # if chore[0].startswith('P000') and chore[0].endswith('F000/SimpleNet4/N000'):
#        if chore[0].endswith('SimpleNet4/N000'):
       spikes_stats(anacl, chore)
    datafile.close()


if __name__ == '__main__':
    fils = sys.argv[1:]

    for fil in fils:
        if fil.endswith('.hdf5'):
            exec_stats(fil, tbreaks=[500, 2500]) 
            exec_SS(fil) 
#            exec_spec(fil) 

            
        else:
            print('Invalid HDF5 file... skipping')
